type ide = string

let fresh =
  let c = ref 0
  in fun x -> (c := !c+1; x ^ (string_of_int !c))

type var_v = ide
type var_i = ide

type value =
  | Var of var_v
  | Lam_v of var_v * exp
  | Lam_i of var_i * exp
  | Lam_t of exp
  | Unit
and exp =
  | Val of value
  | Let of var_v * exp * exp
  | App_v of value * value
  | App_i of value * var_i
  | App_t of value
  | Do of var_i * value
  | Hndl of var_i * exp * hndl * ret
and hndl =
  | Hndl_op of var_v * var_v * exp
and ret =
  | Ret of var_v * exp

type ectx =
  | Mt_e
  | Let_e of var_v * exp * ectx
  | Hndl_e of var_i * hndl * ret * ectx

let rec plug_e ec e =
  match ec with
  | Mt_e ->
    e
  | Let_e (x, e', ec') ->
    plug_e ec' (Let (x, e, e'))
  | Hndl_e (a, h, r, ec') ->
    plug_e ec' (Hndl (a, e, h, r))

type rctx =
  | Mt_r
  | Let_r of var_v * exp * rctx
  | Hndl_r of var_i * hndl * ret * rctx

let rec plug_r ec z =
  match ec with
  | Mt_r ->
    Val (Var z)
  | Let_r (x, e', ec') ->
    Let (x, e', plug_r ec' z)
  | Hndl_r (a, h, r, ec') ->
    Hndl (a, plug_r ec' z, h, r)

type redex =
  | Beta_v of var_v * exp * value
  | Beta_i of var_i * exp * var_i
  | Beta_t of exp
  | Let_v of var_v * value * exp
  | Ret_v of var_v * exp * value
  | Hndl_do of var_i * hndl * ret * rctx * value

type dec =
  | Dec_val of value
  | Dec_red of redex * ectx

let rec split ec a rc =
  match ec with
  | Mt_e ->
    failwith "Missing handler"
  | Let_e (x, e2, ec') ->
    split ec' a (Let_r (x, e2, rc))
  | Hndl_e (b, h, r, ec') ->
    if a = b
    then (h, r, rc, ec')
    else split ec' a (Hndl_r (b, h, r, rc))

let rec decompose_e e ec =
  match e with
  | Val v ->
    decompose_ec ec v
  | Let (x, e1, e2) ->
    decompose_e e1 (Let_e (x, e2, ec))
  | App_v (v1, v2) ->
    (match v1 with
     | Lam_v (x, e') ->
       Dec_red (Beta_v (x, e', v2), ec)
     | _ ->
       failwith "Type error")
  | App_i (v, b) ->
    (match v with
     | Lam_i (a, e') ->
       Dec_red (Beta_i (a, e', b), ec)
     | _ ->
       failwith "Type error")
  | App_t v ->
    (match v with
     | Lam_t e' ->
       Dec_red (Beta_t e', ec)
     | _ ->
       failwith "Type error")
  | Do (a, v) ->
    (match split ec a Mt_r with
     | h, r, rc, ec' ->
       Dec_red (Hndl_do (a, h, r, rc, v), ec'))
  | Hndl (a, e', h, r) ->
    decompose_e e' (Hndl_e (a, h, r, ec))
and decompose_ec ec v =
  match ec with
  | Mt_e ->
    Dec_val v
  | Let_e (x, e2, ec') ->
    Dec_red (Let_v (x, v, e2), ec')
  | Hndl_e (_, _, Ret (x, e) , ec') ->
    Dec_red (Ret_v (x, e, v), ec')

let decompose e =
  decompose_e e Mt_e

let id_sub_v x = Var x
let id_sub_i a = a

let upd_sub s x p =
  fun y -> if y = x then p else s y

let rec subst_e e sv si =
  match e with
  | Val v' ->
    Val (subst_v v' sv si)
  | Let (y, e1, e2) ->
    let y' = fresh y
    in Let (y',
            subst_e e1 sv si,
            subst_e e2 (upd_sub sv y (Var y')) si)
  | App_v (v1, v2) ->
    App_v (subst_v v1 sv si, subst_v v2 sv si)
  | App_i (u, a) ->
    App_i (subst_v u sv si, si a)
  | App_t u ->
    App_t (subst_v u sv si)
  | Do (a, u) ->
    Do (si a, subst_v u sv si)
  | Hndl (a, e', h, r) ->
    let a' = fresh a
    in Hndl (a',
             subst_e e' sv (upd_sub si a a'),
             subst_h h sv si,
             subst_r r sv si)
and subst_h h sv si =
  match h with
  | Hndl_op (y, k, e) ->
    let y' = fresh y in
    let k' = fresh k in
    Hndl_op (y', k',
             subst_e e (upd_sub (upd_sub sv y (Var y')) k (Var k')) si)
and subst_r r sv si =
  match r with
  | Ret (y, e) ->
    let y' = fresh y
    in Ret (y', subst_e e (upd_sub sv y (Var y')) si)
and subst_v u sv si =
  match u with
  | Var y ->
    sv y
  | Lam_v (y, e) ->
    let y' = fresh y
    in Lam_v (y', subst_e e (upd_sub sv y (Var y')) si)
  | Lam_i (a, e) ->
    let a' = fresh a
    in Lam_i (a', subst_e e sv (upd_sub si a a'))
  | Lam_t e ->
    Lam_t (subst_e e sv si)
  | Unit ->
    Unit

let rec contract r =
  match r with
  | Beta_v (x, e, v) ->
    subst_e e (upd_sub id_sub_v x v) id_sub_i
  | Beta_i (a, e, b) ->
    failwith "TODO"
  | Beta_t e ->
    e
  | Let_v (x, v, e) ->
    subst_e e (upd_sub id_sub_v x v) id_sub_i
  | Ret_v (x, e, v) ->
    subst_e e (upd_sub id_sub_v x v) id_sub_i
  | Hndl_do (a, (Hndl_op (x, k, e) as h), r, rc, v) ->
    let z = fresh "z" in
    subst_e e
      (upd_sub
         (upd_sub id_sub_v x v)
         k
         (Lam_v (z, plug_r (Hndl_r (a, h, r, rc)) z)))
      id_sub_i

(*
let reduce e =
  match decompose e with
  | Dec_val v ->
    Val v
  | Dec_red (r, ec) ->
    plug_e ec (contract r)
*)

let rec iterate e =
  match decompose e with
  | Dec_val v ->
    v
  | Dec_red (r, ec) ->
    iterate (plug_e ec (contract r))

let eval e =
  iterate e
