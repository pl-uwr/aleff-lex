Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Set.
Require Import Types.
Require Import Lang.Fresh Lang.Open.
Require Import InterLangLR.Core.

Require Import Coq.Logic.Eqdep_dec.
Export TacticSynonyms.

(** * Equivalence of kind interpretations *)

Definition kind_equiv W (κ : kind) : K⟦ κ @ W⟧ → K⟦ κ @ W⟧ → IProp :=
  match κ with
  | k_type   => λ R₁ R₂, R₁ ≈ᵂ R₂
  | k_effect => λ R₁ R₂, R₁ ≈ᵂ R₂
  | k_sig    => λ R₁ R₂, R₁ ≈ᵂ R₂
  end.

Notation "K⟦ I₁ ≡ I₂ ∷ κ ⟧" := (kind_equiv _ κ I₁ I₂) (I₁ at level 30).

Definition kind_subrel W (κ : kind) : K⟦ κ @ W⟧ → K⟦ κ @ W⟧ → IProp :=
  match κ with
  | k_type   => λ R₁ R₂, R₁ ≾ᵂ R₂
  | k_effect => λ R₁ R₂, R₁ ≾ᵂ R₂
  | k_sig    => λ R₁ R₂, R₁ ≈ᵂ R₂
  end.

Notation "K⟦ I₁ ⊆ I₂ ∷ κ ⟧" := (kind_subrel _ κ I₁ I₂) (I₁ at level 30).

Instance CRefl_kequiv W (κ : kind) n : CReflexive (I_valid_at n) (kind_equiv W κ).
Proof.
  intros I; destruct κ; unfold kind_equiv; reflexivity.
Qed.

Instance CSymm_kequiv W (κ : kind) n : CSymmetric (I_valid_at n) (kind_equiv W κ).
Proof.
  intros I₁ I₂ HI; destruct κ; unfold kind_equiv in *; now symmetry.
Qed.

Instance CTrans_kequiv W (κ : kind) n : CTransitive (I_valid_at n) (kind_equiv W κ).
Proof.
  destruct κ; unfold kind_equiv; intros I₁ I₂ I₃ H1 H2; (etransitivity; [exact H1 | exact H2]).
Qed.

Instance CRefl_ksub W (κ : kind) n : CReflexive (I_valid_at n) (kind_subrel W κ).
Proof.
  intros I; destruct κ; unfold kind_subrel; reflexivity.
Qed.

Instance CTrans_ksub W (κ : kind) n : CTransitive (I_valid_at n) (kind_subrel W κ).
Proof.
  intros I₁ I₂ I₃; destruct κ; unfold kind_subrel; intros H1 H2; (etransitivity; [exact H1 | exact H2]).
Qed.

Instance CSubrel_kind n X (κ : kind) : CSubrelation (I_valid_at n) (kind_equiv X κ) (kind_subrel X κ).
Proof.
  destruct κ; unfold CSubrelation, kind_subrel, kind_equiv in *; intros; [apply subrel ..|]; assumption.
Qed.

Definition kind_equiv_eq {W} {κ₁ κ₂ : kind} (Heq : κ₁ = κ₂) :=
  match Heq in _ = κ₂ return K⟦ κ₁ @ W ⟧ → K⟦ κ₂ @ W ⟧ → IProp with
  | eq_refl => kind_equiv W κ₁
  end.

Notation "K⟦ Heq :⊨ I₁ ≡ I₂ ⟧" := (kind_equiv_eq Heq I₁ I₂) (Heq at level 30, I₂ at level 70).

Definition kind_equiv_by_eq {X Y} {κ₁ κ₂ : kind} (δ : X [→] Y) (ρ : Y [⇒] X)
           {SR : SectRetr (of_arrow δ) ρ} (EQκ : κ₁ = κ₂) : K⟦ κ₁ @ X ⟧ → K⟦ κ₂ @ Y ⟧ → IProp :=
  match EQκ in _ = κ₂ return K⟦ κ₁ @ X ⟧ → K⟦ κ₂ @ Y ⟧ → IProp with
  | eq_refl => kind_equiv_upto δ ρ κ₁
  end.

Notation "K⟦ Heq :⊨ I₁ ≡ I₂ 'by' [ δ , ρ ] ⟧" := (kind_equiv_by_eq δ ρ Heq I₁ I₂) (Heq at level 30, I₂ at level 70).

(** Properties *)

(* XXX: move *)
Lemma fwd_equiv {sig ord W₁ W₂} {RC : RetractCore sig} {RO : ROrd sig ord} {RP : ROPreo sig}
      (μ : WI_ext sig W₁) (ρ₁ ρ₂ : W₁ [⇒] W₂) n
      (EQρ : subst_eq ρ₁ ρ₂) :
  n ⊨ μ ⇑ ρ₁ ≈ᵂ μ ⇑ ρ₂.
Proof.
  iintro W₃; iintro ρ; unfold fwd_R; simpl; apply ext_eq.
  intros x; term_simpl; now rewrite EQρ.
Qed.

Lemma fwd_kint_equiv X W κ (μ₁ μ₂ : K⟦ κ @ X⟧) (ρ₁ ρ₂ : X [⇒] W) n
      (EQρ : subst_eq ρ₁ ρ₂)
      (EQμ : n ⊨ K⟦ μ₁ ≡ μ₂ ∷ κ ⟧) :
  n ⊨ K⟦ fwd_kint ρ₁ μ₁ ≡ fwd_kint ρ₂ μ₂ ∷ κ ⟧.
Proof.
  destruct κ; simpl in *; (etransitivity; [apply fw_eq | apply fwd_equiv]; assumption).
Qed.

Lemma fwd_kint_equiv_eq X W κ₁ κ₂ (μ₁ : K⟦ κ₁ @ X⟧) (μ₂ : K⟦ κ₂ @ X⟧) (ρ₁ ρ₂ : X [⇒] W) n
      (EQκ : κ₁ = κ₂)
      (EQρ : subst_eq ρ₁ ρ₂)
      (EQμ : n ⊨ K⟦ EQκ :⊨ μ₁ ≡ μ₂ ⟧) :
  n ⊨ K⟦ EQκ :⊨ fwd_kint ρ₁ μ₁ ≡ fwd_kint ρ₂ μ₂ ⟧.
Proof.
  revert EQμ; destruct EQκ; simpl; apply fwd_kint_equiv; assumption.
Qed.

Lemma fwd_kint_comp W₁ W₂ W₃ κ (μ : K⟦ κ @ W₁ ⟧) (ρ₁ : W₁ [⇒] W₂) (ρ₂ : W₂ [⇒] W₃) n :
  n ⊨ K⟦ fwd_kint (subst_comp ρ₂ ρ₁) μ ≡ fwd_kint ρ₂ (fwd_kint ρ₁ μ) ∷ κ ⟧.
Proof.
  destruct κ; simpl; apply fwd_comp.
Qed.

Lemma fwd_kint_pure W κ (μ : K⟦ κ @ W ⟧) n :
  n ⊨ K⟦ μ ≡ fwd_kint (subst_pure) μ ∷ κ ⟧.
Proof.
  destruct κ; simpl; apply fwd_pure_id.
Qed.

(** * Equivalence of type variables *)

Lemma kind_eq_proofs_unicity {κ₁ κ₂ : kind} (P₁ P₂ : κ₁ = κ₂) : P₁ = P₂.
Proof.
apply eq_proofs_unicity_on.
decide equality.
Qed.

Lemma rel_equiv_var_fwd X Y W (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W) κ κ₁ κ₂
    (Heq  : κ₁ = κ₂)
    (Heq₁ : κ₁ = κ) (Heq₂ : κ₂ = κ)
    (I₁ : K⟦ κ₁ @ X ⟧) (I₂ : K⟦ κ₂ @ Y ⟧) n :
  n ⊨ K⟦ Heq :⊨ fwd_kint ρ₁ I₁ ≡ fwd_kint ρ₂ I₂ ⟧ →
  n ⊨ K⟦ fwd_kint ρ₁ (eq_rect κ₁ (λ κ, K⟦ κ @ X ⟧) I₁ κ Heq₁)
       ≡ fwd_kint ρ₂ (eq_rect κ₂ (λ κ, K⟦ κ @ Y ⟧) I₂ κ Heq₂) ∷ κ ⟧.
Proof.
  destruct Heq.
  rewrite (kind_eq_proofs_unicity Heq₁ Heq₂); clear Heq₁.
  destruct κ₁; destruct κ; try discriminate;
    rewrite (kind_eq_proofs_unicity Heq₂ eq_refl); simpl; trivial.
Qed.

Lemma rel_equiv_var W κ κ₁ κ₂
    (Heq  : κ₁ = κ₂)
    (Heq₁ : κ₁ = κ) (Heq₂ : κ₂ = κ)
    (I₁ : K⟦ κ₁ @ W ⟧) (I₂ : K⟦ κ₂ @ W ⟧) n :
  n ⊨ K⟦ Heq :⊨ I₁ ≡ I₂ ⟧ →
  n ⊨ K⟦ eq_rect κ₁ (λ κ, K⟦ κ @ W ⟧) I₁ κ Heq₁
       ≡ eq_rect κ₂ (λ κ, K⟦ κ @ W ⟧) I₂ κ Heq₂ ∷ κ ⟧.
Proof.
destruct Heq.
rewrite (kind_eq_proofs_unicity Heq₁ Heq₂); clear Heq₁.
destruct κ₁; destruct κ; try discriminate;
  rewrite (kind_eq_proofs_unicity Heq₂ eq_refl); simpl; auto.
Qed.

Lemma rel_equiv_var_upto {X Y} {κ κ₁ κ₂ : kind} (δ : X [→] Y) (ρ : Y [⇒] X)
      {SR : SectRetr (of_arrow δ) ρ} (Heq  : κ₁ = κ₂) (Heq₁ : κ₁ = κ) (Heq₂ : κ₂ = κ)
      (I₁ : K⟦ κ₁ @ X ⟧) (I₂ : K⟦ κ₂ @ Y ⟧) n :
  n ⊨ K⟦ Heq :⊨ I₁ ≡ I₂ by [δ, ρ] ⟧ →
  n ⊨ K⟦ eq_rect κ₁ (λ κ, K⟦ κ @ X ⟧) I₁ κ Heq₁
       ≡ eq_rect κ₂ (λ κ, K⟦ κ @ Y ⟧) I₂ κ Heq₂ ∷ κ by [δ, ρ]⟧.
Proof.
  destruct Heq; rewrite (kind_eq_proofs_unicity Heq₁ Heq₂); clear Heq₁.
  destruct κ₁; destruct κ; try discriminate;
    rewrite (kind_eq_proofs_unicity Heq₂ eq_refl); simpl; trivial.
Qed.

Lemma shift_kint_equiv_upto X Y κ₁ κ₂ (δ : X [→] Y) (ρ : Y [⇒] X) {SR : SectRetr (of_arrow δ) ρ}
      (EQκ : κ₁ = κ₂) (μ₁ : K⟦ κ₁ @ X⟧) (μ₂ : K⟦ κ₂ @ Y⟧)
      n (EQμ : n ⊨ K⟦ EQκ :⊨ μ₁ ≡ μ₂ by [δ, ρ] ⟧) :
  n ⊨ K⟦ EQκ :⊨ shift_kint μ₁ ≡ shift_kint μ₂ by [liftA δ, liftS ρ] ⟧.
Proof.
  revert EQμ; destruct EQκ; simpl; destruct κ₁; apply fw_shift_upto_lift.
Qed.

Lemma kind_equiv_by_id X κ (μ₁ μ₂ : K⟦ κ @ X⟧) n :
  (n ⊨ K⟦ μ₁ ≡ μ₂ ∷ κ ⟧) ↔ (n ⊨ K⟦ μ₁ ≡ μ₂ ∷ κ by [arrow_id, subst_pure] ⟧).
Proof.
  destruct κ; simpl; apply equiv_upto_id.
Qed.


(** * applying equivalence *)

Lemma apply_type_equiv X (I₁ I₂ : Typ X) W ρ v₁ v₂ n :
  n ⊨ K⟦ I₁ ≡ I₂ ∷ k_type ⟧ → n ⊨ wi_rel I₁ W ρ v₁ v₂ ⇔ wi_rel I₂ W ρ v₁ v₂.
Proof.
  intro H; iespecialize H; isplit; [apply subrel in H | apply csymmetry, subrel in H]; iapply H.
Qed.

Lemma apply_sig_equiv X (I₁ I₂ : Sig X) W ρ v₁ v₂ μ n :
  n ⊨ K⟦ I₁ ≡ I₂ ∷ k_sig ⟧ → n ⊨ wi_rel I₁ W ρ v₁ v₂ μ ⇔ wi_rel I₂ W ρ v₁ v₂ μ.
Proof.
  intro H; iespecialize H; isplit; [apply subrel in H | apply csymmetry, subrel in H]; iapply H;
    reflexivity.
Qed.

Lemma apply_effect_equiv X (I₁ I₂ : Eff X) W ρ v₁ v₂ ls μ n :
  n ⊨ K⟦ I₁ ≡ I₂ ∷ k_effect ⟧ → n ⊨ wi_rel I₁ W ρ v₁ v₂ ls μ ⇔ wi_rel I₂ W ρ v₁ v₂ ls μ.
Proof.
  intro H; iespecialize H; isplit; [apply subrel in H | apply csymmetry, subrel in H]; iapply H;
    reflexivity.
Qed.
