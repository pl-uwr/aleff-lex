Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Product Binding.Env Binding.Set.
Require Import Types Kinding.
Require Import Lang.Fresh Lang.Open Lang.Surface.SimplerTypeSystem.
Require Import InterLangLR.ProgramObs InterLangLR.Core.
Require Import InterLangLR.KindEquiv InterLangLR.KindingIrrelevance.
Require Import InterLangLR.Weakening InterLangLR.Substitution InterLangLR.Compatibility.

Lemma tequiv_preserve {TV LV W : Set} {Δ : TV → kind} {κ τ₁ τ₂}
         {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ κ]}
         {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ κ]}
         (HE : Δ; LV ⊢ τ₁ ≃ τ₂)
         (ρ : LV [⇒] W)
         (η : ∀ α : TV, K⟦ Δ α @ W ⟧) (ϑ : LV → ℑ W) n :
  n ⊨ K⟦ ⟦ τ₁ ∷ κ ⟧ ρ ! η ! ϑ ≡ ⟦ τ₂ ∷ κ ⟧ ρ ! η ! ϑ ∷ κ ⟧.
Proof.
  revert κ WFτ₁ WFτ₂ W ρ η ϑ n; induction HE; intros; invert_kind WFτ₂; [| | | unfold kind_equiv; simpl type_interp ..].
  - apply type_interp_irr.
  - symmetry; apply IHHE.
  - assert (WFσ₂ : K[ Δ; LV ⊢ σ₂ ∷ κ]) by (eapply tequiv_wf; eassumption).
    etransitivity; [apply IHHE1 | apply IHHE2].
  - apply @equiv_arr; [symmetry; apply (IHHE1 k_type) | apply (IHHE2 k_type) | apply (IHHE3 k_effect)].
  - apply equiv_tall; iintro W'; iintro ρ'; iintro R; apply (IHHE k_type).
  - apply equiv_lall; iintro W'; iintro ρ'; iintro l; iintro a.
    etransitivity; [apply (IHHE2 k_type)
                   | apply (type_interp_k_irr k_type); [| iintro .. |]; try reflexivity].
    destruct x as [| x]; [| reflexivity]; isplit; [| reflexivity].
    apply fw_eq, (IHHE1 k_sig).
  - apply equiv_cons; [apply (IHHE1 k_effect) | apply (IHHE2 k_effect)].
  - apply equiv_sig; [apply (IHHE1 k_type) | apply (IHHE2 k_type)].
  - reflexivity.
  - etransitivity; [apply effect_equiv_consC |].
    apply equiv_cons; apply (type_interp_irr k_effect).
  - etransitivity; [| apply effect_equiv_consA]; apply wiequiv_symcl; simpl.
    + iintro W'; iintro ρ'; iintro e₁; iintro e₂; iintro ls; iintro μ₁; iintro μ₂; iintro EQμ; iintro HH.
      idestruct HH as HH HH; [ileft | iright]; [idestruct HH as HH HH; [ileft | iright] |].
      * eapply rel_eff_k_irr; iapply (ext_ord (R := F⟦ ε₁ ⟧ ρ ! η ! ϑ)); [reflexivity | eassumption |].
        eapply rel_eff_k_irr in HH; exact HH.
      * eapply rel_eff_k_irr; iapply (ext_ord (R := F⟦ ε₂ ⟧ ρ ! η ! ϑ)); [reflexivity | eassumption |].
        eapply rel_eff_k_irr in HH; exact HH.
      * eapply rel_eff_k_irr; iapply (ext_ord (R := F⟦ ε₃ ⟧ ρ ! η ! ϑ)); [reflexivity | eassumption |].
        eapply rel_eff_k_irr in HH; exact HH.
    + iintro W'; iintro ρ'; iintro e₁; iintro e₂; iintro ls; iintro μ₁; iintro μ₂; iintro EQμ; iintro HH.
      idestruct HH as HH HH; [ileft | iright]; [idestruct HH as HH HH; [ileft | iright] |].
      * eapply rel_eff_k_irr; iapply (ext_ord (R := F⟦ ε₁ ⟧ ρ ! η ! ϑ)); [reflexivity | eassumption |].
        eapply rel_eff_k_irr in HH; exact HH.
      * eapply rel_eff_k_irr; iapply (ext_ord (R := F⟦ ε₂ ⟧ ρ ! η ! ϑ)); [reflexivity | eassumption |].
        eapply rel_eff_k_irr in HH; exact HH.
      * eapply rel_eff_k_irr; iapply (ext_ord (R := F⟦ ε₃ ⟧ ρ ! η ! ϑ)); [reflexivity | eassumption |].
        eapply rel_eff_k_irr in HH; exact HH.
  - etransitivity; [| apply effect_equiv_cons_pureIL].
    apply (type_interp_irr k_effect).
  - etransitivity; [| apply effect_equiv_cons_pureIR].
    apply (type_interp_irr k_effect).
  - etransitivity; [apply effect_equiv_consI |].
    apply equiv_cons; apply (type_interp_irr k_effect).
Qed.

Lemma subtyp_preserve {TV LV W : Set} {Δ : TV → kind} {κ τ₁ τ₂}
         {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ κ]}
         {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ κ]}
         (HE : Δ; LV ⊢ τ₁ ≲ τ₂)
         (ρ : LV [⇒] W) (η : ∀ α : TV, K⟦ Δ α @ W⟧) (ϑ : LV → ℑ W) n :
  n ⊨ K⟦ ⟦ τ₁ ∷ κ ⟧ ρ ! η ! ϑ ⊆ ⟦ τ₂ ∷ κ ⟧ ρ ! η ! ϑ ∷ κ ⟧.
Proof.
  revert κ WFτ₁ WFτ₂ W ρ η ϑ n; induction HE; intros;
    invert_kind WFτ₁; [| unfold kind_equiv; simpl type_interp ..].
  - apply subrel, tequiv_preserve, HTE.
  - apply @subrel_arr; [apply (IHHE1 k_type) | apply (IHHE3 k_type) | apply (IHHE2 k_effect)].
  - apply subrel_tall; iintro W'; iintro ρ'; iintro R; apply (IHHE k_type).
  - apply subrel_lall; iintro W'; iintro ρ'; iintro l; iintro a.
    invert_kind WFτ₂; etransitivity; [apply (IHHE k_type) | apply subrel, (type_interp_k_irr k_type); [| iintro ..|]; try reflexivity].
    destruct x as [| x]; [| reflexivity]; isplit; [| reflexivity]; simpl.
    apply fw_eq, (tequiv_preserve (κ := k_sig)), HTE.
  - apply subrel_cons; [apply (IHHE1 k_effect) | apply (IHHE2 k_effect)].
  - apply effect_sub_pure.
Qed.

Fixpoint fundamental_property {V TV LV : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV}
  {τ ε : typ_ TV LV} {e : Surface.Syntax.expr_ V LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]} {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {n}
  (HT : Δ ; Θ ; Γ ⊢ e :: τ / ε) {struct HT} :
  n ⊨ T⟦ Δ ; Θ ; Γ ⊨ FT.tr_expr e ≾ OT.tr_expr e ∷ τ / ε ⟧
with fundamental_property_v {V TV LV : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV}
  {τ : typ_ TV LV} {v : Surface.Syntax.value_ V LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]} {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {n} (HT : Δ ; Θ ; Γ ⊢ᵛ v :: τ) {struct HT} :
  n ⊨ T⟦ Δ ; Θ ; Γ ⊨ FT.tr_value v ≾ OT.tr_value v ∷ τ ⟧
with fundamental_property_h {V TV LV : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV}
  {σ τ ε : typ_ TV LV} {h : Surface.Syntax.handler_ V LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]} {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFσ : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {n} (HT : Δ ; Θ ; Γ ⊢ʰ h :: σ @ τ / ε) {struct HT} :
  n ⊨ H⟦ Δ ; Θ ; Γ ⊨ (FT.tr_handler h) ≾ OT.tr_handler h ∷ σ @ τ / ε ⟧.
Proof.
  - destruct HT; simpl FT.tr_expr; simpl OT.tr_expr.
    + apply rel_v_in_rel_e_open, fundamental_property_v; assumption.
    + assert (WFτ₁ := etypes_wf_typ HT1).
      apply (let_compat τ₁); apply fundamental_property; assumption.
    + assert (WFτ₁ := vtypes_wf HT₂).
      apply (app_compat τ₁); apply fundamental_property_v; assumption.
    + assert (WFτ₁ := vtypes_wf HT); invert_kind WFτ₁ as WF.
      refine (rel_e_open_k_irr (tapp_compat _)).
      apply fundamental_property_v; assumption.
    + assert (WFτ₁ := vtypes_wf HT); invert_kind WFτ₁ as WF.
      refine (rel_e_open_k_irr (lapp_compat _)).
      apply fundamental_property_v; assumption.
    + assert (WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type])
        by (specialize (WFΘ a); rewrite HI in WFΘ; now invert_kind WFΘ).
      eapply (do_compat τ₁ (a := a)); [assumption | now apply fundamental_property_v].
    + assert (WFτ' := etypes_wf_typ HT1).
      eapply handle_compat with (τ0 := τ).
      * apply fundamental_property, HT1.
      * apply fundamental_property_h, HTh.
      * apply fundamental_property, HT2.
    + assert (WFτ₁ := etypes_wf_typ HT); assert (WFε₁ := etypes_wf_eff HT).
      eapply fundamental_property with (n := n) in HT.
      iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
      iintro Hϑ; iintro Hδ; iintro Hγ.
      eapply subtyp_preserve in HSTτ; eapply subtyp_preserve in HSTε.
      iapply @subrel_expr_cl; [..| iapply HT]; eassumption.
  - destruct HT; simpl FT.tr_value; simpl OT.tr_value.
    + apply unit_compat.
    + apply (rel_v_open_k_irr (var_compat_v _ _)).
    + invert_kind WFτ as WF; refine (rel_v_open_k_irr (lam_compat _)).
      apply fundamental_property; assumption.
    + invert_kind WFτ; refine (rel_v_open_k_irr (tlam_compat _)).
      apply fundamental_property; assumption.
    + invert_kind WFτ; refine (rel_v_open_k_irr (llam_compat σ _)).
      apply fundamental_property; assumption.
  - destruct HT; simpl FT.tr_handler; simpl OT.tr_handler.
    + invert_kind WFσ.
      assert (HH := handler_do_compat Δ Θ Γ τ₁ τ₂ τ ε (FT.tr_expr e) (OT.tr_expr e) n).
      eapply rel_h_open_k_irr in HH; [assumption |].
      now apply fundamental_property.
Qed.