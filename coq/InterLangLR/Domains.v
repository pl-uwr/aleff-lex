Require Import Utf8.
Require Import IxFree.Lib IxFree.Relations.
Require Import Binding.Lib Binding.Set.
Require Import Lang.Fresh Lang.Open.

Import TacticSynonyms.

Class CSubrelation {A B} (f : B → Prop) {GR : RelationContext f} (R₁ R₂ : A → A → B) :=
  subrel : ∀ a₁ a₂, f (R₁ a₁ a₂) → f (R₂ a₁ a₂).

Instance Subrel_eq_preo sig n : CSubrelation (I_valid_at n) (I_rel_equiv sig) (I_rel_sub sig).
Proof.
  unfold CSubrelation; induction sig; simpl.
  - intros; now idestruct H as H1 H2.
  - intros a₁ a₂ Hx; iintro x; apply H; iapply Hx.
Qed.

Definition WIRel (sig : Set → IRelSig) X :=
  IRel (∀ₛ W (ρ : X [⇒] W), sig W)%irel_sig.

(* Relations with a chosen (per signature) ordering relation *)
Class ROrd (sig : Set → IRelSig) (R : ∀ X, IRel (sig X) → IRel (sig X) → IProp) : Prop.

Definition rord {sig ord} {RO : ROrd sig ord} {X} R₁ R₂ := ord X R₁ R₂.
Definition req  {sig ord} {RO : ROrd sig ord} {X} R₁ R₂ := ord X R₁ R₂ ∧ᵢ ord X R₂ R₁.

Notation "R₁ ≾ᴿ R₂" := (rord R₁ R₂) (at level 70, no associativity).
Notation "R₁ ≈ᴿ R₂" := (req R₁ R₂)  (at level 70, no associativity).

Lemma req_symcl {sig ord} {RO : ROrd sig ord} {X} (R₁ R₂ : IRel (sig X)) n
      (HR₁ : n ⊨ R₁ ≾ᴿ R₂) (HR₂ : n ⊨ R₂ ≾ᴿ R₁) :
  n ⊨ R₁ ≈ᴿ R₂.
Proof.
  isplit; [exact HR₁ | exact HR₂].
Qed.

Instance Subrel_eq_ord {sig ord} {RO : ROrd sig ord} X n :
  CSubrelation (I_valid_at n) (req (X := X)) (rord (X := X)).
Proof.
  unfold CSubrelation; intros; now idestruct H as H1 H2.
Qed.

Instance CSymm_eq {sig ord} {RO : ROrd sig ord} X n : CSymmetric (I_valid_at n) (req (X := X)).
Proof.
  intros R₁ R₂ HR; idestruct HR as HR₁ HR₂; now isplit.
Qed.

(* Note these relations are not necessarily reflexive, since they tend to be extensional;
   this makes the induced equivalence more a PER *)
Class ROPreo sig {R} {RO : ROrd sig R} :=
  { rord_trans n X :> CTransitive (I_valid_at n) (R X);
    rord_preo  n X :> CSubrelation (I_valid_at n) (R X) (I_rel_sub (sig X));
    rord_bot   n X I :> n ⊨ R X ⊥ᵣ%irel I
  }.

Instance CTrans_eq {sig ord} {RO : ROrd sig ord} {ROP : ROPreo sig} X n :
  CTransitive (I_valid_at n) (req (X := X)).
Proof.
  intros R₁ R₂ R₃ HR₁ HR₂; idestruct HR₁ as HR1₁ HR1₂; idestruct HR₂ as HR2₁ HR2₂;
    isplit; etransitivity; eassumption.
Qed.

Class OrdExt {sig ord} {RO : ROrd sig ord} {X} (R : IRel (sig X)) :=
  self_ord n : n ⊨ R ≾ᴿ R.

Lemma self_eq {sig ord} {RO : ROrd sig ord} {X} (R : IRel (sig X)) {OE : OrdExt R} n:
  n ⊨ R ≈ᴿ R.
Proof.
  isplit; apply self_ord.
Qed.

(* Extensional world-indexed relations do not differentiate between equal substitutions.
   Note that ordering on these *is* reflexive. *)
Class WIR_ext {sig ord} {RO : ROrd sig ord} {X} (R : WIRel sig X) :=
  ext_ord W (ρ₁ ρ₂ : X [⇒] W) n (EQρ : subst_eq ρ₁ ρ₂) : n ⊨ R W ρ₁ ≾ᴿ R W ρ₂.
Arguments ext_ord {sig ord RO X R WIR_ext} [W].

Lemma ext_eq {sig ord} {RO : ROrd sig ord} {X} {R : WIRel sig X} {WIE : WIR_ext R}
      W (ρ₁ ρ₂ : X [⇒] W) n (EQρ : subst_eq ρ₁ ρ₂) :
  n ⊨ R W ρ₁ ≈ᴿ R W ρ₂.
Proof.
  isplit; apply ext_ord; [| symmetry]; assumption.
Qed.

Instance WIR_Ord_ext {sig ord} {RO : ROrd sig ord} {X} (R : WIRel sig X) {WIE : WIR_ext R} W ρ :
  OrdExt (R W ρ).
Proof.
  unfold OrdExt; intros; apply ext_ord; reflexivity.
Qed.

(* Retraction of a relation via a substitution, used to define the future-world closedness *)
Class RetractCore (sig : Set → IRelSig) :=
  retract (X Y : Set) (δ : X [⇒] Y) (R : IRel (sig Y)) : IRel (sig X).
Arguments retract {sig RetractCore X Y} δ R.

Class Retract sig {ord} {RC : RetractCore sig} {WP : ROrd sig ord} :=
  { retract_ord {W₁ W₂} (ρ : W₁ [⇒] W₂) (R₁ R₂ : IRel (sig W₂)) n :
      n ⊨ R₁ ≾ᴿ R₂ → n ⊨ retract ρ R₁ ≾ᴿ retract ρ R₂;
    retract_pure W (R : IRel (sig W)) {RE : OrdExt R} n : n ⊨ retract subst_pure R ≈ᴿ R;
    retract_comp {W₁ W₂ W₃} (ρ₁ : W₁ [⇒] W₂) (ρ₂ : W₂ [⇒] W₃) (R : IRel (sig W₃)) {RE : OrdExt R} n :
      n ⊨ retract (subst_comp ρ₂ ρ₁) R ≈ᴿ retract ρ₁ (retract ρ₂ R)
  }.

Class WIR_fwcl {sig ord}  {RC : RetractCore sig} {RO : ROrd sig ord} {X} (R : WIRel sig X) :=
  fw_ord : ∀ W₁ W₂ (ρ₁ : X [⇒] W₁) (ρ₂ : W₁ [⇒] W₂) n, n ⊨ R W₁ ρ₁ ≾ᴿ retract ρ₂ (R W₂ (subst_comp ρ₂ ρ₁)).

Record WI_ext sig {ord} {RO : ROrd sig ord} X : Type :=
  { wi_rel :> WIRel sig X;
    wi_ext : WIR_ext wi_rel
  }.
Arguments wi_rel {sig ord RO X} w.

Record WI_dom sig {ord} {RC : RetractCore sig} {RO : ROrd sig ord} X : Type :=
  { wi_er  :> WI_ext sig X;
    wi_fcl :> WIR_fwcl wi_er
  }.
Arguments WI_dom sig {ord RC RO} X.

Instance WI_ext_ext {sig ord} {RO : ROrd sig ord} {X} (R : WI_ext sig X) : WIR_ext R | 0 := wi_ext _ _ R.
Instance WI_dom_fcl  {sig ord} {RC : RetractCore sig} {RO : ROrd sig ord} {X} (R : WI_dom sig X) :
  WIR_fwcl R | 0 := wi_fcl _ _ R.

Notation "⟨ R ⟩ᴿ" := {| wi_rel := R; wi_ext := _ |}.
Notation "⟨ R ⟩ᵂ" := {| wi_er := ⟨R⟩ᴿ; wi_fcl := _ |}.

Definition wi_preo {sig ord} {RO : ROrd sig ord} {X} (R₁ R₂ : WI_ext sig X) : IProp :=
  ∀ᵢ W (ρ : X [⇒] W), wi_rel R₁ W ρ ≾ᴿ wi_rel R₂ W ρ.

Definition wi_equiv {sig ord} {RO : ROrd sig ord} {X} (R₁ R₂ : WI_ext sig X) : IProp :=
  ∀ᵢ W (ρ : X [⇒] W), wi_rel R₁ W ρ ≈ᴿ wi_rel R₂ W ρ.

Notation "μ ≾ᵂ μ'"     := (@wi_preo _ _ _ _ μ μ') (at level 70, μ' at level 70, no associativity).
Notation "μ ≈ᵂ μ'"     := (@wi_equiv _ _ _ _ μ μ') (at level 70, μ' at level 70, no associativity).

Class SectRetr {X Y} (ρ₁ : X [⇒] Y) (ρ₂ : Y [⇒] X) :=
  inverse : subst_eq (subst_comp ρ₂ ρ₁) subst_pure.

Definition wi_equiv_upto {sig ord} {RO : ROrd sig ord} {X Y} (δ : X [→] Y) (ρ : Y [⇒] X)
           {EQ : SectRetr (of_arrow δ) ρ} (R₁ : WI_ext sig X) (R₂ : WI_ext sig Y) : IProp :=
  ∀ᵢ W (ρ' : X [⇒] W), wi_rel R₁ W ρ' ≈ᴿ wi_rel R₂ W (subst_comp ρ' ρ).

Notation "μ ≈ᵂ μ' 'by' [ δ , ρ ]" := (wi_equiv_upto δ ρ μ μ') (at level 70, μ' at level 70, no associativity).

(*(* Order and equivalence up to a map of world-indexed relations. Note
   this is defined on extensional relations only, in order to be
   respectively a preorder/equivalence relation (when fed an identity map) *)
Definition wi_equiv {sig ord} {RO : ROrd sig ord} {RC : RetractCore sig} {X Y}
           (δ : X [→] Y) (R₁ : WI_ext sig Y) (R₂ : WI_ext sig X) : IProp :=
  ∀ᵢ W (ρ : X [⇒] W), ∃ᵢ W' (δ' : W [→] W') (ρ' : Y [⇒] W'),
      (subst_eq (arrow_subst_comp δ' ρ) (subst_comp ρ' (of_arrow δ)))ᵢ
      ∧ᵢ retract (of_arrow δ') (wi_rel R₁ W' ρ') ≈ᴿ wi_rel R₂ W ρ.
  (*∀ᵢ W (ρ : Y [⇒] W), wi_rel R₁ W ρ ≈ᴿ wi_rel R₂ W (subst_comp ρ (of_arrow δ)).*)
Notation "μ ≈ᵂ μ' 'by' δ" := (@wi_equiv _ _ _ _ _ _ δ μ μ') (at level 70, μ' at level 70, no associativity).
*)

Instance CRefl_wip {sig ord} {RO : ROrd sig ord} {X} n :
  CReflexive (I_valid_at n) (wi_preo (X := X)).
Proof.
  intros R; iintro W; iintro ρ; apply self_ord.
Qed.

Instance CTrans_wip {sig ord} {RO : ROrd sig ord} {ROP : ROPreo sig} {X} n :
  CTransitive (I_valid_at n) (wi_preo (X := X)).
Proof.
  intros R₁ R₂ R₃ HRA HRB; iintro W; iintro ρ.
  etransitivity; [iapply HRA | iapply HRB].
Qed.

Instance CSub_wi {sig ord} {RO : ROrd sig ord} {ROP : ROPreo sig} {RC : RetractCore sig} {X} n :
  CSubrelation (I_valid_at n) (wi_equiv) (wi_preo (X := X)).
Proof.
  intros R₁ R₂ HR; iintro W; iintro ρ; apply subrel; iapply HR.
Qed.

Instance CRefl_wieq {sig ord} {RO : ROrd sig ord} {X} n :
  CReflexive (I_valid_at n) (wi_equiv (X := X)).
Proof.
  intros R; iintro W; iintro ρ; apply ext_eq; intros x; reflexivity.
Qed.

Instance CSymm_wieq {sig ord} {RO : ROrd sig ord} {ROP : ROPreo sig} {X} n :
  CSymmetric (I_valid_at n) (wi_equiv (X := X)).
Proof.
  intros R₁ R₂ HR; iintro W; iintro ρ; symmetry.
  etransitivity; [iapply HR | apply ext_eq; intros x; reflexivity].
Qed.

Instance CTrans_wieq {sig ord} {RO : ROrd sig ord} {ROP : ROPreo sig} {X} n :
  CTransitive (I_valid_at n) (wi_equiv (X := X)).
Proof.
  intros R₁ R₂ R₃ HRA HRB; iintro W; iintro ρ.
  etransitivity; [iapply HRA | etransitivity; [iapply HRB | apply ext_eq]].
  intros x; reflexivity.
Qed.

Instance SR_id X : SectRetr (of_arrow (arrow_id (A := X))) subst_pure.
Proof.
  unfold SectRetr; now intros x.
Qed.

Instance SR_lift X Y (δ : X [→] Y) (ρ : Y [⇒] X) {SR : SectRetr (of_arrow δ) ρ} :
  SectRetr (of_arrow (liftA δ)) (liftS ρ).
Proof.
  intros [| x]; [reflexivity | term_simpl].
  assert (HT := inverse x); term_simpl in HT; now rewrite HT.
Qed.

Instance SR_shift_subst X (l : O.lvar X) : SectRetr (of_arrow mk_shift) (mk_subst l).
Proof.
  unfold SectRetr; apply subst_shift_pure.
Qed.

Lemma equiv_upto_id {sig ord} {RO : ROrd sig ord} {ROP : ROPreo sig} X (R₁ R₂ : WI_ext sig X) n :
  (n ⊨ R₁ ≈ᵂ R₂) ↔ (n ⊨ R₁ ≈ᵂ R₂ by [arrow_id, subst_pure]).
Proof.
  split; intros HR; iintro W; iintro ρ.
  - etransitivity; [iapply HR | iapply (ext_eq (R := R₂)); now intros x].
  - etransitivity; [iapply HR | iapply (ext_eq (R := R₂)); now intros x].
Qed.

Lemma eq_upto_equiv {sig ord} {RO : ROrd sig ord} {ROP : ROPreo sig}
      X Y (δ₁ δ₂ : X [→] Y) (ρ₁ ρ₂ : Y [⇒] X)
      {SR₁ : SectRetr (of_arrow δ₁) ρ₁} {SR₂ : SectRetr (of_arrow δ₂) ρ₂}
      (EQρ : subst_eq ρ₁ ρ₂)
      μ₁ μ₂ n (EQμ : n ⊨ μ₁ ≈ᵂ μ₂ by [δ₁, ρ₁]) : n ⊨ μ₁ ≈ᵂ μ₂ by [δ₂, ρ₂].
Proof.
  iintro W; iintro ρ; etransitivity; [iapply EQμ | iapply (ext_eq (R := μ₂))].
  intros x; simpl; now rewrite EQρ.
Qed.

(* Moving a relation forward by a substitution *)
Definition fwd_R {sig X} (R : WIRel sig X) {W} (ρ : X [⇒] W) : WIRel sig W :=
  λ W' ρ', R W' (subst_comp ρ' ρ).

Instance ext_fwd {sig ord X} {RO : ROrd sig ord} (R : WIRel sig X) {HE : WIR_ext R} W (ρ : X [⇒] W)
       : WIR_ext (fwd_R R ρ).
Proof.
  unfold WIR_ext, fwd_R; intros; apply HE; intros x; simpl; now apply bind_equiv.
Qed.

Notation " μ ⇑ ρ " := {| wi_rel := fwd_R μ ρ;
                         wi_ext := ext_fwd _ _ ρ |} (at level 40, left associativity).

Instance fwcl_fwd {sig ord} {RO : ROrd sig ord} {ROP : ROPreo sig} {RC : RetractCore sig} {RR : Retract sig}
      {X} (R : WIRel sig X) {WE : WIR_ext R} {WF : WIR_fwcl R} {W} (ρ : X [⇒] W) :
  WIR_fwcl (fwd_R R ρ).
Proof.
  intros W₁ W₂ ρ₁ ρ₂ n; unfold fwd_R.
  etransitivity; [apply fw_ord | eapply retract_ord].
  apply ext_ord, subst_eq_symm, subst_comp_assoc.
Qed.

Lemma fw_eq {sig ord X} {RO : ROrd sig ord} {ROP : ROPreo sig} W (ρ : X [⇒] W) μ₁ μ₂ n
      (Hμ : n ⊨ μ₁ ≈ᵂ μ₂) :
  n ⊨ ⟨fwd_R μ₁ ρ⟩ᴿ ≈ᵂ ⟨fwd_R μ₂ ρ⟩ᴿ.
Proof.
  iintro W'; iintro ρ'; unfold fwd_R.
  etransitivity; [iapply Hμ | apply (ext_eq (R := μ₂))].
  intros x; term_simpl; apply bind_equiv; intros y; simpl; reflexivity.
Qed.

Lemma fw_shift_upto_lift {sig ord} {RO : ROrd sig ord} {ROP : ROPreo sig}
      X Y (δ : X [→] Y) (ρ : Y [⇒] X) {SR : SectRetr (of_arrow δ) ρ}
      (μ₁ : WI_ext sig X) (μ₂ : WI_ext sig Y) n (EQμ : n ⊨ μ₁ ≈ᵂ μ₂ by [δ, ρ]) :
  n ⊨ ⟨fwd_R μ₁ (of_arrow mk_shift)⟩ᴿ ≈ᵂ ⟨fwd_R μ₂ (of_arrow mk_shift)⟩ᴿ by [liftA δ, liftS ρ].
Proof.
  iintro W; iintro ρ'; etransitivity; [iapply EQμ | iapply (ext_eq (R := μ₂))].
  intros y; term_simpl; symmetry; rewrite map_to_bind; apply bind_bind_comp; now intros x.
Qed.

Lemma fwd_uptoR {sig ord X Y} {RO : ROrd sig ord} {ROP : ROPreo sig} (δ : X [→] Y) (ρ : Y [⇒] X)
      {SR : SectRetr (of_arrow δ) ρ} (μ : WI_ext sig X) n :
  n ⊨ μ ≈ᵂ ⟨fwd_R μ (of_arrow δ)⟩ᴿ by [δ, ρ].
Proof.
  iintro W; iintro ρ'; unfold fwd_R; simpl; apply ext_eq.
  intros x; term_simpl; symmetry.
  assert (HT := inverse x); term_simpl in HT; now rewrite HT.
Qed.

Lemma fwd_uptoL {sig ord X Y} {RO : ROrd sig ord} {ROP : ROPreo sig} (δ : X [→] Y) (ρ : Y [⇒] X)
      {SR : SectRetr (of_arrow δ) ρ} (μ : WI_ext sig Y) n :
  n ⊨ ⟨fwd_R μ ρ⟩ᴿ ≈ᵂ μ by [δ, ρ].
Proof.
  iintro W; iintro ρ'; unfold fwd_R; simpl; apply ext_eq.
  intros x; term_simpl; reflexivity.
Qed.

(* Semantic type of computations: world-indexed, extensional and
   fw-closed relations on pairs of expressions *)
Definition expr_rel_sig W := (F.expr0 →ₛ O.expr_ ∅ W →ₛ *ₛ)%irel_sig.
Notation Rexpr := (WIRel expr_rel_sig).
Notation Comp  := (WI_dom expr_rel_sig).

Instance RO_expr : ROrd expr_rel_sig (λ X, I_rel_sub (expr_rel_sig X)).

Instance ROP_expr : ROPreo expr_rel_sig.
Proof.
  split; intros; [apply _ | |].
  - unfold CSubrelation; intros; assumption.
  - repeat iintro; contradiction.
Qed.

Lemma ord_expr_unroll X (R₁ R₂ : IRel (expr_rel_sig X)) n
      (HH : ∀ e₁ e₂, n ⊨ R₁ e₁ e₂ ⇔ R₂ e₁ e₂) :
  n ⊨ R₁ ≈ᴿ R₂.
Proof.
  isplit; apply subrel; repeat iintro; [| symmetry]; apply HH.
Qed.

Lemma ord_expr_roll X (R₁ R₂ : IRel (expr_rel_sig X)) e₁ e₂ n
      (HH : n ⊨ R₁ ≈ᴿ R₂) :
  n ⊨ R₁ e₁ e₂ ⇔ R₂ e₁ e₂.
Proof.
  idestruct HH as HH₁ HH₂; isplit; [iapply HH₁ | iapply HH₂].
Qed.

Instance RC_expr : RetractCore expr_rel_sig :=
  λ W₁ W₂ (ρ : W₁ [⇒] W₂) (R : IRel (expr_rel_sig W₂)) e₁ e₂, R e₁ (bind (O.injLSub ρ) e₂).

Instance Retract_expr : Retract expr_rel_sig.
Proof.
  split; intros; [| apply ord_expr_unroll; intros; unfold retract, RC_expr ..].
  - iintro e₁; iintro e₂; unfold retract, RC_expr; iapply H.
  - rewrite bind_pure; reflexivity.
  - erewrite bind_bind_comp; reflexivity.
Qed.

(* Relations on evaluation contexts and control-stuck terms. These are
extensional (to allow ordering/equivalence) but *not* fw-closed *)
Definition ectx_rel_sig X := (F.ectx0 →ₛ O.ectx X →ₛ *ₛ)%irel_sig.
Notation Rectx := (WIRel ectx_rel_sig).

Definition stuck_rel_sig X Y :=
  (F.rctx0 →ₛ O.rctx X Y →ₛ F.expr0 →ₛ O.expr_ ∅ X →ₛ *ₛ)%irel_sig.
Notation Rstuck In := (WIRel (stuck_rel_sig In)).

Instance RO_ectx : ROrd ectx_rel_sig (λ X, I_rel_sub (ectx_rel_sig X)).
Instance RO_stuck X : ROrd (stuck_rel_sig X) (λ Y, I_rel_sub (stuck_rel_sig X Y)).

Instance ROP_ectx : ROPreo ectx_rel_sig.
Proof.
  split; intros; [apply _ |..].
  - unfold CSubrelation; intros; assumption.
  - repeat iintro; contradiction.
Qed.

Instance ROP_stuck X : ROPreo (stuck_rel_sig X).
Proof.
  split; intros n Y; [apply _ |..].
  - unfold CSubrelation; intros; assumption.
  - intros; repeat iintro; contradiction.
Qed.

(* Likewise for the relation on handlers: we do not need fw-closure,
   since we do not quantify over these *)
Definition handler_rel_sig X := (F.handler0 →ₛ O.handler_ ∅ X →ₛ *ₛ)%irel_sig.
Notation Rhandler := (WIRel handler_rel_sig).

Instance RO_handler : ROrd handler_rel_sig (λ X, I_rel_sub (handler_rel_sig X)).

Instance ROP_handler : ROPreo handler_rel_sig.
Proof.
  split; intros; [apply _ |..].
  - unfold CSubrelation; intros; assumption.
  - repeat iintro; contradiction.
Qed.

Instance RC_hndlr : RetractCore handler_rel_sig :=
  λ W₁ W₂ (ρ : W₁ [⇒] W₂) (R : IRel (handler_rel_sig W₂)) h₁ h₂, R h₁ (bind (O.injLSub ρ) h₂).

Instance Retract_hndlr : Retract handler_rel_sig.
Proof.
  split; intros.
  - iintro h₁; iintro h₂; unfold retract, RC_expr; iapply H.
  - isplit; iintro h₁; iintro h₂; unfold retract, RC_hndlr; rewrite bind_pure; reflexivity.
  - isplit; iintro h₁; iintro h₂; unfold retract, RC_hndlr; erewrite bind_bind_comp; reflexivity.
Qed.

(* Semantic types are much like semantic computations *)
Definition type_rel_sig X := (F.value0 →ₛ O.value_ ∅ X →ₛ *ₛ)%irel_sig.
Notation Rtype := (WIRel type_rel_sig).

Instance RO_type : ROrd type_rel_sig (λ X, I_rel_sub (type_rel_sig X)).

Instance ROP_type : ROPreo type_rel_sig.
Proof.
  split; intros; [apply _ | ..].
  - unfold CSubrelation; intros; assumption.
  - repeat iintro; contradiction.
Qed.

Instance RC_type : RetractCore type_rel_sig :=
  λ W₁ W₂ (ρ : W₁ [⇒] W₂) (R : IRel (type_rel_sig W₂)) v₁ v₂, R v₁ (bind (O.injLSub ρ) v₂).

Lemma ord_type_unroll X (R₁ R₂ : IRel (type_rel_sig X)) n
      (HH : ∀ v₁ v₂, n ⊨ R₁ v₁ v₂ ⇔ R₂ v₁ v₂) :
  n ⊨ R₁ ≈ᴿ R₂.
Proof.
  isplit; apply subrel; repeat iintro; [| symmetry]; apply HH.
Qed.

Lemma ord_type_roll X (R₁ R₂ : IRel (type_rel_sig X)) v₁ v₂ n
      (HH : n ⊨ R₁ ≈ᴿ R₂) :
  n ⊨ R₁ v₁ v₂ ⇔ R₂ v₁ v₂.
Proof.
  idestruct HH as HH₁ HH₂; isplit; [iapply HH₁ | iapply HH₂].
Qed.

Instance Retract_type : Retract type_rel_sig.
Proof.
  split; intros; [| apply ord_type_unroll; intros; unfold retract, RC_type ..].
  - iintro v₁; iintro v₂; unfold retract, RC_type; iapply H.
  - rewrite bind_pure; reflexivity.
  - erewrite bind_bind_comp; reflexivity.
Qed.

Notation Typ := (WI_dom type_rel_sig).

(* Semantic signatures are similar, but more complex, as they
*contain* semantic types in the relation. These (and semantic effects)
are the reason for much of the machinery introduced before *)

Definition sig_rel_sig X := (F.value0 →ₛ O.value_ ∅ X →ₛ Typ X →ₛ *ₛ)%irel_sig.
Notation Rsig := (WIRel sig_rel_sig).

Definition sigord (X : Set) (R₁ R₂ : IRel (sig_rel_sig X)) :=
  ∀ᵢ v₁ v₂ (μ₁ μ₂ : Typ X), (μ₁ ≈ᵂ μ₂) ⇒ R₁ v₁ v₂ μ₁ ⇒ R₂ v₁ v₂ μ₂.

Instance RO_sig : ROrd sig_rel_sig sigord.

Instance ROP_sig : ROPreo sig_rel_sig.
Proof.
  split; intros.
  - intros R₁ R₂ R₃ HR₁ HR₂; iintro v₁; iintro v₂; iintro μ₁; iintro μ₂; iintro Hμ; iintro HR.
    iapply HR₂; [reflexivity | iapply HR₁; eassumption].
  - unfold CSubrelation; intros R₁ R₂ HR.
    iintro v₁; iintro v₂; iintro μ; iapply HR; reflexivity.
  - repeat iintro; contradiction.
Qed.

Instance RC_sig : RetractCore sig_rel_sig :=
  λ W₁ W₂ (ρ : W₁ [⇒] W₂) (R : IRel (sig_rel_sig W₂)) v₁ v₂ μ,
  R v₁ (bind (O.injLSub ρ) v₂) ⟨fwd_R μ ρ⟩ᵂ.

Lemma ord_sig_unroll X (R₁ R₂ : IRel (sig_rel_sig X)) n
      (HH : ∀ v₁ v₂ (μ₁ μ₂ : Typ X) n (Hμ : n ⊨ μ₁ ≈ᵂ μ₂), n ⊨ R₁ v₁ v₂ μ₁ ⇔ R₂ v₁ v₂ μ₂) :
  n ⊨ R₁ ≈ᴿ R₂.
Proof.
  isplit; iintro v₁; iintro v₂; iintro μ₁; iintro μ₂; iintro Hμ;
    [| apply csymmetry in Hμ]; eapply HH in Hμ;
      idestruct Hμ as H1 H2; eassumption.
Qed.

Lemma ord_sig_roll X (R₁ R₂ : IRel (sig_rel_sig X)) n v₁ v₂ (μ₁ μ₂ : Typ X)
      (HR : n ⊨ R₁ ≈ᴿ R₂)
      (Hμ : n ⊨ μ₁ ≈ᵂ μ₂) :
  n ⊨ R₁ v₁ v₂ μ₁ ⇔ R₂ v₁ v₂ μ₂.
Proof.
  idestruct HR as HR₁ HR₂; isplit; [iapply HR₁ | iapply HR₂; symmetry]; assumption.
Qed.

Lemma sig_equiv {X} {R : IRel (sig_rel_sig X)} {OE : OrdExt R} v₁ v₂ (μ₁ μ₂ : Typ X) n
      (HH : n ⊨ μ₁ ≈ᵂ μ₂) :
  n ⊨ R v₁ v₂ μ₁ ⇔ R v₁ v₂ μ₂.
Proof.
  isplit; iapply self_ord; [| symmetry]; assumption.
Qed.

Instance Retract_sig : Retract sig_rel_sig.
Proof.
  split; intros; [| apply ord_sig_unroll; intros; unfold retract, RC_sig ..].
  - iintro v₁; iintro v₂; iintro μ₁; iintro μ₂; iintro Hμ; unfold retract, RC_sig; iapply H.
    apply fw_eq, Hμ; apply _.
  - rewrite bind_pure by reflexivity; apply sig_equiv.
    iintro W'; iintro ρ; unfold fwd_R; simpl.
    etransitivity; [iapply (ext_eq (R := μ₁)) | iapply Hμ].
    now intros x.
  - erewrite bind_bind_comp; [apply sig_equiv | split; intros; reflexivity].
    etransitivity; [apply fw_eq, Hμ |].
    iintro W'; iintro ρ; unfold fwd_R; apply (ext_eq (R := μ₂)).
    intros x; term_simpl; apply bind_bind_comp; intros y; term_simpl.
    apply bind_equiv; intros z; reflexivity.
Qed.

Notation Sig := (WI_dom sig_rel_sig).

(* Semantic effects are similar to semantic signatures; the difference
   lies in the fact that the instance semantics needs some freshness
   guarantees and a tag associated with the effect on the left *)
Definition effect_rel_sig X :=
  (F.expr0 →ₛ O.expr_ ∅ X →ₛ list F.ltag →ₛ Comp X →ₛ *ₛ)%irel_sig.
Notation Reff := (WIRel effect_rel_sig).

Class IsSemFresh l {X} (R : Reff X) :=
  semFresh : ∀ W (ρ : X [⇒] W) e₁ e₂ ls μ n, n ⊨ R W ρ e₁ e₂ ls μ → forall l', l ≤ l' → ¬ List.In l' ls.

Definition efford (X : Set) (R₁ R₂ : IRel (effect_rel_sig X)) :=
  ∀ᵢ e₁ e₂ l (μ₁ μ₂ : Comp X), (μ₁ ≈ᵂ μ₂) ⇒ R₁ e₁ e₂ l μ₁ ⇒ R₂ e₁ e₂ l μ₂.

Instance RO_eff : ROrd effect_rel_sig efford.

Instance ROP_eff : ROPreo effect_rel_sig.
Proof.
  split; intros.
  - intros R₁ R₂ R₃ HR₁ HR₂; iintro e₁; iintro e₂; iintro l; iintro μ₁; iintro μ₂; iintro Hμ; iintro HR.
    iapply HR₂; [reflexivity | iapply HR₁; eassumption].
  - unfold CSubrelation; intros R₁ R₂ HR.
    iintro e₁; iintro e₂; iintro l; iintro μ; iapply HR; reflexivity.
  - repeat iintro; contradiction.
Qed.

Instance RC_eff : RetractCore effect_rel_sig :=
  λ W₁ W₂ (ρ : W₁ [⇒] W₂) (R : IRel (effect_rel_sig W₂)) v₁ v₂ ls μ,
  R v₁ (bind (O.injLSub ρ) v₂) ls ⟨fwd_R μ ρ⟩ᵂ.

Lemma ord_eff_unroll X (R₁ R₂ : IRel (effect_rel_sig X)) n
      (HH : ∀ e₁ e₂ l (μ₁ μ₂ : Comp X) n (Hμ : n ⊨ μ₁ ≈ᵂ μ₂),
          n ⊨ R₁ e₁ e₂ l μ₁ ⇔ R₂ e₁ e₂ l μ₂) :
  n ⊨ R₁ ≈ᴿ R₂.
Proof.
  isplit; iintro v₁; iintro v₂; iintro l; iintro μ₁; iintro μ₂; iintro Hμ;
    [| apply csymmetry in Hμ]; eapply HH in Hμ;
      idestruct Hμ as H1 H2; eassumption.
Qed.

Lemma ord_eff_roll X (R₁ R₂ : IRel (effect_rel_sig X)) n v₁ v₂ ls (μ₁ μ₂ : Comp X)
      (HR : n ⊨ R₁ ≈ᴿ R₂)
      (Hμ : n ⊨ μ₁ ≈ᵂ μ₂) :
  n ⊨ R₁ v₁ v₂ ls μ₁ ⇔ R₂ v₁ v₂ ls μ₂.
Proof.
  idestruct HR as HR₁ HR₂; isplit; [iapply HR₁ | iapply HR₂; symmetry]; assumption.
Qed.

Lemma eff_equiv {X} {R : IRel (effect_rel_sig X)} {OE : OrdExt R} e₁ e₂ l (μ₁ μ₂ : Comp X) n
      (HH : n ⊨ μ₁ ≈ᵂ μ₂) :
  n ⊨ R e₁ e₂ l μ₁ ⇔ R e₁ e₂ l μ₂.
Proof.
  isplit; iapply self_ord; [| symmetry]; assumption.
Qed.

Instance Retract_eff : Retract effect_rel_sig.
Proof.
  split; intros; [| apply ord_eff_unroll; intros; unfold retract, RC_eff ..].
  - iintro e₁; iintro e₂; iintro l; iintro μ₁; iintro μ₂; iintro Hμ; iapply H.
    apply fw_eq, Hμ; apply _.
  - rewrite bind_pure by reflexivity; apply eff_equiv.
    iintro W'; iintro ρ; etransitivity; [iapply (ext_eq (R := μ₁)) | iapply Hμ]; now intros x.
  - erewrite bind_bind_comp; [apply eff_equiv | split; intros; reflexivity].
    iintro W'; iintro ρ; unfold fwd_R; etransitivity; [iapply Hμ | apply (ext_eq (R := μ₂))].
    intros x; term_simpl; apply bind_bind_comp; intros y; term_simpl.
    apply bind_equiv; intros z; reflexivity.
Qed.

Record Eff {A} : Type :=
  { fdom_rel :> WI_ext effect_rel_sig A
  ; fdom_ft  : F.ltag
  ; fdom_tfresh :> IsSemFresh fdom_ft fdom_rel
  ; fdom_fcl :> WIR_fwcl fdom_rel
  }.
Arguments Eff A : clear implicits.

Instance Eff_fcl A (E : Eff A) : WIR_fwcl (fdom_rel E) := fdom_fcl E.
Instance Eff_IsFresh {A} (R : Eff A) : IsSemFresh (fdom_ft R) R := fdom_tfresh R.

Notation "⟨ R , l ⟩ᴱ" := {| fdom_rel := ⟨R⟩ᴿ; fdom_ft := l; fdom_tfresh := _; fdom_fcl := _|}.

Instance eff_fresh_fwd {X} (R : Eff X) {W} (ρ : X [⇒] W) : IsSemFresh (fdom_ft R) (fwd_R R ρ).
Proof.
  unfold IsSemFresh; intros; eapply (fdom_tfresh R); [| assumption].
  unfold fwd_R in H; exact H.
Qed.

(** Semantic instances simply pair a semantic signature with a tag
    chosen for the instance in the instance semantics *)
Record ℑ {X} : Type :=
  { ℑ_rel  :> Sig X
  ; ℑ_ltag :  F.ltag
  }.
Arguments ℑ X : clear implicits.

(** * Equivalence of semantic instances *)

Definition instance_equiv {LV} (I₁ I₂ : ℑ LV) : IProp :=
  ℑ_rel I₁ ≈ᵂ ℑ_rel I₂ ∧ᵢ (ℑ_ltag I₁ = ℑ_ltag I₂)ᵢ.

Instance CRefl_instance_equiv LV n : CReflexive (I_valid_at n) (@instance_equiv LV).
Proof.
  intros I; isplit; [reflexivity | now trivial].
Qed.

Notation "⟨ μ , l ⟩ᴵ" := {| ℑ_rel := μ; ℑ_ltag := l |}.
Notation "ν₁ ≈ᴵ ν₂" := (instance_equiv ν₁ ν₂) (at level 70, no associativity).
