Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Product Binding.Env Binding.Set.
Require Import Lang.Fresh Lang.Open.
Require Import InterLangLR.Domains InterLangLR.ProgramObs.

Import TacticSynonyms.

(** Relation on stuck expressions defined as a functor: the
 interpretations of effects and of the "recursive" call to the
 interpretation of expressions are taken as parameters. *)
Definition Frel_stuck_cl {X Y}
    (RelF  : Eff X)
    (RelE  : Rexpr X) : Rstuck Y X :=
  λ W (ρ : X [⇒] W) R₁ R₂ e₁ e₂,
    ∃ᵢ ls μ,
      wi_rel RelF _ (arrow_subst_comp (O.mapOf R₂) ρ) e₁ e₂ ls μ ∧ᵢ
      (F.rfree_l ls R₁)ᵢ ∧ᵢ
      ∀ᵢ W' (ρ' : W [⇒] W') e₁' e₂', 
        wi_rel μ _ (O.liftByρ R₂ ρ') e₁' e₂' ⇒
          ▷ RelE _ (subst_comp ρ' ρ) (F.rplug R₁ e₁') (O.rplug (O.liftByR R₂ ρ') e₂').

(** Relation on evaluation contexts defined as a functor: the
 interpretations of effects, types, and of the "recursive" call to
 the interpretation of expressions are taken as parameters. *)
Definition Frel_ectx_cl {X}
    (RelF  : Eff X)
    (RelV  : Typ X)
    (RelE  : Rexpr X) : Rectx X :=
  λ W ρ E₁ E₂,
    (∀ᵢ v₁ v₂, wi_rel RelV W ρ v₁ v₂ ⇒ Obs (F.plug E₁ v₁) (O.xplug E₂ v₂)) ∧ᵢ
    (∀ᵢ Y R₁ (R₂ : O.rctx Y W) e₁ e₂,
      Frel_stuck_cl RelF RelE W ρ R₁ R₂ e₁ e₂ ⇒
      Obs (F.plug E₁ (F.rplug R₁ e₁)) (O.xplug E₂ (O.rplug R₂ e₂))).

(** Relation on expressions defined as a functor: the interpretations
of effects, types, and of the "recursive" call to the
interpretation of expressions are taken as parameters. *)
Definition Frel_expr_cl {X}
    (RelF  : Eff X)
    (RelV  : Typ X)
    (RelE  : Rexpr X) : Rexpr X :=
  λ W (ρ : X [⇒] W) e₁ e₂,
    ∀ᵢ W' (ρ' : W [⇒] W') E₁ E₂, Frel_ectx_cl RelF RelV RelE W' (subst_comp ρ' ρ) E₁ E₂ ⇒
      Obs (F.plug E₁ e₁) (O.xplug E₂ (bind (O.injLSub ρ') e₂)).


Lemma Frel_expr_cl_contractive {X}
    (RelF  : Eff X)
    (RelV  : Typ X) :
  contractive _ (Frel_expr_cl RelF RelV).
Proof.
  intros R₁ R₂ n; iintro HR.
  iintro W; iintro ρ; iintro e₁; iintro e₂; unfold Frel_expr_cl; auto_contr.
  unfold Frel_ectx_cl; auto_contr.
  unfold Frel_stuck_cl; auto_contr.
  iapply HR.
Qed.

(** With the closurer on expressions defined as a functor, we can tie
 the recursion via the fixed-point operator for COFEs. Note that we
 have to prove the functor is contractive (which it is, as we used
 "later" in the definition). *)
Definition rel_expr_cl_fix {X}
    (RelF  : Eff X)
    (RelV  : Typ X) : Rexpr X :=
  I_fix _
    (Frel_expr_cl RelF RelV)
    (Frel_expr_cl_contractive RelF RelV).

Notation rel_expr_cl RelF RelV :=
  (Frel_expr_cl RelF RelV (rel_expr_cl_fix RelF RelV)).
Notation rel_ectx_cl RelF RelV :=
  (Frel_ectx_cl RelF RelV (rel_expr_cl_fix RelF RelV)).
Notation rel_stuck_cl RelF RelV :=
  (Frel_stuck_cl RelF (rel_expr_cl_fix RelF RelV)).

Arguments Frel_expr_cl {X} RelF RelV RelE W ρ e₁%fsyn e₂%osyn : rename.
Arguments Frel_ectx_cl {X} RelF RelV RelE W ρ E₁%fsyn E₂%osyn : rename.
Arguments Frel_stuck_cl X Y RelF RelE W ρ R₁%fsyn R₂%osyn e₁%fsyn e₂%osyn : rename.

Definition rel_expr_cl1 {X} (RA : Rtype X) (RR : Rexpr X) :
  IRel (F.expr_ (inc ∅) ∅ →ₛ O.expr_ (inc ∅) X →ₛ *ₛ)%irel_sig :=
  λ e₁ e₂, ∀ᵢ W (ρ : X [⇒] W) v₁ v₂,
    RA W ρ v₁ v₂ ⇒ RR W ρ (subst e₁ v₁) (subst (bind (O.injLSub ρ) e₂) v₂).

Definition rel_handler_cl {X} (RS : Sig X) (RA : ∀ W, X [⇒] W → Typ W → Typ W) (RR : Comp X) : Rhandler X :=
  λ W ρ h₁ h₂,
  match h₁, h₂ with
  | F.h_do e₁, O.h_do e₂ =>
    ∀ᵢ W' (ρ' : W [⇒] W') (ρ'' : X [⇒] W') Y (δy : W' [⇒] Y) (ρy : Y [⇒] W') (μ : Typ Y) v₁ v₂ u₁ u₂,
      (subst_eq ρ'' (subst_comp ρ' ρ) ∧ subst_eq (subst_comp ρy δy) subst_pure)ᵢ ⇒
      wi_rel RS Y (subst_comp δy ρ'') v₁ v₂ μ ⇒
      ∃ᵢ (μ' : Typ W'), μ ≈ᵂ μ' ⇑ δy ∧ᵢ
        (wi_rel (RA W' ρ'' ⟨μ ⇑ ρy⟩ᵂ) W' subst_pure u₁ u₂ ⇒
           wi_rel RR W' ρ'' (subst (subst e₁ (shift u₁)) v₁)
                  (subst (subst (bind (O.injLSub ρ') e₂) (shift u₂)) (bind (O.injLSub ρy) v₂)))
  end.

Lemma liftByρ_eq X Y W (R : O.rctx Y X) (ρ₁ ρ₂ : X [⇒] W) (EQρ : subst_eq ρ₁ ρ₂) :
  subst_eq (O.liftByρ R ρ₁) (O.liftByρ R ρ₂).
Proof.
  revert W ρ₁ ρ₂ EQρ; induction R; intros; simpl; [| apply IHR ..]; [assumption .. |].
  now rewrite EQρ.
Qed.

Lemma liftBy_comp_rel X Y W₁ W₂ (μ : Comp Y) (R : O.rctx Y X) (ρ₁ : X [⇒] W₁) (ρ₂ : W₁ [⇒] W₂) e₁ e₂ n
      (Hμ : n ⊨ wi_rel μ (O.liftByT W₂ (O.liftByR R ρ₁)) (subst_comp (O.liftByρ (O.liftByR R ρ₁) ρ₂) (O.liftByρ R ρ₁)) e₁ e₂) :
  n ⊨ wi_rel μ (O.liftByT W₂ R) (O.liftByρ R (subst_comp ρ₂ ρ₁)) e₁ (eq_rect _ (λ X, O.expr_ ∅ X) e₂ _ (OM.liftByT_eq ρ₁ R)).
Proof.
  revert W₁ W₂ ρ₁ ρ₂ e₂ Hμ; induction R; intros; simpl in *; [assumption | ..].
  - apply IHR, Hμ.
  - iapply (ext_ord (R := μ)); [| apply IHR, Hμ].
    apply liftByρ_eq; intros [| x]; term_simpl; [reflexivity |].
    apply bind_map_comp; intros y; reflexivity.
Qed.

Section Properties.
  Context {X} {RF : Eff X} {Rv : Typ X}.

  Section Roll.

    Lemma rel_expr_cl_fix_roll W ρ e₁ e₂ {n}
          (HRe : n ⊨ rel_expr_cl RF Rv W ρ e₁ e₂) :
      n ⊨ rel_expr_cl_fix RF Rv W ρ e₁ e₂.
    Proof.
      apply (I_fix_roll (∀ₛ (W : Set) (_ : X [⇒] W), expr_rel_sig W)%irel_sig),
       HRe.
    Qed.

    Lemma rel_expr_cl_fix_unroll W ρ e₁ e₂ {n}
          (HRe : n ⊨ rel_expr_cl_fix RF Rv W ρ e₁ e₂) :
      n ⊨ rel_expr_cl RF Rv W ρ e₁ e₂.
    Proof.
      apply (I_fix_unroll (∀ₛ (W : Set) (_ : X [⇒] W), expr_rel_sig W)%irel_sig), HRe.
    Qed.

    Lemma rel_expr_cl_fix_eq {n} W ρ :
      n ⊨ (rel_expr_cl RF Rv W ρ : IRel (expr_rel_sig W)) ≈ᴿ rel_expr_cl_fix RF Rv W ρ.
    Proof.
      isplit; iintro e₁'; iintro e₂'; iintro HR;
        auto using rel_expr_cl_fix_roll, rel_expr_cl_fix_unroll.
    Qed.

    Lemma rel_s_roll {Y} W ρ s₁ (s₂ : O.expr_ ∅ Y) R₁ (R₂ : O.rctx Y W) ls μ {n}
          (HRF : n ⊨ wi_rel RF Y (arrow_subst_comp (O.mapOf R₂) ρ) s₁ s₂ ls μ)
          (HRfree : F.rfree_l ls R₁)
          (HRe : ∀ W' (ρ' : W [⇒] W') e₁' e₂',
              n ⊨ wi_rel μ _ (O.liftByρ R₂ ρ') e₁' e₂' ⇒
                ▷ rel_expr_cl RF Rv W' (subst_comp ρ' ρ) (F.rplug R₁ e₁')
                (O.rplug (O.liftByR R₂ ρ') e₂')) :
      n ⊨ rel_stuck_cl RF Rv W ρ R₁ R₂ s₁ s₂.
    Proof.
      iexists ls; iexists μ; isplit; [assumption | isplit; [assumption |]].
      iintro W'; iintro ρ'; iintro e₁'; iintro e₂'; specialize (HRe _ ρ' e₁' e₂'); iintro Hμ.
      ispecialize HRe Hμ; later_shift; now apply rel_expr_cl_fix_roll.
    Qed.

    Lemma rel_s_unroll {Y} W ρ s₁ s₂ R₁ (R₂ : O.rctx Y W) {n}
          (Hs : n ⊨ rel_stuck_cl RF Rv W ρ R₁ R₂ s₁ s₂) :
      ∃ ls μ, (n ⊨ wi_rel RF Y (arrow_subst_comp (O.mapOf R₂) ρ) s₁ s₂ ls μ) ∧
                   (F.rfree_l ls R₁) ∧
                   (n ⊨ ∀ᵢ W' (ρ' : W [⇒] W') e₁' e₂', wi_rel μ _ (O.liftByρ R₂ ρ') e₁' e₂' ⇒
                                      ▷ rel_expr_cl RF Rv W' (subst_comp ρ' ρ) (F.rplug R₁ e₁') (O.rplug (O.liftByR R₂ ρ') e₂')).
    Proof.
      idestruct Hs as ls Hs; idestruct Hs as μ Hs; exists ls, μ.
      idestruct Hs as HF Hs; split; [assumption |].
      idestruct Hs as Hfree Hs; split; [assumption |].
      iintro W'; iintro ρ'; iintro e₁'; iintro e₂'; iintro Hμ; iapply Hs in Hμ; later_shift.
      now apply rel_expr_cl_fix_unroll.
    Qed.

    Lemma rel_k_roll W ρ E₁ E₂ {n}
          (HRv : ∀ v₁ v₂, n ⊨ wi_rel Rv W ρ v₁ v₂ ⇒ Obs (F.plug E₁ v₁) (O.xplug E₂ v₂))
          (HRs : ∀ Y R₁ (R₂ : O.rctx Y W) e₁ e₂, n ⊨ rel_stuck_cl RF Rv W ρ R₁ R₂ e₁ e₂ ⇒
                                  Obs (F.plug E₁ (F.rplug R₁ e₁)) (O.xplug E₂ (O.rplug R₂ e₂))) :
      n ⊨ rel_ectx_cl RF Rv W ρ E₁ E₂.
    Proof.
      isplit.
      - iintro u₁; iintro u₂; apply HRv.
      - iintro Y; iintro R₁'; iintro R₂'; iintro e₁'; iintro e₂'; apply HRs.
    Qed.

  End Roll.

  Section Extensionality.

    Definition ext_sio {sig ord} {RO : ROrd sig ord} {X} (R : WIRel sig X) :=
      ∀ᵢ (W : Set) (ρ₁ ρ₂ : X [⇒] W), (subst_eq ρ₁ ρ₂)ᵢ ⇒ R W ρ₁ ≾ᴿ R W ρ₂.

    Lemma stuck_cl_ext_aux (Y : Set) n (HEcl : n ⊨ ▷ ext_sio (rel_expr_cl RF Rv)) :
      n ⊨ ext_sio (Frel_stuck_cl (Y := Y) RF (rel_expr_cl_fix RF Rv)).
    Proof.
      iintro W; iintro ρ₁; iintro ρ₂; iintro EQρ; iintro R₁; iintro R₂; iintro s₁; iintro s₂.
      iintro HS; apply rel_s_unroll in HS; destruct HS as [ls [μ [HF [HFree HS]]]].
      eapply rel_s_roll with (μ0 := μ); [ | eassumption .. | intros].
      + iapply (ext_ord (R := RF)); [| reflexivity | eassumption].
        now rewrite EQρ.
      + iintro Hμ; iapply HS in Hμ; later_shift.
        iapply HEcl; [| eassumption].
        now rewrite EQρ.
    Qed.

    Lemma ectx_cl_ext_aux n (HEcl : n ⊨ ▷ ext_sio (rel_expr_cl RF Rv)) :
      n ⊨ ext_sio (rel_ectx_cl RF Rv).
    Proof.
      iintro W; iintro ρ₁; iintro ρ₂; iintro EQρ; iintro E₁; iintro E₂; iintro HE.
      idestruct HE as HEv HEs; apply rel_k_roll; intros.
      - iintro Hv; iapply HEv; iapply (ext_ord (R := Rv)); [| eassumption].
        now rewrite EQρ.
      - iintro HS; iapply HEs; iapply stuck_cl_ext_aux; [| symmetry |]; eassumption.
    Qed.

    Global Instance expr_cl_ext : WIR_ext (rel_expr_cl RF Rv).
    Proof.
      unfold WIR_ext; intros W ρ₁ ρ₂ n Hρ.
      assert (HH : n ⊨ ext_sio (rel_expr_cl RF Rv)); [clear | now iapply HH].
      loeb_induction; iintro W; iintro ρ₁; iintro ρ₂; iintro Hρ; iintro e₁; iintro e₂; iintro He.
      iintro W'; iintro ρ'; iintro E₁; iintro E₂; iintro HE; iapply He; clear e₁ e₂ He.
      iapply ectx_cl_ext_aux; [assumption | now rewrite Hρ | eassumption].
    Qed.

    Global Instance ectx_cl_ext : WIR_ext (rel_ectx_cl RF Rv).
    Proof.
      unfold WIR_ext; intros W ρ₁ ρ₂ n Hρ; iapply ectx_cl_ext_aux; [| assumption]; clear.
      later_shift; iintro W; iintro ρ₁; iintro ρ₂; iintro Hρ; now apply ext_ord.
    Qed.

    Global Instance stuck_cl_ext Y : WIR_ext (Frel_stuck_cl (Y := Y) RF (rel_expr_cl_fix RF Rv)).
    Proof.
      intros W ρ₁ ρ₂ n Hρ; iapply stuck_cl_ext_aux; [clear | assumption].
      later_shift; iintro W; iintro ρ₁; iintro ρ₂; iintro Hρ; now apply ext_ord.
    Qed.

    Global Instance handler_cl_ext (S : Sig X) (I : ∀ W, X [⇒] W → Typ W → Typ W) (C : Comp X) :
      WIR_ext (rel_handler_cl S I C).
    Proof.
      intros W ρ₁ ρ₂ n Hρ; iintro h₁; iintro h₂; iintro HH; destruct h₁ as [e₁]; destruct h₂ as [e₂].
      iintro W'; iintro ρ; iintro ρ'; iintro Y; iintro δy; iintro ρy; iintro μ.
      iintro v₁; iintro v₂; iintro u₁; iintro u₂; iintro EQρ; iintro Hv.
      iapply HH in Hv;
        [destruct EQρ as [EQρ₁ EQρ₂]; split; [intros x; now rewrite EQρ₁, Hρ | eassumption] |].
      idestruct Hv as μ' Hv; idestruct Hv as Hμ Hv; iexists μ'; isplit; [assumption | iintro Hu].
      iapply Hv; assumption.
    Qed.

  End Extensionality.

  Global Instance expr_cl_fwcl : WIR_fwcl (rel_expr_cl RF Rv).
  Proof.
    unfold WIR_fwcl; intros; iintro e₁; iintro e₂; iintro HR.
    iintro W₃; iintro ρ'; iintro E₁; iintro E₂; iintro HE.
    rewrite bind_bind_comp'; iapply HR.
    iapply (ext_ord (R := rel_ectx_cl RF Rv)); [| eassumption].
    clear; intros x; term_simpl; apply bind_bind_comp.
    clear; intros x; reflexivity.
  Qed.

  Global Instance handler_cl_fwcl (S : Sig X) (I : ∀ W, X [⇒] W → Typ W → Typ W) (C : Comp X) :
    WIR_fwcl (rel_handler_cl S I C).
  Proof.
    unfold WIR_fwcl; intros; iintro h₁; iintro h₂; iintro HR; destruct h₁ as [e₁]; destruct h₂ as [e₂].
    iintro W₃; iintro ρ₃; iintro ρ; iintro Y; iintro δy; iintro ρy; iintro μ.
    iintro v₁; iintro v₂; iintro u₁; iintro u₂; iintro EQρ; iintro Hv; destruct EQρ as [EQρ₁ EQρ₂].
    iapply HR in Hv; [split; [intros x; now rewrite EQρ₁; apply bind_bind_comp' | eassumption] |].
    idestruct Hv as μ' Hv; idestruct Hv as Hμ Hv; iexists μ'; isplit; [assumption | iintro Hu].
    term_simpl; erewrite bind_bind_comp; [now iapply Hv |].
    split; [intros [| [| x]] | intros x]; term_simpl; reflexivity.
  Qed.

  Section Closed.

    Section Obs.
      Context {Y W} {ρ : X [⇒] W} {v₁ : F.value0} {v₂ : O.value_ ∅ W}
              (E₁ : F.ectx0) (E₂ : O.ectx W) (e₁ : F.expr0) (e₂ : O.expr_ ∅ W)
              (s₁ : F.expr0) (s₂ : O.expr_ ∅ Y) (R₁ : F.rctx0) (R₂ : O.rctx Y W).

      Lemma rel_k_value {n}
            (HRE : n ⊨ rel_ectx_cl RF Rv W ρ E₁ E₂)
            (HRv : n ⊨ wi_rel Rv W ρ v₁ v₂) :
        n ⊨ Obs (F.plug E₁ v₁) (O.xplug E₂ v₂).
      Proof.
        idestruct HRE as HFv HFs; now iapply HFv.
      Qed.

      Lemma rel_k_stuck {n}
            (HRE : n ⊨ rel_ectx_cl RF Rv W ρ E₁ E₂)
            (HRs : n ⊨ rel_stuck_cl RF Rv W ρ R₁ R₂ s₁ s₂) :
        n ⊨ Obs (F.plug E₁ (F.rplug R₁ s₁)) (O.xplug E₂ (O.rplug R₂ s₂)).
      Proof.
        idestruct HRE as HFv HFs; now iapply HFs.
      Qed.

      Lemma rel_k_expr {n}
            (HRE : n ⊨ rel_ectx_cl RF Rv W ρ E₁ E₂)
            (HRe : n ⊨ rel_expr_cl RF Rv W ρ e₁ e₂) :
        n ⊨ Obs (F.plug E₁ e₁) (O.xplug E₂ e₂).
      Proof.
        ispecialize HRe W; ispecialize HRe (subst_pure (A := W)).
        rewrite bind_pure in HRe by reflexivity; iapply HRe; clear HRe.
        iapply (ext_ord (R := rel_ectx_cl _ _)); [| eassumption].
        intros x; symmetry; apply bind_pure'.
      Qed.

    End Obs.

    Context {Y} {W} {ρ : X [⇒] W} {v₁ : F.value0} {v₂ : O.value_ ∅ W}
            (e₁ : F.expr0) (e₂ : O.expr_ ∅ W)
            (s₁ : F.expr0) (s₂ : O.expr_ ∅ Y) (R₁ : F.rctx0) (R₂ : O.rctx Y W).

    Lemma rel_v_in_e {n}
          (HRE : n ⊨ wi_rel Rv W ρ v₁ v₂) :
      n ⊨ rel_expr_cl RF Rv W ρ v₁ v₂.
    Proof.
      iintro W'; iintro ρ'; iintro E₁; iintro E₂; iintro HF; term_simpl.
      eapply rel_k_value; [eassumption |].
      now iapply (fw_ord (R := Rv)).
    Qed.

    Lemma rel_s_in_e {n}
          (HRs : n ⊨ rel_stuck_cl RF Rv W ρ R₁ R₂ s₁ s₂) :
      n ⊨ rel_expr_cl RF Rv W ρ (F.rplug R₁ s₁) (O.rplug R₂ s₂).
    Proof.
      iintro W'; iintro ρ'; iintro E₁; iintro E₂; iintro HRE.
      rewrite OM.liftBy_plug; eapply rel_k_stuck; [eassumption |].
      clear E₁ E₂ HRE; apply rel_s_unroll in HRs; destruct HRs as [ls [μ [HF [HFree HS]]]].
      eapply rel_s_roll with (μ0 := ⟨fwd_R μ _ ⟩ᵂ); [ | eassumption .. | intros].
      - iapply (ext_ord (R := RF)); [| reflexivity | iapply (fw_ord (R := RF)); [reflexivity | apply HF]].
        clear; intros x; term_simpl.
        apply bind_map_comp; symmetry; apply OM.liftBy_mapOf_comm.
      - unfold fwd_R; simpl. iintro Hμ; unfold fwd_R in Hμ; simpl in Hμ.
        assert (HI := ext_ord (R := rel_expr_cl RF Rv)).
        specialize (HI _ (subst_comp (subst_comp ρ'0 ρ') ρ) (subst_comp ρ'0 (subst_comp ρ' ρ)) n).
        specialize (HI (subst_comp_assoc _ _ _ _ _ _ _)); iespecialize HI.
        apply I_later_intro, I_later_arrow_down in HI; iapply HI; clear HI.
        rewrite OM.liftByRcomp_plug; iapply HS; clear -Hμ.
        apply liftBy_comp_rel, Hμ.
    Qed.

    Section Contraction.
      Context {e₁' : F.expr0} {e₂' : O.expr_ ∅ W}.

      Lemma rel_e_contr_both {n}
            (HC₁ : FR.contr e₁' e₁)
            (HC₂ : OR.contr e₂' e₂)
            (HRe : n ⊨ ▷ rel_expr_cl RF Rv W ρ e₁ e₂) :
        n ⊨ rel_expr_cl RF Rv W ρ e₁' e₂'.
      Proof.
        iintro W'; iintro ρ'; iintro F₁; iintro F₂; iintro HF.
        eapply Obs_red_both; [apply FR.red_contr, HC₁ | apply OR.red_contr, OR.contr_bind, HC₂ |].
        later_shift; eapply rel_k_expr; [eassumption |].
        now iapply (fw_ord (R := rel_expr_cl RF Rv) W W' ρ ρ').
      Qed.

      Lemma rel_e_contr_l {n}
            (HC : FR.contr e₁' e₁)
            (HRe : n ⊨ rel_expr_cl RF Rv W ρ e₁ e₂) :
        n ⊨ rel_expr_cl RF Rv W ρ e₁' e₂.
      Proof.
        iintro W'; iintro ρ'; iintro F₁; iintro F₂; iintro HF.
        eapply Obs_red_l; [apply FR.red_contr, HC |].
        eapply rel_k_expr; [eassumption |].
        now iapply (fw_ord (R := rel_expr_cl RF Rv) W W' ρ ρ').
      Qed.

      Lemma rel_e_contr_r {n}
            (HC : OR.contr e₂' e₂)
            (HRe : n ⊨ rel_expr_cl RF Rv W ρ e₁ e₂) :
        n ⊨ rel_expr_cl RF Rv W ρ e₁ e₂'.
      Proof.
        iintro W'; iintro ρ'; iintro F₁; iintro F₂; iintro HF.
        eapply Obs_red_r; [apply OR.red_contr, OR.contr_bind, HC |].
        eapply rel_k_expr; [eassumption |].
        now iapply (fw_ord (R := rel_expr_cl RF Rv) W W' ρ ρ').
      Qed.

    End Contraction.

  End Closed.

End Properties.

Section EquivProperties.

  Lemma subrel_ecl_meet {X Y} (F₁ : Eff X) (F₂ : Eff Y) (T₁ : Typ X) (T₂ : Typ Y) n {W}
        (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W)
        (SubF : n ⊨ F₁ ⇑ ρ₁ ≾ᵂ F₂ ⇑ ρ₂)
        (SubT : n ⊨ T₁ ⇑ ρ₁ ≾ᵂ T₂ ⇑ ρ₂) :
    n ⊨ (rel_expr_cl F₁ T₁) ⇑ ρ₁ ≾ᵂ (rel_expr_cl F₂ T₂) ⇑ ρ₂.
  Proof.
    loeb_induction; iintro W'; iintro ρ'; iintro e₁; iintro e₂; iintro He; iintro W''; iintro ρ''.
    iintro E₁; iintro E₂; iintro HE; iapply He; clear e₁ e₂ He; apply rel_k_roll; intros.
    - iintro Hv; eapply rel_k_value; [eassumption |].
      iapply (ext_ord (R := T₂)); [| iapply SubT; iapply (ext_ord (R := T₁)); [| apply Hv]];
      [| symmetry]; iapply subst_comp_assoc.
    - iintro HS; eapply rel_k_stuck; [eassumption |].
      apply rel_s_unroll in HS; destruct HS as [ls [μ [HF [HFree HS]]]].
      eapply rel_s_roll with (μ0 := μ); [ | eassumption .. | intros].
      + iapply (ext_ord (R := F₂)); [| reflexivity | iapply SubF; [reflexivity |] ].
        * intros y; term_simpl; rewrite map_to_bind, !bind_bind_comp'; reflexivity.
        * iapply (ext_ord (R := F₁)); [| reflexivity | apply HF].
          intros y; term_simpl; rewrite map_to_bind, !bind_bind_comp'; apply bind_equiv; reflexivity.
      + iintro Hμ; iapply HS in Hμ; clear HS; later_shift.
        iapply (ext_ord (R := rel_expr_cl F₂ T₂)); [| iapply IH].
        * intros y; term_simpl; rewrite !bind_bind_comp'; reflexivity.
        * iapply (ext_ord (R := rel_expr_cl F₁ T₁)); [| iapply Hμ].
          intros y; term_simpl; rewrite !bind_bind_comp'; apply bind_equiv; reflexivity.
  Qed.

  Lemma fwd_pure_id {sig ord X} {RO : ROrd sig ord} {ROP : ROPreo sig} {RC : RetractCore sig}
        (R : WIRel sig X) {RE : WIR_ext R} n :
    n ⊨ ⟨R⟩ᴿ ≈ᵂ R ⇑ subst_pure.
  Proof.
    iintro W; iintro ρ; isplit.
    - unfold fwd_R; simpl; apply ext_ord; iapply subst_comp_pure_r.
    - unfold fwd_R; simpl; apply ext_ord; iapply subst_comp_pure_r.
  Qed.

  Lemma subrel_expr_cl {X} {RF₁ RF₂ : Eff X} {Rv₁ Rv₂ : Typ X} {n}
        (SubRF : n ⊨ RF₁ ≾ᵂ RF₂)
        (SubRv : n ⊨ Rv₁ ≾ᵂ Rv₂) :
    n ⊨ ⟨rel_expr_cl RF₁ Rv₁⟩ᴿ ≾ᵂ ⟨rel_expr_cl RF₂ Rv₂⟩ᴿ.
  Proof.
    etransitivity; [apply subrel, fwd_pure_id |].
    etransitivity; [| apply subrel; symmetry; apply fwd_pure_id].
    apply subrel_ecl_meet; (etransitivity; [apply subrel; symmetry; apply fwd_pure_id | etransitivity; [| apply subrel, fwd_pure_id]]).
    - apply SubRF.
    - apply SubRv.
  Qed.

  Lemma equiv_expr_cl {X} {RF₁ RF₂ : Eff X} {Rv₁ Rv₂ : Typ X} {n}
        (EQRF : n ⊨ RF₁ ≈ᵂ RF₂)
        (EQRv : n ⊨ Rv₁ ≈ᵂ Rv₂) :
    n ⊨ ⟨rel_expr_cl RF₁ Rv₁⟩ᴿ ≈ᵂ ⟨rel_expr_cl RF₂ Rv₂⟩ᴿ.
  Proof.
    iintro W; iintro ρ; apply req_symcl.
    - iapply (@subrel_expr_cl X RF₁ RF₂ Rv₁ Rv₂); now apply subrel.
    - iapply (@subrel_expr_cl X RF₂ RF₁ Rv₂ Rv₁); apply subrel; now symmetry.
  Qed.

  Lemma equiv_ecl {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W)
        (F₁ : Eff X) (F₂ : Eff Y) (T₁ : Typ X) (T₂ : Typ Y) n
        (EQF : n ⊨ F₁ ⇑ ρ₁ ≈ᵂ F₂ ⇑ ρ₂)
        (EQT : n ⊨ T₁ ⇑ ρ₁ ≈ᵂ T₂ ⇑ ρ₂) :
    n ⊨ rel_expr_cl F₁ T₁ ⇑ ρ₁ ≈ᵂ rel_expr_cl F₂ T₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ; apply req_symcl.
    - irevert W' ρ; apply subrel_ecl_meet; now apply subrel.
    - irevert W' ρ; apply subrel_ecl_meet; apply subrel; now symmetry.
  Qed.

End EquivProperties.
