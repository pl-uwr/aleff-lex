Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Env Binding.Set Binding.Product.
Require Import Types Kinding.
Require Import Lang.Fresh Lang.Open.
Require Import InterLangLR.Core InterLangLR.KindEquiv InterLangLR.KindingIrrelevance.

Definition lift_Δ_equiv {TV₁ TV₂ LV₁ LV₂ : Set}
    (Φ : {| sfst := TV₁; ssnd := LV₁ |} [→] {| sfst := TV₂; ssnd := LV₂ |})
    {Δ₁ : TV₁ → kind} {Δ₂ : TV₂ → kind}
    (κ : kind)
    (Δequiv : ∀ α, Δ₁ α = Δ₂ (arr₁ Φ α))
    (α : inc TV₁) :=
  match α return (Δ₁ ▹ κ) α = (Δ₂ ▹ κ) (arr₁ (liftA Φ : incV _ [→] incV _) α)
  with
  | VZ   => eq_refl
  | VS β => Δequiv β
  end.

Fixpoint type_interp_weaken (κ : kind) {A X B Y W : Set}
         (Φ : ⟨A, X⟩ [→] ⟨B, Y⟩) (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W)
         {Δ₁ : A → kind} {Δ₂ : B → kind}
         (τ  : typ_ A X)
         (η₁ : ∀ α : A, K⟦ Δ₁ α @ W⟧) (η₂ : ∀ α : B, K⟦ Δ₂ α @ W⟧)
         (ϑ₁ : X → ℑ W) (ϑ₂ : Y → ℑ W)
         {WF₁ : K[ Δ₁ ; X ⊢ τ ∷ κ ]}
         {WF₂ : K[ Δ₂ ; Y ⊢ fmap Φ τ ∷ κ ]}
         (Δequiv : ∀ α, Δ₁ α = Δ₂ (arr₁ Φ α)) n {struct τ} :
  subst_eq ρ₁ (subst_comp ρ₂ (of_arrow (arr₂ Φ))) →
  n ⊨ (∀ᵢ α, K⟦ (Δequiv α) :⊨ η₁ α ≡ η₂ (arr₁ Φ α) ⟧) →
  n ⊨ (∀ᵢ a, ϑ₁ a ≈ᴵ ϑ₂ (arr₂ Φ a)) →
  n ⊨ K⟦ ⟦ τ ∷ κ ⟧ ρ₁ ! η₁ ! ϑ₁ ≡ ⟦ fmap Φ τ ∷ κ ⟧ ρ₂ ! η₂ ! ϑ₂ ∷ κ⟧.
Proof.
  intros EQρ Hη Hϑ; destruct τ as [| α | | | | l | | | |]; invert_kind WF₁; simpl type_interp;
    [reflexivity | | unfold kind_equiv ..].
  - unfold apply_to_t_var; apply rel_equiv_var with (Heq := Δequiv α); iapply Hη.
  - apply @equiv_arr.
    + symmetry; eapply (type_interp_weaken k_type); eassumption.
    + eapply (type_interp_weaken k_type); eassumption.
    + eapply (type_interp_weaken k_effect); eassumption.
  - apply equiv_tall; iintro W'; iintro ρ'; iintro R.
    apply (type_interp_weaken k_type) with (Δequiv := lift_Δ_equiv Φ _ Δequiv).
    + intros x; term_simpl; f_equal; apply EQρ.
    + iintro α; destruct α as [| α]; [reflexivity | simpl; unfold fwdTEnv].
      apply fwd_kint_equiv_eq; [reflexivity | iapply Hη].
    + iintro a; ispecialize Hϑ a; idestruct Hϑ as HR HT; unfold fwdIEnv; isplit; [| assumption].
      simpl; now apply fw_eq.
  - apply equiv_lall; iintro W'; iintro ρ'; iintro l; iintro a; eapply (type_interp_weaken k_type).
    + intros [| x]; term_simpl; [reflexivity | now rewrite EQρ].
    + iintro α; apply fwd_kint_equiv_eq; [reflexivity | iapply Hη].
    + iintro x; destruct x as [| x]; unfold fwdIEnv.
      * isplit; simpl; [eapply fw_eq, (type_interp_weaken k_sig); eassumption | reflexivity].
      * ispecialize Hϑ x; idestruct Hϑ as HR HT; isplit; simpl; [apply fw_eq |]; assumption.
  - rewrite EQρ; apply equiv_instance; iapply Hϑ.
  - reflexivity.
  - apply equiv_cons; eapply (type_interp_weaken k_effect); eassumption.
  - apply equiv_sig; eapply (type_interp_weaken k_type); eassumption.
  - reflexivity.
Qed.

Lemma type_interp_shiftL {A X W : Set} {Δ : A → kind}
      κ
      (τ  : typ_ A X)
      (ρ  : X [⇒] W)
      (η  : ∀ α : A, K⟦ Δ α @ W⟧)
      (ϑ  : X → ℑ W) (I : ℑ W)
      {WF : K[ Δ ; X ⊢ τ ∷ κ]} n :
  n ⊨ K⟦ shift_kint (⟦ τ ∷ κ ⟧ ρ ! η ! ϑ) ≡ (⟦ shift τ ∷ κ ⟧ (liftS ρ) ! (shiftTEnv η) ! (shiftIEnv (ϑ ▹ I))) ∷ κ⟧.
Proof.
  etransitivity; [apply tint_fwd_equiv | etransitivity; [symmetry; apply fwd_kint_pure |]].
  apply type_interp_weaken with (Δequiv := λ α, eq_refl); [| iintro; reflexivity ..].
  intros x; term_simpl; rewrite map_to_bind; apply bind_equiv; reflexivity.
Qed.

Lemma type_interp_shiftT {A X W : Set} {Δ : A → kind}
      κ
      (τ  : typ_ A X)
      (ρ  : X [⇒] W)
      (η  : ∀ α : A, K⟦ Δ α @ W⟧)
      (ϑ  : X → ℑ W) κ' (R : K⟦ κ' @ W⟧)
      {WF : K[ Δ ; X ⊢ τ ∷ κ]} n :
  n ⊨ K⟦ (⟦ τ ∷ κ ⟧ ρ ! η ! ϑ) ≡ (⟦ shift τ ∷ κ ⟧ ρ ! (η :▹ R) ! ϑ) ∷ κ ⟧.
Proof.
  apply type_interp_weaken with (Δequiv := λ α, eq_refl); [intros x | iintro ..]; reflexivity.
Qed.


(* ========================================================================= *)
(* Interpretation of effects *)

Lemma rel_eff_rel_weakenL {A X W : Set} {Δ : A → kind}
    (ε  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) (I : ℑ W)
    {WF : K[ Δ ; X ⊢ ε ∷ k_effect]}
    n :
  n ⊨ shift_kint (F⟦ ε ⟧ ρ ! η ! ϑ) ≈ᵂ
    F⟦ shift ε ⟧ (liftS ρ) ! (shiftTEnv η) ! (shiftIEnv (ϑ ▹ I)).
Proof.
  iapply (type_interp_shiftL k_effect ε ρ η).
Qed.

Lemma rel_eff_rel_weakenT {A X W : Set} {Δ : A → kind}
    (ε  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W ⟧)
    (ϑ  : X → ℑ W) κ (R : K⟦ κ @ W ⟧)
    {WF : K[ Δ ; X ⊢ ε ∷ k_effect]}
    n :
  n ⊨ F⟦ ε ⟧ ρ ! η ! ϑ ≈ᵂ F⟦ shift ε ⟧ ρ ! η :▹ R ! ϑ.
Proof.
  iapply (type_interp_shiftT k_effect ε ρ η).
Qed.

Lemma rel_eff_rel_weakenLl {A X W : Set} {Δ : A → kind}
    (ε  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) (I : ℑ W)
    {WF : K[ Δ ; X ⊢ ε ∷ k_effect]}
    W' ρ' n :
  n ⊨ wi_rel (F⟦ shift ε ⟧ (liftS ρ) ! (shiftTEnv η) ! (shiftIEnv (ϑ ▹ I))) W' ρ'
    ≾ᴿ wi_rel (F⟦ ε ⟧ ρ ! η ! ϑ) W' (subst_comp ρ' (of_arrow mk_shift)).
Proof.
  apply subrel; symmetry; iapply @rel_eff_rel_weakenL.
Qed.

Lemma rel_eff_rel_weakenLr {A X W : Set} {Δ : A → kind}
    (ε  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) (I : ℑ W)
    {WF : K[ Δ ; X ⊢ ε ∷ k_effect]}
    W' ρ' n :
  n ⊨ wi_rel (F⟦ ε ⟧ ρ ! η ! ϑ) W' (subst_comp ρ' (of_arrow mk_shift))
    ≾ᴿ wi_rel (F⟦ shift ε ⟧ (liftS ρ) ! (shiftTEnv η) ! (shiftIEnv (ϑ ▹ I))) W' ρ'.
Proof.
  apply subrel; iapply @rel_eff_rel_weakenL.
Qed.

(* ========================================================================= *)
(* Interpretation of types *)

Lemma rel_v_weakenL {A X W : Set} {Δ : A → kind}
    (τ  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) (I : ℑ W)
    {WF : K[ Δ ; X ⊢ τ ∷ k_type]}
    n :
  n ⊨ shift_kint (⟦ τ ⟧ ρ ! η ! ϑ) ≈ᵂ ⟦ shift τ ⟧ (liftS ρ) ! (shiftTEnv η) ! (shiftIEnv (ϑ ▹ I)).
Proof.
  iapply (type_interp_shiftL k_type τ ρ η).
Qed.

Lemma rel_v_weakenLl {A X W : Set} {Δ : A → kind}
    (τ  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) (I : ℑ W)
    {WF : K[ Δ ; X ⊢ τ ∷ k_type]}
    W' ρ' n :
  n ⊨ (wi_rel (⟦ shift τ ⟧ (liftS ρ) ! (shiftTEnv η) ! (shiftIEnv (ϑ ▹ I))) W' ρ'
        : IRel (type_rel_sig W'))
    ≾ᴿ wi_rel (⟦ τ ⟧ ρ ! η ! ϑ) W' (subst_comp ρ' (of_arrow mk_shift)).
Proof.
  apply subrel; symmetry; iapply @rel_v_weakenL.
Qed.

Lemma rel_v_weakenLr {A X W : Set} {Δ : A → kind}
    (τ  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) (I : ℑ W)
    {WF : K[ Δ ; X ⊢ τ ∷ k_type]}
    W' ρ' n :
  n ⊨ (wi_rel (⟦ τ ⟧ ρ ! η ! ϑ) W' (subst_comp ρ' (of_arrow mk_shift))
        : IRel (type_rel_sig W'))
    ≾ᴿ wi_rel (⟦ shift τ ⟧ (liftS ρ) ! (shiftTEnv η) ! (shiftIEnv (ϑ ▹ I))) W' ρ'.
Proof.
  apply subrel; iapply @rel_v_weakenL.
Qed.

Lemma rel_v_weakenT {A X W : Set} {Δ : A → kind}
    (τ  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) κ (R : K⟦ κ @ W⟧)
    {WF : K[ Δ ; X ⊢ τ ∷ k_type]}
    n :
  n ⊨ ⟦ τ ⟧ ρ ! η ! ϑ ≈ᵂ ⟦ shift τ ⟧ ρ ! η :▹ R ! ϑ.
Proof.
  iapply (type_interp_shiftT k_type τ ρ η).
Qed.

Lemma rel_v_weakenTl {A X W : Set} {Δ : A → kind}
    (τ  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) κ (R : K⟦ κ @ W⟧)
    {WF : K[ Δ ; X ⊢ τ ∷ k_type]}
    W' ρ' n :
  n ⊨ (wi_rel (⟦ shift τ ⟧ ρ ! η :▹ R ! ϑ) W' ρ' : IRel (type_rel_sig W'))
    ≾ᴿ wi_rel (⟦ τ ⟧ ρ ! η ! ϑ) W' ρ'.
Proof.
  apply subrel; symmetry; iapply @rel_v_weakenT.
Qed.

Lemma rel_v_weakenTr {A X W : Set} {Δ : A → kind}
    (τ  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) κ (R : K⟦ κ @ W⟧)
    {WF : K[ Δ ; X ⊢ τ ∷ k_type]}
    W' ρ' n :
  n ⊨ (wi_rel (⟦ τ ⟧ ρ ! η ! ϑ) W' ρ' : IRel (type_rel_sig W'))
    ≾ᴿ wi_rel (⟦ shift τ ⟧ ρ ! η :▹ R ! ϑ) W' ρ'.
Proof.
  apply subrel; iapply @rel_v_weakenT.
Qed.

(* ========================================================================= *)
(* Interpretation of signatures *)

Lemma rel_s_weakenL {A W X : Set} {Δ : A → kind}
    (σ  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) (I : ℑ W)
    {WF : K[ Δ ; X ⊢ σ ∷ k_sig]}
    n :
  n ⊨ shift_kint (Σ⟦ σ ⟧ ρ ! η ! ϑ) ≈ᵂ Σ⟦ shift σ ⟧ (liftS ρ) ! (shiftTEnv η) ! (shiftIEnv (ϑ ▹ I)).
Proof.
  iapply (type_interp_shiftL k_sig σ ρ η).
Qed.

Lemma rel_s_weakenT {A W X : Set} {Δ : A → kind}
    (σ  : typ_ A X)
    (ρ  : X [⇒] W)
    (η  : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ  : X → ℑ W) κ (R : K⟦ κ @ W⟧)
    {WF : K[ Δ ; X ⊢ σ ∷ k_sig]}
    n :
  n ⊨ Σ⟦ σ ⟧ ρ ! η ! ϑ ≈ᵂ Σ⟦ shift σ ⟧ ρ ! η :▹ R ! ϑ.
Proof.
  iapply (type_interp_shiftT k_sig σ ρ η).
Qed.
