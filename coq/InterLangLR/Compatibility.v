(** This module contains proofs that the logical relation is
compatible with the rules of the type system. *)
(** Compatibility is shown by proving for each rule of the type system
a lemma of the same shape, but with a logical approximation instead of
typing judgment. The development at this point is mostly standard for
the logical relations technique, safe for the proof by Löb induction
of the handler case.
*)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Product Binding.Env Binding.Set.
Require Import Types Kinding.
Require Import Lang.Fresh Lang.Open.
Require Import InterLangLR.ProgramObs InterLangLR.Core.
Require Import InterLangLR.KindEquiv InterLangLR.KindingIrrelevance.
Require Import InterLangLR.Weakening InterLangLR.Substitution.

Lemma rel_v_in_rel_e_open {TV LV V : Set} τ ε
      {Δ : TV → kind} {Θ : LV → typ_ TV LV} (Γ : V → typ_ TV LV)
      {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
      {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
      {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
      {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
      v₁ v₂ {n}
      (Hv : n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ τ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ τ / ε⟧.
Proof.
  iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; term_simpl.
  apply rel_v_in_e; iapply Hv; assumption.
Qed.
(* ========================================================================= *)
(* variable *)

Lemma var_compat_v {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} (Γ : V → typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  (x : V) {n} :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ (x : F.evar ⟨V, LV⟩) ≾ (x : O.ev ⟨V, LV⟩) ∷ Γ x ⟧.
Proof.
  iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; term_simpl; iapply Hγ.
Qed.

(* ========================================================================= *)
(* unit *)

Lemma unit_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} (Γ : V → typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]} {n} :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ ()ᵉ ≾ ()ᵉ  ∷ ()ᵗ ⟧%typ.
Proof.
  iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; term_simpl; now isimplP.
Qed.

(* ========================================================================= *)
(* let *)

Lemma subst_extend_G {TV LV V W : Set}
  (Δ : TV → kind) (Γ : V → typ_ TV LV) τ
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  (ρ : LV [⇒] W)
  η ϑ W' ρ' γ₁ (γ₁' : F.sub ⟨inc V, LV⟩ ⟨∅, ∅⟩) γ₂ γ₂' u₁ (u₂ : O.value_ ∅ W') n
  (EQγ₁ : subst_eq γ₁' (subst_comp (mk_subst u₁) (liftS γ₁)))
  (EQγ₂ : subst_eq γ₂' (subst_comp (mk_subst u₂) (liftS γ₂)))
  (HΓ : n ⊨ G⟦ Γ ⟧ ρ ! η ! ϑ W' ρ' γ₁ γ₂)
  (Hτ : n ⊨ wi_rel (⟦ τ ⟧ ρ ! η ! ϑ) W' ρ' u₁ u₂) :
     n ⊨ G⟦ Γ ▹ τ ⟧ ρ ! η ! ϑ W' ρ' γ₁' γ₂'.
Proof.
  iintro x; destruct x as [| x]; term_simpl.
  - destruct EQγ₁ as [EQγ₁ _]; destruct EQγ₂ as [EQγ₂ _]; rewrite EQγ₁, EQγ₂; term_simpl.
    eapply rel_typ_k_irr, Hτ.
  - destruct EQγ₁ as [EQγ₁ _]; destruct EQγ₂ as [EQγ₂ _]; rewrite EQγ₁, EQγ₂; term_simpl.
    eapply rel_typ_k_irr; iapply HΓ.
Qed.

(*
Lemma subst_shift_G {A X V W : Set}
  (Δ : A → kind) (Γ : V → typ_ A X) (ρ : X [⇒] W) (I : ℑ W)
  {WFΓ : K[ Δ; X ⊢ᴱ Γ]}
  η ϑ W' ρ' γ₁ γ₂ l  n
  (HI : l = (ℑ_ltag I))
  (HΓ : n ⊨ G⟦ Γ ⟧ ρ ! η ! (ϑ) W' ρ' γ₁ γ₂) :
  n ⊨ G⟦ shift Γ ⟧ ρ ! η ! (ϑ ▹ I) W' ρ' (subst_comp (mk_subst (F.ltag l)) (liftS γ₁)) (subst_comp (mk_subst (O.lclosed a)) (liftS γ₂)).
Proof.
  idestruct HΓ as HΘ HΓ; isplit; [intros [| a] | iintro x].
  - term_simpl; assumption.
  - term_simpl; change (subst (shift (F.sub_lbl γ₁ a)) l₁ = F.l_tag (ℑ_ltag (ϑ a))); term_simpl; apply HΘ.
  - term_simpl; eapply rel_typ_k_irr, rel_v_weakenL_r.
    iapply HΓ.
Qed.*)

Ltac foldXplug :=
  repeat
    match goal with
    | |- context [F.plug ?E (?b >>= ?e)%fsyn] =>
      change (F.plug E (b >>= e)%fsyn) with (F.plug (F.x_let E e) b)
    | |- context [F.plug ?E (F.e_rhandle ?l ?e ?h ?r)] =>
      change (F.plug E (F.e_rhandle l e h r)) with (F.plug (F.x_handle l E h r) e)
    | |- context [O.xplug ?E (?b >>= ?e)%osyn] =>
      change (O.xplug E (b >>= e)%osyn) with (O.xplug (O.x_let E e) b)
    | |- context [O.xplug ?E (O.e_handle ?e ?h ?r)] =>
      change (O.xplug E (O.e_handle e h r)) with (O.xplug (O.x_handle E h r) e)
    end.

Ltac foldRplug :=
    lazymatch goal with
    | |- context [(F.rplug ?E ?b >>= ?e)%fsyn] =>
      change (F.rplug E b >>= e)%fsyn with (F.rplug (F.r_let E e) b); foldRplug
    | |- context [F.e_rhandle ?l (F.rplug ?E ?e) ?h ?r] =>
      change (F.e_rhandle l (F.rplug E e) h r) with (F.rplug (F.r_handle l E h r) e); foldRplug
    | |- context [(O.rplug ?E ?b >>= ?e)%osyn] =>
      replace (O.rplug E b >>= e)%osyn with (O.rplug (O.r_let E e) b)
        by (simpl; f_equal; apply map_id; split; [intros [| []] | intros x]; reflexivity); foldRplug
    | |- context [O.e_handle (O.rplug ?E ?e) ?h ?r] =>
      replace (O.e_handle (O.rplug E e : O.expr (O.incL ⟨_, _⟩)) h r) with (O.rplug (O.r_handle E h r) e)
        by (simpl; f_equal; apply map_id; split; (intros [| []] || intros [] || intros x); reflexivity); foldRplug
    | |- _ => idtac
    end.

Lemma let_compat_aux {A X W : Set} {Δ : A → kind}
      τ₁ τ₂ ε
      {WFτ₁ : K[ Δ; X ⊢ τ₁ ∷ k_type]}
      {WFτ₂ : K[ Δ; X ⊢ τ₂ ∷ k_type]}
      {WFε  : K[ Δ; X ⊢ ε  ∷ k_effect]}
      (ρ : X [⇒] W) (η : ∀ α, K⟦ Δ α @ W⟧) (ϑ : X → ℑ W)
      e₁ e₂ W' ρ' b₁ b₂ n
      (Hb : n ⊨ E⟦ τ₁ / ε ⟧ ρ ! η ! ϑ W' ρ' b₁ b₂)
      (He : n ⊨ E⟦ τ₁ ⊢ τ₂ / ε ⟧ ρ ! η ! ϑ e₁ e₂) :
  n ⊨ E⟦ τ₂ / ε ⟧ ρ ! η ! ϑ W' ρ' (b₁ >>= e₁)%fsyn (b₂ >>= bind (O.injLSub ρ') e₂)%osyn.
Proof.
  irevert W' ρ' b₁ b₂ Hb; loeb_induction; iintro W'; iintro ρ'; iintro b₁; iintro b₂; iintro Hb.
  iintro W''; iintro ρ''; iintro E₁; iintro E₂; iintro HE; term_simpl; rewrite bind_bind_comp'.
  foldXplug; iapply Hb; clear b₁ b₂ Hb; apply rel_k_roll; intros.
  - iintro Hv; simpl; eapply rel_k_expr; [eassumption |].
    eapply rel_e_contr_both; [apply FR.contr_let | apply OR.contr_let | later_shift].
    erewrite bind_equiv; [iapply He; assumption |].
    clear; split; [intros [| x] | intros x]; reflexivity.
  - iintro HS; apply rel_s_unroll in HS; destruct HS as [ls [μ [HF [Hfree HS]]]]; simpl; foldRplug.
    eapply rel_k_stuck; [eassumption |].
    eapply rel_s_roll; [eassumption | unfold F.rfree_l in *; now auto | intros; iintro He'].
    iapply HS in He'; clear HS; later_shift; simpl.
    rewrite bind_bind_comp', map_id by (split; [intros [| []] | intros x]; reflexivity).
    erewrite bind_equiv with (t := e₂); [iapply IH; exact He' |].
    split; [intros [| x] | intros x]; reflexivity.
Qed.

Lemma let_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} τ₁ {τ₂ ε}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
  {WFε  : K[ Δ; LV ⊢ ε  ∷ k_effect]}
  {b₁ b₂ e₁ e₂} {n}
  (Hb : n ⊨ T⟦ Δ; Θ; Γ ⊨ b₁ ≾ b₂ ∷ τ₁ / ε ⟧)
  (He : n ⊨ T⟦ Δ; Θ; Γ ▹ τ₁ ⊨ e₁ ≾ e₂ ∷ τ₂ / ε ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ b₁ >>= e₁ ≾ b₂ >>= e₂ ∷ τ₂ / ε ⟧.
Proof.
  iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; term_simpl.
  iapply (ext_ord (R := E⟦ τ₂ / ε ⟧ ρ ! η ! ϑ)); [apply subst_comp_pure_l |]; iapply rel_e_fwdR.
  erewrite <- bind_pure with (t := (bind (liftS γ₂) e₂)); [iapply @let_compat_aux |].
  - iapply rel_e_fwdL; unfold fwd_R.
    iapply (ext_ord (R := E⟦ τ₁ / ε ⟧ ρ ! η ! ϑ)); [symmetry; iapply subst_comp_pure_l |].
    iapply Hb; assumption.
  - iintro W''; iintro ρ''; iintro v₁; iintro v₂; iintro Hv; term_simpl.
    iapply rel_e_fwdL; unfold fwd_R.
    rewrite bind_bind_comp'; unfold subst; rewrite !bind_bind_comp'; iapply He; [assumption | ..].
    + destruct Hδ as [Hδ₁ Hδ₂]; split; intros x; term_simpl; rewrite bind_pure by reflexivity;
        [now trivial |].
      rewrite Hδ₂; term_simpl; apply bind_bind_comp; reflexivity.
    + eapply subst_extend_G with (γ₂0 := subst_comp (O.injLSub ρ'') γ₂); [reflexivity | | |].
      * split; [intros [| x] | intros x]; term_simpl; try reflexivity.
        unfold subst, shift; rewrite map_to_bind, !bind_bind_comp'; apply bind_equiv.
        split; intros y; term_simpl; [| apply bind_pure]; reflexivity.
      * iintro x; term_simpl; iapply (fw_ord (R := ⟦ Γ x ⟧ ρ ! η ! ϑ)); iapply Hγ.
      * iapply rel_v_fwdR; exact Hv.
  - split; intros x; term_simpl; reflexivity.
Qed.

(* ========================================================================= *)
(* arrows *)

Lemma lam_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} {τ₁ τ₂ ε}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
  {WFε  : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {e₁ : F.expr (F.incV ⟨V, LV⟩)}
  {e₂ : O.expr (O.incV ⟨V, LV⟩)} {n}
  (He : n ⊨ T⟦ Δ; Θ; Γ ▹ τ₁ ⊨ e₁ ≾ e₂ ∷ τ₂ / ε ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ λᵉ e₁ ≾ λᵉ e₂ ∷ (τ₁ →[ε] τ₂)%typ ⟧.
Proof.
  iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; term_simpl.
  iintro W''; iintro ρ''; iintro u₁; iintro u₂; iintro Hu; term_simpl.
  eapply rel_e_contr_both; [apply FR.contr_beta | apply OR.contr_beta |].
  unfold subst; rewrite !bind_bind_comp'; later_shift.
  iapply He; [assumption | |].
  - destruct Hδ as [Hδ₁ Hδ₂]; split; intros x; term_simpl; [now rewrite bind_pure by reflexivity |].
    rewrite Hδ₂; apply bind_bind_comp; intros y; term_simpl; apply bind_equiv.
    intros z; apply bind_pure; reflexivity.
  - eapply subst_extend_G; [reflexivity | | | eassumption].
    + rewrite subst_comp_assoc, liftS_comp; reflexivity.
    + iintro x; term_simpl; iapply (fw_ord (R := ⟦ Γ x ⟧ ρ ! η ! (ϑ))); iapply Hγ.
Qed.

Lemma app_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} τ₁ {τ₂ ε}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
  {WFε  : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {u₁ u₂ v₁ v₂} {n}
  (Hu : n ⊨ T⟦ Δ; Θ; Γ ⊨ u₁ ≾ u₂ ∷ (τ₁ →[ε] τ₂)%typ ⟧)
  (Hv : n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ τ₁ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ u₁ @ᵉ v₁ ≾ u₂ @ᵉ v₂ ∷ τ₂ / ε  ⟧.
Proof.
  iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; term_simpl.
  rewrite <- bind_pure' with (t := bind γ₂ u₂).
  iapply (ext_ord (R := E⟦ τ₂ / ε ⟧ ρ ! η ! ϑ)); [apply subst_comp_pure_l |].
  iapply Hu; [assumption .. |].
  iapply (ext_ord (R := ⟦ τ₁ ⟧ ρ ! η ! ϑ)); [symmetry; iapply subst_comp_pure_l |].
  iapply Hv; eassumption.
Qed.

(* ========================================================================= *)
(* type quantifiers  *)

Lemma tlam_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} {τ : typ (incV {| sfst := _ |})} {κ}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ  : K[ Δ ▹ κ; LV ⊢ τ ∷ k_type]}
  {e₁ e₂} {n}
  (He : n ⊨ T⟦ Δ ▹ κ; shift Θ; shift Γ ⊨ e₁ ≾ e₂ ∷ τ / ι%typ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ λᵗ e₁ ≾ λᵗ e₂ ∷ (∀ᵗ κ, τ)%typ ⟧.
Proof.
  iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; iintro W''; iintro ρ''; iintro ρ'''; iintro R; iintro EQρ.
  term_simpl; eapply rel_e_contr_both; [apply FR.contr_tbeta | apply OR.contr_tbeta | later_shift].
  rewrite bind_bind_comp'; iapply He; clear -Hϑ Hδ Hγ EQρ.
  - iintro a; etransitivity; [apply fw_eq; iapply Hϑ |].
    etransitivity; [apply (tint_fwd_equiv k_sig) | etransitivity; [symmetry; apply fwd_pure_id |]].
    etransitivity; [apply rel_s_weakenT | apply (type_interp_irr k_sig)].
  - destruct Hδ as [Hδ₁ Hδ₂]; split; intros x; term_simpl; [apply Hδ₁ |].
    rewrite Hδ₂, bind_pure; [| reflexivity]; term_simpl; apply bind_bind_comp; symmetry; assumption.
  - iintro x; term_simpl; eapply rel_typ_k_irr; iapply @rel_v_weakenTr.
    iapply rel_v_fwdL; unfold fwd_R; iapply (ext_ord (R := ⟦ Γ x ⟧ ρ ! η ! (ϑ)));
      [| iapply (fw_ord (R := ⟦ Γ x ⟧ ρ ! η ! (ϑ))); iapply Hγ].
    intros y; term_simpl; now rewrite bind_pure, EQρ by reflexivity.
Qed.

Lemma tapp_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} {τ : typ (incV {| sfst := _ |})} {σ κ}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ  : K[ Δ ▹ κ; LV ⊢ τ ∷ k_type]}
  {WFσ  : K[ Δ; LV ⊢ σ ∷ κ]}
  {v₁ v₂} {n}
  (Hv : n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ (∀ᵗ κ, τ)%typ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ @ᵗ ≾ v₂ @ᵗ ∷ subst τ σ / ι%typ ⟧.
Proof.
  iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; term_simpl; rewrite <- bind_pure' with (t := bind γ₂ v₂).
  iapply (ext_ord (R := rel_expr_cl ι%sem ⟦ subst τ σ ⟧ ρ ! η ! (ϑ))); [apply subst_comp_pure_l |].
  change ι%sem with (F⟦ ι%typ ⟧ ρ ! η ! ϑ); iapply rel_e_fwdR.
  iapply @subrel_expr_cl; [reflexivity | | iapply Hv; [eassumption .. | symmetry; apply subst_comp_pure_l]].
  apply subrel, (type_interp_substT k_type); reflexivity.
Qed.

Lemma llam_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} {τ : typ (incL {| sfst := _ |})} σ
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ  : K[ Δ; inc LV ⊢ τ ∷ k_type]}
  {WFσ  : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {e₁ : F.expr (F.incL ⟨V, LV⟩)}
  {e₂ : O.expr (O.incL ⟨V, LV⟩)} {n}
  (He : n ⊨ T⟦ Δ; shift (Θ ▹ σ); shift Γ ⊨ e₁ ≾ e₂ ∷ τ / ι%typ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ F.v_llam e₁ ≾ O.v_llam e₂ ∷ (t_forallL σ τ)%typ ⟧.
Proof.
  iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; iintro W''; iintro ρ''; iintro ρ'''; iintro l; iintro a.
  iintro EQρ; term_simpl; eapply rel_e_contr_both; [apply FR.contr_lbeta | apply OR.contr_lbeta | later_shift].
  rewrite bind_bind_comp'; unfold subst; rewrite !bind_bind_comp'; iapply He; clear He.
  - iintro x.
    assert (EQρ' : subst_eq ρ''' (subst_comp (subst_comp (mk_subst a) (liftS ρ''')) (of_arrow mk_shift)))
      by (intros y; term_simpl; unfold subst; now rewrite map_to_bind, bind_bind_comp', bind_pure by reflexivity).
    etransitivity; [apply fwd_equiv; exact EQρ' |]; etransitivity; [apply fwd_comp |]; destruct x as [| x].
    + etransitivity; [apply fw_eq, rel_s_weakenL with (I := ⟨Σ⟦ σ ⟧ ρ ! η ! ϑ, l⟩ᴵ) |].
      etransitivity; [apply (tint_fwd_equiv k_sig) |]; etransitivity; [symmetry; apply fwd_pure_id |].
      apply (type_interp_k_irr k_sig); [.. | reflexivity].
      * now rewrite subst_comp_assoc, liftS_comp by reflexivity.
      * iintro α; unfold shiftTEnv, fwdTEnv; etransitivity; [symmetry; apply fwd_kint_comp |].
        eapply fwd_kint_equiv; [symmetry; assumption | reflexivity].
      * iintro x; isplit; [| reflexivity]; unfold shiftIEnv, fwdIEnv; simpl.
        etransitivity; [symmetry; apply fwd_comp | apply fwd_equiv; symmetry; apply EQρ'].
    + etransitivity; [apply fw_eq, fw_eq; iapply Hϑ |].
      etransitivity; [apply fw_eq, rel_s_weakenL with (I := ⟨Σ⟦ σ ⟧ ρ ! η ! ϑ, l⟩ᴵ) |].
      etransitivity; [apply (tint_fwd_equiv k_sig) |]; etransitivity; [symmetry; apply fwd_pure_id |].
      apply (type_interp_k_irr k_sig); [.. | reflexivity].
      * now rewrite subst_comp_assoc, liftS_comp by reflexivity.
      * iintro α; unfold shiftTEnv, fwdTEnv; etransitivity; [symmetry; apply fwd_kint_comp |].
        eapply fwd_kint_equiv; [symmetry; assumption | reflexivity].
      * iintro z; isplit; [| reflexivity]; unfold shiftIEnv, fwdIEnv; simpl.
        etransitivity; [symmetry; apply fwd_comp | apply fwd_equiv; symmetry; apply EQρ'].
  - destruct Hδ as [Hδ₁ Hδ₂]; split; intros [| x]; term_simpl;
      [reflexivity | rewrite Hδ₁; reflexivity | symmetry; apply bind_pure; reflexivity |].
    rewrite Hδ₂; term_simpl; rewrite bind_pure by (intros [| z]; reflexivity).
    rewrite bind_pure by reflexivity; f_equal; rewrite EQρ, <- bind_bind_comp'.
    apply bind_map_comp; intros y; reflexivity.
  - iintro x; term_simpl; ispecialize Hγ x.
    assert (EQρ' : subst_eq ρ''' (subst_comp (subst_comp (mk_subst a) (liftS ρ''')) (of_arrow mk_shift)))
      by (intros y; term_simpl; unfold subst; now rewrite map_to_bind, bind_bind_comp', bind_pure by reflexivity).
    eapply I_iff_elim_M; [apply apply_type_equiv, (type_interp_k_irr k_type)
                      with (τ₂ := shift (Γ x) : typ_ TV (inc LV))
                           (ρ₂ := subst_comp (subst_comp (mk_subst a) (liftS ρ''')) (liftS ρ)) |
                          iapply rel_v_fwdL; iapply @rel_v_weakenLr].
    + clear; intros [| x]; term_simpl; [reflexivity |].
      unfold subst; rewrite !map_to_bind, !bind_bind_comp'; apply bind_equiv; intros y; term_simpl.
      unfold subst; rewrite !map_to_bind, !bind_bind_comp'; apply bind_equiv; reflexivity.
    + iintro α; unfold shiftTEnv, fwdTEnv; etransitivity; [| apply fwd_kint_comp].
      apply fwd_kint_equiv; [apply EQρ' | reflexivity].
    + instantiate (ϑ1 := ϑ); instantiate (I := ⟨ Σ⟦ σ ⟧ ρ ! η ! (ϑ), l ⟩ᴵ); iintro z.
      isplit; simpl; [| reflexivity].
      etransitivity; [apply fwd_equiv, EQρ' | apply fwd_comp].
    + reflexivity.
    + iapply (ext_ord (R := ⟦ Γ x ⟧ ρ ! η ! ϑ)); [| iapply (fw_ord (R := ⟦ Γ x ⟧ ρ ! η ! ϑ)); exact Hγ].
      clear -EQρ EQρ'; intros x; term_simpl; rewrite EQρ; term_simpl.
      symmetry; unfold subst; rewrite map_to_bind, bind_bind_comp', bind_bind_comp'.
      apply bind_pure; reflexivity.
Qed.

Lemma lapp_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} {τ : typ (incL {| sfst := _ |})}
  {a : s_lbl {| sfst := TV |}}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ  : K[ Δ; inc LV ⊢ τ ∷ k_type]}
  {v₁ v₂} {n}
  (Hv : n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ (t_forallL (Θ a) τ)%typ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ @ˡ (F.l_var a) ≾ v₂ @ˡ (O.lclosed a) ∷ subst τ a / ι%typ ⟧.
Proof.
  iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; term_simpl; destruct Hδ as [Hδ₁ Hδ₂]; rewrite Hδ₁.
  rewrite <- bind_pure' with (t := bind γ₂ v₂).
  iapply (ext_ord (R := rel_expr_cl ι%sem ⟦ subst τ a ⟧ ρ ! η ! (ϑ))); [apply subst_comp_pure_l |].
  change ι%sem with (F⟦ ι%typ ⟧ ρ ! η ! ϑ); iapply rel_e_fwdR.
  iapply @subrel_expr_cl; [reflexivity | |
                           iapply Hv; [| split | | symmetry; apply subst_comp_pure_l]; eassumption].
  apply subrel; apply (type_interp_bind k_type) with _.
  - intros [| x]; term_simpl; rewrite Hδ₂; [reflexivity | term_simpl].
    unfold subst; rewrite map_to_bind, bind_bind_comp'; apply bind_pure; reflexivity.
  - iintro α; simpl; unfold apply_to_t_var.
    etransitivity; [| apply rel_equiv_var with (Heq := eq_refl) (Heq₁ := eq_refl)]; reflexivity.
  - iintro x; destruct x as [| x]; isplit; simpl; [| reflexivity ..].
    symmetry; apply fw_eq; iapply Hϑ.
Qed.

(* ========================================================================= *)
(* do *)

Definition Comp_of_Typ {X} (μ : Typ X) : Rexpr X :=
  λ W ρ e₁ e₂, ∃ᵢ v₁ v₂, (e₁ = F.e_value v₁ ∧ e₂ = O.e_value v₂)ᵢ ∧ᵢ wi_rel μ W ρ v₁ v₂.

Instance CoT_ext X (μ : Typ X) : WIR_ext (Comp_of_Typ μ).
Proof.
  unfold WIR_ext; intros; iintro e₁; iintro e₂; iintro HR; unfold Comp_of_Typ in *.
  idestruct HR as v₁ HR; idestruct HR as v₂ HR; idestruct HR as HH HR.
  iexists v₁; iexists v₂; isplit; [assumption |].
  iapply (ext_ord (R := μ)); eassumption.
Qed.

Instance CoT_fw X (μ : Typ X) : WIR_fwcl (Comp_of_Typ μ).
Proof.
  unfold WIR_fwcl; intros; iintro e₁; iintro e₂; iintro HR; unfold Comp_of_Typ in *.
  idestruct HR as v₁ HR; idestruct HR as v₂ HR; idestruct HR as HH HR; destruct HH; subst.
  iexists v₁; iexists (bind (O.injLSub ρ₂) v₂); isplit; term_simpl; [tauto |].
  iapply (fw_ord (R := μ)); eassumption.
Qed.

Lemma ToC_of_CoT_id X (μ : Typ X) n :
  n ⊨ Typ_of_Comp ⟨Comp_of_Typ μ⟩ᵂ ≈ᵂ μ.
Proof.
  iintro W; iintro ρ; isplit; iintro v₁; iintro v₂; iintro Hμ; simpl in *.
  - idestruct Hμ as u₁ Hμ; idestruct Hμ as u₂ Hμ; idestruct Hμ as EQ Hμ; destruct EQ as [EQ₁ EQ₂].
    injection EQ₁; injection EQ₂; intros; subst; assumption.
  - iexists v₁; iexists v₂; isplit; tauto.
Qed.

Lemma do_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} τ₁ {τ₂} {a : LV}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
  v₁ v₂ n :
  (Θ a = (τ₁ ⇒ᵈ τ₂)%typ) →
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ τ₁ ⟧ →
  n ⊨ T⟦ Δ; Θ; Γ ⊨ F.e_do_ (F.l_var a) v₁ ≾ O.e_do (O.lclosed a : O.lvar (ssnd ⟨_, _⟩)) v₂ ∷ τ₂ / (a : s_lbl ⟨_ , _⟩) ⟧.
Proof.
  intros Hop Hv; iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; term_simpl; destruct Hδ as [Hδ₁ Hδ₂].
  apply rel_s_in_e with (R₁ := F.r_hole) (R₂ := O.r_hole); rewrite Hδ₁, Hδ₂; term_simpl.
  apply rel_s_roll with (ls := cons (ℑ_ltag (ϑ a)) nil) (μ := ⟨Comp_of_Typ ⟨fwd_R (⟦ τ₂ ⟧ ρ ! η ! ϑ) ρ'⟩ᵂ⟩ᵂ);
    [| intros x _; exact I |].
  - simpl; unfold instance_rel; iexists (bind γ₁ v₁); iexists (bind γ₂ v₂); term_simpl.
    isplit; [split; [| split]; f_equal |].
    + apply bind_equiv; intros x; term_simpl; symmetry; apply map_id; reflexivity.
    + iapply (ext_ord (R := ϑ a)); [reflexivity | symmetry; apply ToC_of_CoT_id |].
      assert (HH : n ⊨ ϑ a ≈ᵂ Σ⟦ (τ₁ ⇒ᵈ τ₂)%typ ⟧ ρ ! η ! ϑ)
        by (etransitivity; [iapply Hϑ | apply (type_interp_k_irr k_sig); [| iintro .. | assumption]; reflexivity]).
      apply csymmetry, subrel in HH; iapply HH; [reflexivity |].
      unfold sig_rel; isplit; [| simpl; apply fwd_equiv; intros z; symmetry; now apply map_id].
      iapply (ext_ord (R := ⟦ τ₁ ⟧ ρ ! η ! ϑ)); [| iapply Hv; [| split |]; eassumption].
      intros z; symmetry; now apply map_id.
  - intros; iintro He; idestruct He as u₁ He; idestruct He as u₂ He; idestruct He as EQ He.
    destruct EQ; subst; simpl; later_shift; term_simpl; simpl in He; apply rel_v_in_e; exact He.
Qed.

(* ========================================================================= *)
(* handle *)

Lemma openBy_mapOf_id {A B} (R : O.rctx A B) :
  subst_eq (subst_comp (OR.openBy R) (of_arrow (O.mapOf R))) subst_pure.
Proof.
  induction R; intros x; term_simpl; [reflexivity | apply IHR |].
  specialize (IHR (VS x)); term_simpl in IHR; rewrite IHR; term_simpl; reflexivity.
Qed.


Lemma res_inst_gen {A B} (R : O.rctx A B) (ρ : O.sub ⟨inc ∅, B⟩ ⟨∅, B⟩)
      (EQ : ∀ x, O.sub_lbl ρ x = O.lclosed x) :
  bind ρ (O.rplug R (O.v_var (VZ : sfst ⟨inc ∅, A⟩))) =
  O.rplug R (bind (O.injLSub (of_arrow (O.mapOf R))) (O.sub_var ρ VZ : O.expr _)).
Proof.
  induction R; term_simpl; f_equal.
  - rewrite bind_pure; reflexivity.
  - apply IHR; assumption.
  - rewrite !map_to_bind; apply bind_bind_comp; split; [intros [| []]; reflexivity |].
    intros x; term_simpl; apply EQ.
  - rewrite IHR by (intros [| x]; [| simpl; rewrite EQ]; reflexivity); clear IHR.
    f_equal; term_simpl; f_equal; unfold shift; rewrite map_to_bind; apply bind_bind_comp.
    split; [intros [] | intros x]; term_simpl; reflexivity.
  - rewrite !map_to_bind; apply bind_bind_comp; split; [intros []; reflexivity |].
    intros x; term_simpl; apply EQ.
  - rewrite !map_to_bind; apply bind_bind_comp; split; [intros [| []]; reflexivity |].
    intros x; term_simpl; apply EQ.
Qed.

Lemma res_inst {A B} (R : O.rctx A B) (v : O.value ⟨∅, B⟩) :
  subst (O.rplug R (O.v_var (VZ : sfst ⟨inc ∅, A⟩))) v =
  O.rplug R (bind (O.injLSub (of_arrow (O.mapOf R))) (v : O.expr _)).
Proof.
  eapply res_inst_gen; reflexivity.
Qed.

Lemma rhandle_cl_compat {A X W : Set} {Δ : A → kind}
    σ τ' τ ε
    {WFσ  : K[ Δ ; X ⊢ σ  ∷ k_sig    ]}
    {WFε  : K[ Δ ; X ⊢ ε  ∷ k_effect ]}
    {WFτ' : K[ Δ ; X ⊢ τ' ∷ k_type   ]}
    {WFτ  : K[ Δ ; X ⊢ τ  ∷ k_type   ]}
    (ρ : X [⇒] W)
    (η : ∀ a, K⟦ Δ a @ W ⟧)
    (ϑ : X → ℑ W)
    (l : F.ltag) W' ρ' h₁ h₂ r₁ r₂ n :
    fdom_ft (F⟦ ε ⟧ ρ ! η ! ϑ) ≤ l →
    n ⊨ H⟦ σ @ τ / ε ⟧ ρ ! η ! ϑ W' ρ' h₁ h₂ →
    n ⊨ E⟦ τ' ⊢ τ / ε ⟧ ρ ! η ! ϑ r₁ r₂ →
    n ⊨ ∀ᵢ e₁ (e₂ : O.expr (incL ⟨∅, W'⟩)),
        E⟦ shift τ' / ((VZ : s_lbl ⟨_, _⟩) · shift ε)%typ ⟧ liftS ρ !  shiftTEnv η ! (shiftIEnv (ϑ ▹ ⟨Σ⟦ σ ⟧ ρ ! η ! ϑ, l⟩ᴵ)) (inc W') (liftS ρ') e₁ e₂ ⇒
         E⟦ τ / ε ⟧ ρ ! η ! ϑ W' ρ' (F.e_rhandle l e₁ h₁ r₁) (O.e_handle e₂ h₂ (bind (liftS (O.injLSub ρ')) r₂)).
Proof.
  intros Hl Hh Hr; irevert W' ρ' h₁ h₂ Hh; loeb_induction; iintro W'; iintro ρ'; iintro h₁; iintro h₂; iintro Hh.
  iintro e₁; iintro e₂; iintro He; iintro W''; iintro ρ''; iintro E₁; iintro E₂; iintro HE.
  term_simpl; simpl; foldXplug; iapply He; apply rel_k_roll; clear e₁ e₂ He; intros.
  - iintro Hv; simpl; eapply rel_k_expr; [eassumption |].
    eapply rel_e_contr_both; [apply FR.contr_rhandle | apply OR.contr_ret | later_shift].
    erewrite bind_bind_comp', bind_equiv; [iapply Hr | split; [intros [| []] | intros x]; reflexivity].
    iapply (ext_ord (R := ⟦ τ' ⟧ ρ ! η ! ϑ));
      [| iapply (fw_ord (R := ⟦ τ' ⟧ ρ ! η ! ϑ)); iapply @rel_v_weakenLl; exact Hv].
    clear; intros x; term_simpl; rewrite map_to_bind, !bind_bind_comp'; apply bind_equiv.
    intros y; term_simpl; rewrite map_to_bind, bind_bind_comp'; apply bind_pure; reflexivity.
  - iintro Hs; apply rel_s_unroll in Hs; destruct Hs as [ls [μ [HF [HFree HS]]]].
    idestruct HF as HF HF.
    + idestruct HF as v₁ HF; idestruct HF as v₂ HF; idestruct HF as EQ HF.
      destruct EQ as [EQ₁ [EQ₂ EQ₃]]; subst; simpl; foldRplug; destruct h₁ as [eh₁]; destruct h₂ as [eh₂].
      eapply rel_k_expr; [exact HE |]; eapply rel_e_contr_both;
        [ apply FR.contr_do; constructor; [apply HFree; simpl; tauto | constructor]
        | apply OR.contr_do; constructor; term_simpl; constructor | later_shift].
      assert (HH := Hh); ispecialize Hh W''; ispecialize Hh ρ''; ispecialize Hh (subst_comp ρ'' ρ').
      ispecialize Hh Y; ispecialize Hh (of_arrow (arrow_comp (O.mapOf R₂) mk_shift)).
      ispecialize Hh (subst_comp (mk_subst O.lopen) (OR.openBy R₂)).
      iespecialize Hh; ispecialize Hh; [| ispecialize Hh];
        [| iapply (ext_ord (R := Σ⟦ σ ⟧ ρ ! η ! ϑ)); [| reflexivity | exact HF] |].
      * split; [reflexivity | clear; intros x; term_simpl].
        assert (HT := openBy_mapOf_id R₂ (VS x)); term_simpl in HT; rewrite HT; reflexivity.
      * clear; intros x; term_simpl; rewrite !map_to_bind, !bind_bind_comp'; apply bind_equiv.
        intros y; term_simpl; rewrite map_to_bind; apply bind_bind_comp; reflexivity.
      * idestruct Hh as μ' Hh; idestruct Hh as Hμ Hh.
        erewrite bind_equiv with (t := eh₂);
          [iapply Hh; clear Hh | split; [intros [| []] | intros x]; reflexivity].
        iintro W₃; iintro ρ₃; iintro u₁; iintro u₂; iintro Hu; unfold fwd_R in Hu; term_simpl.
        eapply rel_e_contr_both; [apply FR.contr_beta | apply OR.contr_beta |].
        ispecialize HS (inc W₃); ispecialize HS (liftS ρ₃); ispecialize HS (u₁ : F.expr _).
        ispecialize HS (bind (O.injLSub (of_arrow (arrow_comp (O.mapOf (O.liftByR R₂ (liftS ρ₃))) mk_shift))) (u₂ : O.expr _)); ispecialize HS.
        -- assert (Hμ' := Hμ); apply csymmetry, subrel in Hμ'; apply subrel in Hμ.
           iapply Hμ'; unfold fwd_R; iapply Hμ in Hu; unfold fwd_R in Hu.
           iapply (ext_ord (R := μ')); [| iapply (fw_ord (R := μ')); eassumption].
           clear; intros x; term_simpl; assert (HT := openBy_mapOf_id R₂ (VS x)); term_simpl in HT.
           rewrite HT; clear HT; term_simpl.
           assert (HT := OM.liftBy_mapOf_comm R₂ (liftS ρ₃) (VS x)).
           term_simpl in HT; rewrite <- HT; clear HT; term_simpl.
           unfold subst; rewrite !map_to_bind, !bind_bind_comp'; apply bind_equiv.
           intros y; term_simpl; reflexivity.
        -- later_shift; iapply rel_e_fwdL; unfold fwd_R; term_simpl.
           rewrite !subst_shift_id_lifted, !subst_shift_id_lifted2, !map_to_bind, !bind_bind_comp'.
           erewrite bind_equiv with (t := r₂); [iapply IH |].
           ++ iapply (fw_ord (R := H⟦ σ @ τ / ε ⟧ ρ ! η ! ϑ) _ _ ρ' (subst_comp ρ₃ ρ'') n) in HH.
              unfold retract, RC_hndlr in HH; erewrite bind_equiv with (t := eh₂);
                [iapply (ext_ord (R := H⟦ σ @ τ / ε ⟧ ρ ! η ! ϑ)); [| apply HH] |]; clear.
              ** intros x; term_simpl; symmetry; apply bind_bind_comp.
                 intros y; term_simpl; apply bind_equiv; intros z; reflexivity.
              ** split; [intros [| [| []]]; reflexivity | intros x]; term_simpl.
                 apply bind_equiv; intros y; term_simpl; now apply bind_pure.
           ++ match goal with
                |- n ⊨ ?μ ?W ?ρ ?v₁ ?v₂ =>
                evar (v' : O.expr_ ∅ W); cutrewrite (v₂ = v'); revert v'; simpl;
                  [iapply (ext_ord (R := μ)); [| apply HS] |]
              end.
              ** clear; intros [| x]; term_simpl; [reflexivity | rewrite !bind_bind_comp'].
                 apply bind_map_comp; intros y; term_simpl; apply bind_map_comp; reflexivity.
              ** transitivity (bind (subst_comp (mk_subst (shift u₂ : O.value_ ∅ (inc W₃)))
                                                (O.injLSub (liftS ρ₃)))
                                    (O.rplug R₂ (O.v_var (VZ : sfst ⟨inc ∅, Y⟩)))).
                 +++ apply bind_equiv; clear; split; intros [| x]; term_simpl; [reflexivity .. |].
                     apply bind_equiv; intros [| y]; reflexivity.
                 +++ rewrite <- bind_bind_comp'; etransitivity; [apply f_equal, OM.liftBy_plug |].
                     etransitivity; [apply res_inst | f_equal; term_simpl; f_equal].
                     unfold shift; rewrite map_to_bind; apply bind_bind_comp.
                     split; [intros [] |]; intros x; term_simpl; reflexivity.
           ++ clear; split; [intros [| []] | intros x]; term_simpl; [reflexivity |].
              symmetry; apply bind_bind_comp; intros y; term_simpl; apply bind_equiv; intros z.
              term_simpl; symmetry; now apply bind_pure.
    + simpl; foldRplug; eapply rel_k_stuck; [exact HE |].
      eapply rel_s_roll with (ls0 := ls) (μ0 := μ).
      * iapply (ext_ord (R := F⟦ ε ⟧ ρ ! η ! ϑ));
          [| reflexivity | iapply @rel_eff_rel_weakenLl; [reflexivity | exact HF]].
        clear; intros x; term_simpl; rewrite !map_to_bind, !bind_bind_comp'; apply bind_equiv.
        intros y; term_simpl; rewrite map_to_bind; apply bind_bind_comp; reflexivity.
      * intros l₁ Hin; simpl; split; [intros EQ; subst l₁ | now apply HFree].
        assert (HT := fdom_tfresh (F⟦ ε ⟧ ρ ! η ! ϑ)); eapply HT with (n := n); [| eassumption ..].
        iapply @rel_eff_rel_weakenLl; [reflexivity | eassumption].
      * intros; iintro Hμ; simpl in Hμ; iapply HS in Hμ; later_shift.
        simpl; rewrite !map_to_bind, !bind_bind_comp'.
        erewrite bind_equiv with (t := r₂); [iapply IH; clear IH |].
        -- iapply (ext_ord (R := H⟦ σ @ τ / ε ⟧ ρ ! η ! ϑ)); [iapply subst_comp_assoc |].
           rewrite (bind_equiv (g := O.injLSub (subst_comp ρ'0 ρ''))) with (t := h₂);
             [now iapply (fw_ord (R := H⟦ σ @ τ / ε ⟧ ρ ! η ! ϑ)) |].
           clear; split; [intros [] | intros x]; term_simpl; apply bind_equiv.
           intros y; term_simpl; apply bind_pure; reflexivity.
        -- iapply (ext_ord (R := E⟦ shift τ' / ((VZ : s_lbl ⟨_, _⟩) · shift ε)%typ ⟧
                              liftS ρ ! shiftTEnv η ! (shiftIEnv (ϑ ▹ ⟨Σ⟦ σ ⟧ ρ ! η ! (ϑ), l⟩ᴵ))));
             [| exact Hμ].
           clear; intros [| x]; term_simpl; [reflexivity |].
           rewrite !bind_bind_comp'; apply bind_map_comp; intros y; term_simpl.
           apply bind_map_comp; intros z; reflexivity.
        -- clear; split; [intros [| []]; reflexivity | intros x]; term_simpl.
           symmetry; apply bind_bind_comp; intros y; term_simpl.
           apply bind_equiv; intros z; term_simpl; symmetry; apply bind_pure; reflexivity.
Qed.
           
Lemma handle_compat  {TV LV V : Set}
  (Δ : TV → kind) (Θ : LV → typ_ TV LV) (Γ : V → typ_ TV LV) τ τ' σ ε
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFσ  : K[ Δ; LV ⊢ σ  ∷ k_sig]}
  {WFτ  : K[ Δ; LV ⊢ τ  ∷ k_type]}
  {WFτ' : K[ Δ; LV ⊢ τ' ∷ k_type]}
  {WFε  : K[ Δ; LV ⊢ ε  ∷ k_effect]}
  (e₁ : F.expr (F.incL ⟨V, LV⟩))
  (e₂ : O.expr (O.incL ⟨V, LV⟩))
  h₁ h₂ r₁ r₂ n :
  n ⊨ T⟦ Δ; Θ; Γ ▹ τ ⊨ r₁ ≾ r₂ ∷ τ' / ε ⟧ →
  n ⊨ H⟦ Δ; Θ; Γ ⊨ h₁ ≾ h₂ ∷ σ @ τ' / ε ⟧ →
  n ⊨ T⟦ Δ; shift (Θ ▹ σ); shift Γ ⊨ e₁ ≾ e₂ ∷ shift τ / (VZ : s_lbl ⟨_, _⟩) · shift ε ⟧%typ →
  n ⊨ T⟦ Δ; Θ; Γ ⊨ F.e_handle e₁ h₁ r₁ ≾ O.e_handle e₂ h₂ r₂ ∷ τ' / ε ⟧.
Proof.
  intros Hr Hh He; iintro W₁; iintro ρ₁; iintro η; iintro ϑ; iintro W₂; iintro ρ₂.
  iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hδ; iintro Hγ; term_simpl.
  iintro W₃; iintro ρ₃; iintro E₁; iintro E₂; iintro HE.
  remember (fdom_ft (F⟦ ε ⟧ (subst_comp ρ₂ ρ₁) ! (fwdTEnv ρ₂ η) ! (fwdIEnv ρ₂ ϑ))) as l₁.
  destruct (FR.fresh_red E₁ (bind (liftS γ₁) e₁) (bind γ₁ h₁) (bind (liftS γ₁) r₁) l₁) as [l [HLE HR]].
  eapply Obs_red_l; [eassumption | clear HR]; eapply rel_k_expr; [eassumption |].
  subst l₁; clear E₁ E₂ HE; term_simpl.
  iapply rel_e_fwdR; iapply @rhandle_cl_compat; [assumption | instantiate (σ0 := σ) | instantiate (τ'0 := τ) |].
  - term_simpl; rewrite bind_bind_comp'; iapply Hh.
    + iintro x; etransitivity; [| symmetry; apply fwd_pure_id ].
      etransitivity; [| iapply (tint_fwd_equiv k_sig)].
      apply fw_eq; iapply Hϑ.
    + destruct Hδ as [Hδ₁ Hδ₂]; split; intros x; [apply Hδ₁ |]; term_simpl.
      f_equal; apply Hδ₂.
    + iintro x; term_simpl; iapply rel_v_fwdL; unfold fwd_R.
      iapply (fw_ord (R := ⟦ Γ x ⟧ ρ₁ ! η ! ϑ)); iapply Hγ.
  - clear W₃ ρ₃ l HLE; iintro W₃; iintro ρ₃; iintro v₁; iintro v₂; iintro Hv.
    iapply rel_e_fwdL; unfold fwd_R; term_simpl; unfold subst; rewrite !bind_bind_comp'.
    iapply Hr; [assumption | |].
    + destruct Hδ as [Hδ₁ Hδ₂]; split; intros x; term_simpl; [rewrite bind_pure, Hδ₁; reflexivity|].
      specialize (Hδ₂ x); simpl in Hδ₂; rewrite Hδ₂; apply bind_bind_comp; intros y; term_simpl.
      apply bind_equiv; intros z; term_simpl; apply bind_pure; reflexivity.
    + eapply subst_extend_G with (γ₂0 := subst_comp (O.injLSub ρ₃) γ₂);
        [reflexivity | .. | iapply rel_v_fwdR; eassumption].
      * rewrite subst_comp_assoc; apply subst_comp_proper; [reflexivity |].
        split; [intros [| x] | intros x]; term_simpl; try reflexivity; [].
        rewrite <- bind_liftS_shift_comm; apply bind_equiv.
        split; [intros [| z] | intros z]; reflexivity.
      * iintro x; term_simpl; iapply (fw_ord (R := ⟦ Γ x ⟧ ρ₁ ! η ! ϑ)); iapply Hγ.
  - unfold subst; rewrite !bind_bind_comp'; iapply He.
    + iintro x; destruct x as [| x]; term_simpl.
      * etransitivity; [apply rel_s_weakenL | apply (type_interp_irr k_sig)].
      * etransitivity; [symmetry; apply fwd_comp |]; etransitivity; [apply fw_eq; iapply Hϑ |].
        etransitivity; [| apply (type_interp_irr k_sig)]; etransitivity; [| apply rel_s_weakenL].
        etransitivity; [apply fwd_comp | apply fw_eq].
        etransitivity; [apply (tint_fwd_equiv k_sig) | symmetry; apply fwd_pure_id].
    + destruct Hδ as [Hδ₁ Hδ₂]; split; (intros [| x]; term_simpl; [reflexivity |]);
        [| f_equal; f_equal; apply Hδ₂].
      rewrite Hδ₁; reflexivity.
    + iintro x; term_simpl; eapply rel_typ_k_irr; iapply @rel_v_weakenLr; term_simpl.
      iapply rel_v_fwdL; unfold fwd_R; unfold shift; rewrite map_to_bind.
      iapply (ext_ord (R := ⟦ Γ x ⟧ ρ₁ ! η ! ϑ));
        [| do 2 iapply (fw_ord (R := ⟦ Γ x ⟧ ρ₁ ! η ! ϑ)); iapply Hγ].
      intros y; term_simpl; apply bind_bind_comp; intros z; term_simpl.
      rewrite map_to_bind; now apply bind_equiv.
Qed.

(* handlers *)

Lemma handler_do_compat {TV LV V : Set}
  (Δ : TV → kind) (Θ : LV → typ_ TV LV) (Γ : V → typ_ TV LV) τ₁ τ₂ τ ε
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ  : K[ Δ; LV ⊢ τ  ∷ k_type]}
  {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
  {WFε  : K[ Δ; LV ⊢ ε  ∷ k_effect]}
  (e₁ : F.expr (incV (incV ⟨V, LV⟩)))
  (e₂ : O.expr (incV (incV ⟨V, LV⟩))) n :
  n ⊨ T⟦ Δ; Θ; Γ ▹ τ₁ ▹ τ₂ →[ε] τ ⊨ e₁ ≾ e₂ ∷ τ / ε ⟧%typ →
  n ⊨ H⟦ Δ; Θ; Γ ⊨ F.h_do e₁ ≾ O.h_do e₂ ∷ τ₁ ⇒ᵈ τ₂ @ τ / ε ⟧%typ.
Proof.
  intros He; iintro W₁; iintro ρ₁; iintro η; iintro ϑ; iintro W₂; iintro ρ₂; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; simpl; iintro W₃; iintro ρ₃; iintro ρ.
  iintro Y; iintro δy; iintro ρy; iintro μ; iintro v₁; iintro v₂; iintro u₁; iintro u₂.
  iintro EQρ; iintro HS; destruct EQρ as [EQρ₁ EQρ₂]; term_simpl; idestruct HS as HS₁ HS₂.
  iexists ⟨⟦ τ₂ ⟧ ρ₁ ! η ! ϑ ⇑ ρ⟩ᵂ; isplit; [| iintro Hu].
  - etransitivity; [apply HS₂ | apply fwd_comp].
  - unfold subst; rewrite !bind_bind_comp', !bind_mk_subst_comm;
      fold (subst (shift u₁) v₁); fold (subst (shift u₂) (bind (O.injLSub ρy) v₂)).
    rewrite !subst_shift_id, !subst_comp_assoc, !liftS_comp by reflexivity.
    iapply He; [assumption | |].
    + destruct Hδ as [Hδ₁ Hδ₂]; split; intros x; term_simpl;
        rewrite !bind_pure by reflexivity; [apply Hδ₁ |].
      specialize (Hδ₂ x); simpl in Hδ₂; rewrite Hδ₂.
      apply bind_bind_comp; symmetry; exact EQρ₁.
    + eapply subst_extend_G; [reflexivity | | eapply subst_extend_G with (γ₂0 := subst_comp (O.injLSub ρ₃) γ₂); [reflexivity .. | |] |].
      * apply subst_comp_proper; [reflexivity |].
        etransitivity; [apply subst_comp_proper | eapply liftS_comp; reflexivity]; [reflexivity |].
        clear; split; [intros [| [| x]] | intros x]; term_simpl; [reflexivity .. | | reflexivity].
        unfold shift; rewrite !map_map_comp'; apply bind_map_comp.
        split; [intros [] | intros y]; term_simpl; now rewrite map_id.
      * iintro x; term_simpl; iapply (ext_ord (R := ⟦ Γ x ⟧ ρ₁ ! η ! ϑ));
          [| iapply (fw_ord (R := ⟦ Γ x ⟧ ρ₁ ! η ! ϑ)); iapply Hγ].
        symmetry; eexact EQρ₁.
      * iapply (ext_ord (R := ⟦ τ₁ ⟧ ρ₁ ! η ! ϑ));
          [| iapply (fw_ord (R := ⟦ τ₁ ⟧ ρ₁ ! η ! ϑ)); exact HS₁].
        intros x; term_simpl; erewrite bind_bind_comp; [apply bind_pure' | assumption].
      * clear v₁ v₂ HS₁; iintro W₄; iintro ρ₄; iintro v₁; iintro v₂; iintro Hv.
        iapply rel_e_fwdR; iapply Hu; unfold fwd_R.
        apply csymmetry, subrel in HS₂; iapply HS₂; unfold fwd_R.
        iapply (ext_ord (R := ⟦ τ₂ ⟧ ρ₁ ! η ! ϑ)); [| exact Hv].
        intros x; term_simpl; symmetry; apply bind_bind_comp; intros y; term_simpl.
        erewrite <- bind_bind_comp, EQρ₂; [reflexivity |].
        intros z; term_simpl; apply bind_equiv; intros w; reflexivity.
Qed.
