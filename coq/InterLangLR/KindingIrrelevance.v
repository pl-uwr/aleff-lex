Require Import Utf8.
Require Import IxFree.Lib.
Require Import Types Kinding.
Require Import Lang.Fresh Lang.Open.
Require Import InterLangLR.Core InterLangLR.KindEquiv.

Require Import Binding.Lib Binding.Set.

Fixpoint type_interp_k_irr (κ : kind) {A X W : Set} {Δ : A → kind}
    (ρ₁ ρ₂ : X [⇒] W)
    (τ₁ τ₂ : typ_ A X)
    (η₁ η₂ : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ₁ ϑ₂ : X → ℑ W)
    {WFτ₁ : K[ Δ ; X ⊢ τ₁ ∷ κ]}
    {WFτ₂ : K[ Δ ; X ⊢ τ₂ ∷ κ]} n {struct τ₁} :
  (subst_eq ρ₁ ρ₂) →
  (n ⊨ ∀ᵢ α, K⟦ η₁ α ≡ η₂ α ∷ Δ α ⟧) →
  (n ⊨ ∀ᵢ a, ϑ₁ a ≈ᴵ ϑ₂ a) →
  τ₁ = τ₂ →
  n ⊨ K⟦ ⟦ τ₁ ∷ κ ⟧ ρ₁ ! η₁ ! ϑ₁ ≡ ⟦ τ₂ ∷ κ ⟧ ρ₂ ! η₂ ! ϑ₂ ∷ κ ⟧.
Proof.
  intros EQρ Hη Hϑ Heq; destruct τ₁; inversion Heq; subst; invert_kind WFτ₁; simpl type_interp;
    [reflexivity | | unfold kind_equiv ..].
  - unfold apply_to_t_var.
    rewrite (kind_eq_proofs_unicity (K_Var_inv WFτ₁) eq_refl),
    (kind_eq_proofs_unicity (K_Var_inv WFτ₂) eq_refl); simpl; iapply Hη.
  - apply @equiv_arr.
    + symmetry; now apply (type_interp_k_irr k_type).
    + now apply (type_interp_k_irr k_type).
    + now apply (type_interp_k_irr k_effect).
  - apply equiv_tall; iintro W'; iintro ρ; iintro R.
    apply (type_interp_k_irr k_type); [intros x; term_simpl; now rewrite EQρ | |  | reflexivity].
    + iintro α; destruct α as [| α]; [reflexivity | unfold fwdTEnv; simpl].
      apply fwd_kint_equiv; [reflexivity | iapply Hη].
    + iintro a; unfold fwdIEnv; simpl; ispecialize Hϑ a; idestruct Hϑ as Hϑ Hϑt.
      isplit; [apply fw_eq |]; assumption.
  - apply equiv_lall; iintro W'; iintro ρ; iintro l; iintro a.
    apply (type_interp_k_irr k_type);
      [intros [| x]; term_simpl; [reflexivity | now rewrite EQρ] | .. | reflexivity].
    + iintro α; unfold fwdTEnv; simpl; apply fwd_kint_equiv; [reflexivity | iapply Hη].
    + iintro x; destruct x as [| x]; unfold fwdIEnv; simpl.
      * isplit; [apply fw_eq | reflexivity].
        now apply (type_interp_k_irr k_sig).
      * ispecialize Hϑ x; idestruct Hϑ as HR HT; isplit; [apply fw_eq | simpl]; assumption.
  - ispecialize Hϑ s; idestruct Hϑ as HR HT; rewrite EQρ.
    apply equiv_instance; isplit; assumption.
  - reflexivity.
  - apply equiv_cons; now apply (type_interp_k_irr k_effect).
  - apply equiv_sig; now apply (type_interp_k_irr k_type).
  - reflexivity.
Qed.

Lemma type_interp_irr {A X W : Set} {Δ : A → kind} (κ : kind) (ρ : X [⇒] W)
    (τ : typ_ A X)
    (η : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ : X → ℑ W)
    {WFτ₁ : K[ Δ ; X ⊢ τ ∷ κ]}
    {WFτ₂ : K[ Δ ; X ⊢ τ ∷ κ]} n :
  n ⊨ K⟦ type_interp τ κ ρ η ϑ WFτ₁ ≡ type_interp τ κ ρ η ϑ WFτ₂ ∷ κ ⟧.
Proof.
  apply type_interp_k_irr; [| iintro ..|]; reflexivity.
Qed.

Lemma type_interp_ext {A X W : Set} {Δ : A → kind} (κ : kind) (ρ₁ ρ₂ : X [⇒] W)
    (τ : typ_ A X)
    (η₁ η₂ : ∀ α : A, K⟦ Δ α @ W⟧)
    (ϑ₁ ϑ₂ : X → ℑ W)
    {WFτ : K[ Δ ; X ⊢ τ ∷ κ]} n
    (EQρ : subst_eq ρ₁ ρ₂)
    (EQη : n ⊨ ∀ᵢ α, K⟦ η₁ α ≡ η₂ α ∷ Δ α ⟧)
    (EQϑ : n ⊨ ∀ᵢ a, ϑ₁ a ≈ᴵ ϑ₂ a) :
  n ⊨ K⟦ ⟦ τ ∷ κ ⟧ ρ₁ ! η₁ ! ϑ₁ ≡ ⟦ τ ∷ κ ⟧ ρ₂ ! η₂ ! ϑ₂ ∷ κ ⟧.
Proof.
  apply type_interp_k_irr; [assumption .. | reflexivity].
Qed.

Lemma rel_sig_k_irr {TV LV W : Set} {Δ : TV → kind}
    (σ : typ_ TV LV) ρ
    (η : ∀ α : TV, K⟦ Δ α @ W⟧)
    (ϑ : LV → ℑ W)
    {WFσ₁ : K[ Δ ; LV ⊢ σ ∷ k_sig]}
    {WFσ₂ : K[ Δ ; LV ⊢ σ ∷ k_sig]}
    W' ρ' v₁ v₂ μ n :
  n ⊨ wi_rel (type_interp σ _ ρ η ϑ WFσ₁) W' ρ' v₁ v₂ μ →
  n ⊨ wi_rel (type_interp σ _ ρ η ϑ WFσ₂) W' ρ' v₁ v₂ μ.
Proof.
  intros Hσ; eapply I_iff_elim_M, Hσ; clear Hσ.
  apply apply_sig_equiv, (type_interp_irr k_sig).
Qed.

Lemma rel_eff_k_irr {TV LV W : Set} {Δ : TV → kind}
    (σ : typ_ TV LV) ρ
    (η : ∀ α : TV, K⟦ Δ α @ W⟧)
    (ϑ : LV → ℑ W)
    {WFτ₁ : K[ Δ ; LV ⊢ σ ∷ k_effect]}
    {WFτ₂ : K[ Δ ; LV ⊢ σ ∷ k_effect]}
    W' ρ' v₁ v₂ ls μ n :
  n ⊨ wi_rel (type_interp σ _ ρ η ϑ WFτ₁) W' ρ' v₁ v₂ ls μ →
  n ⊨ wi_rel (type_interp σ _ ρ η ϑ WFτ₂) W' ρ' v₁ v₂ ls μ.
Proof.
  intros Hσ; eapply I_iff_elim_M, Hσ; clear Hσ.
  apply apply_effect_equiv, (type_interp_irr k_effect).
Qed.

Lemma rel_typ_k_irr {TV LV W : Set} {Δ : TV → kind}
    (σ : typ_ TV LV) ρ
    (η : ∀ α : TV, K⟦ Δ α @ W⟧)
    (ϑ : LV → ℑ W)
    {WFτ₁ : K[ Δ ; LV ⊢ σ ∷ k_type]}
    {WFτ₂ : K[ Δ ; LV ⊢ σ ∷ k_type]}
    W' ρ' v₁ v₂ n :
  n ⊨ wi_rel (type_interp σ _ ρ η ϑ WFτ₁) W' ρ' v₁ v₂ →
  n ⊨ wi_rel (type_interp σ _ ρ η ϑ WFτ₂) W' ρ' v₁ v₂.
Proof.
  intros Hσ; eapply I_iff_elim_M, Hσ; clear Hσ.
  apply apply_type_equiv, (type_interp_irr k_type).
Qed.

Lemma rel_exp_k_irr {TV LV W : Set} {Δ : TV → kind}
    (τ ε : typ_ TV LV) ρ
    (η : ∀ α : TV, K⟦ Δ α @ W⟧)
    (ϑ : LV → ℑ W)
    {WFτ₁ : K[ Δ ; LV ⊢ τ ∷ k_type]}
    {WFτ₂ : K[ Δ ; LV ⊢ τ ∷ k_type]}
    {WFε₁ : K[ Δ ; LV ⊢ ε ∷ k_effect]}
    {WFε₂ : K[ Δ ; LV ⊢ ε ∷ k_effect]}
    W' ρ' e₁ e₂ n :
  n ⊨ rel_expr_cl (type_interp ε _ ρ η ϑ WFε₁) (type_interp τ _ ρ η ϑ WFτ₁) W' ρ' e₁ e₂ →
  n ⊨ rel_expr_cl (type_interp ε _ ρ η ϑ WFε₂) (type_interp τ _ ρ η ϑ WFτ₂) W' ρ' e₁ e₂.
Proof.
  intros HE; iapply @subrel_expr_cl; [apply subrel .. | exact HE].
  - apply (type_interp_irr k_effect).
  - apply (type_interp_irr k_type).
Qed.

Lemma rel_v_open_k_irr {TV LV V : Set}
  {Δ : TV → kind}
  {Θ : LV → typ_ TV LV}
  {Γ : V  → typ_ TV LV}
  {τ : typ_ TV LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {v₁ v₂} {n} :
  n ⊨ rel_v_open Δ Θ Γ τ (WFτ := WFτ₁) v₁ v₂ →
  n ⊨ rel_v_open Δ Θ Γ τ (WFτ := WFτ₂) v₁ v₂.
Proof.
  intros Hv; iintro W; iintro ρ; iintro η; iintro ϑ; iintro γ₁; iintro γ₂.
  iintro W'; iintro ρ'; iintro Hϑ; iintro Hδ; iintro Hγ.
  apply rel_typ_k_irr with WFτ₁; now iapply Hv.
Qed.

Lemma rel_e_open_k_irr {TV LV V : Set}
  {Δ : TV → kind}
  {Θ : LV → typ_ TV LV}
  {Γ : V  → typ_ TV LV}
  {τ ε : typ_ TV LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε₁ : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {WFε₂ : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {e₁ e₂} {n} :
  n ⊨ rel_e_open Δ Θ Γ τ ε (WFτ := WFτ₁) (WFε := WFε₁) e₁ e₂ →
  n ⊨ rel_e_open Δ Θ Γ τ ε (WFτ := WFτ₂) (WFε := WFε₂) e₁ e₂.
Proof.
  intros HT; iintro W; iintro ρ; iintro η; iintro ϑ; iintro γ₁; iintro γ₂.
  iintro W'; iintro ρ'; iintro Hϑ; iintro Hδ; iintro Hγ.
  apply rel_exp_k_irr with WFτ₁ WFε₁; now iapply HT.
Qed.

Lemma rel_h_open_k_irr {TV LV V : Set}
  {Δ : TV → kind}
  {Θ : LV → typ_ TV LV}
  {Γ : V  → typ_ TV LV}
  {σ τ ε : typ_ TV LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFσ₁ : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {WFσ₂ : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {WFτ₁ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε₁ : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {WFε₂ : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {h₁ h₂} {n} :
  n ⊨ rel_h_open Δ Θ Γ σ τ ε (WFσ := WFσ₁) (WFτ := WFτ₁) (WFε := WFε₁) h₁ h₂ →
  n ⊨ rel_h_open Δ Θ Γ σ τ ε (WFσ := WFσ₂) (WFτ := WFτ₂) (WFε := WFε₂) h₁ h₂.
Proof.
  intros HT; iintro W; iintro ρ; iintro η; iintro ϑ; iintro W'; iintro ρ'; iintro γ₁; iintro γ₂.
  iintro Hϑ; iintro Hδ; iintro Hγ; destruct h₁ as [e₁]; destruct h₂ as [e₂]; simpl.
  iintro W''; iintro ρ''; iintro ρt; iintro Y; iintro δy; iintro ρy.
  iintro v₁; iintro v₂; iintro I; iintro u₁; iintro u₂; iintro EQρ; iintro HS.
  iespecialize HT; do 3 (ispecialize HT; [eassumption |]); simpl in HT; iespecialize HT.
  ispecialize HT; [eassumption |]; ispecialize HT; [eapply rel_sig_k_irr; eassumption |].
  idestruct HT as μ' HT; idestruct HT as EQμ HT; iexists μ'; isplit; [assumption | iintro Hu].
  apply rel_exp_k_irr with WFτ₁ WFε₁; iapply HT; clear HT.
  iintro WW; iintro ρρ; iintro w₁; iintro w₂; iintro Hw.
  iapply @subrel_expr_cl; [apply subrel .. | iapply Hu; exact Hw].
  - apply (type_interp_irr k_effect).
  - apply (type_interp_irr k_type).
Qed.

Fixpoint tint_fwd_equiv κ (A X W W' : Set) (Δ : A → kind) (ρ : X [⇒] W) (ρ' : W [⇒] W')
      (τ : typ_ A X) η ϑ {WF : K[ Δ ; X ⊢ τ ∷ κ]} n :
  n ⊨ K⟦ fwd_kint ρ' (⟦ τ ∷ κ ⟧ ρ ! η ! ϑ) ≡
         fwd_kint subst_pure (⟦ τ ∷ κ ⟧ subst_comp ρ' ρ ! fwdTEnv ρ' η ! (fwdIEnv ρ' ϑ)) ∷ κ ⟧.
Proof.
  destruct τ; invert_kind WF; simpl type_interp; [| | unfold kind_equiv, fwd_kint ..].
  - apply equiv_unit.
  - unfold apply_to_t_var; apply rel_equiv_var_fwd with (Heq := eq_refl); simpl.
    unfold fwdTEnv; etransitivity; [| apply fwd_kint_comp].
    apply fwd_kint_equiv; [symmetry; iapply subst_comp_pure_l | reflexivity].
  - apply equiv_arr_fwd.
    + symmetry; apply (tint_fwd_equiv k_type).
    + apply (tint_fwd_equiv k_type).
    + apply (tint_fwd_equiv k_effect).
  - apply equiv_tall_fwd; iintro W''; iintro ρ''; iintro ρ₁; iintro ρ₂; iintro R; iintro EQρ.
    destruct EQρ as [EQρ₁ EQρ₂]; apply (type_interp_k_irr k_type); [| | | reflexivity].
    + intros x; term_simpl; rewrite EQρ₁, EQρ₂, subst_comp_pure_r; symmetry; apply bind_bind_comp'.
    + iintro α; destruct α as [| α]; simpl; [reflexivity |].
      unfold fwdTEnv; etransitivity; [apply fwd_kint_equiv; [| reflexivity] | apply fwd_kint_comp].
      now rewrite EQρ₁, EQρ₂, subst_comp_pure_r.
    + iintro x; unfold fwdIEnv; isplit; [| reflexivity]; simpl.
      etransitivity; [| apply fwd_comp]; apply fwd_equiv.
      now rewrite EQρ₁, EQρ₂, subst_comp_pure_r.
  - apply equiv_lall_fwd; iintro W''; iintro ρ''; iintro ρ₁; iintro ρ₂; iintro l; iintro a.
    iintro EQρ; destruct EQρ as [EQρ₁ EQρ₂]; apply (type_interp_k_irr k_type); [.. | reflexivity].
    + intros [| x]; term_simpl; [reflexivity |].
      now rewrite EQρ₁, EQρ₂, subst_comp_pure_r, bind_bind_comp'.
    + iintro α; unfold fwdTEnv; etransitivity; [| apply fwd_kint_comp].
      apply fwd_kint_equiv; [now rewrite EQρ₁, EQρ₂, subst_comp_pure_r | reflexivity].
    + iintro x; destruct x as [| x]; unfold fwdIEnv; (isplit; [| reflexivity]); simpl.
      * etransitivity; [apply fwd_equiv; eassumption |].
        etransitivity; [apply fwd_comp |].
        etransitivity; [apply fw_eq, (tint_fwd_equiv k_sig) |].
        etransitivity; [symmetry; apply fwd_comp |].
        apply fwd_equiv; now symmetry.
      * etransitivity; [apply fwd_equiv; eassumption |].
        etransitivity; [apply fwd_comp |].
        apply fwd_equiv; now rewrite EQρ₂, subst_comp_pure_r.
  - simpl; apply equiv_instance_fwd; [symmetry; apply bind_pure; reflexivity | reflexivity | ].
    unfold fwdIEnv; apply fwd_pure_id.
  - apply equiv_pure_fwd.
  - apply equiv_cons_fwd; apply (tint_fwd_equiv k_effect).
  - apply equiv_sig_fwd; apply (tint_fwd_equiv k_type).
  - apply equiv_emp.
Qed.

Lemma rel_v_fwdL (A X W W' : Set) (Δ : A → kind) (ρ : X [⇒] W) (ρ' : W [⇒] W')
      (τ : typ_ A X) η ϑ {WF : K[ Δ ; X ⊢ τ ∷ k_type]} n :
  n ⊨ ⟦ τ ⟧ ρ ! η ! ϑ ⇑ ρ' ≾ᵂ ⟦ τ ⟧ subst_comp ρ' ρ ! fwdTEnv ρ' η ! (fwdIEnv ρ' ϑ).
Proof.
  apply subrel; etransitivity; [apply (tint_fwd_equiv k_type) |].
  unfold fwd_kint; symmetry; apply fwd_pure_id.
Qed.

Lemma rel_v_fwdR (A X W W' : Set) (Δ : A → kind) (ρ : X [⇒] W) (ρ' : W [⇒] W')
      (τ : typ_ A X) η ϑ {WF : K[ Δ ; X ⊢ τ ∷ k_type]} n :
  n ⊨ ⟦ τ ⟧ subst_comp ρ' ρ ! fwdTEnv ρ' η ! (fwdIEnv ρ' ϑ) ≾ᵂ ⟦ τ ⟧ ρ ! η ! ϑ ⇑ ρ'.
Proof.
  apply subrel; etransitivity; [apply fwd_pure_id |].
  symmetry; apply (tint_fwd_equiv k_type).
Qed.

Lemma rel_eff_fwdL (A X W W' : Set) (Δ : A → kind) (ρ : X [⇒] W) (ρ' : W [⇒] W')
      (ε : typ_ A X) η ϑ {WF : K[ Δ ; X ⊢ ε ∷ k_effect]} n :
  n ⊨ F⟦ ε  ⟧ ρ ! η ! ϑ ⇑ ρ' ≾ᵂ F⟦ ε ⟧ subst_comp ρ' ρ ! fwdTEnv ρ' η ! (fwdIEnv ρ' ϑ).
Proof.
  apply subrel; etransitivity; [apply (tint_fwd_equiv k_effect) |].
  unfold fwd_kint; symmetry; apply fwd_pure_id.
Qed.

Lemma rel_eff_fwdR (A X W W' : Set) (Δ : A → kind) (ρ : X [⇒] W) (ρ' : W [⇒] W')
      (ε : typ_ A X) η ϑ {WF : K[ Δ ; X ⊢ ε ∷ k_effect]} n :
  n ⊨ F⟦ ε ⟧ subst_comp ρ' ρ ! fwdTEnv ρ' η ! (fwdIEnv ρ' ϑ) ≾ᵂ F⟦ ε ⟧ ρ ! η ! ϑ ⇑ ρ'.
Proof.
  apply subrel; etransitivity; [apply fwd_pure_id |].
  symmetry; apply (tint_fwd_equiv k_effect).
Qed.

Lemma rel_e_fwdL (A X W W' : Set) (Δ : A → kind) (ρ : X [⇒] W) (ρ' : W [⇒] W')
      (τ ε : typ_ A X) η ϑ
      {WFτ : K[ Δ ; X ⊢ τ ∷ k_type]}
      {WFε : K[ Δ ; X ⊢ ε ∷ k_effect]} n :
  n ⊨ E⟦ τ / ε ⟧ ρ ! η ! ϑ ⇑ ρ' ≾ᵂ ⟨E⟦ τ / ε ⟧ subst_comp ρ' ρ ! fwdTEnv ρ' η ! (fwdIEnv ρ' ϑ)⟩ᴿ.
Proof.
  apply subrel; etransitivity; [| symmetry; apply fwd_pure_id].
  apply equiv_ecl; [apply (tint_fwd_equiv k_effect) | apply (tint_fwd_equiv k_type)].
Qed.

Lemma rel_e_fwdR (A X W W' : Set) (Δ : A → kind) (ρ : X [⇒] W) (ρ' : W [⇒] W')
      (τ ε : typ_ A X) η ϑ
      {WFτ : K[ Δ ; X ⊢ τ ∷ k_type]}
      {WFε : K[ Δ ; X ⊢ ε ∷ k_effect]} n :
  n ⊨ ⟨E⟦ τ / ε ⟧ subst_comp ρ' ρ ! fwdTEnv ρ' η ! (fwdIEnv ρ' ϑ)⟩ᴿ ≾ᵂ E⟦ τ / ε ⟧ ρ ! η ! ϑ ⇑ ρ'.
Proof.
  apply subrel; etransitivity; [apply fwd_pure_id |].
  symmetry; apply equiv_ecl; [apply (tint_fwd_equiv k_effect) | apply (tint_fwd_equiv k_type)].
Qed.
