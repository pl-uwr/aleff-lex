Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Product Binding.Env Binding.Set.
Require Import Surface.Types.
Require Import InterLangLR.Domains InterLangLR.Closures.
Require Import Lang.Fresh Lang.Open.
Import TacticSynonyms.

Local Open Scope irel_scope.

Definition kind_interp X (κ : kind) : Type :=
  match κ with
  | k_type   => Typ X
  | k_effect => Eff X
  | k_sig    => Sig X
  end.

Notation "'K⟦' κ @ V '⟧'" := (kind_interp V κ) (κ at level 30).

Definition kind_equiv_upto {X Y : Set} (δ : X [→] Y) (ρ : Y [⇒] X) {SR : SectRetr (of_arrow δ) ρ}
           (κ : kind) : K⟦ κ @ X⟧ → K⟦ κ @ Y⟧ → IProp :=
  match κ with
  | k_type   => λ R₁ R₂, R₁ ≈ᵂ R₂ by [δ, ρ]
  | k_effect => λ R₁ R₂, R₁ ≈ᵂ R₂ by [δ, ρ]
  | k_sig    => λ R₁ R₂, R₁ ≈ᵂ R₂ by [δ, ρ]
  end.

Notation "K⟦ I₁ ≡ I₂ ∷ κ 'by' [ δ , ρ ] ⟧" := (kind_equiv_upto δ ρ κ I₁ I₂) (I₁ at level 30).

Definition fwd_kint {X Y} (ρ : X [⇒] Y) {κ} : K⟦ κ @ X ⟧ → K⟦ κ @ Y ⟧.
    destruct κ; simpl; intros R.
    - exact ⟨fwd_R R ρ⟩ᵂ.
    - exact ⟨fwd_R R ρ, fdom_ft R⟩ᴱ.
    - exact ⟨fwd_R R ρ⟩ᵂ.
Defined.

(*
  Lemma fwd_kint_eqR X Y (δ : X [→] Y) (ρ : Y [⇒] X) {SR : SectRetr (of_arrow δ) ρ}
        (κ : kind) (R : K⟦ κ @ X ⟧) n :
    n ⊨ K⟦ R ≡ fwd_kint (of_arrow δ) R ∷ κ by [δ, ρ] ⟧.
  Proof.
    destruct κ; simpl; apply fwd_uptoR.
  Qed.

  Lemma fwd_kint_eqL X Y (δ : X [→] Y) (ρ : Y [⇒] X) {SR : SectRetr (of_arrow δ) ρ}
        (κ : kind) (R : K⟦ κ @ Y ⟧) n :
    n ⊨ K⟦ fwd_kint ρ R ≡ R ∷ κ by [δ, ρ] ⟧.
  Proof.
    destruct κ; simpl; apply fwd_uptoL.
  Qed.
*)

Delimit Scope sem_scope with sem.

(** Generic empty domain *)

Definition empR sig {ord} {RO : ROrd sig ord} (X : Set) : WIRel sig X := ⊥ᵣ.
Arguments empR sig {ord RO} X W ρ / : rename.
  
Instance ext_emp {sig ord} {RO : ROrd sig ord} {ROP : ROPreo sig} (X : Set) :
  WIR_ext (empR sig X).
Proof.
  unfold WIR_ext; intros; simpl; apply rord_bot.
Qed.
Instance fwcl_emp {sig ord} {RO : ROrd sig ord} {RC : RetractCore sig} {ROP : ROPreo sig} (X : Set) :
  WIR_fwcl (empR sig X).
Proof.
  unfold WIR_fwcl; intros; unfold empR; apply rord_bot.
Qed.

Notation "∅ᵂ" := ⟨empR _ _⟩ᵂ : sem_scope.

(** Effects *)

(** Pure *)
Definition pure_eff {X} : Reff X := empR _ X.
Arguments pure_eff {X} [W] ρ / : rename.

Instance semFresh_pure X : IsSemFresh (X := X) 0 pure_eff.
Proof.
  unfold IsSemFresh; intros; simpl in *; isimplP; contradiction.
Qed.

Notation "'ι'" := ⟨ pure_eff, 0 ⟩ᴱ : sem_scope.

(** Cons *)
Definition sum_eff {X} (R₁ R₂ : Reff X) : Reff X := R₁ ∪ᵣ R₂.

Instance semFresh_or_max A {R₁ R₂ l₁ l₂}
      (HF₁ : IsSemFresh l₁ R₁)
      (HF₂ : IsSemFresh l₂ R₂) :
  IsSemFresh (X := A) (max l₁ l₂) (sum_eff R₁ R₂).
Proof.
  unfold IsSemFresh; intros; idestruct H as HR HR; (eapply semFresh in HR; [exact HR |]).
  - eapply Max.max_lub_l, H0.
  - eapply Max.max_lub_r, H0.
Qed.

Instance ext_sum {X} {R₁ R₂ : Reff X} {HE₁ : WIR_ext R₁} {HE₂ : WIR_ext R₂} :
  WIR_ext (sum_eff R₁ R₂).
Proof.
  unfold WIR_ext; intros; iintro s₁; iintro s₂; iintro ls; iintro μ₁; iintro μ₂; iintro EQμ.
  iintro HR; idestruct HR as HR HR; [ileft; iapply (ext_ord (R := R₁)) |
                                     iright; iapply (ext_ord (R := R₂))]; eassumption.
Qed.

Instance fwcl_sum {X} {R₁ R₂ : Reff X} {HC₁ : WIR_fwcl R₁} {HC₂ : WIR_fwcl R₂} :
  WIR_fwcl (sum_eff  R₁ R₂).
Proof.
  unfold WIR_fwcl; intros; iintro s₁; iintro s₂; iintro ls; iintro μ₁; iintro μ₂; iintro EQμ.
  iintro HR; idestruct HR as HR HR; [ileft; iapply (fw_ord (R := R₁)) |
                                     iright; iapply (fw_ord (R := R₂))]; eassumption.
Qed.

Notation " ε₁ · ε₂ " := ⟨sum_eff ε₁ ε₂, max (fdom_ft ε₁) (fdom_ft ε₂)⟩ᴱ
                          (at level 20, left associativity) : sem_scope.

(** Semantic computations as semantic types (values are expressions) *)
Instance ext_Comp_as_Type {X} (R : Rexpr X) {HE : WIR_ext R} : WIR_ext (R : Rtype X).
Proof.
  unfold WIR_ext; intros; iintro v₁; iintro v₂; now iapply (ext_ord (R := R)).
Qed.

Instance fwcl_Comp_as_Type {X} (R : Rexpr X) {HE : WIR_fwcl R} : WIR_fwcl (R : Rtype X).
Proof.
  unfold WIR_fwcl; intros; iintro v₁; iintro v₂; iapply (fw_ord (R := R)).
Qed.

Definition Typ_of_Comp {X} (μ : Comp X) : Typ X := ⟨wi_rel μ : Rtype X⟩ᵂ.

(* XXX: move *)

Lemma wiequiv_symcl {sig ord X} {RO : ROrd sig ord} {RP : ROPreo sig} (R₁ R₂ : WI_ext sig X) n
  (HR₁ : n ⊨ R₁ ≾ᵂ R₂) (HR₂ : n ⊨ R₂ ≾ᵂ R₁) : n ⊨ R₁ ≈ᵂ R₂.
Proof.
  iintro W; iintro ρ; apply req_symcl.
  - etransitivity; [iapply HR₁ | iapply (ext_ord (R := R₂)); intros x; reflexivity].
  - etransitivity; [iapply HR₂ | iapply (ext_ord (R := R₁)); intros x; reflexivity].
Qed.

Lemma ToC_equiv {X} (μ₁ μ₂ : Comp X) n (EQμ : n ⊨ μ₁ ≈ᵂ μ₂) : n ⊨ Typ_of_Comp μ₁ ≈ᵂ Typ_of_Comp μ₂.
Proof.
  apply wiequiv_symcl; iintro W; iintro ρ; iintro v₁; iintro v₂.
  - apply subrel in EQμ; iapply EQμ.
  - apply csymmetry, subrel in EQμ; iapply EQμ.
Qed.

(** Instances *)
Definition instance_rel {X} (ν : ℑ X) (a : O.lvar X) : Reff X :=
  λ W ρ e₁ e₂ ls μ,
  ∃ᵢ v₁ v₂,
    (e₁ = F.e_do (F.l_tag (ℑ_ltag ν)) v₁ ∧
     e₂ = O.e_do (bind ρ a : O.lvar (O.lv ⟨∅, W⟩)) v₂ ∧
     ls = (ℑ_ltag ν :: nil)%list)ᵢ ∧ᵢ
    wi_rel (ℑ_rel ν) W ρ v₁ v₂ (Typ_of_Comp μ).

Instance SemFresh_inst X (ν : ℑ X) a :
  IsSemFresh (S (ℑ_ltag ν)) (instance_rel ν a).
Proof.
  unfold IsSemFresh; intros.
  idestruct H as v₁ HH; idestruct HH as v₂ HH; idestruct HH as HR HH;
    destruct HR as [He₁ [He₂ Hls]]; subst; simpl.
  intros [EQ | HF]; [rewrite EQ in H0 | contradiction].
  contradiction (PeanoNat.Nat.nle_succ_diag_l l').
Qed.

Instance ext_inst X (ν : ℑ X) l : WIR_ext (instance_rel ν l).
Proof.
  unfold WIR_ext; intros; iintro e₁; iintro e₂; iintro ls; iintro μ₁; iintro μ₂; iintro EQμ.
  iintro HR; idestruct HR as v₁ HR; idestruct HR as v₂ HR; idestruct HR as HR HH.
  iexists v₁; iexists v₂; isplit; [now rewrite <- EQρ |].
  iapply (ext_ord (R := ν)); [| apply ToC_equiv |]; eassumption.
Qed.

Instance fwcl_inst X (ν : ℑ X) l : WIR_fwcl (instance_rel ν l).
Proof.
  unfold WIR_fwcl; intros; iintro e₁; iintro e₂; iintro ls; iintro μ₁; iintro μ₂; iintro EQμ.
  iintro HR; idestruct HR as v₁ HR; idestruct HR as v₂ HR; idestruct HR as HR HH.
  unfold retract, RC_eff; iexists v₁; iexists (bind (O.injLSub ρ₂) v₂); isplit.
  - destruct HR as [HR₁ [HR₂ HT]]; split; [assumption | split; [subst e₂ | assumption]].
    term_simpl; f_equal; apply bind_bind_comp; reflexivity.
  - clear HR; iapply (fw_ord (R := ν) _ _ ρ₁ ρ₂ n) in HH; [apply ToC_equiv; eassumption |].
    iapply (ext_ord (R := ν)); [reflexivity | | eassumption].
    clear.
    iintro W₃; iintro ρ₃; apply req_symcl; iintro v₁; iintro v₂; unfold fwd_R;
      iapply (ext_ord (R := μ₂)); intros x; term_simpl; apply bind_equiv; intros y; reflexivity.
Qed.

Notation "l ⇝ ν" := ⟨instance_rel ν l, S (ℑ_ltag ν)⟩ᴱ (at level 15, no associativity) : sem_scope.

(** Signatures *)

Definition sig_rel {X} (IA IR : Rtype X) {EIR : WIR_ext IR} : Rsig X :=
  λ W ρ v₁ v₂ μ, IA W ρ v₁ v₂ ∧ᵢ (μ ≈ᵂ ⟨fwd_R IR ρ⟩ᴿ).

Instance ext_sig X (IA IR : Rtype X) {HEA : WIR_ext IA} {HER : WIR_ext IR} :
  WIR_ext (sig_rel IA IR).
Proof.
  unfold WIR_ext; intros; iintro v₁; iintro v₂; iintro μ₁; iintro μ₂; iintro EQμ; iintro HR.
  idestruct HR as HRA HRR; isplit; [iapply (ext_ord (R := IA)); eassumption |].
  etransitivity; [symmetry; exact EQμ |]; etransitivity; [exact HRR |].
  iintro W'; iintro ρ'; unfold fwd_R; simpl; eapply ext_eq; clear -EQρ.
  intros x; term_simpl; rewrite EQρ; apply bind_equiv; intros y; reflexivity.
Qed.

Instance fw_sig X (IA IR : Rtype X) {HFA : WIR_fwcl IA} {HER : WIR_ext IR} {HFR : WIR_fwcl IR} :
  WIR_fwcl (sig_rel IA IR).
Proof.
  unfold WIR_fwcl; intros; iintro v₁; iintro v₂; iintro μ₁; iintro μ₂; iintro EQμ; iintro HR.
  idestruct HR as HRA HRR; isplit; [now iapply (fw_ord (R := IA)) |].
  etransitivity; [symmetry; apply fw_eq, EQμ |].
  etransitivity; [apply fw_eq, HRR |].
  iintro W'; iintro ρ'; unfold fwd_R; simpl; eapply ext_eq; clear.
  intros x; term_simpl; symmetry; apply bind_bind_comp.
  intros y; term_simpl; apply bind_equiv; now intros z.
Qed.

Notation " τ₁ '⇒ᵈ' τ₂ " := ⟨sig_rel τ₁ τ₂⟩ᵂ (at level 20, no associativity) : sem_scope.


(** Proper types *)

(** Unit *)
Definition unit_rel X : Rtype X := λ W ρ v₁ v₂, (v₁ = (()ᵉ)%fsyn ∧ v₂ = (()ᵉ)%osyn)ᵢ.
Arguments unit_rel {X} [W] ρ / : rename.

Instance ext_unit X : WIR_ext (@unit_rel X).
Proof.
  unfold WIR_ext; intros; iintro v₁; iintro v₂; simpl; reflexivity.
Qed.

Instance fw_unit X : WIR_fwcl (@unit_rel X).
Proof.
  unfold WIR_fwcl; intros; iintro v₁; iintro v₂; simpl; iintro HR.
  destruct HR; subst; unfold retract, RC_type; term_simpl; isimplP; tauto.
Qed.

Notation " '()ᵗ' " := ⟨unit_rel⟩ᵂ : sem_scope.

(** Arrow types *)
Definition arrow_rel {X} (IA : Rtype X) (IR : Rexpr X) : Rtype X :=
  λ W ρ v₁ v₂, ∀ᵢ W' (ρ' : W [⇒] W') u₁ u₂,
    IA W' (subst_comp ρ' ρ) u₁ u₂ ⇒
       IR W' (subst_comp ρ' ρ) (v₁ @ᵉ u₁)%fsyn (bind (O.injLSub ρ') v₂ @ᵉ u₂)%osyn.

Instance ext_arrow X (IA : Rtype X) (IR : Rexpr X) {HEA : WIR_ext IA} {HER : WIR_ext IR} :
  WIR_ext (arrow_rel IA IR).
Proof.
  unfold WIR_ext; intros; iintro v₁; iintro v₂; iintro Hv; iintro W'; iintro ρ'.
  iintro u₁; iintro u₂; iintro Hu.
  iapply (ext_ord (R := IR)); [| iapply Hv; iapply (ext_ord (R := IA)); [| exact Hu]]; clear -EQρ.
  - intros x; term_simpl; now rewrite EQρ.
  - intros x; term_simpl; now rewrite EQρ.
Qed.

Instance fw_arrow X (IA : Rtype X) (IR : Rexpr X) {HFA : WIR_ext IA} {HFR : WIR_ext IR} :
  WIR_fwcl (arrow_rel IA IR).
Proof.
  unfold WIR_fwcl; intros; iintro v₁; iintro v₂; iintro Hv; iintro W'; iintro ρ'.
  iintro u₁; iintro u₂; iintro Hu.
  rewrite bind_bind_comp with (h := O.injLSub (subst_comp ρ' ρ₂)) by (split; now term_simpl).
  iapply (ext_ord (R := IR)); [| iapply Hv; iapply (ext_ord (R := IA)); [| eassumption]].
  - clear; intros x; term_simpl; symmetry; apply bind_bind_comp; intros y; reflexivity.
  - clear; intros x; term_simpl; apply bind_bind_comp; intros y; reflexivity.
Qed.

Notation " τ₁ '→[' ε ']' τ₂ " :=
  ⟨arrow_rel τ₁ ⟨rel_expr_cl ε τ₂⟩ᵂ⟩ᵂ (at level 30, right associativity) : sem_scope.

(** type universals *)
Definition tall_rel {X κ} (I : ∀ W (ρ : X [⇒] W), K⟦ κ @ W ⟧ → Typ W) : Rtype X :=
  λ W ρ v₁ v₂, ∀ᵢ W' ρ' ρ'' (R : K⟦ κ @ W'⟧), (subst_eq ρ'' (subst_comp ρ' ρ))ᵢ ⇒
    rel_expr_cl ι%sem (I W' ρ'' R) W' subst_pure (F.e_tapp v₁) (O.e_tapp (bind (O.injLSub ρ') v₂)).

Instance ext_tall {X κ} (I : ∀ W (ρ : X [⇒] W), K⟦ κ @ W ⟧ → Typ W) : WIR_ext (tall_rel I).
Proof.
  unfold WIR_ext; intros; iintro v₁; iintro v₂; iintro Hv.
  iintro W'; iintro ρ; iintro ρ'; iintro R; iintro EQρ'.
  iapply Hv; now rewrite EQρ', EQρ.
Qed.

Instance fw_tall X κ (I : ∀ W (ρ : X [⇒] W), K⟦ κ @ W ⟧ → Typ W) :
         (*(Iext : ∀ W ρ₁ ρ₂ R n, subst_eq ρ₁ ρ₂ → n ⊨ I W ρ₁ R ≾ᵂ I W ρ₂ R) *)
  WIR_fwcl (tall_rel I).
Proof.
  unfold WIR_fwcl; intros; iintro v₁; iintro v₂; iintro Hv.
  iintro W₃; iintro ρ₃; iintro ρ'; iintro R; iintro EQρ.
  rewrite bind_bind_comp', (bind_equiv (g := (O.injLSub (subst_comp ρ₃ ρ₂))))
    by (split; intros x; term_simpl; reflexivity).
  iapply Hv; rewrite EQρ; symmetry; iapply subst_comp_assoc.
Qed.

(*Notation " '∀ᵗ' κ .. κ' , τ " := ⟨tall_rel (λ κ, .. ⟨tall_rel (λ κ', τ)⟩ᵂ ..)⟩ᵂ
  (at level 30, κ binder, κ' binder, right associativity) : sem_scope.*)

(** instance universals *)
(*
Definition lall_rel {X} (IA : Sig X) (IR : ℑ X → Comp (inc X)) : Rtype X :=
  λ W ρ v₁ v₂, ∀ᵢ (ν : ℑ X), ν ≈ᵂ IA ⇒
    wi_rel (IR ν) (inc W) (liftS ρ) (v₁ @ˡ F.l_tag (ℑ_ltag ν))%fsyn (shift v₂ @ˡ O.lclosed VZ)%osyn.
 *)

Definition lall_rel {X} (IR : ∀ W (ρ : X [⇒] W), F.ltag → O.lvar W → Typ W) : Rtype X :=
  λ W ρ v₁ v₂, ∀ᵢ W' ρ' ρ'' l a, (subst_eq ρ'' (subst_comp ρ' ρ))ᵢ ⇒
    rel_expr_cl ι%sem (IR W' ρ'' l a) W' subst_pure (v₁ @ˡ F.l_tag l)%fsyn ((bind (O.injLSub ρ') v₂) @ˡ a)%osyn.

Instance ext_lall X (IR : ∀ W (ρ : X [⇒] W), F.ltag → O.lvar W → Typ W) : WIR_ext (lall_rel IR).
Proof.
  unfold WIR_ext; intros; iintro v₁; iintro v₂; iintro Hv.
  iintro W'; iintro ρ; iintro ρ'; iintro l; iintro a; iintro EQρ'.
  iapply Hv; now rewrite EQρ', EQρ.
Qed.

Instance fw_lall X (IR : ∀ W (ρ : X [⇒] W), F.ltag → O.lvar W → Typ W) : WIR_fwcl (lall_rel IR).
Proof.
  unfold WIR_fwcl; intros; iintro v₁; iintro v₂; iintro Hv.
  iintro W₃; iintro ρ₃; iintro ρ; iintro l; iintro a; iintro EQρ.
  rewrite bind_bind_comp', (bind_equiv (g := (O.injLSub (subst_comp ρ₃ ρ₂))))
    by (split; intros x; term_simpl; reflexivity).
  iapply Hv; rewrite EQρ; symmetry; iapply subst_comp_assoc.
Qed.

Notation " '∀ˡ' τ " := ⟨lall_rel (λ a, ⟨rel_expr_cl ι%sem (τ a)⟩ᵂ)⟩ᵂ
  (at level 30, no associativity) : sem_scope.

Section Properties.
  Local Open Scope sem_scope.

  (* empty *)
  Lemma equiv_emp {sig ord} {RO : ROrd sig ord} {RC : RetractCore sig} {ROP : ROPreo sig}
        {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W) n :
    n ⊨ ∅ᵂ ⇑ ρ₁ ≈ᵂ ∅ᵂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ; isplit; simpl; apply rord_bot.
  Qed.

  (* unit *)
  Lemma equiv_unit {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W) n :
    n ⊨ ()ᵗ ⇑ ρ₁ ≈ᵂ ()ᵗ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ; isplit; unfold fwd_R; simpl; repeat iintro; assumption.
  Qed.
    
  (* arrows *)
  Lemma subrel_arr_meet {X Y W} (IA₁ IR₁ : Typ X) (IA₂ IR₂ : Typ Y) (IE₁ : Eff X) (IE₂ : Eff Y)
        (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W) n
        (HA : n ⊨ IA₂ ⇑ ρ₂ ≾ᵂ IA₁ ⇑ ρ₁)
        (HR : n ⊨ IR₁ ⇑ ρ₁ ≾ᵂ IR₂ ⇑ ρ₂)
        (HE : n ⊨ IE₁ ⇑ ρ₁ ≾ᵂ IE₂ ⇑ ρ₂) :
    n ⊨ IA₁ →[ IE₁ ] IR₁ ⇑ ρ₁ ≾ᵂ IA₂ →[ IE₂ ] IR₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ; iintro v₁; iintro v₂; iintro HH; iintro W''; iintro ρ';
      iintro u₁; iintro u₂; iintro Hu.
    iapply @subrel_ecl_meet; [.. | iapply HH].
    - clear -HE; iintro W''; iintro ρ'; iintro e₁; iintro e₂; iintro ls; iintro μ₁; iintro μ₂.
      iintro EQμ; iintro HH; iapply (ext_ord (R := IE₂)); [iapply subst_comp_assoc | eassumption |].
      iapply HE; [reflexivity |]; iapply (ext_ord (R := IE₁)); [| reflexivity | apply HH].
      symmetry; iapply subst_comp_assoc.
    - clear -HR; iintro W''; iintro ρ'; iintro v₁; iintro v₂; iintro Hv.
      iapply (ext_ord (R := IR₂)); [iapply subst_comp_assoc | iapply HR].
      iapply (ext_ord (R := IR₁)); [symmetry; iapply subst_comp_assoc| apply Hv].
    - iapply (ext_ord (R := IA₁)); [iapply subst_comp_assoc | iapply HA].
      iapply (ext_ord (R := IA₂)); [symmetry; iapply subst_comp_assoc | apply Hu].
  Qed.

  Lemma subrel_arr {X} (IA₁ IR₁ IA₂ IR₂ : Typ X) (IE₁ IE₂ : Eff X) n
        (HA : n ⊨ IA₂ ≾ᵂ IA₁)
        (HR : n ⊨ IR₁ ≾ᵂ IR₂)
        (HE : n ⊨ IE₁ ≾ᵂ IE₂) :
    n ⊨ IA₁ →[ IE₁ ] IR₁ ≾ᵂ IA₂ →[ IE₂ ] IR₂.
  Proof.
    iintro W; iintro ρ; iintro v₁; iintro v₂; iintro Hv.
    iintro W'; iintro ρ'; iintro u₁; iintro u₂; iintro Hu.
    iapply @subrel_expr_cl; [eassumption .. |].
    iapply Hv; iapply HA; assumption.
  Qed.
    
  Lemma equiv_arr_fwd X Y W (IA₁ IR₁ : Typ X) (IA₂ IR₂ : Typ Y) (IE₁ : Eff X) (IE₂ : Eff Y)
        (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W) n
        (HA : n ⊨ IA₂ ⇑ ρ₂ ≈ᵂ IA₁ ⇑ ρ₁)
        (HR : n ⊨ IR₁ ⇑ ρ₁ ≈ᵂ IR₂ ⇑ ρ₂)
        (HE : n ⊨ IE₁ ⇑ ρ₁ ≈ᵂ IE₂ ⇑ ρ₂) :
    n ⊨ IA₁ →[ IE₁ ] IR₁ ⇑ ρ₁ ≈ᵂ IA₂ →[ IE₂ ] IR₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ; apply req_symcl.
    - assert (HT := subrel_arr_meet IA₁ IR₁ IA₂ IR₂ IE₁ IE₂ ρ₁ ρ₂ n).
      iapply HT; apply subrel; assumption.
    - assert (HT := subrel_arr_meet IA₂ IR₂ IA₁ IR₁ IE₂ IE₁ ρ₂ ρ₁ n).
      iapply HT; apply subrel; symmetry; assumption.
  Qed.

  Lemma equiv_arr {X} (IA₁ IR₁ IA₂ IR₂ : Typ X) (IE₁ IE₂ : Eff X) n
        (HA : n ⊨ IA₂ ≈ᵂ IA₁)
        (HR : n ⊨ IR₁ ≈ᵂ IR₂)
        (HE : n ⊨ IE₁ ≈ᵂ IE₂) :
    n ⊨ IA₁ →[ IE₁ ] IR₁ ≈ᵂ IA₂ →[ IE₂ ] IR₂.
  Proof.
    etransitivity; [apply fwd_pure_id | etransitivity; [| symmetry; apply fwd_pure_id]].
    apply equiv_arr_fwd; (etransitivity; [symmetry; apply fwd_pure_id | etransitivity; [| apply fwd_pure_id]]); assumption.
  Qed.
  
  (* type universals *)
  Lemma subrel_tall_meet {X Y W κ} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W)
        (I₁ : ∀ W, (X [⇒] W) → K⟦ κ @ W ⟧ → Typ W)
        (I₂ : ∀ W, (Y [⇒] W) → K⟦ κ @ W ⟧ → Typ W) n
        (EQI : n ⊨ ∀ᵢ W' (ρ : W [⇒] W') ρ₁' ρ₂' R,
                   (subst_eq ρ₁' (subst_comp ρ ρ₁) ∧ subst_eq ρ₂' (subst_comp ρ ρ₂))ᵢ ⇒
                   I₁ W' ρ₁' R ≾ᵂ I₂ W' ρ₂' R) :
    n ⊨ tall_rel I₁ ⇑ ρ₁ ≾ᵂ tall_rel I₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ; iintro v₁; iintro v₂; iintro Hv.
    iintro W''; iintro ρ'; iintro ρ''; iintro R; iintro EQρ.
    iapply @subrel_expr_cl; [reflexivity | | iapply Hv; reflexivity].
    iapply EQI; split; [| rewrite EQρ]; symmetry; iapply subst_comp_assoc.
  Qed.

  Lemma subrel_tall X κ
        (I₁ I₂ : ∀ W, (X [⇒] W) → K⟦ κ @ W ⟧ → Typ W) n
        (EQI : n ⊨ ∀ᵢ W (ρ : X [⇒] W) R, I₁ W ρ R ≾ᵂ I₂ W ρ R) : 
    n ⊨ ⟨tall_rel I₁⟩ᴿ ≾ᵂ ⟨tall_rel I₂⟩ᴿ.
  Proof.
    iintro W; iintro ρ; iintro v₁; iintro v₂; iintro Hv.
    iintro W'; iintro ρ'; iintro ρ''; iintro R; iintro EQρ.
    iapply @subrel_expr_cl; [reflexivity | iapply EQI | iapply Hv; assumption].
  Qed.

  Lemma equiv_tall_fwd {X Y W κ} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W)
        (I₁ : ∀ W, (X [⇒] W) → K⟦ κ @ W ⟧ → Typ W)
        (I₂ : ∀ W, (Y [⇒] W) → K⟦ κ @ W ⟧ → Typ W) n
        (EQI : n ⊨ ∀ᵢ W' (ρ : W [⇒] W') ρ₁' ρ₂' R,
                   (subst_eq ρ₁' (subst_comp ρ ρ₁) ∧ subst_eq ρ₂' (subst_comp ρ ρ₂))ᵢ ⇒
                   I₁ W' ρ₁' R ≈ᵂ I₂ W' ρ₂' R) :
    n ⊨ tall_rel I₁ ⇑ ρ₁ ≈ᵂ tall_rel I₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ'; apply req_symcl; irevert W' ρ'; apply @subrel_tall_meet; clear -EQI.
    - iintro W'; iintro ρ'; iintro ρ₁'; iintro ρ₂'; iintro R; iintro EQρ'.
      apply subrel; iapply EQI; eassumption.
    - iintro W'; iintro ρ'; iintro ρ₁'; iintro ρ₂'; iintro R; iintro EQρ'.
      apply subrel; symmetry; iapply EQI; destruct EQρ'; split; eassumption.
  Qed.

  Lemma equiv_tall X κ
        (I₁ I₂ : ∀ W, (X [⇒] W) → K⟦ κ @ W ⟧ → Typ W) n
        (EQI : n ⊨ ∀ᵢ W (ρ : X [⇒] W) R, I₁ W ρ R ≈ᵂ I₂ W ρ R) : 
    n ⊨ ⟨tall_rel I₁⟩ᴿ ≈ᵂ ⟨tall_rel I₂⟩ᴿ.
  Proof.
    iintro W'; iintro ρ'; apply req_symcl; irevert W' ρ'; apply subrel_tall; clear -EQI.
    - iintro W'; iintro ρ'; iintro ρ₁'; iintro ρ₂'; iintro R.
      apply subrel; iapply EQI.
    - iintro W'; iintro ρ'; iintro ρ₁'; iintro ρ₂'; iintro R.
      apply subrel; symmetry; iapply EQI.
  Qed.
    
  (* instance universals *)
  Lemma subrel_lall_meet {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W)
        (I₁ : ∀ W, (X [⇒] W) → F.ltag → O.lvar W → Typ W)
        (I₂ : ∀ W, (Y [⇒] W) → F.ltag → O.lvar W → Typ W) n
        (EQI : n ⊨ ∀ᵢ W' (ρ : W [⇒] W') ρ₁' ρ₂' l a,
                   (subst_eq ρ₁' (subst_comp ρ ρ₁) ∧ subst_eq ρ₂' (subst_comp ρ ρ₂))ᵢ ⇒
                   I₁ W' ρ₁' l a ≾ᵂ I₂ W' ρ₂' l a) :
    n ⊨ lall_rel I₁ ⇑ ρ₁ ≾ᵂ lall_rel I₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ; iintro v₁; iintro v₂; iintro Hv.
    iintro W''; iintro ρ'; iintro ρ''; iintro l; iintro a; iintro EQρ.
    iapply @subrel_expr_cl; [reflexivity | iapply EQI | iapply Hv; reflexivity].
    rewrite EQρ; clear ρ'' EQρ; split; symmetry; iapply subst_comp_assoc.
  Qed.

  Lemma subrel_lall X (IR₁ IR₂ : ∀ W, (X [⇒] W) → F.ltag → O.lvar W → Typ W) n
        (HIR : n ⊨ ∀ᵢ W ρ l a, IR₁ W ρ l a ≾ᵂ IR₂ W ρ l a) :
    n ⊨ ⟨lall_rel IR₁⟩ᴿ ≾ᵂ ⟨lall_rel IR₂⟩ᴿ.
  Proof.
    iintro W; iintro ρ; iintro v₁; iintro v₂; iintro HH.
    iintro W''; iintro ρ'; iintro ρ''; iintro l; iintro a; iintro EQρ.
    iapply @subrel_expr_cl; [reflexivity | iapply HIR | iapply HH; assumption].
  Qed.

  Lemma equiv_lall_fwd {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W)
        (I₁ : ∀ W, (X [⇒] W) → F.ltag → O.lvar W → Typ W)
        (I₂ : ∀ W, (Y [⇒] W) → F.ltag → O.lvar W → Typ W) n
        (EQI : n ⊨ ∀ᵢ W' (ρ : W [⇒] W') ρ₁' ρ₂' l a,
                   (subst_eq ρ₁' (subst_comp ρ ρ₁) ∧ subst_eq ρ₂' (subst_comp ρ ρ₂))ᵢ ⇒
                   I₁ W' ρ₁' l a ≈ᵂ I₂ W' ρ₂' l a) :
    n ⊨ lall_rel I₁ ⇑ ρ₁ ≈ᵂ lall_rel I₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ'; apply req_symcl; irevert W' ρ'; apply @subrel_lall_meet; clear -EQI.
    - iintro W'; iintro ρ'; iintro ρ₁'; iintro ρ₂'; iintro l; iintro a; iintro EQρ'.
      apply subrel; iapply EQI; eassumption.
    - iintro W'; iintro ρ'; iintro ρ₁'; iintro ρ₂'; iintro l; iintro a; iintro EQρ'.
      apply subrel; symmetry; iapply EQI; destruct EQρ'; split; eassumption.
  Qed.

  Lemma equiv_lall X (IR₁ IR₂ : ∀ W, (X [⇒] W) → F.ltag → O.lvar W → Typ W) n
        (HIR : n ⊨ ∀ᵢ W ρ l a, IR₁ W ρ l a ≈ᵂ IR₂ W ρ l a) :
    n ⊨ ⟨lall_rel IR₁⟩ᴿ ≈ᵂ ⟨lall_rel IR₂⟩ᴿ.
  Proof.
    iintro W'; iintro ρ'; apply req_symcl; irevert W' ρ'; apply @subrel_lall; clear -HIR.
    - iintro W'; iintro ρ'; iintro ρ₁'; iintro ρ₂'; iintro l; iintro a.
      apply subrel; iapply HIR.
    - iintro W'; iintro ρ'; iintro ρ₁'; iintro ρ₂'; iintro l; iintro a.
      apply subrel; symmetry; iapply HIR.
  Qed.

  (* instance effects *)
  Lemma subrel_instance_meet {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W) (ν₁ : ℑ X) (ν₂ : ℑ Y) a₁ a₂ n
        (EQA : bind ρ₁ a₁ = bind ρ₂ a₂)
        (EQT : ℑ_ltag ν₁ = ℑ_ltag ν₂)
        (EQI : n ⊨ ν₁ ⇑ ρ₁ ≾ᵂ ν₂ ⇑ ρ₂) :
    n ⊨ (a₁ ⇝ ν₁) ⇑ ρ₁ ≾ᵂ (a₂ ⇝ ν₂) ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ; iintro e₁; iintro e₂; iintro ls; iintro μ₁; iintro μ₂; iintro EQμ.
    iintro HF; idestruct HF as v₁ HF; idestruct HF as v₂ HF; idestruct HF as HR HF.
    iexists v₁; iexists v₂; isplit; [destruct HR as [HR₁ [HR₂ HRF]]; subst |].
    + split; [congruence | split; [term_simpl; f_equal | congruence]].
      rewrite (bind_equiv (g := subst_comp ρ ρ₁)) by reflexivity;
        rewrite (bind_equiv (g := subst_comp ρ ρ₂)) by reflexivity.
      rewrite <- !bind_bind_comp'; congruence.
    + iapply EQI; [apply ToC_equiv, EQμ | apply HF].
  Qed.

  Lemma equiv_instance_fwd {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W) (ν₁ : ℑ X) (ν₂ : ℑ Y) a₁ a₂ n
        (EQA : bind ρ₁ a₁ = bind ρ₂ a₂)
        (EQT : ℑ_ltag ν₁ = ℑ_ltag ν₂)
        (EQI : n ⊨ ν₁ ⇑ ρ₁ ≈ᵂ ν₂ ⇑ ρ₂) :
    n ⊨ (a₁ ⇝ ν₁) ⇑ ρ₁ ≈ᵂ (a₂ ⇝ ν₂) ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ'; apply req_symcl.
    - iapply @subrel_instance_meet; [.. | apply subrel]; assumption.
    - iapply @subrel_instance_meet; [.. | apply subrel]; symmetry; assumption.
  Qed.

  Lemma equiv_instance {X} (ν₁ ν₂ : ℑ X) a n (EQν : n ⊨ ν₁ ≈ᴵ ν₂) :
    n ⊨ a ⇝ ν₁ ≈ᵂ a ⇝ ν₂.
  Proof.
    etransitivity; [apply fwd_pure_id | etransitivity; [| symmetry; apply fwd_pure_id]].
    idestruct EQν as EQν EQT; apply equiv_instance_fwd; [reflexivity | assumption |].
    etransitivity; [symmetry; apply fwd_pure_id | etransitivity; [| apply fwd_pure_id]]; assumption.
  Qed.
  
  (* pure effect *)
  Lemma effect_sub_pure X (I : Eff X) n :  n ⊨ ι ≾ᵂ I.
  Proof.
    repeat iintro; contradiction.
  Qed.

  Lemma equiv_pure_fwd {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W) n :
    n ⊨ ι ⇑ ρ₁ ≈ᵂ ι ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ'; isplit; unfold fwd_R; simpl; repeat iintro; assumption.
  Qed.

  (* effect composition *)
  Lemma subrel_cons_meet {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W) (IA₁ IA₂ : Eff X) (IB₁ IB₂ : Eff Y) n
        (HE₁ : n ⊨ IA₁ ⇑ ρ₁ ≾ᵂ IB₁ ⇑ ρ₂)
        (HE₂ : n ⊨ IA₂ ⇑ ρ₁ ≾ᵂ IB₂ ⇑ ρ₂) :
    n ⊨ IA₁ · IA₂ ⇑ ρ₁ ≾ᵂ IB₁ · IB₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ; iintro e₁; iintro e₂; iintro ls; iintro μ₁; iintro μ₂; iintro EQμ.
    iintro HF; idestruct HF as HF HF; [ileft | iright].
    - iapply HE₁; [eassumption | apply HF].
    - iapply HE₂; [eassumption | apply HF].
  Qed.
    
  Lemma subrel_cons X (IA₁ IA₂ IB₁ IB₂ : Eff X) n
        (HE₁ : n ⊨ IA₁ ≾ᵂ IB₁)
        (HE₂ : n ⊨ IA₂ ≾ᵂ IB₂) :
    n ⊨ IA₁ · IA₂ ≾ᵂ IB₁ · IB₂.
  Proof.
    iintro W; iintro ρ; iintro e₁; iintro e₂; iintro ls; iintro μ₁; iintro μ₂; iintro EQμ.
     iintro HH; idestruct HH as HH HH; [ileft; iapply HE₁ | iright; iapply HE₂]; eassumption.
  Qed.

  Lemma equiv_cons_fwd {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W) (IA₁ IA₂ : Eff X) (IB₁ IB₂ : Eff Y) n
        (HE₁ : n ⊨ IA₁ ⇑ ρ₁ ≈ᵂ IB₁ ⇑ ρ₂)
        (HE₂ : n ⊨ IA₂ ⇑ ρ₁ ≈ᵂ IB₂ ⇑ ρ₂) :
    n ⊨ IA₁ · IA₂ ⇑ ρ₁ ≈ᵂ IB₁ · IB₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ'; apply req_symcl.
    - iapply @subrel_cons_meet; apply subrel; assumption.
    - iapply @subrel_cons_meet; apply subrel; symmetry; assumption.
  Qed.

  Lemma equiv_cons X (IA₁ IA₂ IB₁ IB₂ : Eff X) n
        (HE₁ : n ⊨ IA₁ ≈ᵂ IB₁)
        (HE₂ : n ⊨ IA₂ ≈ᵂ IB₂) :
    n ⊨ IA₁ · IA₂ ≈ᵂ IB₁ · IB₂.
  Proof.
    etransitivity; [apply fwd_pure_id | etransitivity; [| symmetry; apply fwd_pure_id]].
    apply equiv_cons_fwd; (etransitivity; [symmetry; apply fwd_pure_id | etransitivity; [| apply fwd_pure_id]]); assumption.
  Qed.
  
  Lemma effect_equiv_consC X (I₁ I₂ : Eff X) n :
    n ⊨ I₁ · I₂ ≈ᵂ I₂ · I₁.
  Proof.
    apply wiequiv_symcl; iintro W; iintro ρ; iintro e₁; iintro e₂; iintro ls;
      iintro μ₁; iintro μ₂; iintro EQμ; (iintro HH; idestruct HH as HH HH; [iright | ileft]).
    - iapply (ext_ord (R := I₁)); [reflexivity | eassumption ..].
    - iapply (ext_ord (R := I₂)); [reflexivity | eassumption ..].
    - iapply (ext_ord (R := I₂)); [reflexivity | eassumption ..].
    - iapply (ext_ord (R := I₁)); [reflexivity | eassumption ..].
  Qed.

  Lemma effect_equiv_consA X (I₁ I₂ I₃ : Eff X) n :
    n ⊨  (I₁ · I₂) · I₃ ≈ᵂ I₁ · (I₂ · I₃).
  Proof.
    apply wiequiv_symcl; iintro W; iintro ρ; iintro e₁; iintro e₂; iintro ls;
      iintro μ₁; iintro μ₂; iintro EQμ; iintro HH.
    - idestruct HH as HH HH; [idestruct HH as HH HH |];
        [ileft; iapply (ext_ord (R := I₁)) | iright; ileft; iapply (ext_ord (R := I₂)) |
         iright; iright; iapply (ext_ord (R := I₃))]; eassumption || reflexivity.
    - idestruct HH as HH HH; [| idestruct HH as HH HH];
        [ileft; ileft; iapply (ext_ord (R := I₁)) | ileft; iright; iapply (ext_ord (R := I₂))
         | iright; iapply (ext_ord (R := I₃))]; eassumption || reflexivity.
  Qed.

  Lemma effect_equiv_cons_pureIL X (I : Eff X) n :
    n ⊨ I ≈ᵂ  ⟨ pure_eff (X := X), 0 ⟩ᴱ · I.
  Proof.
    apply wiequiv_symcl; iintro W; iintro ρ; iintro e₁; iintro e₂; iintro ls;
      iintro μ₁; iintro μ₂; iintro EQμ; iintro HH;
        [iright | idestruct HH as HH HH; [simpl in HH; isimplP; contradiction |]];
        iapply (ext_ord (R := I)); eassumption || reflexivity.
  Qed.

  Lemma effect_equiv_cons_pureIR X (I : Eff X) n :
    n ⊨ I ≈ᵂ I · ⟨ pure_eff (X := X), 0 ⟩ᴱ .
  Proof.
    apply wiequiv_symcl; iintro W; iintro ρ; iintro e₁; iintro e₂; iintro ls;
      iintro μ₁; iintro μ₂; iintro EQμ; iintro HH;
        [ileft | idestruct HH as HH HH; [| simpl in HH; isimplP; contradiction]];
        iapply (ext_ord (R := I)); eassumption || reflexivity.
  Qed.

  Lemma effect_equiv_consI X (I : Eff X) n :
    n ⊨ I ≈ᵂ I · I.
  Proof.
    apply wiequiv_symcl; iintro W; iintro ρ; iintro e₁; iintro e₂; iintro ls;
      iintro μ₁; iintro μ₂; iintro EQμ; iintro HH; [iright | idestruct HH as HH HH];
        iapply (ext_ord (R := I)); eassumption || reflexivity.
  Qed.

  (* XXX: move *)
  Lemma fwd_comp {sig ord W₁ W₂ W₃} {RC : RetractCore sig} {RO : ROrd sig ord} {RP : ROPreo sig}
        (μ : WI_ext sig W₁) (ρ₁ : W₁ [⇒] W₂) (ρ₂ : W₂ [⇒] W₃) n :
    n ⊨ μ ⇑ subst_comp ρ₂ ρ₁ ≈ᵂ μ ⇑ ρ₁ ⇑ ρ₂.
  Proof.
    iintro W; iintro ρ; unfold fwd_R; simpl; apply ext_eq.
    intros x; term_simpl; apply bind_bind_comp; intros y; reflexivity.
  Qed.
  
  (* operation signatures *)
  Lemma subrel_sig_meet {X Y W}  (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W)
        (IA₁ IR₁ : Typ X) (IA₂ IR₂ : Typ Y) n
        (SA : n ⊨ IA₁ ⇑ ρ₁ ≾ᵂ IA₂ ⇑ ρ₂)
        (SR : n ⊨ IR₁ ⇑ ρ₁ ≈ᵂ IR₂ ⇑ ρ₂) :
    n ⊨ IA₁ ⇒ᵈ IR₁ ⇑ ρ₁ ≾ᵂ IA₂ ⇒ᵈ IR₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ'; iintro v₁; iintro v₂; iintro μ₁; iintro μ₂; iintro EQμ; iintro HH.
    idestruct HH as HA HR; isplit.
    - iapply SA; apply HA.
    - etransitivity; [symmetry; exact EQμ |]; etransitivity; [exact HR |].
      etransitivity; [apply fwd_comp | etransitivity; [| symmetry; apply fwd_comp]].
      apply fw_eq; assumption.
  Qed.

  Lemma equiv_sig_fwd {X Y W} (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W)
        (IA₁ IR₁ : Typ X) (IA₂ IR₂ : Typ Y) n
        (EQA : n ⊨ IA₁ ⇑ ρ₁ ≈ᵂ IA₂ ⇑ ρ₂)
        (EQR : n ⊨ IR₁ ⇑ ρ₁ ≈ᵂ IR₂ ⇑ ρ₂) :
    n ⊨ IA₁ ⇒ᵈ IR₁ ⇑ ρ₁ ≈ᵂ IA₂ ⇒ᵈ IR₂ ⇑ ρ₂.
  Proof.
    iintro W'; iintro ρ; apply req_symcl.
    - iapply @subrel_sig_meet; [apply subrel |]; assumption.
    - iapply @subrel_sig_meet; [apply subrel |]; symmetry; assumption.
  Qed.

  Lemma equiv_sig X (IA₁ IR₁ IA₂ IR₂ : Typ X) n
        (EQA : n ⊨ IA₁ ≈ᵂ IA₂)
        (EQR : n ⊨ IR₁ ≈ᵂ IR₂) :
    n ⊨ IA₁ ⇒ᵈ IR₁ ≈ᵂ IA₂ ⇒ᵈ IR₂.
  Proof.
    etransitivity; [apply fwd_pure_id | etransitivity; [| symmetry; apply fwd_pure_id]].
    apply equiv_sig_fwd; (etransitivity; [symmetry; apply fwd_pure_id | etransitivity; [| apply fwd_pure_id]]); assumption.
  Qed.

End Properties.
