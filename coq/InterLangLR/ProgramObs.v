Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Fresh Lang.Open.
Require Import Relation_Operators.

Definition obs_sig := (F.expr0 →ₛ O.expr0 →ₛ *ₛ)%irel_sig.

Definition F_Obs (R : IRel obs_sig) (eᶠ : F.expr0) (eᵒ : O.expr0) : IProp :=
  (eᶠ = F.v_unit ∧ OR.red_rtc eᵒ O.v_unit)ᵢ
  ∨ᵢ (FR.red_rtc eᶠ F.v_unit ∧ eᵒ = O.v_unit)ᵢ
  ∨ᵢ ∃ᵢ eᶠ' eᵒ', (FR.red eᶠ eᶠ' ∧ OR.red eᵒ eᵒ')ᵢ ∧ᵢ ▷R eᶠ' eᵒ'.

Lemma F_Obs_contractive : contractive obs_sig F_Obs.
Proof.
  intros R₁ R₂ n; iintro HR; iintro eᶠ; iintro eᵒ.
  unfold F_Obs; auto_contr.
  iespecialize HR; exact HR.
Qed.

Definition Obs := I_fix obs_sig F_Obs F_Obs_contractive.
Arguments Obs (eᶠ%fsyn) (eᵒ%osyn) : rename.

Lemma Obs_roll (n : nat) eᶠ eᵒ
      (HO : n ⊨ F_Obs Obs eᶠ eᵒ) :
  n ⊨ Obs eᶠ eᵒ.
Proof.
  unfold Obs; apply (I_fix_roll obs_sig); assumption.
Qed.

Lemma Obs_unroll (n : nat) eᶠ eᵒ
      (HO : n ⊨ Obs eᶠ eᵒ) :
  n ⊨ F_Obs Obs eᶠ eᵒ.
Proof.
  apply (I_fix_unroll obs_sig); assumption.
Qed.

Lemma Obs_red_both eᶠ₁ eᶠ₂ eᵒ₁ eᵒ₂ (n : nat)
      (HRᶠ : FR.red eᶠ₁ eᶠ₂)
      (HRᵒ : OR.red eᵒ₁ eᵒ₂)
      (HO  : n ⊨ ▷ Obs eᶠ₂ eᵒ₂) :
  n ⊨ Obs eᶠ₁ eᵒ₁.
Proof.
  apply Obs_roll; iright; iright; iexists eᶠ₂; iexists eᵒ₂; isplit; [split |]; assumption.
Qed.

Lemma Obs_red_l eᶠ₁ eᶠ₂ eᵒ (n : nat)
      (HR : FR.red eᶠ₁ eᶠ₂)
      (HO : n ⊨ Obs eᶠ₂ eᵒ) :
  n ⊨ Obs eᶠ₁ eᵒ.
Proof.
  irevert eᶠ₁ eᶠ₂ eᵒ HR HO; loeb_induction; iintro eᶠ₁; iintro eᶠ₂; iintro eᵒ; iintro HR; iintro HO.
  apply Obs_roll; apply Obs_unroll in HO; idestruct HO as HO HO; [| idestruct HO as HO HO].
  - destruct HO as [HEq HRᵒ]; subst; inversion_clear HRᵒ; clear IH.
    + iright; ileft; now eauto using clos_refl_trans_1n.
    + iright; iright; iexists (F.v_unit : F.expr0); iexists y; isplit; [now split |].
      later_shift; apply Obs_roll; ileft; now trivial.
  - destruct HO as [HRᶠ HEq]; subst.
    iright; ileft; now eauto using clos_refl_trans_1n.
  - idestruct HO as eᶠ₃ HO; idestruct HO as eᵒ₂ HO; idestruct HO as HH HO.
    destruct HH as [HRᶠ HRᵒ]; eapply Obs_unroll, Obs_red_both; [eassumption .. | later_shift].
    iapply IH; isimplP; eassumption.
Qed.

Lemma Obs_red_r eᶠ eᵒ₁ eᵒ₂ (n : nat)
      (HR : OR.red eᵒ₁ eᵒ₂)
      (HO : n ⊨ Obs eᶠ eᵒ₂) :
  n ⊨ Obs eᶠ eᵒ₁.
Proof.
  irevert eᶠ eᵒ₁ eᵒ₂ HR HO; loeb_induction; iintro eᶠ; iintro eᵒ₁; iintro eᵒ₂; iintro HR; iintro HO.
  apply Obs_roll; apply Obs_unroll in HO; idestruct HO as HO HO; [| idestruct HO as HO HO].
  - destruct HO as [HEq HRᵒ]; subst.
    ileft; now eauto using clos_refl_trans_1n.
  - destruct HO as [HRᶠ HEq]; subst; inversion_clear HRᶠ; clear IH.
    + ileft; now eauto using clos_refl_trans_1n.
    + iright; iright; iexists y; iexists (O.v_unit : O.expr0); isplit; [now trivial |].
      later_shift; apply Obs_roll; iright; ileft; now trivial.
  - idestruct HO as eᶠ₂ HO; idestruct HO as eᵒ₃ HO; idestruct HO as HH HO.
    destruct HH as [HRᶠ HRᵒ]; eapply Obs_unroll, Obs_red_both; [eassumption .. | later_shift].
    iapply IH; eassumption.
Qed.
