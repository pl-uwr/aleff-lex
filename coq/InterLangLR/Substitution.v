Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Env Binding.Set Binding.Product.
Require Import Types Kinding.
Require Import Lang.Fresh Lang.Open.
Require Import InterLangLR.Core InterLangLR.KindEquiv.
Require Import InterLangLR.KindingIrrelevance.
Require Import InterLangLR.Weakening.

Lemma type_interp_bind_var {A X W : Set} (Δ : A → kind) (τ : typ_ A X)
      (ρ : X [⇒] W) (η : ∀ α : A, K⟦ Δ α @ W⟧) (ϑ : X → ℑ W)
      κ₁ κ₂ (Heq : κ₁ = κ₂) (I : K⟦ κ₁ @ W⟧)
      {WF₁ : K[ Δ; X ⊢ τ ∷ κ₁ ]}
      {WF₂ : K[ Δ; X ⊢ τ ∷ κ₂ ]}
      n :
  n ⊨ K⟦ I ≡ ⟦ τ ∷ κ₁ ⟧ ρ ! η ! ϑ ∷ κ₁ ⟧ →
  n ⊨ K⟦ eq_rect κ₁ (λ κ, K⟦ κ @ W⟧) I κ₂ Heq ≡ ⟦ τ ∷ κ₂ ⟧ ρ ! η ! ϑ ∷ κ₂ ⟧.
Proof.
  intro Hκ₁.
  destruct κ₁; destruct κ₂; try discriminate; rewrite (kind_eq_proofs_unicity Heq eq_refl);
    simpl eq_rect; (etransitivity; [eassumption | apply type_interp_irr]).
Qed.

Fixpoint type_interp_bind (κ : kind) {A B X Y W : Set}
    (Φ : ⟨A, X⟩ {t⇒} ⟨B, Y⟩)
    {Δ₁ : A → kind} {Δ₂ : B → kind}
    (τ  : typ_ A X)
    (ρ₁ : X [⇒] W) (ρ₂ : Y [⇒] W)
    (η₁ : ∀ α : A, K⟦ Δ₁ α @ W⟧) (η₂ : ∀ α : B, K⟦ Δ₂ α @ W⟧)
    (ϑ₁ : X → ℑ W) (ϑ₂ : Y → ℑ W)
    {WF₁ : K[ Δ₁ ; X ⊢ τ ∷ κ ]}
    {WF₂ : K[ Δ₂ ; Y ⊢ bind Φ τ ∷ κ ]}
    {WFΦ : wf_subst Δ₁ Δ₂ Φ}
    n {struct τ} :
  subst_eq ρ₁ (subst_comp ρ₂ (of_arrow (Build_Arr _ _ (tsub_lbl Φ)))) →
  n ⊨ (∀ᵢ α, K⟦ η₁ α ≡ ⟦ tsub_var Φ α ∷ Δ₁ α ⟧ ρ₂ ! η₂ ! ϑ₂ ∷ (Δ₁ α) ⟧) →
  n ⊨ (∀ᵢ a, ϑ₁ a ≈ᴵ ϑ₂ (tsub_lbl Φ a)) →
  n ⊨ K⟦ ⟦ τ ∷ κ ⟧ ρ₁ ! η₁ ! ϑ₁ ≡ ⟦ bind Φ τ ∷ κ ⟧ ρ₂ ! η₂ ! ϑ₂ ∷ κ ⟧.
Proof.
  intros EQρ Hη Hϑ; destruct τ as [| α | | | | l | | | |]; invert_kind WF₁ as WF; simpl type_interp;
    [reflexivity | | unfold kind_equiv_upto ..].
  - eapply type_interp_bind_var; etransitivity; [iapply Hη |].
    apply type_interp_k_irr; [| iintro .. |]; reflexivity.
  - apply @equiv_arr.
    + symmetry; eapply (type_interp_bind k_type); eassumption.
    + eapply (type_interp_bind k_type); eassumption.
    + eapply (type_interp_bind k_effect); eassumption.
  - apply equiv_tall; iintro W'; iintro ρ'; iintro R.
    eapply (type_interp_bind k_type).
    + intros x; term_simpl; now rewrite EQρ.
    + iintro α; destruct α as [| α]; simpl; [reflexivity | unfold fwdTEnv at 1].
      etransitivity; [apply fwd_kint_equiv; [reflexivity | iapply Hη] |].
      etransitivity; [apply tint_fwd_equiv | etransitivity; [symmetry; apply fwd_kint_pure |]].
      apply type_interp_shiftT.
    + iintro a; ispecialize Hϑ a; idestruct Hϑ as HR HT; unfold fwdIEnv; isplit; [| assumption].
      simpl; apply fw_eq; assumption.
  - apply equiv_lall; iintro W'; iintro ρ'; iintro l; iintro a; eapply (type_interp_bind k_type).
    + intros [| x]; term_simpl; [| rewrite EQρ]; reflexivity.
    + iintro α; simpl.
      assert (EQρ' : subst_eq ρ' (subst_comp (subst_comp (mk_subst a) (liftS ρ')) (of_arrow mk_shift))).
      { intros x; term_simpl; change (fmap {| apply_arr := VS |} (ρ' x)) with (shift (ρ' x)).
        symmetry; apply subst_shift_id. }
      etransitivity; [apply fwd_kint_equiv; [exact EQρ' | iapply Hη] |].
      etransitivity; [apply fwd_kint_comp |].
      etransitivity; [apply fwd_kint_equiv;
                      [reflexivity | apply type_interp_shiftL with (I := ⟨Σ⟦ τ1 ⟧ ρ₁ ! η₁ ! ϑ₁, l⟩ᴵ)] |].
      etransitivity; [apply tint_fwd_equiv | etransitivity; [symmetry; apply fwd_kint_pure |]].
      apply type_interp_k_irr; [.. | reflexivity].
      * intros [| y]; term_simpl; [reflexivity |].
        unfold subst; rewrite !map_to_bind, !bind_bind_comp'; apply bind_equiv.
        intros x; term_simpl; unfold subst; rewrite map_to_bind, !bind_bind_comp'; apply bind_equiv.
        intros z; term_simpl; reflexivity.
      * iintro β; unfold shiftTEnv, fwdTEnv; etransitivity; [symmetry; apply fwd_kint_comp |].
        apply fwd_kint_equiv; [now symmetry | reflexivity].
      * iintro y; destruct y as [| y]; (isplit; simpl; [| reflexivity]).
        -- etransitivity; [symmetry; apply fwd_comp |].
           etransitivity; [apply fwd_equiv; symmetry; apply EQρ' |].
           eapply fw_eq, (type_interp_bind k_sig); eassumption.
        -- etransitivity; [symmetry; apply fwd_comp |].
           apply fwd_equiv; symmetry; apply EQρ'.
    + iintro x; destruct x as [| x]; unfold fwdIEnv; [isplit; simpl; [| reflexivity] |].
      * eapply fw_eq, (type_interp_bind k_sig); eassumption.
      * ispecialize Hϑ x; idestruct Hϑ as HR HS; isplit; simpl; [apply fw_eq |]; assumption.
  - rewrite EQρ; apply equiv_instance; iapply Hϑ.
  - reflexivity.
  - apply equiv_cons; eapply (type_interp_bind k_effect); eassumption.
  - apply equiv_sig; eapply (type_interp_bind k_type); eassumption.
  - reflexivity.
Qed.

Lemma type_interp_substL {A X W : Set} {Δ : A → kind} κ
      (τ : typ_ A (inc X))
      (ρ : X [⇒] W) (η : ∀ α, K⟦ Δ α @ W ⟧) (ϑ : X → ℑ W) (I : ℑ W)
      {WFτ : K[Δ; inc X ⊢ τ ∷ κ]}
      (a : X) n
      (HI : n ⊨ I ≈ᴵ ϑ a) :
  n ⊨ K⟦ ⟦ τ ∷ κ ⟧ (subst_comp ρ (mk_subst (O.lclosed a))) ! η ! (ϑ ▹ I) ≡
         ⟦ subst τ a ∷ κ ⟧ ρ ! η ! ϑ ∷ κ ⟧.
Proof.
  apply type_interp_bind with _.
  - intros [| x]; reflexivity.
  - iintro αl; simpl; apply rel_equiv_var with (Heq := eq_refl) (Heq₁ := eq_refl); reflexivity.
  - iintro b; destruct b as [| b]; simpl; [assumption | reflexivity].
Qed.

Lemma type_interp_substT {A X W: Set} {Δ : A → kind} κ κ'
      (τ : typ_ (inc A) X) (σ : typ_ A X)
      (ρ : X [⇒] W) (η : ∀ α, K⟦ Δ α @ W ⟧) (ϑ : X → ℑ W)
      {WFτ : K[Δ ▹ κ'; X ⊢ τ ∷ κ]}
      {WFσ : K[Δ; X ⊢ σ ∷ κ']}
      R n
      (HR : n ⊨ K⟦ R ≡ ⟦ σ ∷ κ' ⟧ ρ ! η ! ϑ ∷ κ' ⟧) :
  n ⊨ K⟦ ⟦ τ ∷ κ ⟧ ρ ! η :▹ R ! ϑ ≡ ⟦ subst τ σ ∷ κ ⟧ ρ ! η ! ϑ ∷ κ ⟧.
Proof.
  apply type_interp_bind with _.
  - intros x; reflexivity.
  - iintro α; destruct α as [| α]; simpl.
    + etransitivity; [eassumption | apply type_interp_irr].
    + apply rel_equiv_var with (Heq := eq_refl) (Heq₁ := eq_refl); reflexivity.
  - iintro b; simpl; reflexivity.
Qed.

Lemma rel_v_substL {A X W : Set} {Δ : A → kind}
      (τ : typ_ A (inc X))
      (ρ : X [⇒] W) (η : ∀ α, K⟦ Δ α @ W ⟧) (ϑ : X → ℑ W) (I : ℑ W)
      {WFτ : K[Δ; inc X ⊢ τ ∷ k_type]}
      (a : X) W' ρ' v₁ v₂ n
      (HI : n ⊨ I ≈ᴵ ϑ a) :
        n ⊨ wi_rel (⟦ τ ⟧ (subst_comp ρ (mk_subst (O.lclosed a))) ! η ! (ϑ ▹ I)) W' ρ' v₁ v₂ ⇔
          wi_rel (⟦ subst τ a ⟧ ρ ! η ! ϑ) W' ρ' v₁ v₂.
Proof.
  apply apply_type_equiv, type_interp_substL, HI.
Qed.
