Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Product Binding.Set Binding.Env.
Require Import Types Kinding.
Require Import Lang.Fresh Lang.Open.
Require Export InterLangLR.Domains InterLangLR.Closures InterLangLR.Constructions.

Definition apply_to_t_var {A X W : Set} {Δ : A → kind}
  (η : ∀ α : A, K⟦ Δ α @ W ⟧)
  (α : A) (κ : kind)
  (WF : K[ Δ ; X ⊢ t_var (α : s_var ⟨A, X⟩) ∷ κ ]) :
    K⟦ κ @ W⟧ :=
  eq_rect _ (λ κ, K⟦ κ @ W⟧) (η α) _ (K_Var_inv WF).

Definition extend_tvar_interp {TV LV : Set} {Δ : TV → kind} {κ : kind}
  (η : ∀ α : TV, K⟦ Δ α @ LV⟧) (R : K⟦ κ @ LV⟧) (α : inc TV) : K⟦ (Δ ▹ κ) α @ LV⟧ :=
  match α with
  | VZ   => R
  | VS α => η α
  end.

Definition shift_kint {X κ} := @fwd_kint X (inc X) (of_arrow mk_shift) κ.
Definition shift_inst {LV} (ν : ℑ LV) : ℑ (inc LV) :=
  ⟨⟨fwd_R ν (of_arrow mk_shift)⟩ᵂ, ℑ_ltag ν⟩ᴵ.

Definition fwdTEnv {A W₁ W₂ : Set} {Δ : A → kind} (ρ : W₁ [⇒] W₂) (η : ∀ α : A, K⟦ Δ α @ W₁ ⟧)
  : ∀ α : A, K⟦ Δ α @ W₂ ⟧ := λ α, fwd_kint ρ (η α).
Definition fwdIEnv {X W₁ W₂ : Set} (ρ : W₁ [⇒] W₂) (ϑ : X → ℑ W₁) : X → ℑ W₂ :=
  λ a, ⟨⟨ϑ a ⇑ ρ⟩ᵂ, ℑ_ltag (ϑ a)⟩ᴵ.

Definition shiftTEnv {A W : Set} {Δ : A → kind} (η : ∀ α : A, K⟦ Δ α @ W⟧) :
  ∀ α : A, K⟦ Δ α @ inc W⟧ := fwdTEnv (of_arrow mk_shift) η.
Definition shiftIEnv {X W : Set} (ϑ : X → ℑ W) : X → ℑ (inc W) :=
  fwdIEnv (of_arrow mk_shift) ϑ.

Notation "η :▹ R" := (extend_tvar_interp η R)
  (at level 40, R at next level, left associativity).
Local Open Scope irel_scope.
Local Open Scope sem_scope.

Program Fixpoint type_interp {A X : Set} {Δ : A → kind}
  (τ : typ_ A X) (κ : kind) {W} (ρ : X [⇒] W)
  (η : ∀ α : A, K⟦ Δ α @ W⟧)
  (ϑ : X → ℑ W) : K[ Δ ; X ⊢ τ ∷ κ] → K⟦ κ @ W⟧ :=
  match τ return K[ Δ ; X ⊢ τ ∷ κ] → K⟦ κ @ W ⟧ with
  | t_var α => apply_to_t_var η α κ
  | t_unit  =>
    match κ with
    | k_type => λ WF, ()ᵗ
    | k_effect => λ _, ι
    | k_sig    => λ _, ∅ᵂ
    end
  | t_arrow τ₁ ε τ₂ =>
    match κ return K[ Δ ; X ⊢ _ ∷ κ] → K⟦ κ @ W⟧ with
    | k_type => λ WF,
      (type_interp τ₁ _ ρ η ϑ (K_Arrow_inv1 WF)) →[type_interp ε _ ρ η ϑ (K_Arrow_inv2 WF)] (type_interp τ₂  _ ρ η ϑ (K_Arrow_inv3 WF))
    | k_effect => λ _, ι
    | k_sig    => λ _, ∅ᵂ
    end
  | t_forallT κ₀ τ =>
    match κ return K[ Δ ; X ⊢ _ ∷ κ] → K⟦ κ @ W⟧ with
    | k_type => λ WF,
      ⟨tall_rel (λ W' ρ' R, type_interp τ _ (subst_comp ρ' ρ) (fwdTEnv ρ' η :▹ R) (fwdIEnv ρ' ϑ) (K_ForallT_inv WF))⟩ᵂ
    | k_effect => λ _, ι
    | k_sig    => λ _, ∅ᵂ
    end
  | t_forallL σ τ => 
    match κ return K[ Δ ; X ⊢ _ ∷ κ] → K⟦ κ @ W⟧ with
    | k_type => λ WF,
      ⟨lall_rel (λ W' ρ' l a, type_interp τ _ (subst_comp (mk_subst a) (liftS (subst_comp ρ' ρ))) (fwdTEnv ρ' η) (fwdIEnv ρ' (ϑ ▹ ⟨type_interp σ _ ρ η ϑ (K_ForallL_inv1 WF), l⟩ᴵ)) (K_ForallL_inv2 WF))⟩ᵂ
    | k_effect => λ _, ι
    | k_sig    => λ _, ∅ᵂ
    end
  | t_label a =>
    match κ return K[ Δ ; X ⊢ _ ∷ κ] → K⟦ κ @ W⟧ with
    | k_effect => λ WF, ρ a ⇝ (ϑ a)
    | k_type => λ _, ∅ᵂ
    | k_sig  => λ _, ∅ᵂ
    end
  | t_pure =>
    match κ return K[ Δ ; X ⊢ _ ∷ κ] → K⟦ κ @ W⟧ with
    | k_effect => λ WF, ι
    | k_type => λ _, ∅ᵂ
    | k_sig  => λ _, ∅ᵂ
    end
  | t_cons ε₁ ε₂ =>
    match κ return K[ Δ ; X ⊢ _ ∷ κ] → K⟦ κ @ W⟧ with
    | k_effect => λ WF, (type_interp ε₁ _ ρ η ϑ (K_Cons_inv1 WF)) · (type_interp ε₂ _ ρ η ϑ (K_Cons_inv2 WF))
    | k_type => λ _, ∅ᵂ
    | k_sig  => λ _, ∅ᵂ
    end
  | t_opsig τ₁ τ₂ =>
    match κ return K[ Δ ; X ⊢ _ ∷ κ] → K⟦ κ @ W⟧ with
    | k_sig    => λ WF, (type_interp τ₁ _ ρ η ϑ (K_OpSig_inv1 WF)) ⇒ᵈ (type_interp τ₂ _ ρ η ϑ (K_OpSig_inv2 WF))
    | k_type   => λ _, ∅ᵂ
    | k_effect => λ _, ι
    end
  | t_allTS κ₀ τ =>
    match κ return K[ Δ ; X ⊢ _ ∷ κ] → K⟦ κ @ W⟧ with
    | k_sig  => λ _, ∅ᵂ
    | k_type => λ _, ∅ᵂ
    | k_effect => λ _, ι
    end
  end.

(*Arguments type_interp {TV LV Δ} τ κ η ϑ WF v*)

Notation "'⟦' τ '∷' κ '⟧' ρ '!' η '!' ϑ" := (type_interp τ κ ρ η ϑ _) (at level 0).
Notation "'⟦' τ '⟧' ρ '!' η '!' ϑ" := (⟦ τ ∷ k_type ⟧ ρ ! η ! ϑ) (at level 0).
Notation "'F⟦' ε '⟧' ρ '!' η '!' ϑ" := (⟦ ε ∷ k_effect ⟧ ρ ! η ! ϑ) (at level 0).
Notation "'Σ⟦' σ '⟧' ρ '!' η '!' ϑ" := (⟦ σ ∷ k_sig ⟧ ρ ! η ! ϑ) (at level 0).
Notation "'E⟦' τ / ε '⟧' ρ '!' η '!' ϑ" :=
  (rel_expr_cl (F⟦ ε ⟧ ρ ! η ! ϑ) (⟦ τ ⟧ ρ ! η ! ϑ)) (at level 0, τ at level 30).
Notation "'K⟦' τ / ε '⟧' ρ ! η '!' ϑ" :=
  (rel_ectx_cl (F⟦ ε ⟧ ρ ! η ! ϑ) (⟦ τ ⟧ ρ ! η ! ϑ)) (at level 0, τ at level 30).
Notation "'S⟦' τ / ε '⟧' ρ ! η '!' ϑ" :=
  (rel_stuck_cl (F⟦ ε ⟧ ρ ! η ! ϑ) (⟦ τ ⟧ ρ ! η ! ϑ)) (at level 0, τ at level 30).
Notation "'E⟦' τ₁ ⊢ τ / ε ⟧ ρ ! η ! ϑ" :=
  (rel_expr_cl1 (⟦ τ₁ ⟧ ρ ! η ! ϑ)  (E⟦ τ / ε ⟧ ρ ! η ! ϑ)) (at level 0, τ₁, τ at level 30).

Definition rel_t {A X W : Set} {Δ : A → kind}
  (Θ : X → typ_ A X) (ρ : X [⇒] W) (η : ∀ α : A, K⟦ Δ α @ W⟧)
  {WF : ∀ a, K[ Δ; X ⊢ Θ a ∷ k_sig]}
  (ϑ : X → ℑ W) : IProp :=
  ∀ᵢ a, ϑ a ≈ᵂ Σ⟦ Θ a ⟧ ρ ! η ! ϑ.

Notation "'Θ⟦' Θ '⟧' ρ '!' η '!' ϑ" := (rel_t Θ ρ η ϑ) (at level 0).

Definition rel_d {X V W W' : Set} (ρ : X [⇒] W) (ϑ : X → ℑ W') (γ₁ : F.sub ⟨V, X⟩ ⟨∅, ∅⟩) (γ₂ : O.sub ⟨V, X⟩ ⟨∅, W⟩) :=
  (∀ a, F.sub_lbl γ₁ a = F.l_tag (ℑ_ltag (ϑ a))) ∧ (subst_eq (O.sub_lbl γ₂) ρ).

Definition rel_g {A X V W : Set} {Δ : A → kind}
  (Γ : V → typ_ A X)
  (ρ : X [⇒] W)
  (η : ∀ α : A, K⟦ Δ α @ W⟧)
  (ϑ : X → ℑ W)
  {WF : K[ Δ; X ⊢ᴱ Γ]}
  W' ρ'
  (γ₁ : F.sub ⟨V, X⟩ ⟨∅, ∅⟩)
  (γ₂ : O.sub ⟨V, X⟩ ⟨∅, W'⟩) :=
  ∀ᵢ x, wi_rel (⟦ Γ x ⟧ ρ ! η ! ϑ) W' ρ' (F.sub_var γ₁ x) (O.sub_var γ₂ x).

Notation "'G⟦' Γ '⟧' ρ ! η '!' ϑ" := (rel_g Γ ρ η ϑ) (at level 0).

Definition rel_h {A X W : Set} {Δ : A → kind}
    σ τ ε
    {WFσ : K[ Δ ; X ⊢ σ ∷ k_sig    ]}
    {WFε : K[ Δ ; X ⊢ ε ∷ k_effect ]}
    {WFτ : K[ Δ ; X ⊢ τ ∷ k_type   ]}
    (ρ : X [⇒] W)
    (η : ∀ a, K⟦ Δ a @ W ⟧)
    (ϑ : X → ℑ W) : Rhandler W :=
  rel_handler_cl (Σ⟦ σ ⟧ ρ ! η ! ϑ)
    (λ W' ρ' I, I →[F⟦ ε ⟧ (subst_comp ρ' ρ) ! (fwdTEnv ρ' η) ! (fwdIEnv ρ' ϑ)] (⟦ τ ⟧ (subst_comp ρ' ρ) ! (fwdTEnv ρ' η) ! (fwdIEnv ρ' ϑ)))
    ⟨E⟦ τ / ε ⟧ ρ ! η ! ϑ⟩ᵂ.

Notation "'H⟦' σ @ τ / ε ⟧ ρ ! η ! θ" := (rel_h σ τ ε ρ η θ) (at level 0, σ at level 40, τ at level 30).

Definition rel_v_open {A X V : Set}
  (Δ : A → kind)
  (Θ : X → typ_ A X)
  (Γ : V → typ_ A X)
  (τ : typ_ A X)
  {WFΘ : K[ Δ; X ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; X ⊢ᴱ Γ]}
  {WFτ : K[ Δ; X ⊢ τ ∷ k_type]} :=
  λ v₁ v₂,
    ∀ᵢ W (ρ : X [⇒] W) η ϑ W' ρ' γ₁ γ₂,
      Θ⟦ Θ ⟧ ρ ! η ! ϑ ⇒ (rel_d (subst_comp ρ' ρ) ϑ γ₁ γ₂)ᵢ ⇒ G⟦ Γ ⟧ ρ ! η ! ϑ W' ρ' γ₁ γ₂ ⇒
       wi_rel (⟦ τ ⟧ ρ ! η ! ϑ) W' ρ' (bind γ₁ v₁) (bind γ₂ v₂).
Arguments rel_v_open {A X V} Δ Θ%typ Γ%typ τ%typ {WFΘ WFΓ WFτ} v₁%fsyn v₂%osyn.

Notation "'T⟦' Δ ; Θ ; Γ ⊨ v₁ ≾ v₂ ∷ τ ⟧" :=
  (rel_v_open Δ Θ Γ τ v₁ v₂) (Δ, Θ, Γ at level 40, τ at level 30).

Definition rel_e_open {TV LV V : Set}
  (Δ : TV → kind)
  (Θ : LV → typ_ TV LV)
  (Γ : V  → typ_ TV LV)
  (τ : typ_ TV LV)
  (ε : typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]} :=
  λ e₁ e₂,
    ∀ᵢ W (ρ : LV [⇒] W) η ϑ W' ρ' γ₁ γ₂,
      Θ⟦ Θ ⟧ ρ ! η ! ϑ ⇒ (rel_d (subst_comp ρ' ρ) ϑ γ₁ γ₂)ᵢ ⇒ G⟦ Γ ⟧ ρ ! η ! ϑ W' ρ' γ₁ γ₂ ⇒
       (E⟦ τ / ε ⟧ ρ ! η ! ϑ) W' ρ' (bind γ₁ e₁) (bind γ₂ e₂).
Arguments rel_e_open {TV LV V} Δ%typ Θ%typ Γ%typ τ%typ ε%typ {WFΘ WFΓ WFτ WFε} e₁%fsyn e₂%osyn.

Notation "'T⟦' Δ ; Θ ; Γ ⊨ e₁ ≾ e₂ ∷ τ / ε ⟧" :=
  (rel_e_open Δ Θ Γ τ ε e₁ e₂) (Δ, Θ, Γ at level 40, τ at level 30).

Definition rel_h_open {TV LV V : Set}
  (Δ : TV → kind)
  (Θ : LV → typ_ TV LV)
  (Γ : V  → typ_ TV LV)
  (σ : typ_ TV LV)
  (τ : typ_ TV LV)
  (ε : typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFσ : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
  (h₁ : F.handler_ V LV)
  (h₂ : O.handler_ V LV) : IProp :=
  ∀ᵢ W (ρ : LV [⇒] W) η ϑ W' ρ' γ₁ γ₂,
    Θ⟦ Θ ⟧ ρ ! η ! ϑ ⇒ (rel_d (subst_comp ρ' ρ) ϑ γ₁ γ₂)ᵢ ⇒ G⟦ Γ ⟧ ρ ! η ! ϑ W' ρ' γ₁ γ₂ ⇒
     H⟦ σ @ τ / ε ⟧ ρ ! η ! ϑ W' ρ' (bind γ₁ h₁) (bind γ₂ h₂).

Notation "'H⟦' Δ ; Θ ; Γ ⊨ h₁ ≾ h₂ ∷ σ @ τ / ε ⟧" :=
  (rel_h_open Δ Θ Γ σ τ ε h₁ h₂) (Δ, Θ, Γ at level 40, σ at level 40, τ at level 30).

