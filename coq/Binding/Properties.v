Require Import Utf8.
Require Import Binding.Core.
Require Vector.
Require Import Morphisms.

Section Properties.
Context {Obj : Type}.
Context {Arr : Obj → Obj → Type} {AC : ArrowCore Arr} {AA : Arrow Arr}.
Context {Sub : Obj → Obj → Type} {PSC : PreSubstCore Sub} {PS : PreSubst Sub}.
Context {SC  : SubstCore Arr Sub} {SS : Subst Arr Sub}.
Context {F G : Obj → Type}.
Context {Inc : Obj → Obj}.
Context {Sh  : ShiftableCore Arr Inc}.
Context {ALC : ALiftableCore Arr Inc}.
Context {SLC : SLiftableCore Sub Inc}.
Context {Sb : SubstitutableCore Sub F Inc}.
Context {LAS : LiftAShift Arr Inc}.
Context {LSS : LiftSShift Arr Sub Inc}.
Context {mapF : ∀ A B, Arr A B → F A → F B} {MapF : IsMapCore mapF}.
Context {mapG : ∀ A B, Arr A B → G A → G B} {MapG : IsMapCore mapG}
  {MG : FMap mapG}.
Context {bndF : ∀ A B, Sub A B → F A → F B} {BindF : IsBindCore bndF}.
Context {bndG : ∀ A B, Sub A B → G A → G B} {BindG : IsBindCore bndG}
  {BMG : BindMap mapG bndG} {BG : Bind bndG}.
Context {SbShF  : SubstShift Arr Sub F Inc}.
Context {SbMapF : SubstFMap  Arr Sub F Inc}.
Context {SbBndF : SubstBind  Arr Sub F Inc}.
Context {AL     : ALiftable Arr Inc}.
Context {ASL    : ASLiftable Arr Sub Inc}.
Context {SL     : SLiftable Arr Sub Inc}.

Global Instance EquivArrEq {A B : Obj} : Equivalence (arrow_eq (A:=A) (B:=B)).
Proof.
  split.
  - intros x; apply arrow_eq_refl.
  - intros x y EQ; apply arrow_eq_symm; assumption.
  - intros x y z EQ1 EQ2; eapply arrow_eq_trans; eassumption.
Qed.

Global Instance ProperArrowEq {A B : Obj} :
  Proper (arrow_eq ==> arrow_eq ==> iff) (arrow_eq (A:=A) (B:=B)).
Proof.
  intros f₁ f₂ EQf g₁ g₂ EQg; split; intros EQ.
  - etransitivity; [symmetry | etransitivity]; eassumption.
  - etransitivity; [etransitivity | symmetry]; eassumption.
Qed.

Global Instance ProperArrowCompEq {A B C : Obj} :
  Proper (arrow_eq ==> arrow_eq ==> arrow_eq) (arrow_comp (A:=A) (B:=B) (C:=C)).
Proof.
  intros f₁ f₂ EQf g₁ g₂ EQg; now apply arrow_comp_proper.
Qed.

Global Instance ProperArrowLiftEq {A B : Obj} :
  Proper (arrow_eq ==> arrow_eq) (liftA (A:= A) (B:=B)).
Proof.
  intros f₁ f₂ EQf; eapply liftA_proper, EQf; apply _.
Qed.

Global Instance EquivSubEq {A B : Obj} : Equivalence (subst_eq (A:=A) (B:=B)).
Proof.
  split.
  - intros x; apply subst_eq_refl.
  - intros x y EQ; apply subst_eq_symm; assumption.
  - intros x y z EQ1 EQ2; eapply subst_eq_trans; eassumption.
Qed.

Global Instance ProperSubstEq {A B : Obj} :
  Proper (subst_eq ==> subst_eq ==> iff) (subst_eq (A:=A) (B:=B)).
Proof.
  intros f₁ f₂ EQf g₁ g₂ EQg; split; intros EQ.
  - etransitivity; [symmetry | etransitivity]; eassumption.
  - etransitivity; [etransitivity | symmetry]; eassumption.
Qed.

Global Instance ProperSubstCompEq {A B C : Obj} :
  Proper (subst_eq ==> subst_eq ==> subst_eq) (subst_comp (A:=A) (B:=B) (C:=C)).
Proof.
  intros f₁ f₂ EQf g₁ g₂ EQg; now apply subst_comp_proper.
Qed.

Global Instance ProperArrowSubstCompEq {A B C : Obj} :
  Proper (arrow_eq ==> subst_eq ==> subst_eq) (arrow_subst_comp (A:=A) (B:=B) (C:=C)).
Proof.
  intros f₁ f₂ EQf g₁ g₂ EQg; now apply arrow_subst_proper.
Qed.

Global Instance ProperSubstLiftEq {A B : Obj} :
  Proper (subst_eq ==> subst_eq) (liftS (A:= A) (B:=B)).
Proof.
  intros f₁ f₂ EQf; eapply liftS_proper, EQf; apply _.
Qed.

Global Instance ProperOfArrowEq {A B : Obj} :
  Proper (arrow_eq ==> subst_eq) (of_arrow (A:= A) (B:=B)).
Proof.
  intros f₁ f₂ EQf; unfold of_arrow; now rewrite EQf.
Qed.

Lemma of_arrow_subst_comp A B C (f : Arr B C) (g : Sub A B) :
    subst_eq (arrow_subst_comp f g) (subst_comp (of_arrow f) g).
Proof.
  unfold of_arrow; now rewrite arrow_subst_assoc, subst_comp_pure_l.
Qed.

Lemma map_id' {A : Obj} (t : G A) :
  fmap arrow_id t = t.
Proof.
apply map_id; reflexivity.
Qed.

Lemma map_map_comp' {A B C : Obj} (f : Arr B C) (g : Arr A B) (t : G A) :
  fmap f (fmap g t) = fmap (arrow_comp f g) t.
Proof.
apply map_map_comp; reflexivity.
Qed.

Lemma map_equiv {A B : Obj} (f g : Arr A B) (t : G A) :
  arrow_eq f g → fmap f t = fmap g t.
Proof.
intro Heq; rewrite <- (map_id' (fmap f t)).
apply map_map_comp.
etransitivity; [ | eassumption ].
apply arrow_comp_id_l.
Qed.

Global Instance ProperMap {A B : Obj} : Proper (arrow_eq ==> eq ==> eq) (fmap (A:=A) (B:=B)).
Proof.
  intros f₁ f₂ EQf t t' EQt; subst t'; now apply map_equiv.
Qed.

Lemma bind_pure' {A : Obj} (t : G A) :
  bind subst_pure t = t.
Proof.
apply bind_pure; reflexivity.
Qed.

Lemma bind_bind_comp' {A B C : Obj} (f : Sub B C) (g : Sub A B) (t : G A) :
  bind f (bind g t) = bind (subst_comp f g) t.
Proof.
apply bind_bind_comp; reflexivity.
Qed.

Lemma bind_equiv {A B : Obj} {f g : Sub A B} (t : G A) :
  subst_eq f g → bind f t = bind g t.
Proof.
intro Heq; rewrite <- (bind_pure' (bind f t)).
apply bind_bind_comp.
etransitivity; [ | eassumption ].
apply subst_comp_pure_l.
Qed.

Global Instance ProperBind {A B : Obj} : Proper (subst_eq ==> eq ==> eq) (bind (A:=A) (B:=B)).
Proof.
  intros f₁ f₂ EQf t t' EQt; subst t'; now apply bind_equiv.
Qed.

Lemma map_to_bind {A B : Obj} (f : Arr A B) (t : G A) :
  fmap f t = bind (of_arrow f) t.
Proof.
rewrite <- (bind_pure' (fmap f t)).
rewrite <- (map_id' (bind (of_arrow f) t)).
apply bind_map_comp.
etransitivity; [ apply subst_comp_pure_l | ].
symmetry; apply arrow_subst_comp_id.
Qed.

Lemma fmap_liftA_shift_comm {A B : Obj} (f : Arr A B) (t : G A) :
  fmap (liftA f) (shift t) = shift (fmap f t).
Proof.
unfold shift.
rewrite (map_map_comp' mk_shift f), <- liftA_mk_shift_comm.
apply map_map_comp'.
Qed.

Lemma bind_liftS_shift_comm {A B : Obj} (f : Sub A B) (a : G A) :
  bind (liftS f) (shift a) = shift (bind f a).
Proof.
unfold shift; apply bind_map_comp.
apply liftS_mk_shift_comm.
Qed.

Lemma subst_shift_id {A : Obj} (t : G A) (v : F A):
  subst (shift t) v = t.
Proof.
unfold shift; rewrite map_to_bind.
unfold subst; rewrite bind_bind_comp'.
apply bind_pure, subst_shift_pure.
Qed.

Lemma subst_shift_id_lifted {A : Obj} (t : G (Inc A)) (v : F A):
  bind (liftS (mk_subst v)) (fmap (liftA mk_shift) t) = t.
Proof.
unfold shift; rewrite map_to_bind.
unfold subst; rewrite bind_bind_comp'.
apply bind_pure; etransitivity.
+ eapply liftS_liftA_comp.
  rewrite subst_shift_pure.
  symmetry; apply arrow_subst_comp_id.
+ rewrite liftA_id; [ | reflexivity ].
  rewrite arrow_subst_comp_id.
  apply liftS_pure; reflexivity.
Qed.

Lemma subst_shift_id_lifted2 {A : Obj} (t : G (Inc (Inc A))) (v : F A):
  bind (liftS (liftS (mk_subst v))) (fmap (liftA (liftA mk_shift)) t) = t.
Proof.
unfold shift; rewrite map_to_bind.
unfold subst; rewrite bind_bind_comp'.
apply bind_pure; etransitivity.
+ eapply liftS_liftA_comp, liftS_liftA_comp.
  rewrite subst_shift_pure.
  symmetry; apply arrow_subst_comp_id.
+ rewrite liftA_id; [ | apply liftA_id; reflexivity ].
  rewrite arrow_subst_comp_id.
  apply liftS_pure, liftS_pure; reflexivity.
Qed.

Lemma fmap_subst {A B : Obj} (f : Arr A B) (t : G (Inc A)) (v : F A) :
  fmap f (subst t v) = subst (fmap (liftA f) t) (fmap f v).
Proof.
symmetry; apply bind_map_comp; symmetry.
apply map_mk_subst_comm.
Qed.

Lemma shift_subst {Inc' : Obj → Obj} {Sh' : ShiftableCore Arr Inc'}
  {A : Obj} (t : G (Inc A)) (v : F A) :
  shift (Inc:=Inc') (subst t v) = subst (fmap (liftA mk_shift) t) (shift v).
Proof.
apply fmap_subst.
Qed.

Lemma bind_subst {A B : Obj} (f : Sub A B) (t : G (Inc A)) (v : F A) :
  bind f (subst t v) = subst (bind (liftS f) t) (bind f v).
Proof.
unfold subst at 2.
rewrite bind_bind_comp'; apply bind_bind_comp.
apply bind_mk_subst_comm.
Qed.

Lemma lift_of_arrow {A B : Obj} (δ : Arr A B) :
  subst_eq (liftS (of_arrow δ)) (of_arrow (liftA δ)).
Proof.
  rewrite <- subst_comp_pure_l with (f := of_arrow (liftA δ)), <- liftS_pure; [| reflexivity].
  erewrite liftS_liftA_comp with (g' := arrow_id) (f' := of_arrow δ).
  - now rewrite liftA_id, arrow_subst_comp_id.
  - now rewrite arrow_subst_comp_id, subst_comp_pure_l.
Qed.

Lemma lift_arrow_subst_comp {A B C : Obj} (δ : Arr B C) (η : Sub A B) :
  subst_eq (arrow_subst_comp (liftA δ) (liftS η)) (liftS (arrow_subst_comp δ η)).
Proof.
  rewrite !of_arrow_subst_comp, <- lift_of_arrow, liftS_comp; reflexivity.
Qed.

Lemma shiftn_map {A B : Obj} n (δ : Arr A B) :
  arrow_eq (arrow_comp (mk_shiftn n) δ) (arrow_comp (liftAn n δ) (mk_shiftn n)).
Proof.
  induction n; simpl; [now rewrite arrow_comp_id_l, arrow_comp_id_r |].
  rewrite arrow_comp_assoc, <- liftA_mk_shift_comm, <- arrow_comp_assoc, liftA_comp; [| apply IHn].
  erewrite <- liftA_comp, arrow_comp_assoc; reflexivity.
Qed.

Lemma shiftn_bind {A B : Obj} n (η : Sub A B) :
  subst_eq (arrow_subst_comp (mk_shiftn n) η) (subst_comp (liftSn n η) (of_arrow (mk_shiftn n))).
Proof.
  induction n; simpl.
  - unfold of_arrow; now rewrite !arrow_subst_comp_id, subst_comp_pure_r.
  - rewrite arrow_comp_subst, <- liftS_mk_shift_comm, <- arrow_subst_assoc.
    erewrite lift_arrow_subst_comp, IHn, <- liftS_comp, subst_comp_assoc by reflexivity.
    apply subst_comp_proper; [reflexivity |].
    unfold of_arrow at 3; rewrite lift_of_arrow, arrow_comp_subst, of_arrow_subst_comp; reflexivity.
Qed.

Lemma substV_map {A B : Obj} n (xs : Vector.t (F A) n) (δ : Arr A B) :
  subst_eq (subst_comp (mk_substV (Vector.map (fmap δ) xs)) (of_arrow (liftAn n δ)))
           (arrow_subst_comp δ (mk_substV xs)).
Proof.
  induction xs; simpl; [apply subst_comp_pure_l |].
  rewrite subst_comp_assoc, <- lift_of_arrow, liftS_comp by apply IHxs.
  rewrite <- lift_arrow_subst_comp, !of_arrow_subst_comp, <- !subst_comp_assoc.
  now rewrite <- map_mk_subst_comm, of_arrow_subst_comp.
Qed.

Lemma substV_bind {A B : Obj} n (xs : Vector.t (F A) n) (η : Sub A B) :
  subst_eq (subst_comp (mk_substV (Vector.map (bind η) xs)) (liftSn n η))
           (subst_comp η (mk_substV xs)).
Proof.
  induction xs; simpl; [now rewrite subst_comp_pure_l, subst_comp_pure_r |].
  rewrite <- subst_comp_assoc, bind_mk_subst_comm, !subst_comp_assoc.
  apply subst_comp_proper; [reflexivity |].
  rewrite liftS_comp by apply IHxs.
  now rewrite liftS_comp by reflexivity.
Qed.

Lemma substV_shiftn_id {A : Obj} {n} (xs : Vector.t (F A) n) (t : G A) :
  substV xs (shiftn n t) = t.
Proof.
  unfold substV, shiftn; rewrite map_to_bind, bind_bind_comp', bind_pure; [reflexivity | clear t].
  induction xs; simpl; unfold of_arrow.
  - now rewrite subst_comp_pure_l, arrow_subst_comp_id.
  - rewrite arrow_comp_subst, of_arrow_subst_comp, subst_comp_assoc.
    rewrite <- subst_comp_assoc with (f := liftS _), <- lift_of_arrow, liftS_comp by exact IHxs.
    rewrite liftS_pure, subst_comp_pure_l by reflexivity.
    apply subst_shift_pure.
Qed.

End Properties.
