Require Import Utf8.
Require Import Binding.Lib.

Record sprod (U₁ U₂ : Type) : Type :=
  { sfst : U₁
  ; ssnd : U₂
  }.

Arguments sfst {U₁} {U₂}.
Arguments ssnd {U₁} {U₂}.

Notation "U₁ × U₂" := (sprod U₁ U₂) (at level 40) : type_scope.
Notation "⟨ A , B ⟩" := {| sfst := A ; ssnd := B |}.

Section Defs.
  Context {U₁ U₂ : Type} {Arr₁ : U₁ → U₁ → Type} {Arr₂ : U₂ → U₂ → Type}
          {AC₁ : ArrowCore Arr₁} {AC₂ : ArrowCore Arr₂} {AR₁ : Arrow Arr₁} {AR₂ : Arrow Arr₂}.

  Record prod_arr {X Y : U₁ × U₂} : Type :=
    { arr₁ : Arr₁ (sfst X) (sfst Y);
      arr₂ : Arr₂ (ssnd X) (ssnd Y)
    }.
  Global Arguments prod_arr X Y : clear implicits.

  Global Instance ArrowCore_vsig_arr : ArrowCore prod_arr :=
    {| arrow_id := λ A, {| arr₁ := arrow_id; arr₂ := arrow_id |};
       arrow_comp :=  λ A B C f g, {| arr₁ := arrow_comp (arr₁ f) (arr₁ g);
                                      arr₂ := arrow_comp (arr₂ f) (arr₂ g) |};
       arrow_eq := λ A B f g, arrow_eq (arr₁ f) (arr₁ g) ∧ arrow_eq (arr₂ f) (arr₂ g) |}.

  Global Instance Arrow_prod_arr : Arrow prod_arr.
  Proof.
    split; intros.
    - split; reflexivity.
    - split; symmetry; apply H.
    - split; etransitivity; now (apply H || apply H0).
    - split; apply arrow_comp_id_l.
    - split; apply arrow_comp_id_r.
    - split; apply arrow_comp_assoc.
    - split; eapply arrow_comp_proper; now (apply EQf || apply EQg).
  Qed.

  Section ALift.
    Context (F₁ : U₁ → U₁) (F₂ : U₂ → U₂) {ALC₁ : ALiftableCore Arr₁ F₁}
            {ALC₂ : ALiftableCore Arr₂ F₂} {AL₁ : ALiftable Arr₁ F₁} {AL₂ : ALiftable Arr₂ F₂}.

    Definition on₁ (X : U₁ × U₂) := {| sfst := F₁ (sfst X); ssnd := ssnd X |}.
    Definition on₂ (X : U₁ × U₂) := {| sfst := sfst X; ssnd := F₂ (ssnd X) |}.

    Global Instance ALC_prod_left : ALiftableCore prod_arr on₁ :=
      { liftA := λ (A B : U₁ × U₂) f,
                 {| arr₁ := liftA (arr₁ f);
                    arr₂ := arr₂ f : Arr₂ (ssnd (on₁ A)) (ssnd (on₁ B)) |}}.

    Global Instance AL_prod_left : ALiftable prod_arr on₁.
    Proof.
      split; intros.
      - split; simpl; [apply liftA_id |]; apply H.
      - split; simpl; [apply liftA_comp |]; apply H.
      - split; simpl; [apply liftA_proper; [assumption |] |]; apply EQf.
    Qed.

    Global Instance ALC_prod_right : ALiftableCore prod_arr on₂ :=
      { liftA := λ (A B : U₁ × U₂) f,
                 {| arr₁ := arr₁ f : Arr₁ (sfst (on₂ A)) (sfst (on₂ B));
                    arr₂ := liftA (arr₂ f) |}}.

    Global Instance AL_prod_right : ALiftable prod_arr on₂.
    Proof.
      split; intros.
      - split; simpl; [| apply liftA_id]; apply H.
      - split; simpl; [| apply liftA_comp]; apply H.
      - split; simpl; [| apply liftA_proper; [assumption |]]; apply EQf.
    Qed.

  End ALift.

  Section Shift.
    Context (Inc₁ : U₁ → U₁) (Inc₂ : U₂ → U₂)
            {SC₁ : ShiftableCore Arr₁ Inc₁} {SC₂ : ShiftableCore Arr₂ Inc₂}.

    Global Instance SC_prod_left : ShiftableCore prod_arr (on₁ Inc₁) :=
      { mk_shift := λ A, {| arr₁ := mk_shift;
                            arr₂ := arrow_id : Arr₂ (ssnd A) (ssnd (on₁ Inc₁ A)) |}}.

    Global Instance SC_prod_right : ShiftableCore prod_arr (on₂ Inc₂) :=
      { mk_shift := λ A, {| arr₁ := arrow_id : Arr₁ (sfst A) (sfst (on₂ Inc₂ A)); arr₂ := mk_shift |} }.

  End Shift.
  
  Section LiftA_Shift.
    Context (F₁ : U₁ → U₁) (F₂ : U₂ → U₂)
            {ALC₁ : ALiftableCore Arr₁ F₁} {ALC₂ : ALiftableCore Arr₂ F₂}
            {AL₁ : ALiftable Arr₁ F₁} {AL₂ : ALiftable Arr₂ F₂}
            {SC₁ : ShiftableCore Arr₁ F₁} {SC₂ : ShiftableCore Arr₂ F₂}
            {LAS₁ : LiftAShift Arr₁ F₁} {LAS₂ : LiftAShift Arr₂ F₂}.

    Global Instance LiftAShift_prod_left : LiftAShift prod_arr (on₁ F₁).
    Proof.
      intros A B f; split; simpl.
      + apply LAS₁.
      + etransitivity; [ apply arrow_comp_id_r | ].
        symmetry; apply arrow_comp_id_l.
    Qed.


    Global Instance LiftAShift_prod_right : LiftAShift prod_arr (on₂ F₂).
    Proof.
      intros A B f; split; simpl.
      + etransitivity; [ apply arrow_comp_id_r | ].
        symmetry; apply arrow_comp_id_l.
      + apply LAS₂.
    Qed.
  End LiftA_Shift.

End Defs.