Require Import Utf8.
Require Import Binding.Lib.
Require Import SetoidClass.

Definition eq_ext {A B} (f g : A → B) := ∀ x, f x = g x.

Lemma Equivalence_eq_ext {A B} : Equivalence (@eq_ext A B).
Proof.
  split.
  - intros Γ x; reflexivity.
  - intros Γ₁ Γ₂ HEq x; symmetry; apply HEq.
  - intros Γ₁ Γ₂ Γ₃ HEq₁ HEq₂ x; etransitivity; [apply HEq₁ | apply HEq₂].
Qed.

Instance Setoid_equiv {A B} : Setoid (A → B) := Build_Setoid Equivalence_eq_ext.

Definition empty_env {A} : ∅ → A := λ x, match x with end.


Definition extend {V : Set} {A} (Γ : V → A) (τ : A) : inc V → A :=
  λ x, match x with VZ => τ | VS x => Γ x end.

Definition compose {A B C} (f : B → C) (g : A → B) (x : A) := f (g x).

Notation "f ∘ g" := (compose f g) (at level 40, left associativity).
Notation "f ▹ v" := (extend f v) (at level 40, v at next level, left associativity).
Notation "□" := empty_env.
Notation "x ≃ y" := (equiv x y) (at level 70, no associativity).

Arguments compose {A B C} f g x /.

Ltac destr_refl x :=
  match type of x with
  | inc _ => destruct x as [| x]; [term_simpl; reflexivity | destr_refl x]
  | _ => term_simpl
  end.

Ltac solve_simple_eq :=
  match goal with
  | [ H: ?X ≃ ?Y |- ?X ?x = ?Y ?x ] => apply H
  | [ |- ?f ?v1 = ?f ?v2 ] => f_equal; solve_simple_eq
  | _ => now eauto
  end.

Ltac solve_equiv :=
  match goal with
  | [|- ?X ≃ ?Y] =>
      let x := fresh "x"
      in intros x; destr_refl x; solve_simple_eq
  | _ => eassumption || reflexivity
  end.

Lemma equiv_extend {A B : Set} (f g : A → B) v
      (HEq : f ≃ g) :
  f ▹ v ≃ g ▹ v.
Proof.
  solve_equiv.
Qed.

Section Definitions.
  Context {U : Type} {T : U → Type} {Arr : U → U → Type} {AC : ArrowCore Arr}
          {tmap : ∀ A B : U, A [→] B → T A → T B} {MapT : IsMapCore tmap}.

  Definition envmap {V : Set} {A B : U} (f : A [→] B) (Γ : V → T A) x :=
    fmap f (Γ x).

  Context {Sub : U → U → Type} {PSC : PreSubstCore Sub} {SC : SubstCore Arr Sub}
          {tbind : ∀ A B : U, Sub A B → T A → T B} {BindT : IsBindCore tbind}.

  Definition envbind {V : Set} {A B : U} (f : Sub A B) (Γ : V → T A) x :=
    bind f (Γ x).

  Global Instance IsMapCore_env V : IsMapCore (@envmap V).
  Global Instance IsBindCore_env V : IsBindCore (@envbind V).

  Global Arguments envmap {V A B} f Γ x /.
  Global Arguments envbind {V A B} f Γ x /.

End Definitions.

Require Import Binding.Set.

Lemma env_extend_equiv (V : Type) (A B : Set) (Δ₁ : A → V) Δ₂ (δ : B [→] A) (κ : V)
      (HEq : Δ₂ ≃ Δ₁ ∘ δ) :
  Δ₂ ▹ κ ≃ (Δ₁ ▹ κ) ∘ (liftA δ : inc B [→] inc A).
Proof.
  solve_equiv.
Qed.

Hint Extern 4 (_ ≃ _) => solve_equiv.

Ltac simpl_HFInd :=
  subst; try discriminate;
  repeat match goal with
         | [ G : ?x = ?x → _ |- _ ] => specialize (G eq_refl)
         | [ G : inc ?x = ?x → _ |- _ ] => clear G
         end.
