(** * Relation C for partial evaluation contexts *)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Fresh.Syntax Types Kinding.
Require Import LogicalRelation.Core.

(** The relation for partial evaluation contexts *)
Definition rel_c {TV LV : Set} {Δ : TV → kind}
    τ₁ ε₁ τ₂ ε₂
    {WFτ₁ : K[ Δ ; LV ⊢ τ₁ ∷ k_type   ]}
    {WFε₁ : K[ Δ ; LV ⊢ ε₁ ∷ k_effect ]}
    {WFτ₂ : K[ Δ ; LV ⊢ τ₂ ∷ k_type   ]}
    {WFε₂ : K[ Δ ; LV ⊢ ε₂ ∷ k_effect ]}
    (η : ∀ a, K⟦ Δ a ⟧)
    (θ : LV → instance_dom)
    (E₁ E₂ : rctx0) : IProp :=
  (∀ᵢ v₁ v₂, ⟦ τ₁ ⟧ η ! θ v₁ v₂ ⇒
      E⟦ τ₂ // ε₂⟧ η ! θ (rplug E₁ v₁) (rplug E₂ v₂)) ∧ᵢ
  (∀ᵢ E₁' E₂' e₁ e₂, S⟦τ₁ // ε₁⟧ η ! θ E₁' E₂' e₁ e₂ ⇒
      E⟦ τ₂ // ε₂ ⟧ η ! θ (rplug E₁ (rplug E₁' e₁)) (rplug E₂ (rplug E₂' e₂))).
Notation "'C⟦' τ₁ '//' ε₁ '↝' τ₂ '//' ε₂ '⟧' η ! θ" :=
  (rel_c τ₁ ε₁ τ₂ ε₂ η θ) (at level 0).

(** Relation rel_c is compatible with relation for expressions *)
Lemma rel_c_compat_e {TV LV : Set} {Δ : TV → kind}
    τ₁ ε₁ τ₂ ε₂
    {WFτ₁ : K[ Δ ; LV ⊢ τ₁ ∷ k_type   ]}
    {WFε₁ : K[ Δ ; LV ⊢ ε₁ ∷ k_effect ]}
    {WFτ₂ : K[ Δ ; LV ⊢ τ₂ ∷ k_type   ]}
    {WFε₂ : K[ Δ ; LV ⊢ ε₂ ∷ k_effect ]}
    (η : ∀ a, K⟦ Δ a ⟧)
    (θ : LV → instance_dom)
    (E₁ E₂ : rctx0) (e₁ e₂ : expr0) n :
  n ⊨ C⟦ τ₁ // ε₁ ↝ τ₂ // ε₂⟧ η ! θ E₁ E₂ →
  n ⊨ E⟦ τ₁ // ε₁⟧ η ! θ e₁ e₂ →
  n ⊨ E⟦ τ₂ // ε₂⟧ η ! θ (rplug E₁ e₁) (rplug E₂ e₂).
Proof.
intros HE He.
iintro F₁; iintro F₂; iintro HF.
(*
rewrite <- plug_xcompr; rewrite <- plug_xcompr.
eapply rel_k_expr; [ | exact He ].
clear e₁ e₂ He.
apply rel_k_roll.
+ intros v₁ v₂; iintro Hv.
  rewrite plug_xcompr; rewrite plug_xcompr.
  eapply rel_k_expr; [ exact HF | ].
  idestruct HE as HEv HEs; iespecialize HEv; iapply HEv.
  exact Hv.
+ intros E₁' E₂' e₁ e₂; iintro Hs.
  rewrite plug_xcompr; rewrite plug_xcompr.
  eapply rel_k_expr; [ exact HF | ].
  idestruct HE as HEv HEs; iespecialize HEs; iapply HEs.
  exact Hs.
*)
Admitted.