Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Product Binding.Env.
Require Import Types Kinding.
Require Import Fresh.Syntax.
Require Export LogicalRelation.Closures LogicalRelation.Constructions.

Definition apply_to_t_var {TV LV : Set} {Δ : TV → kind}
  (η : ∀ α : TV, K⟦ Δ α ⟧)
  (α : TV) (κ : kind)
  (WF : K[ Δ ; LV ⊢ t_var (α : s_var {| sfst := TV |}) ∷ κ ]) :
    K⟦ κ ⟧ :=
  eq_rect _ (λ κ, K⟦ κ ⟧) (η α) _ (K_Var_inv WF).

Definition extend_tvar_interp {TV : Set} {Δ : TV → kind} {κ : kind}
  (η : ∀ α : TV, K⟦ Δ α ⟧) (R : K⟦ κ ⟧) (α : inc TV) : K⟦ (Δ ▹ κ) α ⟧ :=
  match α with
  | VZ   => R
  | VS α => η α
  end.

Notation "η :▹ R" := (extend_tvar_interp η R)
  (at level 40, R at next level, left associativity).

Fixpoint type_interp {TV LV : Set} {Δ : TV → kind}
  (τ : typ_ TV LV) (κ : kind)
  (η : ∀ α : TV, K⟦ Δ α ⟧)
  (ϑ : LV → ℑ) : K[ Δ ; LV ⊢ τ ∷ κ] → K⟦ κ ⟧ :=
  match τ return K[ Δ ; LV ⊢ τ ∷ κ] → K⟦ κ ⟧ with
  | t_var α => apply_to_t_var η α κ
  | t_unit  =>
    match κ with
    | k_type => λ WF v₁ v₂, (v₁ = v_unit ∧ v₂ = v_unit)ᵢ
    | k_effect => λ _, pure_effect
    | k_sig    => λ _ _ _ _, (False)ᵢ
    end
  | t_arrow τ₁ ε τ₂ =>
    match κ return K[ Δ ; LV ⊢ _ ∷ κ] → K⟦ κ ⟧ with
    | k_type => λ WF, arrow_rel (type_interp τ₁ _ η ϑ (K_Arrow_inv1 WF))
                                (type_interp τ₂ _ η ϑ (K_Arrow_inv3 WF))
                                (type_interp ε  _ η ϑ (K_Arrow_inv2 WF))
    | k_effect => λ _, pure_effect
    | k_sig    => λ _ _ _ _, (False)ᵢ
    end
  | t_forallT κ₀ τ =>
    match κ return K[ Δ ; LV ⊢ _ ∷ κ] → K⟦ κ ⟧ with
    | k_type => λ WF, tall_rel κ₀ (λ R, type_interp τ _ (η :▹ R) ϑ (K_ForallT_inv WF))
    | k_effect => λ _, pure_effect
    | k_sig    => λ _ _ _ _, (False)ᵢ
    end
  | t_forallL σ τ => 
    match κ return K[ Δ ; LV ⊢ _ ∷ κ] → K⟦ κ ⟧ with
    | k_type => λ WF, lall_rel (type_interp σ _ η ϑ (K_ForallL_inv1 WF))
                               (λ I, type_interp τ _ η (ϑ ▹ I) (K_ForallL_inv2 WF))
    | k_effect => λ _, pure_effect
    | k_sig    => λ _ _ _ _, (False)ᵢ
    end
  | t_label a =>
    match κ return K[ Δ ; LV ⊢ _ ∷ κ] → K⟦ κ ⟧ with
    | k_effect => λ WF, instance_effect (ϑ a)
    | k_type => λ _ _ _, (False)ᵢ
    | k_sig  => λ _ _ _ _, (False)ᵢ
    end
  | t_pure =>
    match κ return K[ Δ ; LV ⊢ _ ∷ κ] → K⟦ κ ⟧ with
    | k_effect => λ WF, pure_effect
    | k_type => λ _ _ _, (False)ᵢ
    | k_sig  => λ _ _ _ _, (False)ᵢ
    end
  | t_cons ε₁ ε₂ =>
    match κ return K[ Δ ; LV ⊢ _ ∷ κ] → K⟦ κ ⟧ with
    | k_effect => λ WF,
      cons_effect
        (type_interp ε₁ _ η ϑ (K_Cons_inv1 WF))
        (type_interp ε₂ _ η ϑ (K_Cons_inv2 WF))
    | k_type => λ _ _ _, (False)ᵢ
    | k_sig  => λ _ _ _ _, (False)ᵢ
    end
  | t_opsig τ₁ τ₂ =>
    match κ return K[ Δ ; LV ⊢ _ ∷ κ] → K⟦ κ ⟧ with
    | k_sig    => λ WF, sig_rel (type_interp τ₁ _ η ϑ (K_OpSig_inv1 WF))
                                (type_interp τ₂ _ η ϑ (K_OpSig_inv2 WF))
    | k_type   => λ _ _ _, (False)ᵢ
    | k_effect => λ _, pure_effect
    end
  | t_allTS κ₀ τ =>
    match κ return K[ Δ ; LV ⊢ _ ∷ κ] → K⟦ κ ⟧ with
    | k_sig  => λ WF, talls_rel κ₀ (λ R, type_interp τ _ (η :▹ R) ϑ (K_AllTS_inv WF))
    | k_type => λ _ _ _, (False)ᵢ
    | k_effect => λ _, pure_effect
    end
  end.

(* Arguments type_interp {TV LV Δ} τ κ η ϑ {WF} : rename. *)

Notation "'⟦' τ '∷' κ '⟧' η '!' ϑ" := (type_interp τ κ η ϑ _) (at level 0).
Notation "'⟦' τ '⟧' η '!' ϑ" := (⟦ τ ∷ k_type ⟧ η ! ϑ) (at level 0).
Notation "'F⟦' ε '⟧' η '!' ϑ" := (⟦ ε ∷ k_effect ⟧ η ! ϑ) (at level 0).
Notation "'Σ⟦' σ '⟧' η '!' ϑ" := (⟦ σ ∷ k_sig ⟧ η ! ϑ) (at level 0).
Notation "'E⟦' τ / ε '⟧' η '!' ϑ" :=
  (rel_expr_cl (F⟦ ε ⟧ η ! ϑ) (⟦ τ ⟧ η ! ϑ)) (at level 0, τ at level 30).
Notation "'K⟦' τ / ε '⟧' η '!' ϑ" :=
  (rel_ectx_cl (F⟦ ε ⟧ η ! ϑ) (⟦ τ ⟧ η ! ϑ)) (at level 0, τ at level 30).
Notation "'S⟦' τ / ε '⟧' η '!' ϑ" :=
  (rel_stuck_cl (F⟦ ε ⟧ η ! ϑ) (⟦ τ ⟧ η ! ϑ)) (at level 0, τ at level 30).
Notation "'E⟦' τ₁ ⊢ τ / ε ⟧ η ! ϑ" :=
  (rel_expr_cl1 (⟦ τ₁ ⟧ η ! ϑ)  (E⟦ τ / ε ⟧ η ! ϑ)) (at level 0, τ₁, τ at level 30).


Definition rel_g {TV LV V : Set} {Δ : TV → kind}
  (Γ : V → typ_ TV LV)
  (η : ∀ α : TV, K⟦ Δ α ⟧)
  (ϑ : LV → ℑ)
  {WF : K[ Δ; LV ⊢ᴱ Γ]}
  (γ₁ γ₂ : {| sfst := V; ssnd := LV |} {⇒} {| sfst := ∅; ssnd := ∅ |}) :=
  (∀ a, sub_lbl γ₁ a = l_tag (ℑ_ltag1 (ϑ a))
      ∧ sub_lbl γ₂ a = l_tag (ℑ_ltag2 (ϑ a)))ᵢ ∧ᵢ
  ∀ᵢ x, ⟦ Γ x ⟧ η ! ϑ (sub_var γ₁ x) (sub_var γ₂ x).

Notation "'G⟦' Γ '⟧' η '!' ϑ" := (rel_g Γ η ϑ) (at level 0).

Definition rel_t {TV LV : Set} {Δ : TV → kind}
  (Θ : LV → typ_ TV LV)
  (η : ∀ α : TV, K⟦ Δ α ⟧)
  {WF : ∀ a, K[ Δ; LV ⊢ Θ a ∷ k_sig]}
  (ϑ : LV → ℑ) : IProp :=
  ∀ᵢ a, ℑ_rel (ϑ a) ≈ᵢ Σ⟦ Θ a ⟧ η ! ϑ.

Notation "'Θ⟦' Θ '⟧' η '!' ϑ" := (rel_t Θ η ϑ) (at level 0).

Definition rel_h {TV LV : Set} {Δ : TV → kind}
    σ τ ε
    {WFσ : K[ Δ ; LV ⊢ σ ∷ k_sig    ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    (η : ∀ a, K⟦ Δ a ⟧)
    (ϑ : LV → ℑ) : RChandler :=
  rel_handler_cl (Σ⟦ σ ⟧ η ! ϑ) (λ I, arrow_rel I (⟦ τ ⟧ η ! ϑ) (F⟦ ε ⟧ η ! ϑ)) (E⟦ τ / ε ⟧ η ! ϑ).

Notation "'H⟦' σ @ τ / ε ⟧ η ! θ" := (rel_h σ τ ε η θ) (at level 0, σ at level 40, τ at level 30).

Definition rel_v_open {TV LV V : Set}
  (Δ : TV → kind)
  (Θ : LV → typ_ TV LV)
  (Γ : V  → typ_ TV LV)
  (τ : typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]} : Rtype V LV :=
  λ v₁ v₂,
    ∀ᵢ η ϑ γ₁ γ₂,
      Θ⟦ Θ ⟧ η ! ϑ ⇒ G⟦ Γ ⟧ η ! ϑ γ₁ γ₂ ⇒
      ⟦ τ ⟧ η ! ϑ (bind γ₁ v₁) (bind γ₂ v₂).

Notation "'T⟦' Δ ; Θ ; Γ ⊨ v₁ ≾ v₂ ∷ τ ⟧" :=
  (rel_v_open Δ Θ Γ τ v₁ v₂) (Δ, Θ, Γ at level 40, τ at level 30).

Definition rel_e_open {TV LV V : Set}
  (Δ : TV → kind)
  (Θ : LV → typ_ TV LV)
  (Γ : V  → typ_ TV LV)
  (τ : typ_ TV LV)
  (ε : typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]} : Rexpr V LV :=
  λ e₁ e₂,
    ∀ᵢ η ϑ γ₁ γ₂,
      Θ⟦ Θ ⟧ η ! ϑ ⇒ G⟦ Γ ⟧ η ! ϑ γ₁ γ₂ ⇒
       E⟦ τ / ε ⟧ η ! ϑ (bind γ₁ e₁) (bind γ₂ e₂).

Notation "'T⟦' Δ ; Θ ; Γ ⊨ e₁ ≾ e₂ ∷ τ / ε ⟧" :=
  (rel_e_open Δ Θ Γ τ ε e₁ e₂) (Δ, Θ, Γ at level 40, τ at level 30).

Definition rel_h_open {TV LV V : Set}
  (Δ : TV → kind)
  (Θ : LV → typ_ TV LV)
  (Γ : V  → typ_ TV LV)
  (σ : typ_ TV LV)
  (τ : typ_ TV LV)
  (ε : typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFσ : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
  (h₁ h₂ : handler_ V LV) : IProp :=
  ∀ᵢ η ϑ γ₁ γ₂,
    Θ⟦ Θ ⟧ η ! ϑ ⇒ G⟦ Γ ⟧ η ! ϑ γ₁ γ₂ ⇒
     H⟦ σ @ τ / ε ⟧ η ! ϑ (bind γ₁ h₁) (bind γ₂ h₂).

Notation "'H⟦' Δ ; Θ ; Γ ⊨ h₁ ≾ h₂ ∷ σ @ τ / ε ⟧" :=
  (rel_h_open Δ Θ Γ σ τ ε h₁ h₂) (Δ, Θ, Γ at level 40, σ at level 40, τ at level 30).
