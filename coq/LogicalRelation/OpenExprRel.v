Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib.
Require Import Types Kinding.
Require Import Fresh.Syntax.
Require Import LogicalRelation.Core.

Definition rel_e_cl1 {TV LV : Set} {Δ : TV → kind}
    τ₁ τ ε
    {WFτ₁ : K[ Δ ; LV ⊢ τ₁ ∷ k_type   ]}
    {WFτ  : K[ Δ ; LV ⊢ τ  ∷ k_type   ]}
    {WFε  : K[ Δ ; LV ⊢ ε  ∷ k_effect ]}
    (η : ∀ a, K⟦ Δ a ⟧)
    (θ : LV → instance_dom)
    (e₁ e₂ : expr (incV _)) : IProp :=
  ∀ᵢ v₁ v₂,
    ⟦ τ₁ ⟧ η ! θ v₁ v₂ ⇒
    E⟦ τ // ε ⟧ η ! θ (subst e₁ v₁) (subst e₂ v₂).
Notation "'E⟦' τ₁ ⊢ τ // ε ⟧ η ! θ" := (rel_e_cl1 τ₁ τ ε η θ) (at level 0).

Definition rel_e_cl2 {TV LV : Set} {Δ : TV → kind}
    τ₁ τ₂ τ ε
    {WFτ₁ : K[ Δ ; LV ⊢ τ₁ ∷ k_type   ]}
    {WFτ₂ : K[ Δ ; LV ⊢ τ₂ ∷ k_type   ]}
    {WFτ  : K[ Δ ; LV ⊢ τ  ∷ k_type   ]}
    {WFε  : K[ Δ ; LV ⊢ ε  ∷ k_effect ]}
    (η : ∀ a, K⟦ Δ a ⟧)
    (θ : LV → instance_dom)
    (e₁ e₂ : expr (incV (incV _))) : IProp :=
  ∀ᵢ v₁ v₂ u₁ u₂,
    (⟦ τ₁ ⟧ η ! θ v₁ v₂ ∧ᵢ ⟦ τ₂ ⟧ η ! θ u₁ u₂) ⇒
    E⟦ τ // ε ⟧ η ! θ
      (subst (subst e₁ (shift u₁)) v₁) (subst (subst e₂ (shift u₂)) v₂).
Notation "'E⟦' τ₁ ; τ₂ ⊢ τ // ε ⟧ η ! θ" :=
  (rel_e_cl2 τ₁ τ₂ τ ε η θ) (at level 0).

Lemma rel_e_cl2_shift_eq {TV LV : Set} {Δ : TV → kind}
    τ₁ τ₂ τ ε
    {WFτ₁ : K[ Δ ; LV ⊢ τ₁ ∷ k_type   ]}
    {WFτ₂ : K[ Δ ; LV ⊢ τ₂ ∷ k_type   ]}
    {WFτ  : K[ Δ ; LV ⊢ τ  ∷ k_type   ]}
    {WFε  : K[ Δ ; LV ⊢ ε  ∷ k_effect ]}
    (η : ∀ a, K⟦ Δ a ⟧)
    (θ : LV → instance_dom)
    (e₁ e₂ : expr (incV (incV _))) v₁ v₂ u₁ u₂ w₁ w₂ n :
  w₁ = shift u₁ →
  w₂ = shift u₂ →
  n ⊨ ⟦ τ₁ ⟧ η ! θ v₁ v₂ →
  n ⊨ ⟦ τ₂ ⟧ η ! θ u₁ u₂ →
  n ⊨ E⟦ τ₁ ; τ₂ ⊢ τ // ε ⟧ η ! θ e₁ e₂ →
  n ⊨ E⟦ τ // ε ⟧ η ! θ (subst (subst e₁ w₁) v₁) (subst (subst e₂ w₂) v₂).
Proof.
  intros Heq₁ Heq₂ Hv Hu He; subst.
  iapply He; now isplit.
Qed.
