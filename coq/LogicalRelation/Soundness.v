Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Env.
Require Import Lang.Surface.Syntax.
Require Import Lang.Surface.TypeSystem.
Require Import Lang.Fresh.SurfaceTrans.
Require Import LogicalRelation.Core LogicalRelation.Compatibility.
Require Import LogicalRelation.KindingIrrelevance LogicalRelation.KindEquiv.

Lemma tequiv_preserve {TV LV : Set} {Δ : TV → kind} {κ τ₁ τ₂}
         {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ κ]}
         {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ κ]}
         (HE : Δ; LV ⊢ τ₁ ≃ τ₂)
         (η : ∀ α : TV, K⟦ Δ α ⟧) (ϑ : LV → ℑ) n :
  n ⊨ K⟦ ⟦ τ₁ ∷ κ ⟧ η ! ϑ ≡ ⟦ τ₂ ∷ κ ⟧ η ! ϑ ∷ κ ⟧.
Proof.
  revert κ WFτ₁ WFτ₂ η ϑ n; induction HE; intros; invert_kind WFτ₂; [| | | unfold kind_equiv; simpl type_interp ..].
  - apply type_interp_irr.
  - symmetry; apply IHHE.
  - assert (WFσ₂ : K[ Δ; LV ⊢ σ₂ ∷ κ]) by (eapply tequiv_wf; eassumption).
    etransitivity; [apply IHHE1 | apply IHHE2].
  - apply equiv_arr; [apply (IHHE1 k_type) | apply (IHHE2 k_type) | apply (IHHE3 k_effect)].
  - apply equiv_tall; iintro R; apply (IHHE k_type).
  - apply equiv_lall; [apply (IHHE1 k_sig) | iintro R; apply (IHHE2 k_type)].
  - apply equiv_cons; [apply (IHHE1 k_effect) | apply (IHHE2 k_effect)].
  - apply equiv_sig; [apply (IHHE1 k_type) | apply (IHHE2 k_type)].
  - apply equiv_talls; intros HR; apply (IHHE k_sig).
  - etransitivity; [apply effect_equiv_consC |].
    apply equiv_cons; apply (type_interp_irr k_effect).
  - etransitivity; [apply effect_equiv_consA |].
    apply equiv_cons; [| apply equiv_cons]; apply (type_interp_irr k_effect).
  - etransitivity; [| apply effect_equiv_cons_pureIL].
    apply (type_interp_irr k_effect).
  - etransitivity; [| apply effect_equiv_cons_pureIR].
    apply (type_interp_irr k_effect).
  - etransitivity; [apply effect_equiv_consI |].
    apply equiv_cons; apply (type_interp_irr k_effect).
Qed.

Lemma subtyp_preserve {TV LV : Set} {Δ : TV → kind} {κ τ₁ τ₂}
         {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ κ]}
         {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ κ]}
         (HE : Δ; LV ⊢ τ₁ ≲ τ₂)
         (η : ∀ α : TV, K⟦ Δ α ⟧) (ϑ : LV → ℑ) n :
  n ⊨ K⟦ ⟦ τ₁ ∷ κ ⟧ η ! ϑ ⊆ ⟦ τ₂ ∷ κ ⟧ η ! ϑ ∷ κ ⟧.
Proof.
  revert κ WFτ₁ WFτ₂ η ϑ n; induction HE; intros;
    invert_kind WFτ₁; [| unfold kind_equiv; simpl type_interp ..].
  - apply kind_subrel_equiv, tequiv_preserve, HTE.
  - apply subrel_arr; [apply (IHHE1 k_type) | apply (IHHE3 k_type) | apply (IHHE2 k_effect)].
  - apply subrel_tall; iintro R; apply (IHHE k_type).
  - apply subrel_lall; [apply (tequiv_preserve (κ := k_sig)), HTE |].
    iintro; apply (IHHE k_type).
  - apply effect_sub_cons; [apply (IHHE1 k_effect) | apply (IHHE2 k_effect)].
  - apply effect_sub_pure.
Qed.

Lemma sinst_preserve {TV LV : Set} {Δ : TV → kind} {σ τ₁ τ₂}
         {WFσ  : K[ Δ; LV ⊢ σ  ∷ k_sig]}
         {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
         {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
         (HI : Δ ⊢ σ ⇝ τ₁ ⇒ τ₂)
         (η : ∀ α : TV, K⟦ Δ α ⟧) (ϑ : LV → ℑ) n :
  n ⊨ Σ⟦ (τ₁ ⇒ᵈ τ₂)%typ ⟧ η ! ϑ ≾ᵢ Σ⟦ σ ⟧ η ! ϑ.
Proof.
  induction HI.
  - apply subrel_sub_equiv, (type_interp_irr k_sig).
  - invert_kind WFσ as WF; etransitivity; [apply IHHI | clear].
    etransitivity; [| simpl type_interp; apply talls_inst].
    apply subrel_sub_equiv; symmetry.
    etransitivity; [apply (Substitution.type_interp_substT k_sig); reflexivity |].
    apply (type_interp_irr k_sig).
Qed.

Fixpoint fundamental_property {V TV LV : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV}
  {τ ε : typ_ TV LV} {e : expr_ V LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]} {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {n}
  (HT : Δ ; Θ ; Γ ⊢ e :: τ / ε) {struct HT} :
  n ⊨ T⟦ Δ ; Θ ; Γ ⊨ tr_expr e ≾ tr_expr e ∷ τ / ε ⟧
with fundamental_property_v {V TV LV : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV}
  {τ : typ_ TV LV} {v : value_ V LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]} {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {n} (HT : Δ ; Θ ; Γ ⊢ᵛ v :: τ) {struct HT} :
  n ⊨ T⟦ Δ ; Θ ; Γ ⊨ tr_value v ≾ tr_value v ∷ τ ⟧
with fundamental_property_h {V TV LV : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV}
  {σ τ ε : typ_ TV LV} {h : handler_ V LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]} {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFσ : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {n} (HT : Δ ; Θ ; Γ ⊢ʰ h :: σ ⇝ τ / ε) {struct HT} :
  n ⊨ H⟦ Δ ; Θ ; Γ ⊨ (tr_handler h) ≾ tr_handler h ∷ σ @ τ / ε ⟧.
Proof.
  - destruct HT; simpl tr_expr.
    + apply rel_v_in_rel_e_open, fundamental_property_v; assumption.
    + assert (WFτ₁ := etypes_wf_typ HT1).
      apply (let_compat τ₁); apply fundamental_property; assumption.
    + assert (WFτ₁ := vtypes_wf HT₂).
      apply (app_compat τ₁); apply fundamental_property_v; assumption.
    + assert (WFτ₁ := vtypes_wf HT); invert_kind WFτ₁ as WF.
      refine (rel_e_open_k_irr (tapp_compat _)).
      apply fundamental_property_v; assumption.
    + assert (WFτ₁ := vtypes_wf HT); invert_kind WFτ₁ as WF.
      refine (rel_e_open_k_irr (lapp_compat _)).
      apply fundamental_property_v; assumption.
    + assert (WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]) by
          (specialize (WFΘ a); apply sinst_wf in HI; [exact (proj1 HI) | assumption]).
      eapply (do_compat τ₁ (a := a)); [| now apply fundamental_property_v].
      intros; eapply sinst_preserve in HI; apply HI.
    + assert (WFτ' := etypes_wf_typ HT1).
      eapply handle_compat with (τ0 := τ).
      * apply fundamental_property, HT1.
      * apply fundamental_property_h, HTh.
      * apply fundamental_property, HT2.
    + assert (WFτ₁ := etypes_wf_typ HT); assert (WFε₁ := etypes_wf_eff HT).
      eapply fundamental_property with (n := n) in HT.
      iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ.
      eapply subtyp_preserve in HSTτ; eapply subtyp_preserve in HSTε.
      iapply @subrel_expr_cl; [..| iapply HT]; eassumption.
  - destruct HT; simpl tr_value.
    + apply unit_compat.
    + apply (rel_v_open_k_irr (var_compat_v _ _)).
    + invert_kind WFτ as WF; refine (rel_v_open_k_irr (lam_compat _)).
      apply fundamental_property; assumption.
    + invert_kind WFτ; refine (rel_v_open_k_irr (tlam_compat _)).
      apply fundamental_property; assumption.
    + invert_kind WFτ; refine (rel_v_open_k_irr (llam_compat σ _)).
      apply fundamental_property; assumption.
  - destruct HT; simpl tr_handler.
    + invert_kind WFσ.
      assert (HH := handler_do_compat Δ Θ Γ τ₁ τ₂ τ ε (tr_expr e) (tr_expr e) n).
      eapply rel_h_open_k_irr in HH; [assumption |].
      now apply fundamental_property.
    + invert_kind WFσ.
      assert (HH := handler_all_compat Δ Θ Γ κ σ τ ε (tr_handler h) (tr_handler h) n).
      apply rel_h_open_k_irr in HH; [assumption |].
      now apply fundamental_property_h.
Qed.
