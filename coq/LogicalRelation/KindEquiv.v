Require Import Utf8.
Require Import IxFree.Lib.
Require Import Fresh.Syntax Types.
Require Import LogicalRelation.Core.

Require Import Coq.Logic.Eqdep_dec.
Export TacticSynonyms.

(** * Equivalence of kind interpretations *)

Definition kind_equiv (κ : kind) : K⟦ κ ⟧ → K⟦ κ ⟧ → IProp :=
  match κ with
  | k_type   => λ R₁ R₂, R₁ ≈ᵢ R₂
  | k_effect => λ R₁ R₂, R₁ ≈ᵢ R₂
  | k_sig    => λ R₁ R₂, R₁ ≈ᵢ R₂
  end.

Notation "K⟦ I₁ ≡ I₂ ∷ κ ⟧" := (kind_equiv κ I₁ I₂) (I₁ at level 30).

Definition kind_subrel (κ : kind) : K⟦ κ ⟧ → K⟦ κ ⟧ → IProp :=
  match κ with
  | k_type   => λ R₁ R₂, R₁ ≾ᵢ R₂
  | k_effect => λ R₁ R₂, R₁ ≾ᵢ R₂
  | k_sig    => λ R₁ R₂, R₁ ≈ᵢ R₂
  end.

Notation "K⟦ I₁ ⊆ I₂ ∷ κ ⟧" := (kind_subrel κ I₁ I₂) (I₁ at level 30).

Instance CRefl_kequiv (κ : kind) n : CReflexive (I_valid_at n) (kind_equiv κ).
Proof.
  intros I; destruct κ; unfold kind_equiv; reflexivity.
Qed.

Instance CSymm_kequiv (κ : kind) n : CSymmetric (I_valid_at n) (kind_equiv κ).
Proof.
  intros I₁ I₂ HI; destruct κ; unfold kind_equiv in *; now symmetry.
Qed.

Instance CTrans_kequiv (κ : kind) n : CTransitive (I_valid_at n) (kind_equiv κ).
Proof.
  destruct κ; unfold kind_equiv; intros I₁ I₂ I₃ H1 H2; etransitivity; eassumption.
Qed.

Instance CRefl_ksub (κ : kind) n : CReflexive (I_valid_at n) (kind_subrel κ).
Proof.
  intros I; destruct κ; unfold kind_subrel; reflexivity.
Qed.

Instance CTrans_ksub (κ : kind) n : CTransitive (I_valid_at n) (kind_subrel κ).
Proof.
  intros I₁ I₂ I₃; destruct κ; unfold kind_subrel; intros H1 H2; etransitivity; eassumption.
Qed.

Lemma kind_subrel_equiv (κ : kind) (I₁ I₂ : K⟦ κ ⟧) n
      (HR : n ⊨ K⟦ I₁ ≡ I₂ ∷ κ ⟧) :
  n ⊨ K⟦ I₁ ⊆ I₂ ∷ κ ⟧.
Proof.
  destruct κ; unfold kind_subrel, kind_equiv in *; [apply subrel_sub_equiv .. |]; assumption.
Qed.

Definition kind_equiv_eq {κ₁ κ₂ : kind} (Heq : κ₁ = κ₂) :=
  match Heq in _ = κ₂ return K⟦ κ₁ ⟧ → K⟦ κ₂ ⟧ → IProp with
  | eq_refl => kind_equiv κ₁
  end.

Notation "K⟦ Heq :⊨ I₁ ≡ I₂ ⟧" := (kind_equiv_eq Heq I₁ I₂) (Heq at level 30).

(** * Equivalence of type variables *)

Lemma kind_eq_proofs_unicity {κ₁ κ₂ : kind} (P₁ P₂ : κ₁ = κ₂) : P₁ = P₂.
Proof.
apply eq_proofs_unicity_on.
decide equality.
Qed.

Lemma rel_equiv_var κ κ₁ κ₂
    (Heq  : κ₁ = κ₂)
    (Heq₁ : κ₁ = κ) (Heq₂ : κ₂ = κ)
    (I₁ : K⟦ κ₁ ⟧) (I₂ : K⟦ κ₂ ⟧) n :
  n ⊨ K⟦ Heq :⊨ I₁ ≡ I₂ ⟧ →
  n ⊨ K⟦ eq_rect κ₁ (λ κ, K⟦ κ ⟧) I₁ κ Heq₁ 
       ≡ eq_rect κ₂ (λ κ, K⟦ κ ⟧) I₂ κ Heq₂ ∷ κ ⟧.
Proof.
destruct Heq.
rewrite (kind_eq_proofs_unicity Heq₁ Heq₂); clear Heq₁.
destruct κ₁; destruct κ; try discriminate;
  rewrite (kind_eq_proofs_unicity Heq₂ eq_refl); simpl; auto.
Qed.

(** * applying equivalence *)

Lemma apply_type_equiv I₁ I₂ v₁ v₂ n :
  n ⊨ K⟦ I₁ ≡ I₂ ∷ k_type ⟧ → n ⊨ I₁ v₁ v₂ ⇔ I₂ v₁ v₂.
Proof.
  intro H; iapply H.
Qed.

Lemma apply_sig_equiv I₁ I₂ v₁ v₂ μ n :
  n ⊨ K⟦ I₁ ≡ I₂ ∷ k_sig ⟧ → n ⊨ I₁ v₁ v₂ μ ⇔ I₂ v₁ v₂ μ.
Proof.
  intro H; iapply H.
Qed.

Lemma apply_effect_equiv I₁ I₂ v₁ v₂ ls₁ ls₂ μ n :
  n ⊨ K⟦ I₁ ≡ I₂ ∷ k_effect ⟧ → n ⊨ edom_rel I₁ v₁ v₂ ls₁ ls₂ μ ⇔ edom_rel I₂ v₁ v₂ ls₁ ls₂ μ.
Proof.
  intro H; iapply H.
Qed.
