Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Env Binding.Set Binding.Product.
Require Import Types Kinding Fresh.Syntax.
Require Import LogicalRelation.Core LogicalRelation.KindEquiv.

Definition lift_Δ_equiv {TV₁ TV₂ LV₁ LV₂ : Set}
    (Φ : {| sfst := TV₁; ssnd := LV₁ |} [→] {| sfst := TV₂; ssnd := LV₂ |})
    {Δ₁ : TV₁ → kind} {Δ₂ : TV₂ → kind}
    (κ : kind)
    (Δequiv : ∀ α, Δ₁ α = Δ₂ (arr₁ Φ α))
    (α : inc TV₁) :=
  match α return (Δ₁ ▹ κ) α = (Δ₂ ▹ κ) (arr₁ (liftA Φ : incV _ [→] incV _) α)
  with
  | VZ   => eq_refl
  | VS β => Δequiv β
  end.

Fixpoint type_interp_weaken (κ : kind) {TV₁ TV₂ LV₁ LV₂ : Set}
    (Φ : {| sfst := TV₁; ssnd := LV₁ |} [→] {| sfst := TV₂; ssnd := LV₂ |})
    {Δ₁ : TV₁ → kind} {Δ₂ : TV₂ → kind}
    (τ  : typ_ TV₁ LV₁)
    (η₁ : ∀ α : TV₁, K⟦ Δ₁ α ⟧) (η₂ : ∀ α : TV₂, K⟦ Δ₂ α ⟧)
    (ϑ₁ : LV₁ → ℑ) (ϑ₂ : LV₂ → ℑ)
    {WF₁ : K[ Δ₁ ; LV₁ ⊢ τ ∷ κ ]}
    {WF₂ : K[ Δ₂ ; LV₂ ⊢ fmap Φ τ ∷ κ ]}
    (Δequiv : ∀ α, Δ₁ α = Δ₂ (arr₁ Φ α)) n {struct τ} :
  n ⊨ (∀ᵢ α, K⟦ (Δequiv α) :⊨ (η₁ α) ≡ (η₂ (arr₁ Φ α)) ⟧) →
  n ⊨ (∀ᵢ a, ϑ₁ a ≈ᴵ ϑ₂ (arr₂ Φ a)) →
  n ⊨ K⟦ (⟦ τ ∷ κ ⟧ η₁ ! ϑ₁) ≡ (⟦ fmap Φ τ ∷ κ ⟧ η₂ ! ϑ₂) ∷ κ ⟧.
Proof.
  intros Hη Hϑ; destruct τ as [| α | | | | l | | | |]; invert_kind WF₁; simpl type_interp;
    [reflexivity | | unfold kind_equiv ..].
  - eapply rel_equiv_var; iapply Hη.
  - apply equiv_arr.
    + eapply (type_interp_weaken k_type); eassumption.
    + eapply (type_interp_weaken k_type); eassumption.
    + eapply (type_interp_weaken k_effect); eassumption.
  - apply equiv_tall; iintro R.
    apply type_interp_weaken with (κ := k_type) (Δequiv := lift_Δ_equiv Φ _ Δequiv); [| exact Hϑ].
    iintro α; destruct α as [| α]; simpl; [reflexivity | iapply Hη].
  - apply equiv_lall; [eapply (type_interp_weaken k_sig); eassumption | iintro R].
    eapply (type_interp_weaken k_type); [eassumption |].
    iintro a; destruct a as [| a]; simpl; [reflexivity | iapply Hϑ].
  - apply equiv_instance; iapply Hϑ.
  - reflexivity.
  - apply equiv_cons; eapply (type_interp_weaken k_effect); eassumption.
  - apply equiv_sig; eapply (type_interp_weaken k_type); eassumption.
  - apply equiv_talls; intros R.
    apply type_interp_weaken with (κ := k_sig) (Δequiv := lift_Δ_equiv Φ _ Δequiv); [| exact Hϑ].
    iintro α; destruct α as [| α]; simpl; [reflexivity | iapply Hη].
Qed.

Lemma type_interp_shiftL {TV LV : Set} {Δ : TV → kind}
      κ
      (τ  : typ_ TV LV)
      (η  : ∀ α : TV, K⟦ Δ α ⟧)
      (ϑ  : LV → ℑ) (I : ℑ)
      {WF : K[ Δ ; LV ⊢ τ ∷ κ]} n :
  n ⊨ K⟦ (⟦ τ ∷ κ ⟧ η ! ϑ) ≡ (⟦ shift τ ∷ κ ⟧ η ! (ϑ ▹ I)) ∷ κ ⟧.
Proof.
  apply type_interp_weaken with (Δequiv := λ α, eq_refl); iintro; reflexivity.
Qed.

Lemma type_interp_shiftT {TV LV : Set} {Δ : TV → kind}
      κ
      (τ  : typ_ TV LV)
      (η  : ∀ α : TV, K⟦ Δ α ⟧)
      (ϑ  : LV → ℑ) κ' (R : K⟦ κ' ⟧)
      {WF : K[ Δ ; LV ⊢ τ ∷ κ]} n :
  n ⊨ K⟦ (⟦ τ ∷ κ ⟧ η ! ϑ) ≡ (⟦ shift τ ∷ κ ⟧ (η :▹ R) ! ϑ) ∷ κ ⟧.
Proof.
  apply type_interp_weaken with (Δequiv := λ α, eq_refl); iintro; reflexivity.
Qed.

(* ========================================================================= *)
(* Interpretation of effects *)

Lemma rel_eff_rel_weakenL {TV LV : Set} {Δ : TV → kind}
    (ε  : typ_ TV LV)
    (η  : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ  : LV → ℑ) (I : ℑ)
    {WF : K[ Δ ; LV ⊢ ε ∷ k_effect]}
    (e₁ e₂ : expr0) ls₁ ls₂ μ n :
  n ⊨ edom_rel (F⟦ ε ⟧ η ! ϑ) e₁ e₂ ls₁ ls₂ μ ⇔ edom_rel (F⟦ shift ε ⟧ η ! (ϑ ▹ I)) e₁ e₂ ls₁ ls₂ μ.
Proof.
  apply apply_effect_equiv, (type_interp_shiftL k_effect).
Qed.

Lemma rel_eff_rel_weakenT {TV LV : Set} {Δ : TV → kind}
    (ε  : typ_ TV LV)
    (η  : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ  : LV → ℑ) κ (R : K⟦ κ ⟧)
    {WF : K[ Δ ; LV ⊢ ε ∷ k_effect]}
    (e₁ e₂ : expr0) ls₁ ls₂ μ n :
  n ⊨ edom_rel (F⟦ ε ⟧ η ! ϑ) e₁ e₂ ls₁ ls₂ μ ⇔ edom_rel (F⟦ shift ε ⟧ η :▹ R ! ϑ) e₁ e₂ ls₁ ls₂ μ.
Proof.
  apply apply_effect_equiv, (type_interp_shiftT k_effect).
Qed.

Lemma rel_eff_rel_weakenL_l {TV LV : Set} {Δ : TV → kind}
    (ε  : typ_ TV LV)
    (η  : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ  : LV → ℑ) (I : ℑ)
    {WF : K[ Δ ; LV ⊢ ε ∷ k_effect]}
    (e₁ e₂ : expr0) ls₁ ls₂ μ n :
  n ⊨ edom_rel F⟦ shift ε ⟧ η ! (ϑ ▹ I) e₁ e₂ ls₁ ls₂ μ →
  n ⊨ edom_rel F⟦ ε ⟧ η ! ϑ e₁ e₂ ls₁ ls₂ μ.
Proof.
  intros HH; eapply I_iff_elim_M, HH.
  apply rel_eff_rel_weakenL.
Qed.

(* ========================================================================= *)
(* Interpretation of types *)

Lemma rel_v_weakenL {TV LV : Set} {Δ : TV → kind}
    (τ  : typ_ TV LV)
    (η  : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ  : LV → ℑ) (I : ℑ)
    {WF : K[ Δ ; LV ⊢ τ ∷ k_type]}
    (v₁ v₂ : value0) n :
  n ⊨ (⟦ τ ⟧ η ! ϑ) v₁ v₂ ⇔
      (⟦ shift τ ⟧ η ! (ϑ ▹ I)) v₁ v₂.
Proof.
  apply apply_type_equiv, (type_interp_shiftL k_type).
Qed.

Lemma rel_v_weakenL_l {TV LV : Set} {Δ : TV → kind}
    (τ  : typ_ TV LV)
    (η  : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ  : LV → ℑ) (I : ℑ)
    {WF : K[ Δ ; LV ⊢ τ ∷ k_type]}
    (v₁ v₂ : value0) n :
  n ⊨ (⟦ shift τ ⟧ η ! (ϑ ▹ I)) v₁ v₂ →
  n ⊨ (⟦ τ ⟧ η ! ϑ) v₁ v₂.
Proof.
  intros HH; eapply I_iff_elim_M, HH; apply rel_v_weakenL.
Qed.

Lemma rel_v_weakenL_r {TV LV : Set} {Δ : TV → kind}
    (τ  : typ_ TV LV)
    (η  : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ  : LV → ℑ) (I : ℑ)
    {WF : K[ Δ ; LV ⊢ τ ∷ k_type]}
    (v₁ v₂ : value0) n :
  n ⊨ (⟦ τ ⟧ η ! ϑ) v₁ v₂ →
  n ⊨ (⟦ shift τ ⟧ η ! (ϑ ▹ I)) v₁ v₂.
Proof.
  intros HH; eapply I_iff_elim_M, HH.
  symmetry; apply rel_v_weakenL.
Qed.

Lemma rel_v_weakenT {TV LV : Set} {Δ : TV → kind}
      (τ : typ_ TV LV)
      (η  : ∀ α : TV, K⟦ Δ α ⟧)
      (ϑ  : LV → ℑ) κ (R : K⟦ κ ⟧)
      {WF : K[ Δ ; LV ⊢ τ ∷ k_type]}
      (v₁ v₂ : value0) n :
  n ⊨ (⟦ τ ⟧ η ! ϑ) v₁ v₂ ⇔
      (⟦ shift τ ⟧ η :▹ R ! ϑ) v₁ v₂.
Proof.
  apply apply_type_equiv, (type_interp_shiftT k_type).
Qed.

Lemma rel_v_weakenTl {TV LV : Set} {Δ : TV → kind}
      (τ : typ_ TV LV)
      (η  : ∀ α : TV, K⟦ Δ α ⟧)
      (ϑ  : LV → ℑ) κ (R : K⟦ κ ⟧)
      {WF : K[ Δ ; LV ⊢ τ ∷ k_type]}
      (v₁ v₂ : value0) n :
  n ⊨ (⟦ τ ⟧ η ! ϑ) v₁ v₂ →
  n ⊨ (⟦ shift τ ⟧ η :▹ R ! ϑ) v₁ v₂.
Proof.
  intros HH; eapply I_iff_elim_M, HH.
  symmetry; apply rel_v_weakenT.
Qed.

Lemma rel_v_weakenTr {TV LV : Set} {Δ : TV → kind}
      (τ : typ_ TV LV)
      (η  : ∀ α : TV, K⟦ Δ α ⟧)
      (ϑ  : LV → ℑ) κ (R : K⟦ κ ⟧)
      {WF : K[ Δ ; LV ⊢ τ ∷ k_type]}
      (v₁ v₂ : value0) n :
  n ⊨ (⟦ shift τ ⟧ η :▹ R ! ϑ) v₁ v₂ →
  n ⊨ (⟦ τ ⟧ η ! ϑ) v₁ v₂.
Proof.
  intros HH; eapply I_iff_elim_M, HH; apply rel_v_weakenT.
Qed.

(* ========================================================================= *)
(* Interpretation of signatures *)

Lemma rel_s_weakenL {TV LV : Set} {Δ : TV → kind}
    (τ  : typ_ TV LV)
    (η  : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ  : LV → ℑ) (I : ℑ)
    {WF : K[ Δ ; LV ⊢ τ ∷ k_sig]}
    (v₁ v₂ : value0) μ n :
  n ⊨ (Σ⟦ τ ⟧ η ! ϑ) v₁ v₂ μ ⇔
      (Σ⟦ shift τ ⟧ η ! (ϑ ▹ I)) v₁ v₂ μ.
Proof.
  apply apply_sig_equiv, (type_interp_shiftL k_sig).
Qed.

Lemma rel_s_weakenT {TV LV : Set} {Δ : TV → kind}
      (τ : typ_ TV LV)
      (η  : ∀ α : TV, K⟦ Δ α ⟧)
      (ϑ  : LV → ℑ) κ (R : K⟦ κ ⟧)
      {WF : K[ Δ ; LV ⊢ τ ∷ k_sig]}
      (v₁ v₂ : value0) μ n :
  n ⊨ (Σ⟦ τ ⟧ η ! ϑ) v₁ v₂ μ ⇔
      (Σ⟦ shift τ ⟧ η :▹ R ! ϑ) v₁ v₂ μ.
Proof.
  apply apply_sig_equiv, (type_interp_shiftT k_sig).
Qed.
