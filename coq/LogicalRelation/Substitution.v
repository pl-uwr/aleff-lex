Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Env Binding.Set Binding.Product.
Require Import Types Kinding Fresh.Syntax.
Require Import LogicalRelation.Core LogicalRelation.KindEquiv.
Require Import LogicalRelation.KindingIrrelevance.
Require Import LogicalRelation.Weakening.

Lemma type_interp_bind_var {TV LV : Set} (τ : typ_ TV LV)
  (Δ : TV → kind)
  (η : ∀ α : TV, K⟦ Δ α ⟧) (ϑ : LV → ℑ)
  κ₁ κ₂ (Heq : κ₁ = κ₂) (I : K⟦ κ₁ ⟧)
  {WF₁ : K[ Δ; LV ⊢ τ ∷ κ₁ ]}
  {WF₂ : K[ Δ; LV ⊢ τ ∷ κ₂ ]}
  n :
  n ⊨ K⟦ I ≡ ⟦ τ ∷ κ₁ ⟧ η ! ϑ ∷ κ₁ ⟧ →
  n ⊨ K⟦ eq_rect κ₁ (λ κ, K⟦ κ ⟧) I κ₂ Heq ≡ ⟦ τ ∷ κ₂ ⟧ η ! ϑ ∷ κ₂ ⟧.
Proof.
  intro Hκ₁.
  destruct κ₁; destruct κ₂; try discriminate; rewrite (kind_eq_proofs_unicity Heq eq_refl);
    simpl eq_rect; (etransitivity; [eassumption | apply type_interp_irr]).
Qed.

Fixpoint type_interp_bind (κ : kind) {TV₁ TV₂ LV₁ LV₂ : Set}
    (Φ : {| sfst := TV₁; ssnd := LV₁ |} {t⇒} {| sfst := TV₂; ssnd := LV₂ |})
    {Δ₁ : TV₁ → kind} {Δ₂ : TV₂ → kind}
    (τ  : typ_ TV₁ LV₁)
    (η₁ : ∀ α : TV₁, K⟦ Δ₁ α ⟧) (η₂ : ∀ α : TV₂, K⟦ Δ₂ α ⟧)
    (ϑ₁ : LV₁ → ℑ) (ϑ₂ : LV₂ → ℑ)
    {WF₁ : K[ Δ₁ ; LV₁ ⊢ τ ∷ κ ]}
    {WF₂ : K[ Δ₂ ; LV₂ ⊢ bind Φ τ ∷ κ ]}
    {WFΦ : wf_subst Δ₁ Δ₂ Φ}
    n {struct τ} :
  n ⊨ (∀ᵢ α, K⟦ η₁ α ≡ ⟦ tsub_var Φ α ∷ Δ₁ α ⟧ η₂ ! ϑ₂ ∷ (Δ₁ α) ⟧) →
  n ⊨ (∀ᵢ a, ϑ₁ a ≈ᴵ ϑ₂ (tsub_lbl Φ a)) →
  n ⊨ K⟦ (⟦ τ ∷ κ ⟧ η₁ ! ϑ₁) ≡ (⟦ bind Φ τ ∷ κ ⟧ η₂ ! ϑ₂) ∷ κ ⟧.
Proof.
  intros Hη Hϑ; destruct τ as [| α | | | | l | | | |]; invert_kind WF₁ as WF; simpl type_interp;
    [reflexivity | | unfold kind_equiv ..].
  - unfold apply_to_t_var; eapply type_interp_bind_var.
    term_simpl; etransitivity; [iapply Hη | apply type_interp_irr].
  - apply equiv_arr.
    + eapply (type_interp_bind k_type); eassumption.
    + eapply (type_interp_bind k_type); eassumption.
    + eapply (type_interp_bind k_effect); eassumption.
  - apply equiv_tall; iintro R; eapply (type_interp_bind k_type); [| exact Hϑ].
    iintro α; destruct α as [| α]; simpl.
    + eapply rel_equiv_var with (Heq := eq_refl) (Heq₁ := eq_refl); reflexivity.
    + etransitivity; [iapply Hη | apply type_interp_shiftT].
  - apply equiv_lall; [eapply (type_interp_bind k_sig); eassumption | iintro R].
    eapply (type_interp_bind k_type).
    + simpl; iintro α; etransitivity; [iapply Hη | apply type_interp_shiftL].
    + iintro a; destruct a as [| a]; simpl; [reflexivity | iapply Hϑ].
  - apply equiv_instance; iapply Hϑ.
  - reflexivity.
  - apply equiv_cons; eapply (type_interp_bind k_effect); eassumption.
  - apply equiv_sig; eapply (type_interp_bind k_type); eassumption.
  - apply equiv_talls; intros R; eapply (type_interp_bind k_sig); [| exact Hϑ].
    iintro α; destruct α as [| α]; simpl.
    + eapply rel_equiv_var with (Heq := eq_refl) (Heq₁ := eq_refl); reflexivity.
    + etransitivity; [iapply Hη | apply type_interp_shiftT].
Qed.

Lemma type_interp_substL {TV LV : Set} {Δ : TV → kind}
      {η : ∀ α, K⟦ Δ α ⟧} {ϑ : LV → ℑ} {I : ℑ}
      κ {τ : typ (incL {| sfst := TV |})}
      {WFτ : K[Δ; inc LV ⊢ τ ∷ κ]}
      {a : s_lbl {| sfst := TV |}} n
      (HI : n ⊨ instance_equiv I (ϑ a)) :
  n ⊨ K⟦ ⟦ τ ∷ κ ⟧ η ! (ϑ ▹ I) ≡ ⟦ subst τ a ∷ κ ⟧ η ! ϑ ∷ κ ⟧.
Proof.
  apply type_interp_bind with _.
  - iintro α; simpl; apply rel_equiv_var with (Heq := eq_refl) (Heq₁ := eq_refl); reflexivity.
  - iintro b; destruct b as [| b]; simpl; [assumption | reflexivity].
Qed.

Lemma type_interp_substT {TV LV : Set} {Δ : TV → kind}
      {η : ∀ α, K⟦ Δ α ⟧} {ϑ : LV → ℑ}
      κ κ' {τ : typ (incV {| sfst := TV |})} σ
      {WFτ : K[Δ ▹ κ'; LV ⊢ τ ∷ κ]}
      {WFσ : K[Δ; LV ⊢ σ ∷ κ']} R n
      (HR : n ⊨ K⟦ R ≡ ⟦ σ ∷ κ' ⟧ η ! ϑ ∷ κ' ⟧) :
  n ⊨ K⟦ ⟦ τ ∷ κ ⟧ η :▹ R ! ϑ ≡ ⟦ subst τ σ ∷ κ ⟧ η ! ϑ ∷ κ ⟧.
Proof.
  apply type_interp_bind with _.
  - iintro α; destruct α as [| α]; simpl.
    + etransitivity; [eassumption | apply type_interp_irr].
    + apply rel_equiv_var with (Heq := eq_refl) (Heq₁ := eq_refl); reflexivity.
  - iintro b; simpl; reflexivity.
Qed.

Lemma rel_v_substL {TV LV : Set} {Δ : TV → kind}
      {η : ∀ α, K⟦ Δ α ⟧} {ϑ : LV → ℑ} {I : ℑ}
      {τ : typ (incL {| sfst := TV |})}
      {WFτ : K[Δ; inc LV ⊢ τ ∷ k_type]}
      {a : s_lbl {| sfst := TV |}}
      {v₁ v₂ n}
      (HI : n ⊨ instance_equiv I (ϑ a)) :
        n ⊨ (⟦ τ ⟧ η ! (ϑ ▹ I)) v₁ v₂ ⇔
          (⟦ subst τ a ⟧ η ! ϑ) v₁ v₂.
Proof.
  apply apply_type_equiv, type_interp_substL, HI.
Qed.
