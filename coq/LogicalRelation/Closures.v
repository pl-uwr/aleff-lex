Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Product Binding.Env.
Require Import Fresh.Syntax Fresh.Reduction.
Require Import LogicalRelation.ProgramObs.
Import TacticSynonyms.

Definition expr_rel_sig V LV := (expr_ V LV →ₛ expr_ V LV →ₛ *ₛ)%irel_sig.
Notation Rexpr V LV := (IRel (expr_rel_sig V LV)).
Notation RCexpr := (Rexpr ∅ ∅).

Definition ectx_rel_sig V LV := (ectx_ V LV →ₛ ectx_ V LV →ₛ *ₛ)%irel_sig.
Notation Rectx V LV := (IRel (ectx_rel_sig V LV)).
Notation RCectx := (Rectx ∅ ∅).

Definition stuck_rel_sig V LV :=
  (rctx_ V LV →ₛ rctx_ V LV →ₛ expr_ V LV →ₛ expr_ V LV →ₛ *ₛ)%irel_sig.
Notation Rstuck V LV := (IRel (stuck_rel_sig V LV)).
Notation RCstuck := (Rstuck ∅ ∅).

Definition handler_rel_sig V LV :=
  (handler_ V LV →ₛ handler_ V LV →ₛ *ₛ)%irel_sig.
Notation Rhandler V LV := (IRel (handler_rel_sig V LV)).
Notation RChandler := (Rhandler ∅ ∅).

Definition type_rel_sig V LV := (value_ V LV →ₛ value_ V LV →ₛ *ₛ)%irel_sig.
Notation Rtype V LV := (IRel (type_rel_sig V LV)).
Notation RCtype := (Rtype ∅ ∅).

Definition effect_rel_sig V LV :=
  (expr_ V LV →ₛ expr_ V LV →ₛ list ltag →ₛ list ltag →ₛ Rexpr V LV →ₛ *ₛ)%irel_sig.
Notation Reff V LV := (IRel (effect_rel_sig V LV)).
Notation RCeff := (Reff ∅ ∅).

Definition sig_rel_sig V LV :=
  (value_ V LV →ₛ value_ V LV →ₛ Rtype V LV →ₛ *ₛ)%irel_sig.
Notation Rsig V LV := (IRel (sig_rel_sig V LV)).
Notation RCsig := (Rsig ∅ ∅).

Definition subst_rel_sig V LV :=
  ((⟨V, LV⟩ {⇒} ⟨∅, ∅⟩) →ₛ (⟨V, LV⟩ {⇒} ⟨∅, ∅⟩) →ₛ *ₛ)%irel_sig.
Notation Rsubst V LV := (IRel (subst_rel_sig V LV)).

Definition semFresh1 l (R : RCeff) : IProp :=
  ∀ᵢ e₁ e₂ ls₁ ls₂ μ, R e₁ e₂ ls₁ ls₂ μ ⇒ (forall l', l ≤ l' → ¬ List.In l' ls₁)ᵢ.

Definition semFresh2 l (R : RCeff) : IProp :=
  ∀ᵢ e₁ e₂ ls₁ ls₂ μ, R e₁ e₂ ls₁ ls₂ μ ⇒ (forall l', l ≤ l' → ¬ List.In l' ls₂)ᵢ.

Record effect_dom : Type :=
  { edom_rel :> RCeff
  ; edom_ft1 : ltag
  ; edom_ft2 : ltag
  ; edom_t1_fresh : ⊨ semFresh1 edom_ft1 edom_rel
  ; edom_t2_fresh : ⊨ semFresh2 edom_ft2 edom_rel
  }.

Record ℑ : Type :=
  { ℑ_rel   :> RCsig
  ; ℑ_ltag1 :  ltag
  ; ℑ_ltag2 :  ltag
  }.

(** Relation on stuck expressions defined as a functor: the
 interpretations of effects and of the "recursive" call to the
 interpretation of expressions are taken as parameters. *)
Definition Frel_stuck_cl
    (RelF  : effect_dom)
    (RelE  : RCexpr) : RCstuck :=
  λ R₁ R₂ e₁ e₂,
    ∃ᵢ ls₁ ls₂ μ,
      edom_rel RelF e₁ e₂ ls₁ ls₂ μ ∧ᵢ
      (rfree_l ls₁ R₁ ∧ rfree_l ls₂ R₂)ᵢ ∧ᵢ
      ∀ᵢ e₁' e₂', μ e₁' e₂' ⇒ ▷ RelE (rplug R₁ e₁') (rplug R₂ e₂').

(** Relation on evaluation contexts defined as a functor: the
 interpretations of effects, types, and of the "recursive" call to
 the interpretation of expressions are taken as parameters. *)
Definition Frel_ectx_cl
    (RelF  : effect_dom)
    (RelV  : RCtype)
    (RelE  : RCexpr) : RCectx :=
  λ E₁ E₂,
    (∀ᵢ v₁ v₂, RelV v₁ v₂ ⇒ Obs (plug E₁ v₁) (plug E₂ v₂)) ∧ᵢ
    (∀ᵢ R₁ R₂ e₁ e₂,
      Frel_stuck_cl RelF RelE R₁ R₂ e₁ e₂ ⇒
      Obs (plug E₁ (rplug R₁ e₁)) (plug E₂ (rplug R₂ e₂))).

(** Relation on expressions defined as a functor: the interpretations
of effects, types, and of the "recursive" call to the
interpretation of expressions are taken as parameters. *)
Definition Frel_expr_cl
    (RelF  : effect_dom)
    (RelV  : RCtype)
    (RelE  : RCexpr) : RCexpr :=
  λ e₁ e₂,
    ∀ᵢ E₁ E₂, Frel_ectx_cl RelF RelV RelE E₁ E₂ ⇒
      Obs (plug E₁ e₁) (plug E₂ e₂).

Lemma Frel_expr_cl_contractive
    (RelF  : effect_dom)
    (RelV  : RCtype) :
  contractive _ (Frel_expr_cl RelF RelV).
Proof.
  intros R₁ R₂ n; iintro HR.
  iintro e₁; iintro e₂; unfold Frel_expr_cl; auto_contr.
  unfold Frel_ectx_cl; auto_contr.
  unfold Frel_stuck_cl; auto_contr.
  iapply HR.
Qed.

(** With the closurer on expressions defined as a functor, we can tie
 the recursion via the fixed-point operator for COFEs. Note that we
 have to prove the functor is contractive (which it is, as we used
 "later" in the definition). *)
Definition rel_expr_cl_fix
    (RelF  : effect_dom)
    (RelV  : RCtype) : RCexpr :=
  I_fix _
    (Frel_expr_cl RelF RelV)
    (Frel_expr_cl_contractive RelF RelV).

Notation rel_expr_cl RelF RelV :=
  (Frel_expr_cl RelF RelV (rel_expr_cl_fix RelF RelV)).
Notation rel_ectx_cl RelF RelV :=
  (Frel_ectx_cl RelF RelV (rel_expr_cl_fix RelF RelV)).
Notation rel_stuck_cl RelF RelV :=
  (Frel_stuck_cl RelF (rel_expr_cl_fix RelF RelV)).

Definition rel_expr_cl1 (RA : RCtype) (RR : RCexpr) : Rexpr (inc ∅) ∅ :=
  λ e₁ e₂, ∀ᵢ v₁ v₂, RA v₁ v₂ ⇒ RR (subst e₁ v₁) (subst e₂ v₂).

Definition rel_expr_cl_sub {V LV} (RG : Rsubst V LV) (RR : RCexpr) : Rexpr V LV :=
  λ e₁ e₂, ∀ᵢ γ₁ γ₂, RG γ₁ γ₂ ⇒ RR (bind γ₁ e₁) (bind γ₂ e₂).

Definition rel_val_cl_sub {V LV} (RG : Rsubst V LV) (RR : RCtype) : Rtype V LV :=
  λ v₁ v₂, ∀ᵢ γ₁ γ₂, RG γ₁ γ₂ ⇒ RR (bind γ₁ v₁) (bind γ₂ v₂).

Definition rel_handler_cl (RS : RCsig) (RA : RCtype → RCtype) (RR : RCexpr) : RChandler :=
  λ h₁ h₂,
  match h₁, h₂ with
  | h_do e₁, h_do e₂ =>
    ∀ᵢ v₁ v₂ I, RS v₁ v₂ I ⇒
       ∀ᵢ u₁ u₂, RA I u₁ u₂ ⇒ RR (subst (subst e₁ (shift u₁)) v₁) (subst (subst e₂ (shift u₂)) v₂)
  end.

Section Properties.
  Context {V LV} {RG : Rsubst V LV} {RF : effect_dom} {Rv : RCtype}.

  Section Closed.
    Context {v₁ v₂ : value0} (e₁ e₂ : expr0) (R₁ R₂ : rctx0).

    Section Obs.
      Context (E₁ E₂ : ectx0).

      Lemma rel_k_value {n}
            (HRE : n ⊨ rel_ectx_cl RF Rv E₁ E₂)
            (HRv : n ⊨ Rv v₁ v₂) :
        n ⊨ Obs (plug E₁ v₁) (plug E₂ v₂).
      Proof.
        idestruct HRE as HFv HFs; now iapply HFv.
      Qed.

      Lemma rel_k_stuck {n}
            (HRE : n ⊨ rel_ectx_cl RF Rv E₁ E₂)
            (HRs : n ⊨ rel_stuck_cl RF Rv R₁ R₂ e₁ e₂) :
        n ⊨ Obs (plug E₁ (rplug R₁ e₁)) (plug E₂ (rplug R₂ e₂)).
      Proof.
        idestruct HRE as HFv HFs; now iapply HFs.
      Qed.

      Lemma rel_k_expr {n}
            (HRE : n ⊨ rel_ectx_cl RF Rv E₁ E₂)
            (HRe : n ⊨ rel_expr_cl RF Rv e₁ e₂) :
        n ⊨ Obs (plug E₁ e₁) (plug E₂ e₂).
      Proof.
        now iapply HRe.
      Qed.

    End Obs.

    Lemma rel_v_in_e {n}
          (HRE : n ⊨ Rv v₁ v₂) :
      n ⊨ rel_expr_cl RF Rv v₁ v₂.
    Proof.
      iintro E₁; iintro E₂; iintro HF; now apply rel_k_value.
    Qed.

    Lemma rel_s_in_e {n}
          (HRs : n ⊨ rel_stuck_cl RF Rv R₁ R₂ e₁ e₂) :
      n ⊨ rel_expr_cl RF Rv (rplug R₁ e₁) (rplug R₂ e₂).
    Proof.
      iintro E₁; iintro E₂; iintro HRE; now apply rel_k_stuck.
    Qed.

    Section Contraction.
      Context {e₁' e₂' : expr0}.

      Lemma rel_e_contr_l {n}
            (HC : contr e₁' e₁)
            (HRe : n ⊨ ▷ rel_expr_cl RF Rv e₁ e₂) :
        n ⊨ rel_expr_cl RF Rv e₁' e₂.
      Proof.
        iintro F₁; iintro F₂; iintro HF.
        eapply Obs_red_l; [apply red_contr, HC |].
        later_shift; now apply rel_k_expr.
      Qed.

      Lemma rel_e_contr_r {n}
            (HC : contr e₂' e₂)
            (HRe : n ⊨ rel_expr_cl RF Rv e₁ e₂) :
        n ⊨ rel_expr_cl RF Rv e₁ e₂'.
      Proof.
        iintro F₁; iintro F₂; iintro HF.
        eapply Obs_red_r; [apply red_contr, HC |].
        now apply rel_k_expr.
      Qed.

      Lemma rel_e_contr_both {n}
            (HC₁ : contr e₁' e₁)
            (HC₂ : contr e₂' e₂)
            (HRe : n ⊨ ▷ rel_expr_cl RF Rv e₁ e₂) :
        n ⊨ rel_expr_cl RF Rv e₁' e₂'.
      Proof.
        iintro F₁; iintro F₂; iintro HF.
        eapply Obs_red_both; [apply red_contr; eassumption ..|].
        later_shift; now apply rel_k_expr.
      Qed.

    End Contraction.

  End Closed.

  Lemma rel_v_in_e_open {v₁ v₂} {n}
        (HRv : n ⊨ rel_val_cl_sub RG Rv v₁ v₂) :
    n ⊨ rel_expr_cl_sub RG (rel_expr_cl RF Rv) v₁ v₂.
  Proof.
    iintro γ₁; iintro γ₂; iintro H; term_simpl.
    apply rel_v_in_e; now iapply HRv.
  Qed.

  Section Roll.

    Lemma rel_expr_cl_fix_roll (e₁ e₂ : expr0) {n}
          (HRe : n ⊨ rel_expr_cl RF Rv e₁ e₂) :
      n ⊨ rel_expr_cl_fix RF Rv e₁ e₂.
    Proof.
      apply (I_fix_roll (expr_rel_sig ∅ ∅)), HRe.
    Qed.

    Lemma rel_expr_cl_fix_unroll (e₁ e₂ : expr0) {n}
          (HRe : n ⊨ rel_expr_cl_fix RF Rv e₁ e₂) :
      n ⊨ rel_expr_cl RF Rv e₁ e₂.
    Proof.
      apply (I_fix_unroll (expr_rel_sig ∅ ∅)), HRe.
    Qed.

    Lemma rel_expr_cl_fix_eq {n} :
      n ⊨ rel_expr_cl RF Rv ≈ᵢ rel_expr_cl_fix RF Rv.
    Proof.
      iintro e₁'; iintro e₂'; isplit; iintro HR;
        auto using rel_expr_cl_fix_roll, rel_expr_cl_fix_unroll.
    Qed.

    Lemma rel_s_roll e₁ e₂ R₁ R₂ ls₁ ls₂ μ {n}
          (HRF : n ⊨ edom_rel RF e₁ e₂ ls₁ ls₂ μ)
          (HRfree₁ : rfree_l ls₁ R₁)
          (HRfree₂ : rfree_l ls₂ R₂)
          (HRe : ∀ e₁' e₂', n ⊨ μ e₁' e₂' ⇒ ▷ rel_expr_cl RF Rv (rplug R₁ e₁') (rplug R₂ e₂')) :
      n ⊨ rel_stuck_cl RF Rv R₁ R₂ e₁ e₂.
    Proof.
      iexists ls₁; iexists ls₂; iexists μ; isplit; [assumption | isplit; [now split |]].
      iintro e₁'; iintro e₂'; specialize (HRe e₁' e₂'); iintro Hμ.
      ispecialize HRe Hμ; later_shift; now apply rel_expr_cl_fix_roll.
    Qed.

    Lemma rel_s_unroll e₁ e₂ R₁ R₂ {n}
          (Hs : n ⊨ rel_stuck_cl RF Rv R₁ R₂ e₁ e₂) :
      ∃ ls₁ ls₂ μ, (n ⊨ edom_rel RF e₁ e₂ ls₁ ls₂ μ) ∧
                   (rfree_l ls₁ R₁ ∧ rfree_l ls₂ R₂) ∧
                   (n ⊨ ∀ᵢ e₁' e₂', μ e₁' e₂' ⇒ ▷ rel_expr_cl RF Rv (rplug R₁ e₁') (rplug R₂ e₂')).
    Proof.
      idestruct Hs as ls₁ Hs; idestruct Hs as ls₂ Hs; idestruct Hs as μ Hs; exists ls₁, ls₂, μ.
      idestruct Hs as HF Hs; split; [assumption |].
      idestruct Hs as Hfree Hs; split; [assumption |].
      iintro e₁'; iintro e₂'; iintro Hμ; iapply Hs in Hμ; later_shift.
      now apply rel_expr_cl_fix_unroll.
    Qed.

    Lemma rel_k_roll E₁ E₂ {n}
          (HRv : ∀ v₁ v₂, n ⊨ Rv v₁ v₂ ⇒ Obs (plug E₁ v₁) (plug E₂ v₂))
          (HRs : ∀ R₁ R₂ e₁ e₂, n ⊨ rel_stuck_cl RF Rv R₁ R₂ e₁ e₂ ⇒
                                  Obs (plug E₁ (rplug R₁ e₁)) (plug E₂ (rplug R₂ e₂))) :
      n ⊨ rel_ectx_cl RF Rv E₁ E₂.
    Proof.
      isplit.
      - iintro u₁; iintro u₂; apply HRv.
      - iintro R₁'; iintro R₂'; iintro e₁'; iintro e₂'; apply HRs.
    Qed.

  End Roll.

End Properties.

(** * Equivalence of semantic instances *)

Definition instance_equiv (I₁ I₂ : ℑ) : IProp :=
  ℑ_rel I₁ ≈ᵢ ℑ_rel I₂ ∧ᵢ
  (ℑ_ltag1 I₁ = ℑ_ltag1 I₂ ∧ ℑ_ltag2 I₁ = ℑ_ltag2 I₂)ᵢ.

Instance CRefl_instance_equiv n : CReflexive (I_valid_at n) instance_equiv.
Proof.
  intros I; isplit; [reflexivity | now trivial].
Qed.

Notation "⟨ μ , l₁ , l₂ ⟩ᴵ" := {| ℑ_rel := μ; ℑ_ltag1 := l₁; ℑ_ltag2 := l₂ |}.
Notation "ν₁ ≈ᴵ ν₂" := (instance_equiv ν₁ ν₂) (at level 70, no associativity).

Section EquivProperties.

  Lemma subrel_expr_cl {RF₁ RF₂ : effect_dom} {Rv₁ Rv₂ : RCtype} {n}
        (SubRF : n ⊨ RF₁ ≾ᵢ RF₂)
        (SubRv : n ⊨ Rv₁ ≾ᵢ Rv₂) :
    n ⊨ rel_expr_cl RF₁ Rv₁ ≾ᵢ rel_expr_cl RF₂ Rv₂.
  Proof.
    loeb_induction; iintro e₁; iintro e₂; iintro HR.
    iintro E₁; iintro E₂; iintro HE; iapply HR.
    apply rel_k_roll; intros.
    - iintro HV; eapply rel_k_value; [eassumption |].
      now iapply SubRv.
    - iintro HS; eapply rel_k_stuck; [eassumption |].
      apply rel_s_unroll in HS; destruct HS as [ls₁ [ls₂ [μ [HF [[HFree₁ HFree₂] HS]]]]].
      eapply rel_s_roll; [iapply SubRF; exact HF | eassumption .. | intros].
      iintro Hμ; iapply HS in Hμ; later_shift.
      now iapply IH.
  Qed.

  Lemma equiv_expr_cl {RF₁ RF₂ : effect_dom} {Rv₁ Rv₂ : RCtype} {n}
        (EQRF : n ⊨ RF₁ ≈ᵢ RF₂)
        (EQRv : n ⊨ Rv₁ ≈ᵢ Rv₂) :
    n ⊨ rel_expr_cl RF₁ Rv₁ ≈ᵢ rel_expr_cl RF₂ Rv₂.
  Proof.
    apply equiv_subrel_symcl; apply subrel_expr_cl; apply subrel_sub_equiv;
      assumption || symmetry; assumption.
  Qed.

  Lemma subrel_open_val {V LV} {RG₁ RG₂ : Rsubst V LV} {Rv₁ Rv₂ : RCtype} {n}
        (SubRG : n ⊨ RG₂ ≾ᵢ RG₁)
        (SubRv : n ⊨ Rv₁ ≾ᵢ Rv₂) :
    n ⊨ rel_val_cl_sub RG₁ Rv₁ ≾ᵢ rel_val_cl_sub RG₂ Rv₂.
  Proof.
    iintro v₁; iintro v₂; iintro Hv; iintro γ₁; iintro γ₂; iintro HRγ.
    iapply SubRv; iapply Hv; now iapply SubRG.
  Qed.

  Lemma equiv_open_val {V LV} {RG₁ RG₂ : Rsubst V LV} {Rv₁ Rv₂ : RCtype} {n}
        (SubRG : n ⊨ RG₂ ≈ᵢ RG₁)
        (SubRv : n ⊨ Rv₁ ≈ᵢ Rv₂) :
    n ⊨ rel_val_cl_sub RG₁ Rv₁ ≈ᵢ rel_val_cl_sub RG₂ Rv₂.
  Proof.
    apply equiv_subrel_symcl; apply subrel_open_val; apply subrel_sub_equiv;
      assumption || symmetry; assumption.
  Qed.

  Lemma subrel_open_exp {V LV} {RG₁ RG₂ : Rsubst V LV} {Re₁ Re₂ : RCexpr} {n}
        (SubRG : n ⊨ RG₂ ≾ᵢ RG₁)
        (SubRe : n ⊨ Re₁ ≾ᵢ Re₂) :
    n ⊨ rel_expr_cl_sub RG₁ Re₁ ≾ᵢ rel_expr_cl_sub RG₂ Re₂.
  Proof.
    iintro e₁; iintro e₂; iintro He; iintro γ₁; iintro γ₂; iintro HRγ.
    iapply SubRe; iapply He; now iapply SubRG.
  Qed.

  Lemma equiv_open_exp {V LV} {RG₁ RG₂ : Rsubst V LV} {Re₁ Re₂ : RCexpr} {n}
        (SubRG : n ⊨ RG₂ ≈ᵢ RG₁)
        (SubRe : n ⊨ Re₁ ≈ᵢ Re₂) :
    n ⊨ rel_expr_cl_sub RG₁ Re₁ ≈ᵢ rel_expr_cl_sub RG₂ Re₂.
  Proof.
    apply equiv_subrel_symcl; apply subrel_open_exp; apply subrel_sub_equiv;
      assumption || symmetry; assumption.
  Qed.

End EquivProperties.
