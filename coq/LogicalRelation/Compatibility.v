(** This module contains proofs that the logical relation is
compatible with the rules of the type system. *)
(** Compatibility is shown by proving for each rule of the type system
a lemma of the same shape, but with a logical approximation instead of
typing judgment. The development at this point is mostly standard for
the logical relations technique, safe for the proof by Löb induction
of the handler case.
*)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Product Binding.Env.
Require Import Types Kinding.
Require Import Fresh.Syntax Fresh.SyntaxMeta Fresh.Reduction Fresh.Fresh.
Require Import LogicalRelation.ProgramObs LogicalRelation.Core.
Require Import LogicalRelation.KindEquiv LogicalRelation.KindingIrrelevance.
Require Import LogicalRelation.Weakening LogicalRelation.Substitution.


Lemma rel_v_in_rel_e_open {TV LV V : Set} τ ε
      {Δ : TV → kind} {Θ : LV → typ_ TV LV} (Γ : V → typ_ TV LV)
      {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
      {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
      {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
      {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
      (v₁ v₂ : value_ V LV) {n}
      (Hv : n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ τ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ τ / ε⟧.
Proof.
  iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; term_simpl.
  apply rel_v_in_e; iapply Hv; assumption.
Qed.

(* ========================================================================= *)
(* variable *)

Lemma var_compat_v {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} (Γ : V → typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  (x : V) {n} :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ (x : evar {| sfst := V |})
                 ≾ (x : evar {| sfst := V |}) ∷ Γ x ⟧.
Proof.
  iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; term_simpl.
  idestruct Hγ as Htag Hvar; iapply Hvar.
Qed.

(* ========================================================================= *)
(* unit *)

Lemma unit_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} (Γ : V → typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]} {n} :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v_unit ≾ v_unit  ∷ ()ᵗ ⟧%typ.
Proof.
  iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; term_simpl.
  now isimplP.
Qed.

(* ========================================================================= *)
(* let *)

Lemma subst_extend_G {TV LV V : Set}
  (Δ : TV → kind) (Γ : V → typ_ TV LV) τ
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  η ϑ γ₁ γ₂ u₁ u₂ n
  (HΓ : n ⊨ G⟦ Γ ⟧ η ! ϑ γ₁ γ₂)
  (Hτ : n ⊨ ⟦ τ ⟧ η ! ϑ u₁ u₂) :
  n ⊨ G⟦ Γ ▹ τ ⟧ η ! ϑ (subst_comp (mk_subst u₁) (liftS γ₁)) (subst_comp (mk_subst u₂) (liftS γ₂)).
Proof.
  idestruct HΓ as HΘ HΓ; isplit; [intros a | iintro x; destruct x as [| x]]; term_simpl.
  - split; rewrite bind_pure by (intros x; reflexivity); apply HΘ.
  - eapply rel_typ_k_irr, Hτ.
  - eapply rel_typ_k_irr; iapply HΓ.
Qed.

Lemma subst_shift_G {TV LV V : Set}
  (Δ : TV → kind) (Γ : V → typ_ TV LV) I
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  η ϑ γ₁ γ₂ l₁ l₂ n
  (HI : l₁ = l_tag (ℑ_ltag1 I) ∧ l₂ = l_tag (ℑ_ltag2 I))
  (HΓ : n ⊨ G⟦ Γ ⟧ η ! (ϑ) γ₁ γ₂) :
  n ⊨ G⟦ shift Γ ⟧ η ! (ϑ ▹ I) (subst_comp (mk_subst l₁) (liftS γ₁)) (subst_comp (mk_subst l₂) (liftS γ₂)).
Proof.
  idestruct HΓ as HΘ HΓ; isplit; [intros [| a] | iintro x].
  - term_simpl; assumption.
  - term_simpl; split.
    + change (subst (shift (sub_lbl γ₁ a)) l₁ = l_tag (ℑ_ltag1 (ϑ a))); term_simpl; apply HΘ.
    + change (subst (shift (sub_lbl γ₂ a)) l₂ = l_tag (ℑ_ltag2 (ϑ a))); term_simpl; apply HΘ.
  - term_simpl; eapply rel_typ_k_irr, rel_v_weakenL_r.
    iapply HΓ.
Qed.

Lemma let_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} τ₁ {τ₂ ε}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
  {WFε  : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {b₁ b₂ : expr_ V LV}
  {e₁ e₂ : expr (incV {| sfst := V; ssnd := LV |})} {n}
  (Hb : n ⊨ T⟦ Δ; Θ; Γ ⊨ b₁ ≾ b₂ ∷ τ₁ / ε ⟧)
  (He : n ⊨ T⟦ Δ; Θ; Γ ▹ τ₁ ⊨ e₁ ≾ e₂ ∷ τ₂ / ε ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ e_let b₁ e₁ ≾ e_let b₂ e₂ ∷ τ₂ / ε ⟧.
Proof.
  iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; term_simpl.
  iespecialize Hb; ispecialize Hb Hϑ; ispecialize Hb Hγ.
  assert (He' : n ⊨ E⟦ τ₁ ⊢ τ₂ / ε ⟧ η ! (ϑ) (bind (liftS γ₁) e₁) (bind (liftS γ₂) e₂)).
  { iintro u₁; iintro u₂; iintro Hu; unfold subst; rewrite !bind_bind_comp'.
    iapply He; [| apply subst_extend_G]; assumption.
  }
  revert Hb He'; clear He Hϑ Hγ.
  generalize (bind γ₁ b₁) as b₁', (bind (liftS γ₁) e₁) as e₁', (bind γ₂ b₂) as b₂', (bind (liftS γ₂) e₂) as e₂'.
  clear γ₁ γ₂ b₁ b₂ e₁ e₂; intros b₁ e₁ b₂ e₂ Hb He.
  irevert b₁ b₂ Hb; loeb_induction; iintro b₁; iintro b₂; iintro Hb.
  iintro F₁; iintro F₂; iintro HF; change (plug F₁ (e_let b₁ e₁)) with (plug (x_let F₁ e₁) b₁).
  change (plug F₂ (e_let b₂ e₂)) with (plug (x_let F₂ e₂) b₂).
  iapply Hb; apply rel_k_roll.
  - intros v₁ v₂; iintro Hv; simpl; eapply rel_k_expr; [eassumption |].
    eapply rel_e_contr_both; [now eauto 2 using contr ..| later_shift; now iapply He].
  - intros R₁ R₂ s₁ s₂; iintro HS; apply rel_s_unroll in HS.
    destruct HS as [ls₁ [ls₂ [μ [Hμ [[Hfree₁ Hfree₂] HS]]]]]; simpl.
    change (e_let (rplug R₁ s₁) e₁) with (rplug (r_let R₁ e₁) s₁);
      change (e_let (rplug R₂ s₂) e₂) with (rplug (r_let R₂ e₂) s₂).
    eapply rel_k_stuck; [eassumption |].
    eapply rel_s_roll; [eassumption | unfold rfree_l; now auto .. | intros; iintro He'].
    simpl; iapply HS in He'; later_shift; now iapply IH.
Qed.

(* ========================================================================= *)
(* arrows *)

Lemma lam_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} {τ₁ τ₂ ε}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
  {WFε  : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {e₁ e₂ : expr (incV {| sfst := V; ssnd := LV |})} {n}
  (He : n ⊨ T⟦ Δ; Θ; Γ ▹ τ₁ ⊨ e₁ ≾ e₂ ∷ τ₂ / ε ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v_lam e₁ ≾ v_lam e₂ ∷ (τ₁ →[ε] τ₂)%typ ⟧.
Proof.
  iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; term_simpl.
  iintro u₁; iintro u₂; iintro Hu; term_simpl.
  eapply rel_e_contr_both; [now eauto 2 using contr .. |].
  unfold subst; rewrite !bind_bind_comp'; later_shift.
  iapply He; auto using subst_extend_G.
Qed.

Lemma app_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} τ₁ {τ₂ ε}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
  {WFε  : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {u₁ u₂ v₁ v₂ : value_ V LV} {n}
  (Hu : n ⊨ T⟦ Δ; Θ; Γ ⊨ u₁ ≾ u₂ ∷ (τ₁ →[ε] τ₂)%typ ⟧)
  (Hv : n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ τ₁ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ e_app u₁ v₁ ≾ e_app u₂ v₂ ∷ τ₂ / ε  ⟧.
Proof.
  iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; term_simpl.
  iapply Hu; [..| iapply Hv]; assumption.
Qed.

(* ========================================================================= *)
(* type quantifiers  *)

Lemma tlam_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} {τ : typ (incV {| sfst := _ |})} {κ}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ  : K[ Δ ▹ κ; LV ⊢ τ ∷ k_type]}
  {e₁ e₂ : expr_ V LV} {n}
  (He : n ⊨ T⟦ Δ ▹ κ; shift Θ; shift Γ ⊨ e₁ ≾ e₂ ∷ τ / ι%typ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v_tlam e₁ ≾ v_tlam e₂ ∷ (∀ᵗ κ, τ)%typ ⟧.
Proof.
  iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ.
  term_simpl; iintro R; term_simpl; change pure_effect with ( F⟦ ι%typ ⟧ η :▹ R ! (ϑ) ).
  eapply rel_e_contr_both; [now eauto 2 using contr .. |]; later_shift.
  iapply He; clear -Hϑ Hγ.
  - iintro a; etransitivity; [iapply Hϑ |].
    etransitivity; [apply (type_interp_shiftT k_sig) | apply (type_interp_irr k_sig)].
  - idestruct Hγ as Hδ Hγ; isplit; [apply Hδ | iintro x; term_simpl].
    destruct γ₁ as [γ₁ δ₁]; destruct γ₂ as [γ₂ δ₂]; simpl.
    eapply rel_typ_k_irr, rel_v_weakenTl; iapply Hγ.
Qed.

Lemma tapp_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} {τ : typ (incV {| sfst := _ |})} {σ κ}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ  : K[ Δ ▹ κ; LV ⊢ τ ∷ k_type]}
  {WFσ  : K[ Δ; LV ⊢ σ ∷ κ]}
  {v₁ v₂ : value_ V LV} {n}
  (Hv : n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ (∀ᵗ κ, τ)%typ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ e_tapp v₁ ≾ e_tapp v₂ ∷ subst τ σ / ι%typ ⟧.
Proof.
  iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; term_simpl.
  iapply @subrel_expr_cl; [reflexivity | | iapply Hv; eassumption].
  apply subrel_sub_equiv; apply (type_interp_substT k_type); reflexivity.
Qed.

Lemma llam_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} {τ : typ (incL {| sfst := _ |})} σ
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ  : K[ Δ; inc LV ⊢ τ ∷ k_type]}
  {WFσ  : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {e₁ e₂ : expr (incL {| sfst := V |})} {n}
  (He : n ⊨ T⟦ Δ; shift (Θ ▹ σ); shift Γ ⊨ e₁ ≾ e₂ ∷ τ / ι%typ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v_llam e₁ ≾ v_llam e₂ ∷ (∀ˡ σ, τ)%typ ⟧.
Proof.
  iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ.
  term_simpl; iintro I; iintro HEq; term_simpl.
  change pure_effect with (F⟦ ι%typ ⟧ η ! (ϑ ▹ I)).
  eapply rel_e_contr_both; [now eauto 2 using contr .. |]; later_shift.
  unfold subst; rewrite !bind_bind_comp'; iapply He; clear He.
  - iintro a; destruct a as [| a].
    + etransitivity; [apply HEq |].
      etransitivity; [apply (type_interp_shiftL k_sig) with (I0 := I) |].
      apply (type_interp_irr k_sig).
    + etransitivity; [iapply Hϑ |].
      etransitivity; [apply (type_interp_shiftL k_sig) with (I0 := I) |].
      apply (type_interp_irr k_sig).
  - idestruct Hγ as Hδ Hγ.
    isplit; [| iintro x; term_simpl; eapply rel_typ_k_irr, rel_v_weakenL_r; iapply Hγ].
    intros [| a]; [term_simpl; split; reflexivity |].
    (* hackish, since term_simpl fails to recognize substs nicely *)
    split; (etransitivity; [| apply Hδ]); term_simpl; (etransitivity; [| eapply subst_shift_id]); reflexivity.
Qed.

Lemma lapp_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} {τ : typ (incL {| sfst := _ |})}
  {a : s_lbl {| sfst := TV |}}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ  : K[ Δ; inc LV ⊢ τ ∷ k_type]}
  {v₁ v₂ : value_ V LV} {n}
  (Hv : n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ (∀ˡ (Θ a), τ)%typ ⟧) :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ e_lapp v₁ (l_var a) ≾ e_lapp v₂ (l_var a) ∷ subst τ a / ι%typ ⟧.
Proof.
  iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; term_simpl.
  iespecialize Hv; ispecialize Hv Hϑ; ispecialize Hv Hγ; change pure_effect with (F⟦ ι%typ ⟧ η ! ϑ).
  idestruct Hγ as Hδ Hγ; destruct (Hδ a) as [Hl₁ Hl₂].
  rewrite Hl₁, Hl₂; simpl in Hv; unfold lall_rel in Hv; ispecialize Hv (ϑ a).
  ispecialize Hv; [iapply Hϑ |].
  iapply @subrel_expr_cl; [reflexivity | apply subrel_sub_equiv | apply Hv].
  apply (type_interp_substL k_type); reflexivity.
Qed.

(* ========================================================================= *)
(* do *)

Lemma do_compat {TV LV V : Set}
  {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV} τ₁ {τ₂} {a : LV}
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
  (v₁ v₂ : value_ V LV) n :
  (∀ η ϑ, n ⊨ sig_rel (⟦ τ₁ ⟧ η ! ϑ) (⟦ τ₂ ⟧ η ! ϑ) ≾ᵢ Σ⟦ Θ a ⟧ η ! ϑ) →
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ τ₁ ⟧ →
  n ⊨ T⟦ Δ; Θ; Γ ⊨ e_do_ (l_var a) v₁ ≾ e_do_ (l_var a) v₂ ∷ τ₂ / (a : s_lbl ⟨_ , _⟩) ⟧.
Proof.
  intros Hop Hv; iintro η; iintro ϑ; specialize (Hop η ϑ); iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ.
  term_simpl; apply rel_s_in_e with (R₁ := r_hole) (R₂ := r_hole).
  apply rel_s_roll with (ls₁ := cons (ℑ_ltag1 (ϑ a)) nil) (ls₂ := cons (ℑ_ltag2 (ϑ a)) nil)
    (μ := λ e₁ e₂ : expr0,
            ∃ᵢ v₁ v₂ : value0, (e₁ = e_value v₁ ∧ e₂ = e_value v₂)ᵢ ∧ᵢ ⟦ τ₂ ⟧ η ! ϑ v₁ v₂);
    [| unfold rfree_l; now simpl .. |].
  - simpl; unfold instance_rel.
    iexists (bind γ₁ v₁); iexists (bind γ₂ v₂).
    isplit; [idestruct Hγ as Hδ Hγ; repeat split; f_equal; apply Hδ |].
    eapply I_iff_elim_M; [iapply Hϑ |].
    iapply Hop; unfold sig_rel.
    isplit; [now iapply Hv | clear].
    iintro u₁; iintro u₂; isplit; iintro Hu.
    + idestruct Hu as u₁' Hu; idestruct Hu as u₂' Hu; idestruct Hu as HEQ Hu.
      destruct HEQ as [EQ₁ EQ₂]; injection EQ₁; injection EQ₂; intros; subst; assumption.
    + iexists u₁; iexists u₂; isplit; [tauto | assumption].
  - intros e₁' e₂'; iintro He'; simpl rplug.
    idestruct He' as v₁' He'; idestruct He' as v₂' He'.
    idestruct He' as Heq He'; destruct Heq; subst.
    iintro. (* We have one later here, that can be added to μ *)
    now apply rel_v_in_e.
Qed.

(* ========================================================================= *)
(* handle *)

Lemma rhandle_cl_compat {TV LV : Set} {Δ : TV → kind}
    σ τ' τ ε
    {WFσ  : K[ Δ ; LV ⊢ σ  ∷ k_sig    ]}
    {WFε  : K[ Δ ; LV ⊢ ε  ∷ k_effect ]}
    {WFτ' : K[ Δ ; LV ⊢ τ' ∷ k_type   ]}
    {WFτ  : K[ Δ ; LV ⊢ τ  ∷ k_type   ]}
    (η : ∀ a, K⟦ Δ a ⟧)
    (ϑ : LV → ℑ)
    (l₁ l₂ : ltag) (h₁ h₂ : handler0) r₁ r₂ n :
    edom_ft1 (F⟦ ε ⟧ η ! ϑ) ≤ l₁ →
    edom_ft2 (F⟦ ε ⟧ η ! ϑ) ≤ l₂ →
    n ⊨ H⟦ σ @ τ / ε ⟧ η ! ϑ h₁ h₂ →
    n ⊨ E⟦ τ' ⊢ τ / ε ⟧ η ! ϑ r₁ r₂ →
    n ⊨ ∀ᵢ e₁ e₂,
      E⟦ shift τ' / ((VZ : s_lbl ⟨_, _⟩) · shift ε)%typ ⟧ η !
       (ϑ ▹ ⟨Σ⟦ σ ⟧ η ! ϑ, l₁, l₂⟩ᴵ) e₁ e₂ ⇒
      E⟦ τ / ε ⟧ η ! ϑ (e_rhandle l₁ e₁ h₁ r₁) (e_rhandle l₂ e₂ h₂ r₂).
Proof.
  intros Hl₁ Hl₂ Hh Hr; loeb_induction.
  iintro e₁; iintro e₂; iintro He.
  iintro F₁; iintro F₂; iintro HF.
  change (n ⊨ Obs (plug (x_handle l₁ F₁ h₁ r₁) e₁)
            (plug (x_handle l₂ F₂ h₂ r₂) e₂)).
  iapply He; apply rel_k_roll.
  - intros v₁ v₂; iintro Hv; simpl; eapply rel_k_expr; [eassumption |].
    eapply rel_e_contr_both; [now eauto 2 using contr .. | later_shift].
    iapply Hr; eapply rel_v_weakenL_l; exact Hv.
  - intros E₁ E₂ s₁ s₂; iintro Hs; apply rel_s_unroll in Hs.
    destruct Hs as [ls₁ [ls₂ [μ [Hμ [[Hfree₁ Hfree₂] Hs]]]]].
    simpl in Hμ; idestruct Hμ as Hμ Hμ.
    + unfold instance_rel in Hμ; simpl in Hμ.
      idestruct Hμ as v₁ Hμ; idestruct Hμ as v₂ Hμ.
      idestruct Hμ as Heq Hμ; destruct Heq as [EQe₁ [EQe₂ [EQls₁ EQls₂]]]; subst; simpl.
      eapply rel_k_expr; [exact HF |]; destruct h₁ as [ h₁ ]; destruct h₂ as [ h₂ ].
      eapply rel_e_contr_both; [apply contr_do with (E := r_handle _ _ _ _) ..|].
      * constructor; [apply Hfree₁; simpl; tauto | constructor].
      * constructor; [apply Hfree₂; simpl; tauto | constructor].
      * later_shift; iapply Hh; [eassumption |].
        iintro u₁; iintro u₂; iintro Hu.
        eapply rel_e_contr_both; [now eauto 2 using contr ..|].
        term_simpl.
        rewrite! subst_shift_id_lifted.
        rewrite! subst_shift_id_lifted2.
        iapply Hs in Hu; later_shift; now iapply IH.
    + change (n ⊨ Obs (plug F₁ (rplug (r_handle l₁ E₁ h₁ r₁) s₁))
                (plug F₂ (rplug (r_handle l₂ E₂ h₂ r₂) s₂))).
      eapply rel_k_stuck; [exact HF |].
      eapply rel_s_roll with (μ0 := μ).
      * eapply rel_eff_rel_weakenL_l; exact Hμ.
      * intros l Hin; simpl; split; [| now apply Hfree₁].
        intros EQ; subst l₁; apply rel_eff_rel_weakenL_l in Hμ.
        assert (HT := edom_t1_fresh F⟦ ε ⟧ η ! (ϑ) n).
        iapply HT; eassumption.
      * intros l Hin; simpl; split; [| now apply Hfree₂].
        intros EQ; subst l₂; apply rel_eff_rel_weakenL_l in Hμ.
        assert (HT := edom_t2_fresh F⟦ ε ⟧ η ! (ϑ) n).
        iapply HT; eassumption.
      * intros e₁' e₂'; iintro He'.
        iapply Hs in He'; simpl; later_shift; now iapply IH.
Qed.

Lemma handle_compat  {TV LV V : Set}
  (Δ : TV → kind) (Θ : LV → typ_ TV LV) (Γ : V → typ_ TV LV) τ τ' σ ε
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFσ  : K[ Δ; LV ⊢ σ  ∷ k_sig]}
  {WFτ  : K[ Δ; LV ⊢ τ  ∷ k_type]}
  {WFτ' : K[ Δ; LV ⊢ τ' ∷ k_type]}
  {WFε  : K[ Δ; LV ⊢ ε  ∷ k_effect]}
  (e₁ e₂ : expr (incL {| sfst := V; ssnd := LV |}))
  (h₁ h₂ : handler_ V LV)
  (r₁ r₂ : expr (incV {| sfst := V |})) n :
  n ⊨ T⟦ Δ; Θ; Γ ▹ τ ⊨ r₁ ≾ r₂ ∷ τ' / ε ⟧ →
  n ⊨ H⟦ Δ; Θ; Γ ⊨ h₁ ≾ h₂ ∷ σ @ τ' / ε ⟧ →
  n ⊨ T⟦ Δ; shift (Θ ▹ σ); shift Γ ⊨ e₁ ≾ e₂ ∷ shift τ / (VZ : s_lbl ⟨_, _⟩) · shift ε ⟧%typ →
  n ⊨ T⟦ Δ; Θ; Γ ⊨ e_handle e₁ h₁ r₁ ≾ e_handle e₂ h₂ r₂ ∷ τ' / ε ⟧.
Proof.
  intros Hr Hh He; iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; term_simpl.
  assert (Hr' : n ⊨ E⟦ τ ⊢ τ' / ε ⟧ η ! ϑ (bind (liftS γ₁) r₁) (bind (liftS γ₂) r₂)).
  { iintro v₁; iintro v₂; iintro Hv; unfold subst; rewrite !bind_bind_comp'.
    iapply Hr; auto using subst_extend_G.
  }
  revert Hr'; generalize (bind (liftS γ₁) r₁) as r₁', (bind (liftS γ₂) r₂) as r₂';
    clear r₁ r₂ Hr; intros r₁ r₂ Hr.
  assert (Hh' : n ⊨ H⟦ σ @ τ' / ε ⟧ η ! ϑ (bind γ₁ h₁) (bind γ₂ h₂)) by now iapply Hh.
  revert Hh'; generalize (bind γ₁ h₁) as h₁', (bind γ₂ h₂ ) as h₂';
    clear h₁ h₂ Hh; intros h₁ h₂ Hh.
  remember (edom_ft1 (F⟦ ε ⟧ η ! ϑ)) as l₁.
  remember (edom_ft2 (F⟦ ε ⟧ η ! ϑ)) as l₂.
  iintro F₁; iintro F₂; iintro HF.
  destruct (fresh_red F₁ (bind (liftS γ₁) e₁) h₁ r₁ l₁) as [l₁' [HLE₁ HR₁]].
  destruct (fresh_red F₂ (bind (liftS γ₂) e₂) h₂ r₂ l₂) as [l₂' [HLE₂ HR₂]].
  eapply Obs_red_both; [eassumption .. |]; clear HR₁ HR₂; later_shift.
  subst; iapply @rhandle_cl_compat; [eassumption ..| | assumption].
  unfold subst; rewrite !bind_bind_comp'; iapply He; [| now auto using subst_shift_G].
  iintro b; destruct b as [| b].
  - etransitivity; [apply (type_interp_shiftL k_sig) | apply (type_interp_irr k_sig)].
  - etransitivity; [iapply Hϑ |].
    etransitivity; [apply (type_interp_shiftL k_sig) | apply (type_interp_irr k_sig)].
Qed.

(* handlers *)

Lemma handler_do_compat {TV LV V : Set}
  (Δ : TV → kind) (Θ : LV → typ_ TV LV) (Γ : V → typ_ TV LV) τ₁ τ₂ τ ε
  {WFΘ  : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ  : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ  : K[ Δ; LV ⊢ τ  ∷ k_type]}
  {WFτ₁ : K[ Δ; LV ⊢ τ₁ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ₂ ∷ k_type]}
  {WFε  : K[ Δ; LV ⊢ ε  ∷ k_effect]}
  (e₁ e₂ : expr (incV (incV {| sfst := V; ssnd := LV |}))) n :
  n ⊨ T⟦ Δ; Θ; Γ ▹ τ₁ ▹ τ₂ →[ε] τ ⊨ e₁ ≾ e₂ ∷ τ / ε ⟧%typ →
  n ⊨ H⟦ Δ; Θ; Γ ⊨ h_do e₁ ≾ h_do e₂ ∷ τ₁ ⇒ᵈ τ₂ @ τ / ε ⟧%typ.
Proof.
  intros He; iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; simpl.
  iintro v₁; iintro v₂; iintro I; iintro HS; iintro u₁; iintro u₂; iintro Hu.
  change (ebind (liftS (liftS γ₁)) e₁) with (bind (liftS (liftS γ₁)) e₁).
  change (ebind (liftS (liftS γ₂)) e₂) with (bind (liftS (liftS γ₂)) e₂).
  unfold subst; rewrite !bind_bind_comp', !bind_mk_subst_comm;
    fold (subst (shift u₁) v₁); fold (subst (shift u₂) v₂).
  rewrite !subst_shift_id, !subst_comp_assoc, !liftS_comp by reflexivity.
  iapply He; [assumption | idestruct HS as Hv HI].
  apply subst_extend_G; [apply subst_extend_G |]; [assumption ..|].
  eapply I_iff_elim_M, Hu.
  apply apply_type_equiv, equiv_arr; [now symmetry | reflexivity ..].
Qed.

Lemma handler_all_compat {TV LV V : Set}
      (Δ : TV → kind) (Θ : LV → typ_ TV LV) (Γ : V → typ_ TV LV)
      κ (σ : typ (incV {| sfst := TV |})) τ ε
      {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
      {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
      {WFσ : K[ Δ ▹ κ; LV ⊢ σ  ∷ k_sig]}
      {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
      {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
      h₁ h₂ n :
  n ⊨ H⟦ Δ ▹ κ; shift Θ; shift Γ ⊨ h₁ ≾ h₂ ∷ σ @ shift τ / shift ε ⟧%typ →
  n ⊨ H⟦ Δ; Θ; Γ ⊨ h₁ ≾ h₂ ∷ ∀ˢ κ, σ @ τ / ε ⟧%typ.
Proof.
  intros Hh; iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ; simpl.
  destruct h₁ as [e₁]; destruct h₂ as [e₂]; simpl.
  iintro v₁; iintro v₂; iintro I; iintro HR.
  iintro u₁; iintro u₂; iintro HU; idestruct HR as R HR.
  iapply @subrel_expr_cl; [.. | iapply Hh; [.. | eassumption |]].
  - apply subrel_sub_equiv; symmetry; apply (type_interp_shiftT k_effect).
  - apply subrel_sub_equiv; symmetry; apply (type_interp_shiftT k_type).
  - iintro a; etransitivity; [iapply Hϑ |].
    etransitivity; [apply (type_interp_shiftT k_sig) | apply (type_interp_irr k_sig)].
  - idestruct Hγ as Hδ Hγ; isplit; [intros a | iintro x]; term_simpl; [apply Hδ |].
    eapply rel_typ_k_irr, rel_v_weakenTl; iapply Hγ.
  - iapply subrel_arr; [reflexivity | apply subrel_sub_equiv ..| exact HU].
    + apply (type_interp_shiftT k_type).
    + apply (type_interp_shiftT k_effect).
Qed.
