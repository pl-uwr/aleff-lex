Require Import Utf8.
Require Import IxFree.Lib.
Require Import Types Kinding Fresh.Syntax.
Require Import LogicalRelation.Core LogicalRelation.KindEquiv.

Fixpoint type_interp_k_irr (κ : kind) {TV LV : Set} {Δ : TV → kind}
    (τ₁ τ₂ : typ_ TV LV)
    (η : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ₁ ϑ₂ : LV → ℑ)
    {WFτ₁ : K[ Δ ; LV ⊢ τ₁ ∷ κ]}
    {WFτ₂ : K[ Δ ; LV ⊢ τ₂ ∷ κ]} n {struct τ₁} :
  (n ⊨ ∀ᵢ a, ϑ₁ a ≈ᴵ ϑ₂ a) →
  τ₁ = τ₂ →
  n ⊨ K⟦ ⟦ τ₁ ∷ κ ⟧ η ! ϑ₁ ≡ ⟦ τ₂ ∷ κ ⟧ η ! ϑ₂ ∷ κ ⟧.
Proof.
  intros Hϑ Heq; destruct τ₁; inversion Heq; subst; invert_kind WFτ₁; simpl type_interp;
    [reflexivity | | unfold kind_equiv ..].
  - unfold apply_to_t_var.
    rewrite (kind_eq_proofs_unicity (K_Var_inv WFτ₁) (K_Var_inv WFτ₂)); reflexivity.
  - apply equiv_arr.
    + now apply (type_interp_k_irr k_type).
    + now apply (type_interp_k_irr k_type).
    + now apply (type_interp_k_irr k_effect).
  - apply equiv_tall; iintro R; now apply (type_interp_k_irr k_type).
  - apply equiv_lall; [now apply (type_interp_k_irr k_sig) |].
    iintro R; apply (type_interp_k_irr k_type); [| reflexivity].
    iintro a; destruct a as [| a]; simpl; [reflexivity | iapply Hϑ].
  - apply equiv_instance; iapply Hϑ.
  - reflexivity.
  - apply equiv_cons; now eapply (type_interp_k_irr k_effect).
  - apply equiv_sig; now eapply (type_interp_k_irr k_type).
  - apply equiv_talls; intros R; now eapply (type_interp_k_irr k_sig).
Qed.

Lemma type_interp_irr {TV LV : Set} {Δ : TV → kind} (κ : kind)
    (τ : typ_ TV LV)
    (η : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ : LV → ℑ)
    {WFτ₁ : K[ Δ ; LV ⊢ τ ∷ κ]}
    {WFτ₂ : K[ Δ ; LV ⊢ τ ∷ κ]} n :
  n ⊨ K⟦ type_interp τ κ η ϑ WFτ₁ ≡ type_interp τ κ η ϑ WFτ₂ ∷ κ ⟧.
Proof.
  apply type_interp_k_irr; [iintro; reflexivity | reflexivity].
Qed.

Lemma rel_sig_k_irr {TV LV : Set} {Δ : TV → kind}
    (σ : typ_ TV LV)
    (η : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ : LV → ℑ)
    {WFσ₁ : K[ Δ ; LV ⊢ σ ∷ k_sig]}
    {WFσ₂ : K[ Δ ; LV ⊢ σ ∷ k_sig]}
    v₁ v₂ μ n :
  n ⊨ type_interp σ _ η ϑ WFσ₁ v₁ v₂ μ →
  n ⊨ type_interp σ _ η ϑ WFσ₂ v₁ v₂ μ.
Proof.
  intros Hσ; eapply I_iff_elim_M, Hσ; clear Hσ.
  apply apply_sig_equiv, (type_interp_irr k_sig).
Qed.

Lemma rel_eff_k_irr {TV LV : Set} {Δ : TV → kind}
    (σ : typ_ TV LV)
    (η : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ : LV → ℑ)
    {WFτ₁ : K[ Δ ; LV ⊢ σ ∷ k_effect]}
    {WFτ₂ : K[ Δ ; LV ⊢ σ ∷ k_effect]}
    v₁ v₂ ls₁ ls₂ μ n :
  n ⊨ edom_rel (type_interp σ _ η ϑ WFτ₁) v₁ v₂ ls₁ ls₂ μ →
  n ⊨ edom_rel (type_interp σ _ η ϑ WFτ₂) v₁ v₂ ls₁ ls₂ μ.
Proof.
  intros Hσ; eapply I_iff_elim_M, Hσ; clear Hσ.
  apply apply_effect_equiv, (type_interp_irr k_effect).
Qed.

Lemma rel_typ_k_irr {TV LV : Set} {Δ : TV → kind}
    (σ : typ_ TV LV)
    (η : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ : LV → ℑ)
    {WFτ₁ : K[ Δ ; LV ⊢ σ ∷ k_type]}
    {WFτ₂ : K[ Δ ; LV ⊢ σ ∷ k_type]}
    v₁ v₂ n :
  n ⊨ type_interp σ _ η ϑ WFτ₁ v₁ v₂ →
  n ⊨ type_interp σ _ η ϑ WFτ₂ v₁ v₂.
Proof.
  intros Hσ; eapply I_iff_elim_M, Hσ; clear Hσ.
  apply apply_type_equiv, (type_interp_irr k_type).
Qed.

Lemma rel_exp_k_irr {TV LV : Set} {Δ : TV → kind}
    (τ ε : typ_ TV LV)
    (η : ∀ α : TV, K⟦ Δ α ⟧)
    (ϑ : LV → ℑ)
    {WFτ₁ : K[ Δ ; LV ⊢ τ ∷ k_type]}
    {WFτ₂ : K[ Δ ; LV ⊢ τ ∷ k_type]}
    {WFε₁ : K[ Δ ; LV ⊢ ε ∷ k_effect]}
    {WFε₂ : K[ Δ ; LV ⊢ ε ∷ k_effect]}
    e₁ e₂ n :
  n ⊨ rel_expr_cl (type_interp ε _ η ϑ WFε₁) (type_interp τ _ η ϑ WFτ₁) e₁ e₂ →
  n ⊨ rel_expr_cl (type_interp ε _ η ϑ WFε₂) (type_interp τ _ η ϑ WFτ₂) e₁ e₂.
Proof.
  intros HE; iapply @subrel_expr_cl; [apply subrel_sub_equiv .. | exact HE].
  - apply (type_interp_irr k_effect).
  - apply (type_interp_irr k_type).
Qed.

Lemma rel_v_open_k_irr {TV LV V : Set}
  {Δ : TV → kind}
  {Θ : LV → typ_ TV LV}
  {Γ : V  → typ_ TV LV}
  {τ : typ_ TV LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {v₁ v₂ : value_ V LV} {n} :
  n ⊨ rel_v_open Δ Θ Γ τ (WFτ := WFτ₁) v₁ v₂ →
  n ⊨ rel_v_open Δ Θ Γ τ (WFτ := WFτ₂) v₁ v₂.
Proof.
  intros Hv; iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ.
  apply rel_typ_k_irr with WFτ₁; now iapply Hv.
Qed.

Lemma rel_e_open_k_irr {TV LV V : Set}
  {Δ : TV → kind}
  {Θ : LV → typ_ TV LV}
  {Γ : V  → typ_ TV LV}
  {τ ε : typ_ TV LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ₁ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε₁ : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {WFε₂ : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {e₁ e₂ : expr_ V LV} {n} :
  n ⊨ rel_e_open Δ Θ Γ τ ε (WFτ := WFτ₁) (WFε := WFε₁) e₁ e₂ →
  n ⊨ rel_e_open Δ Θ Γ τ ε (WFτ := WFτ₂) (WFε := WFε₂) e₁ e₂.
Proof.
  intros HT; iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ.
  apply rel_exp_k_irr with WFτ₁ WFε₁; now iapply HT.
Qed.

Lemma rel_h_open_k_irr {TV LV V : Set}
  {Δ : TV → kind}
  {Θ : LV → typ_ TV LV}
  {Γ : V  → typ_ TV LV}
  {σ τ ε : typ_ TV LV}
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFσ₁ : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {WFσ₂ : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {WFτ₁ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFτ₂ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε₁ : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {WFε₂ : K[ Δ; LV ⊢ ε ∷ k_effect]}
  {h₁ h₂ : handler_ V LV} {n} :
  n ⊨ rel_h_open Δ Θ Γ σ τ ε (WFσ := WFσ₁) (WFτ := WFτ₁) (WFε := WFε₁) h₁ h₂ →
  n ⊨ rel_h_open Δ Θ Γ σ τ ε (WFσ := WFσ₂) (WFτ := WFτ₂) (WFε := WFε₂) h₁ h₂.
Proof.
  intros HT; iintro η; iintro ϑ; iintro γ₁; iintro γ₂; iintro Hϑ; iintro Hγ.
  destruct h₁ as [e₁]; destruct h₂ as [e₂]; simpl.
  iintro v₁; iintro v₂; iintro I; iintro HS; iintro u₁; iintro u₂; iintro Hu.
  apply rel_exp_k_irr with WFτ₁ WFε₁; iapply HT; [assumption .. | |].
  - eapply rel_sig_k_irr, HS; reflexivity.
  - eapply I_iff_elim_M, Hu; igeneralize u₂; igeneralize u₁; clear.
    eapply equiv_arr; [reflexivity | apply (type_interp_irr k_type) | apply (type_interp_irr k_effect)].
Qed.
