Require Import Utf8.
Require Import IxFree.Lib.
Require Import Fresh.Syntax.
Require Import Types Kinding.
Require Import LogicalRelation.Core LogicalRelation.OpenExprRel.
Require Import Binding.Lib.

Definition rel_h {TV LV : Set} {Δ : TV → kind}
    σ τ ε
    {WFσ : K[ Δ ; LV ⊢ σ ∷ k_sig    ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    (η : ∀ a, K⟦ Δ a ⟧)
    (θ : LV → instance_dom)
    ah₁ ah₂ : IProp :=
  match ah₁, ah₂ with
  | h_do e₁, h_do e₂ =>
    ∀ᵢ v₁ v₂ I, Σ⟦ σ ⟧ η ! θ v₁ v₂ I ⇒
        ∀ᵢ u₁ u₂, arrow_rel I (⟦ τ ⟧ η ! θ) (F⟦ ε ⟧ η ! θ) u₁ u₂ ⇒
          rel_expr_cl (F⟦ ε ⟧ η ! θ) (⟦ τ ⟧ η ! θ) (subst (subst e₁ (shift u₁)) v₁) (subst (subst e₂ (shift u₂)) v₂)
  end.

Notation "'H⟦' σ @ τ // ε ⟧ η ! θ" := (rel_h σ τ ε η θ)
  (at level 0, σ at level 40).

Definition rel_h_open {TV LV V : Set}
  (Δ : TV → kind)
  (Θ : LV → typ_ TV LV)
  (Γ : V  → typ_ TV LV)
  (σ : typ_ TV LV)
  (τ : typ_ TV LV)
  (ε : typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFσ : K[ Δ; LV ⊢ σ ∷ k_sig]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
  (h₁ h₂ : handler_ V LV) : IProp :=
  ∀ᵢ η θ γ₁ γ₂, 
    (Θ⟦ Θ ⟧ η ! θ ∧ᵢ G⟦ Γ ⟧ η ! θ γ₁ γ₂) ⇒
    H⟦ σ @ τ // ε ⟧ η ! θ (bind γ₁ h₁) (bind γ₂ h₂).

Notation "'H⟦' Δ ; Θ ; Γ ⊨ h₁ ≾ h₂ ∷ σ ⇝ τ // ε ⟧" :=
  (rel_h_open Δ Θ Γ σ τ ε h₁ h₂) (Δ, Θ, Γ at level 40).
