(** This module provides convenience lemmas for rolling and unrolling
the definitions of the logical relation. This is useful, since going
through the fixed-point operation explicitly can be somewhat
cumbersome. *)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib.
Require Import Types Kinding.
Require Import Fresh.Syntax.
Require Import LogicalRelation.Core.

Lemma rel_expr_cl_fix_roll RelR RelV (e₁ e₂ : expr0) n :
  n ⊨ rel_expr_cl RelR RelV e₁ e₂ →
  n ⊨ rel_expr_cl_fix RelR RelV e₁ e₂.
Proof.
intro He; apply (I_fix_roll rel_e2_sig); exact He.
Qed.

Lemma rel_expr_cl_fix_unroll RelR RelV (e₁ e₂ : expr0) n :
  n ⊨ rel_expr_cl_fix RelR RelV e₁ e₂ →
  n ⊨ rel_expr_cl RelR RelV e₁ e₂.
Proof.
intro He; apply (I_fix_unroll rel_e2_sig); exact He.
Qed.

Lemma rel_s_roll {TV LV : Set} {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    μ E₁ E₂ e₁ e₂ ls₁ ls₂
    {n : nat} :
  n ⊨ edom_rel F⟦ ε ⟧ η ! θ e₁ e₂ ls₁ ls₂ μ →
  rfree_l ls₁ E₁ →
  rfree_l ls₂ E₂ →
  (∀ e₁' e₂', n ⊨ μ e₁' e₂' ⇒
    ▷ E⟦ τ // ε ⟧ η ! θ (rplug E₁ e₁') (rplug E₂ e₂')) →
  n ⊨ S⟦ τ // ε ⟧ η ! θ E₁ E₂ e₁ e₂.
Proof.
  intros Heff HE₁ HE₂ Hμ; unfold rel_stuck_cl.
  iexists ls₁; iexists ls₂; iexists μ; isplit; [| isplit]; [assumption | now split |].
  iintro e₁'; iintro e₂'; specialize (Hμ e₁' e₂').
  iintro He'; ispecialize Hμ He'; later_shift.
  apply rel_expr_cl_fix_roll; assumption.
Qed.

Lemma rel_s_unroll {TV LV : Set} {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    E₁ E₂ e₁ e₂
    {n : nat} :
  n ⊨ S⟦ τ // ε ⟧ η ! θ E₁ E₂ e₁ e₂ →
  ∃ ls₁ ls₂ μ, (n ⊨ edom_rel F⟦ ε ⟧ η ! θ e₁ e₂ ls₁ ls₂ μ) ∧
    ( rfree_l ls₁ E₁ ∧ rfree_l ls₂ E₂) ∧
  (n ⊨ ∀ᵢ e₁' e₂', μ e₁' e₂' ⇒
    ▷ E⟦ τ // ε ⟧ η ! θ (rplug E₁ e₁') (rplug E₂ e₂')).
Proof.
  intro Hs; unfold rel_stuck_cl in Hs.
  idestruct Hs as ls₁ Hs; idestruct Hs as ls₂ Hs; idestruct Hs as μ Hs; exists ls₁, ls₂, μ.
  idestruct Hs as Hμ Hs; split; [ assumption | ].
  idestruct Hs as Hfree Hs; split; [assumption | ].
  iintro e₁'; iintro e₂'; iintro He'.
  iapply Hs in He'; later_shift.
  now apply rel_expr_cl_fix_unroll.
Qed.

Lemma rel_k_roll {TV LV : Set} {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]} 
    F₁ F₂ {n : nat } :
  (∀ v₁ v₂, n ⊨ ⟦ τ ⟧ η ! θ v₁ v₂ ⇒
      ProgramObs.Obs (plug F₁ v₁) (plug F₂ v₂)) →
  (∀ E₁ E₂ e₁ e₂, n ⊨ S⟦ τ // ε ⟧ η ! θ E₁ E₂ e₁ e₂ ⇒
      ProgramObs.Obs (plug F₁ (rplug E₁ e₁)) (plug F₂ (rplug E₂ e₂))) →
  n ⊨ K⟦ τ // ε ⟧ η ! θ F₁ F₂.
Proof.
intros Hv Hs.
unfold rel_ectx_cl; isplit.
+ iintro v₁; iintro v₂; apply Hv.
+ iintro E₁; iintro E₂; iintro e₁; iintro e₂; apply Hs.
Qed.

Lemma rel_v_open_unroll {TV LV V : Set}
  (Δ : TV → kind)
  (Θ : LV → typ_ TV LV)
  (Γ : V  → typ_ TV LV)
  (τ : typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  (v₁ v₂ : value_ V LV) η θ γ₁ γ₂ n :
  n ⊨ T⟦ Δ; Θ; Γ ⊨ v₁ ≾ v₂ ∷ τ ⟧ →
  n ⊨ Θ⟦ Θ ⟧ η ! θ ∧ᵢ G⟦ Γ ⟧ η ! θ γ₁ γ₂ →
  n ⊨ ⟦ τ ⟧ η ! θ (bind γ₁ v₁) (bind γ₂ v₂).
Proof.
  intros Hv Hθγ; now iapply Hv.
Qed.
