(** This module provides some simple properties of the logical *)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Types Kinding.
Require Import Fresh.Syntax Fresh.Reduction.
Require Import LogicalRelation.ProgramObs LogicalRelation.Core.

Lemma rel_k_value {TV LV : Set} F₁ F₂ {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    {v₁ v₂ : value0} {n} :
  n ⊨ K⟦ τ // ε ⟧ η ! θ F₁ F₂ →
  n ⊨ ⟦ τ ⟧ η ! θ v₁ v₂ →
  n ⊨ Obs (plug F₁ v₁) (plug F₂ v₂).
Proof.
  intros HF Hs; unfold rel_ectx_cl in HF.
  idestruct HF as HFv HFs; now iapply HFv.
Qed.

Lemma rel_k_stuck {TV LV : Set} F₁ F₂ {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    E₁ E₂ {e₁} {e₂} {n} :
  n ⊨ K⟦ τ // ε ⟧ η ! θ F₁ F₂ →
  n ⊨ S⟦ τ // ε ⟧ η ! θ E₁ E₂ e₁ e₂ →
  n ⊨ Obs (plug F₁ (rplug E₁ e₁)) (plug F₂ (rplug E₂ e₂)).
Proof.
  intros HF Hs; unfold rel_ectx_cl in HF.
  idestruct HF as HFv HFs; now iapply HFs.
Qed.

Lemma rel_k_expr {TV LV : Set} F₁ F₂ {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    {e₁} {e₂} {n} :
  n ⊨ K⟦ τ // ε ⟧ η ! θ F₁ F₂ →
  n ⊨ E⟦ τ // ε ⟧ η ! θ e₁ e₂ →
  n ⊨ Obs (plug F₁ e₁) (plug F₂ e₂).
Proof.
  intros HF He; now iapply He.
Qed.

Lemma rel_v_in_e {TV LV : Set} {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε 
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    {v₁ v₂ : value0} {n} :
  n ⊨ ⟦ τ ⟧ η ! θ v₁ v₂ →
  n ⊨ E⟦ τ // ε ⟧ η ! θ v₁ v₂.
Proof.
  intros Hs; iintro F₁; iintro F₂; iintro HF.
  eapply rel_k_value; eassumption.
Qed.

Lemma rel_s_in_e {TV LV : Set} {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    E₁ E₂ {e₁} {e₂} {n} :
  n ⊨ S⟦ τ // ε ⟧ η ! θ E₁ E₂ e₁ e₂ →
  n ⊨ E⟦ τ // ε ⟧ η ! θ (rplug E₁ e₁) (rplug E₂ e₂).
Proof.
  intro Hs; iintro F₁; iintro F₂; iintro HF.
  eapply rel_k_stuck; eassumption.
Qed.

Lemma rel_e_contr_l {TV LV : Set} {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    e₁ e₁' e₂ n :
  contr e₁ e₁' →
  n ⊨ ▷E⟦ τ // ε ⟧ η ! θ e₁' e₂ →
  n ⊨ E⟦ τ // ε ⟧ η ! θ e₁ e₂.
Proof.
  intros Hcontr He; iintro F₁; iintro F₂; iintro HF.
  eapply Obs_red_l; [apply red_contr with (F := F₁); eassumption |].
  later_shift; eapply rel_k_expr; eassumption.
Qed.

Lemma rel_e_contr_r {TV LV : Set} {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    e₁ e₂ e₂' n :
  contr e₂ e₂' →
  n ⊨ E⟦ τ // ε ⟧ η ! θ e₁ e₂' →
  n ⊨ E⟦ τ // ε ⟧ η ! θ e₁ e₂.
Proof.
  intros Hcontr He; iintro F₁; iintro F₂; iintro HF.
  eapply Obs_red_r; [apply red_contr with (F := F₂); eassumption |].
  eapply rel_k_expr; eassumption.
Qed.

Lemma rel_e_contr_both {TV LV : Set} {Δ : TV → kind}
    (η : ∀ α, K⟦ Δ α ⟧) (θ : LV → instance_dom)
    τ ε
    {WFτ : K[ Δ ; LV ⊢ τ ∷ k_type   ]}
    {WFε : K[ Δ ; LV ⊢ ε ∷ k_effect ]}
    e₁ e₁' e₂ e₂' n :
  contr e₁ e₁' → contr e₂ e₂' →
  n ⊨ ▷E⟦ τ // ε ⟧ η ! θ e₁' e₂' →
  n ⊨ E⟦ τ // ε ⟧ η ! θ e₁ e₂.
Proof.
  intros Hcontr_l Hcontr_r He.
  eapply rel_e_contr_r; [eassumption |].
  eapply rel_e_contr_l; eassumption.
Qed.

Lemma rel_v_open_in_rel_e_open {TV LV V : Set}
  (Δ : TV → kind)
  (Θ : LV → typ_ TV LV)
  (Γ : V  → typ_ TV LV)
  (τ : typ_ TV LV)
  (ε : typ_ TV LV)
  {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
  {WFΓ : K[ Δ; LV ⊢ᴱ Γ]}
  {WFτ : K[ Δ; LV ⊢ τ ∷ k_type]}
  {WFε : K[ Δ; LV ⊢ ε ∷ k_effect]}
  (v₁ v₂ : value_ V LV) {n} :
  n ⊨ T⟦ Δ ; Θ ; Γ ⊨ v₁ ≾ v₂ ∷ τ ⟧ →
  n ⊨ T⟦ Δ ; Θ ; Γ ⊨ v₁ ≾ v₂ ∷ τ // ε ⟧.
Proof.
  intro Hv; iintro η; iintro θ; iintro γ₁; iintro γ₂; iintro Hγ.
  apply rel_v_in_e; now iapply Hv.
Qed.
