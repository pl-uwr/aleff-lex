Require Import Utf8.
Require Import IxFree.Lib.
Require Import Binding.Lib Binding.Product Binding.Env.
Require Import Surface.Types.
Require Import Fresh.Syntax Fresh.Reduction.
Require Import LogicalRelation.Closures.
Import TacticSynonyms.

Local Open Scope irel_scope.

Fixpoint kind_interp (κ : kind) {struct κ} : Type :=
  match κ with
  | k_type   => RCtype
  | k_effect => effect_dom
  | k_sig    => RCsig
  end.

Notation "'K⟦' κ '⟧'" := (kind_interp κ) (κ at level 30).

Lemma semFresh1_pure : ⊨ semFresh1 0 ⊥ᵣ.
Proof.
  intros n; repeat iintro; simpl in *; isimplP; contradiction.
Qed.

Lemma semFresh2_pure : ⊨ semFresh2 0 ⊥ᵣ.
Proof.
  intros n; repeat iintro; simpl in *; isimplP; contradiction.
Qed.

Definition pure_effect : effect_dom :=
  {| edom_rel := ⊥ᵣ
  ;  edom_ft1 := 0
  ;  edom_ft2 := 0
  ;  edom_t1_fresh := semFresh1_pure
  ;  edom_t2_fresh := semFresh2_pure
  |}.

Lemma semFresh1_or_max {R₁ R₂ l₁ l₂}
      (HF₁ : ⊨ semFresh1 l₁ R₁)
      (HF₂ : ⊨ semFresh1 l₂ R₂) :
  ⊨ semFresh1 (max l₁ l₂) (R₁ ∪ᵣ R₂).
Proof.
  intros n; iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; iintro HR; idestruct HR as HR HR.
  - specialize (HF₁ n); iespecialize HF₁; ispecialize HF₁; [eassumption |].
    intros l HLE; eapply HF₁, Max.max_lub_l, HLE.
  - specialize (HF₂ n); iespecialize HF₂; ispecialize HF₂; [eassumption |].
    intros l HLE; eapply HF₂, Max.max_lub_r, HLE.
Qed.

Lemma semFresh2_or_max {R₁ R₂ l₁ l₂}
      (HF₁ : ⊨ semFresh2 l₁ R₁)
      (HF₂ : ⊨ semFresh2 l₂ R₂) :
  ⊨ semFresh2 (max l₁ l₂) (R₁ ∪ᵣ R₂).
Proof.
  intros n; iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; iintro HR; idestruct HR as HR HR.
  - specialize (HF₁ n); iespecialize HF₁; ispecialize HF₁; [eassumption |].
    intros l HLE; eapply HF₁, Max.max_lub_l, HLE.
  - specialize (HF₂ n); iespecialize HF₂; ispecialize HF₂; [eassumption |].
    intros l HLE; eapply HF₂, Max.max_lub_r, HLE.
Qed.

Definition cons_effect (η₁ η₂ : effect_dom) : effect_dom :=
  {| edom_rel := η₁ ∪ᵣ η₂
  ;  edom_ft1 := max (edom_ft1 η₁) (edom_ft1 η₂)
  ;  edom_ft2 := max (edom_ft2 η₁) (edom_ft2 η₂)
  ;  edom_t1_fresh := semFresh1_or_max (edom_t1_fresh η₁) (edom_t1_fresh η₂)
  ;  edom_t2_fresh := semFresh2_or_max (edom_t2_fresh η₁) (edom_t2_fresh η₂)
  |}.

Definition instance_rel (ν : ℑ) : RCeff :=
  λ e₁ e₂ ls₁ ls₂ μ,
  ∃ᵢ v₁ v₂,
    (e₁ = e_do (l_tag (ℑ_ltag1 ν)) v₁ ∧
     e₂ = e_do (l_tag (ℑ_ltag2 ν)) v₂ ∧
     ls₁ = cons (ℑ_ltag1 ν) nil ∧
     ls₂ = cons (ℑ_ltag2 ν) nil)ᵢ ∧ᵢ
    ℑ_rel ν v₁ v₂ μ.

Lemma semFresh1_inst_S (ν : ℑ) :
  ⊨ semFresh1 (S (ℑ_ltag1 ν)) (instance_rel ν).
Proof.
  intros n; iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; iintro HH; intros.
  idestruct HH as v₁ HH; idestruct HH as v₂ HH; idestruct HH as HR HH.
  destruct HR as [He₁ [He₂ [Hls₁ Hls₂]]]; subst; simpl.
  intros [EQ | HF]; [rewrite EQ in H | contradiction].
  contradiction (PeanoNat.Nat.nle_succ_diag_l l').
Qed.

Lemma semFresh2_inst_S (ν : ℑ) :
  ⊨ semFresh2 (S (ℑ_ltag2 ν)) (instance_rel ν).
Proof.
  intros n; iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; iintro HH; intros.
  idestruct HH as v₁ HH; idestruct HH as v₂ HH; idestruct HH as HR HH.
  destruct HR as [He₁ [He₂ [Hls₁ Hls₂]]]; subst; simpl.
  intros [EQ | HF]; [rewrite EQ in H | contradiction].
  contradiction (PeanoNat.Nat.nle_succ_diag_l l').
Qed.

Definition instance_effect (ν : ℑ) : effect_dom :=
  {| edom_rel := instance_rel ν
  ;  edom_ft1 := S (ℑ_ltag1 ν)
  ;  edom_ft2 := S (ℑ_ltag2 ν)
  ;  edom_t1_fresh := semFresh1_inst_S ν
  ;  edom_t2_fresh := semFresh2_inst_S ν
  |}.

Definition arrow_rel (IA IR : RCtype) (IE : effect_dom) : RCtype :=
  λ v₁ v₂, ∀ᵢ u₁ u₂, IA u₁ u₂ ⇒ rel_expr_cl IE IR (e_app v₁ u₁) (e_app v₂ u₂).

Definition tall_rel κ (I : K⟦ κ ⟧ → RCtype) : RCtype :=
  λ v₁ v₂, ∀ᵢ (R : K⟦ κ ⟧), rel_expr_cl pure_effect (I R) (e_tapp v₁) (e_tapp v₂).

Definition lall_rel (IA : RCsig) (IR : ℑ → RCtype) : RCtype :=
  λ v₁ v₂, ∀ᵢ I : ℑ, I ≈ᵢ IA ⇒
    rel_expr_cl pure_effect (IR I) (e_lapp v₁ (l_tag (ℑ_ltag1 I))) (e_lapp v₂ (l_tag (ℑ_ltag2 I))).

Definition sig_rel (IA IR : RCtype) : RCsig :=
  λ v₁ v₂ μ, IA v₁ v₂ ∧ᵢ μ ≈ᵢ IR.

Definition talls_rel κ (I : K⟦ κ ⟧ → RCsig) : RCsig :=
  λ v₁ v₂ μ, ∃ᵢ R, I R v₁ v₂ μ.

Section Properties.

  

  (* arrows *)
  Lemma subrel_arr IA₁ IA₂ IR₁ IR₂ (IE₁ IE₂ : effect_dom) n
        (HA : n ⊨ IA₂ ≾ᵢ IA₁) (HR : n ⊨ IR₁ ≾ᵢ IR₂) (HE : n ⊨ IE₁ ≾ᵢ IE₂) :
    n ⊨ arrow_rel IA₁ IR₁ IE₁ ≾ᵢ arrow_rel IA₂ IR₂ IE₂.
  Proof.
    iintro v₁; iintro v₂; iintro HH; iintro u₁; iintro u₂; iintro Hu.
    iapply @subrel_expr_cl; [eassumption .. |].
    iapply HH; now iapply HA.
  Qed.

  Lemma equiv_arr IA₁ IA₂ IR₁ IR₂ (IE₁ IE₂ : effect_dom) n
        (HA : n ⊨ IA₁ ≈ᵢ IA₂) (HR : n ⊨ IR₁ ≈ᵢ IR₂) (HE : n ⊨ IE₁ ≈ᵢ IE₂) :
    n ⊨ arrow_rel IA₁ IR₁ IE₁ ≈ᵢ arrow_rel IA₂ IR₂ IE₂.
  Proof.
    apply equiv_subrel_symcl; apply subrel_arr; apply subrel_sub_equiv;
      assumption || symmetry; assumption.
  Qed.

  (* type universals *)
  Lemma subrel_tall κ I₁ I₂ n (HI : n ⊨ ∀ᵢ R, I₁ R ≾ᵢ I₂ R) :
    n ⊨ tall_rel κ I₁ ≾ᵢ tall_rel κ I₂.
  Proof.
    iintro v₁; iintro v₂; iintro HH; iintro R.
    iapply @subrel_expr_cl; [reflexivity | iapply HI | iapply HH].
  Qed.

  Lemma equiv_tall κ I₁ I₂ n (HI : n ⊨ ∀ᵢ R, I₁ R ≈ᵢ I₂ R) :
    n ⊨ tall_rel κ I₁ ≈ᵢ tall_rel κ I₂.
  Proof.
    apply equiv_subrel_symcl; apply subrel_tall; iintro R; apply subrel_sub_equiv;
      [| symmetry]; iapply HI.
  Qed.

  (* instance universals *)
  Lemma subrel_lall I₁ I₂ IR₁ IR₂ n (HI : n ⊨ I₁ ≈ᵢ I₂) (HIR : n ⊨ ∀ᵢ R, IR₁ R ≾ᵢ IR₂ R) :
    n ⊨ lall_rel I₁ IR₁ ≾ᵢ lall_rel I₂ IR₂.
  Proof.
    iintro v₁; iintro v₂; iintro HH; iintro ν; iintro HEq; ispecialize HIR ν.
    iapply @subrel_expr_cl; [reflexivity | eassumption | iapply HH; clear HH HIR].
    etransitivity; [eassumption | symmetry; assumption].
  Qed.

  Lemma equiv_lall I₁ I₂ IR₁ IR₂ n (HI : n ⊨ I₁ ≈ᵢ I₂) (HIR : n ⊨ ∀ᵢ R, IR₁ R ≈ᵢ IR₂ R) :
    n ⊨ lall_rel I₁ IR₁ ≈ᵢ lall_rel I₂ IR₂.
  Proof.
    apply equiv_subrel_symcl; apply subrel_lall;
      first [assumption | symmetry; assumption | iintro]; apply subrel_sub_equiv;
      [| symmetry]; iapply HIR.
  Qed.

  (* instance effects *)
  Lemma equiv_instance ν₁ ν₂ n (HA : n ⊨ ν₁ ≈ᴵ ν₂) :
    n ⊨ instance_rel ν₁ ≈ᵢ instance_rel ν₂.
  Proof.
    iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; idestruct HA as HA HB.
    destruct HB as [H₁ H₂]; unfold instance_rel; auto_contr.
    - rewrite H₁, H₂; reflexivity.
    - iapply HA.
  Qed.

  (* effect composition *)
  Lemma effect_sub_cons (IA₁ IA₂ IB₁ IB₂ : effect_dom) n
        (HE₁ : n ⊨ IA₁ ≾ᵢ IB₁)
        (HE₂ : n ⊨ IA₂ ≾ᵢ IB₂) :
    n ⊨ cons_effect IA₁ IA₂ ≾ᵢ cons_effect IB₁ IB₂.
  Proof.
    iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; iintro HH.
    idestruct HH as HH HH; [ileft; now iapply HE₁ | iright; now iapply HE₂].
  Qed.

  Lemma subrel_cons (IA₁ IA₂ IB₁ IB₂ : effect_dom) n
        (HE₁ : n ⊨ IA₁ ≾ᵢ IB₁)
        (HE₂ : n ⊨ IA₂ ≾ᵢ IB₂) :
    n ⊨ cons_effect IA₁ IA₂ ≾ᵢ cons_effect IB₁ IB₂.
  Proof.
    iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; iintro HH.
    idestruct HH as HH HH; [ileft; now iapply HE₁ | iright; now iapply HE₂].
  Qed.

  Lemma equiv_cons (IA₁ IA₂ IB₁ IB₂ : effect_dom) n
        (HE₁ : n ⊨ IA₁ ≈ᵢ IB₁)
        (HE₂ : n ⊨ IA₂ ≈ᵢ IB₂) :
    n ⊨ cons_effect IA₁ IA₂ ≈ᵢ cons_effect IB₁ IB₂.
  Proof.
    apply equiv_subrel_symcl; apply subrel_cons; apply subrel_sub_equiv;
      assumption || symmetry; assumption.
  Qed.

  Lemma effect_equiv_consC I₁ I₂ n :
    n ⊨ cons_effect I₁ I₂ ≈ᵢ cons_effect I₂ I₁.
  Proof.
    iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; isplit; iintro HH;
      now (idestruct HH as HH HH; [iright | ileft]).
  Qed.

  Lemma effect_equiv_consA I₁ I₂ I₃ n :
    n ⊨ cons_effect (cons_effect I₁ I₂) I₃ ≈ᵢ cons_effect I₁ (cons_effect I₂ I₃).
  Proof.
    iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; isplit; iintro HH.
    - idestruct HH as HH HH; [idestruct HH as HH HH |];
        [ileft | iright; ileft | iright; iright]; assumption.
    - idestruct HH as HH HH; [| idestruct HH as HH HH];
        [ileft; ileft | ileft; iright | iright]; assumption.
  Qed.

  Lemma effect_equiv_cons_pureIL (I : effect_dom) n :
    n ⊨ I ≈ᵢ cons_effect pure_effect I.
  Proof.
    iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; isplit; iintro HH; [now iright |].
    idestruct HH as HH HH; [contradiction | assumption].
  Qed.

  Lemma effect_equiv_cons_pureIR (I : effect_dom) n :
    n ⊨ I ≈ᵢ cons_effect I pure_effect.
  Proof.
    iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ; isplit; iintro HH; [now ileft |].
    idestruct HH as HH HH; [assumption | contradiction].
  Qed.

  Lemma effect_equiv_consI (I : effect_dom) n :
    n ⊨ I ≈ᵢ cons_effect I I.
  Proof.
    iintro e₁; iintro e₂; iintro ls₁; iintro ls₂; iintro μ.
    isplit; iintro HH; [iright | idestruct HH as HH HH]; assumption.
  Qed.

  (* pure effects *)
  Lemma effect_sub_pure I n :  n ⊨ pure_effect ≾ᵢ I.
  Proof.
    repeat iintro; contradiction.
  Qed.

  (* operation signatures *)
  Lemma equiv_sig  IA₁ IA₂ IR₁ IR₂ n
        (HA : n ⊨ IA₁ ≈ᵢ IA₂) (HR : n ⊨ IR₁ ≈ᵢ IR₂) :
    n ⊨ sig_rel IA₁ IR₁ ≈ᵢ sig_rel IA₂ IR₂.
  Proof.
    iintro v₁; iintro v₂; iintro μ; unfold sig_rel; auto_contr.
    - iapply HA.
    - simpl; auto_contr; iapply HR.
  Qed.

  (* universals in signatures *)
  Lemma talls_inst {κ} (R : K⟦ κ ⟧) (I : K⟦ κ ⟧ → RCsig) n :
    n ⊨ I R ≾ᵢ talls_rel κ I.
  Proof.
    iintro v₁; iintro v₂; iintro μ; iintro HR; iexists R; exact HR.
  Qed.

  Lemma equiv_talls κ I₁ I₂ n (HI : forall R, n ⊨ I₁ R ≈ᵢ I₂ R) :
    n ⊨ talls_rel κ I₁ ≈ᵢ talls_rel κ I₂.
  Proof.
    iintro v₁; iintro v₂; iintro μ; unfold talls_rel; auto_contr.
    iapply (HI x).
  Qed.

End Properties.
