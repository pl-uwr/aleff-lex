Require Import Utf8.
Require Import IxFree.Lib.
Require Import Fresh.Syntax Fresh.Reduction.

Definition obs_sig := (expr0 →ₛ expr0 →ₛ *ₛ)%irel_sig.

Definition F_Obs (R : IRel obs_sig) (e₁ e₂ : expr0) : IProp :=
  (e₁ = v_unit ∧ red_rtc e₂ v_unit)ᵢ ∨ᵢ
  ∃ᵢ e₁', (red e₁ e₁')ᵢ ∧ᵢ ▷R e₁' e₂.

Lemma F_Obs_contractive : contractive obs_sig F_Obs.
Proof.
intros R₁ R₂ n; iintro HR; iintro e₁; iintro e₂.
unfold F_Obs; auto_contr.
iespecialize HR; exact HR.
Qed.

Definition Obs := I_fix obs_sig F_Obs F_Obs_contractive.

Lemma Obs_roll (n : nat) (e₁ e₂ : expr0) :
  n ⊨ F_Obs Obs e₁ e₂ → n ⊨ Obs e₁ e₂.
Proof.
intro He; unfold Obs; apply (I_fix_roll obs_sig); assumption.
Qed.

Lemma Obs_unroll (n : nat) (e₁ e₂ : expr0) :
  n ⊨ Obs e₁ e₂ → n ⊨ F_Obs Obs e₁ e₂.
Proof.
intro He; apply (I_fix_unroll obs_sig); assumption.
Qed.

Lemma Obs_red_l (e₁ e₁' e₂ : expr0) (n : nat) :
  red e₁ e₁' →
  n ⊨ ▷Obs e₁' e₂ →
  n ⊨ Obs e₁ e₂.
Proof.
intros Hred He'; apply Obs_roll; unfold F_Obs.
iright; iexists e₁'; isplit; assumption.
Qed.

Lemma Obs_red_r (e₁ e₂ e₂' : expr0) (n : nat) :
  red e₂ e₂' →
  n ⊨ Obs e₁ e₂' →
  n ⊨ Obs e₁ e₂.
Proof.
  intros Hred HO; irevert e₁ HO; loeb_induction; iintro e₁; iintro He'.
  apply Obs_roll; apply Obs_unroll in He'; idestruct He' as He' He'.
  + ileft; isimplP.
    destruct He' as [Heq Hred']; split; [assumption |].
    econstructor 2; eassumption.
  + idestruct He' as e₁' He₁'.
    idestruct He₁' as Hred₁ He'.
    iright; iexists e₁'; isplit; [ assumption | ].
    later_shift.
    now iapply IH.
Qed.

Lemma Obs_red_both (e₁ e₁' e₂ e₂' : expr0) (n : nat) :
  red e₁ e₁' → red e₂ e₂' →
  n ⊨ ▷ Obs e₁' e₂' →
  n ⊨ Obs e₁ e₂.
Proof.
intros; eapply Obs_red_r; [ eassumption | ].
eapply Obs_red_l; eassumption.
Qed.
