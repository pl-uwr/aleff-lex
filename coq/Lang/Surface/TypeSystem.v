Require Import Utf8.
Require Import Binding.Lib Binding.Env Binding.Product.
Require Export Types Kinding Subtypes.
Require Import Surface.Syntax.

Local Open Scope typ_scope.

Reserved Notation " Δ ; Σ ; Γ ⊢ e '::' τ / ε " (at level 70, no associativity, Σ, Γ, e at level 40, τ, ε at level 30).
Reserved Notation " Δ ; Σ ; Γ '⊢ᵛ' e '::' τ " (at level 70, no associativity, Σ, Γ, e at level 40, τ at level 30).
Reserved Notation " Δ ; Σ ; Γ '⊢ʰ' h '::' σ ⇝ τ / ε " (at level 70, no associativity, Σ, Γ, h, σ at level 40, τ, ε at level 30).

Reserved Notation " Δ ⊢ σ ⇝ τ₁ ⇒ τ₂ " (at level 70, no associativity, σ, τ₁, τ₂ at level 30).

Inductive sinst {TV LV : Set} (Δ : TV → kind) : typ_ TV LV → typ_ TV LV → typ_ TV LV → Prop :=
| si_do (τ₁ τ₂ : typ_ TV LV) :
    Δ ⊢ (τ₁ ⇒ᵈ τ₂) ⇝ τ₁ ⇒ τ₂
| si_all κ τ₁ τ₂ τ (σ : typ (incV {| sfst := TV |}))
         (HK : K[ Δ ; LV ⊢ τ ∷ κ ])
         (HI : Δ ⊢ subst σ τ ⇝ τ₁ ⇒ τ₂) :
    Δ ⊢ ∀ˢ κ, σ ⇝ τ₁ ⇒ τ₂
where " Δ ⊢ σ ⇝ τ₁ ⇒ τ₂ " := (@sinst _ _ Δ σ τ₁ τ₂).

Inductive vtypes {V TV LV : Set} (Δ : TV → kind) (Σ : LV → typ_ TV LV) (Γ : V → typ_ TV LV)
  : value_ V LV → typ_ TV LV → Prop :=
| T_unit : Δ; Σ; Γ ⊢ᵛ v_unit :: t_unit
| T_var x : Δ; Σ; Γ ⊢ᵛ v_var x :: Γ x
| T_lam (e : expr (incV {| sfst := _ |})) τ₁ τ₂ ε
        (HK : K[ Δ ; LV ⊢ τ₁ ∷ k_type ])
        (HT : Δ; Σ; Γ ▹ τ₁ ⊢ e :: τ₂ / ε) :
    Δ; Σ; Γ ⊢ᵛ v_lam e :: τ₁ →[ε] τ₂
| T_tlam e κ (τ : typ (incV {| sfst := _ |}))
         (HT : Δ ▹ κ; shift Σ; shift Γ ⊢ e :: τ / ι) :
    Δ; Σ; Γ ⊢ᵛ v_tlam e :: ∀ᵗ κ, τ
| T_llam (e : expr (incL {| sfst := V |})) σ (τ : typ (incL {| sfst := _ |}))
         (HK : K[ Δ ; LV ⊢ σ ∷ k_sig ])
         (HT : Δ; shift (Σ ▹ σ); shift Γ ⊢ e :: τ / ι) :
    Δ; Σ; Γ ⊢ᵛ v_llam e :: ∀ˡ σ, τ
where " Δ ; Σ ; Γ '⊢ᵛ' v '::' τ " := (@vtypes _ _ _ Δ Σ Γ v τ)
with etypes {V TV LV : Set} (Δ : TV → kind) (Σ : LV → typ_ TV LV) (Γ : V → typ_ TV LV)
  : expr_ V LV → typ_ TV LV → typ_ TV LV → Prop :=
| T_val v τ ε
        (HK : K[Δ; LV ⊢ ε ∷ k_effect])
        (HT : Δ; Σ; Γ ⊢ᵛ v :: τ) :
    Δ; Σ; Γ ⊢ v :: τ / ε
| T_let e₁ e₂ τ₁ τ₂ ε
        (HT₁ : Δ; Σ; Γ ⊢ e₁ :: τ₁ / ε)
        (HT₂ : Δ; Σ; Γ ▹ τ₁ ⊢ e₂ :: τ₂ / ε) :
    Δ; Σ; Γ ⊢ e_let e₁ e₂ :: τ₂ / ε
| T_app (v₁ : value _) (v₂ : value _) τ₁ τ₂ ε
        (HT₁ : Δ; Σ; Γ ⊢ᵛ v₁ :: τ₁ →[ε] τ₂)
        (HT₂ : Δ; Σ; Γ ⊢ᵛ v₂ :: τ₁) :
    Δ; Σ; Γ ⊢ e_app v₁ v₂ :: τ₂ / ε
| T_tapp (v : value _) κ τ σ
         (HK : K[ Δ ; LV ⊢ σ ∷ κ ])
         (HT : Δ; Σ; Γ ⊢ᵛ v :: ∀ᵗ κ, τ) :
    Δ; Σ; Γ ⊢ e_tapp v :: subst τ σ / ι
| T_lapp (v : value _) τ (a : s_lbl {| sfst := TV |})
         (HT : Δ; Σ; Γ ⊢ᵛ v :: ∀ˡ (Σ a), τ) :
    Δ; Σ; Γ ⊢ e_lapp v a :: subst τ a / ι
| T_do (v : value _) τ₁ τ₂ a
       (HI : Δ ⊢ Σ a ⇝ τ₁ ⇒ τ₂)
       (HT : Δ; Σ; Γ ⊢ᵛ v :: τ₁) :
    Δ; Σ; Γ ⊢ e_do (a : lvar {| sfst := V |}) v :: τ₂ / (a : s_lbl {| sfst := TV |})
| T_handle (e : expr (incL {| sfst := V |})) h r τ τ' σ ε
           (HKσ : K[ Δ ; LV ⊢ σ ∷ k_sig ])
           (HKτ : K[ Δ ; LV ⊢ τ ∷ k_type ])
           (HTr : Δ; Σ; Γ ▹ τ ⊢ r :: τ' / ε)
           (HTh : Δ; Σ; Γ ⊢ʰ h :: σ ⇝ τ' / ε)
           (HTe : Δ; shift (Σ ▹ σ); shift Γ ⊢ e :: shift τ / (VZ : s_lbl (incL {| sfst := TV |})) · shift ε) :
    Δ; Σ; Γ ⊢ e_handle e h r :: τ' / ε

| T_ST e τ₁ τ₂ ε₁ ε₂
       (HSTτ : Δ; LV ⊢ τ₁ ≲ τ₂)
       (HSTε : Δ; LV ⊢ ε₁ ≲ ε₂)
       (HT : Δ; Σ; Γ ⊢ e :: τ₁ / ε₁) :
    Δ; Σ; Γ ⊢ e :: τ₂ / ε₂
where " Δ ; Σ ; Γ ⊢ e :: τ / ε " := (@etypes _ _ _ Δ Σ Γ e τ ε)
with htypes {V TV LV : Set} (Δ : TV → kind) (Σ : LV → typ_ TV LV) (Γ : V → typ_ TV LV)
     : handler_ V LV → typ_ TV LV → typ_ TV LV → typ_ TV LV → Prop :=
| H_do τ₁ τ₂ τ ε (e : expr (incV (incV {| sfst := V |})))
       (HT : Δ; Σ; Γ ▹ τ₁ ▹ τ₂ →[ε] τ ⊢ e :: τ / ε) :
    Δ; Σ; Γ ⊢ʰ h_do e :: τ₁ ⇒ᵈ τ₂ ⇝ τ / ε
| H_all κ (σ : typ (incV {| sfst := TV |})) τ ε h
        (HT : Δ ▹ κ; shift Σ; shift Γ ⊢ʰ h :: σ ⇝ shift τ / shift ε) :
    Δ; Σ; Γ ⊢ʰ h :: ∀ˢ κ, σ ⇝ τ / ε
where "Δ ; Σ ; Γ ⊢ʰ h :: σ ⇝ τ / ε" := (@htypes _ _ _ Δ Σ Γ h σ τ ε).

Scheme vtypes_Ind := Induction for vtypes Sort Prop
  with etypes_Ind := Induction for etypes Sort Prop
  with htypes_Ind := Induction for htypes Sort Prop.

Lemma sinst_wf {TV LV : Set} {Δ : TV → kind} {σ τ₁ τ₂ : typ_ TV LV}
      (HI : Δ ⊢ σ ⇝ τ₁ ⇒ τ₂)
      (HK : K[Δ; LV ⊢ σ ∷ k_sig]) :
  K[Δ; LV ⊢ τ₁ ∷ k_type] ∧ K[Δ; LV ⊢ τ₂ ∷ k_type].
Proof.
  revert HK; induction HI; intros.
  - inversion_clear HK; split; assumption.
  - inversion_clear HK0; eauto with typeclass_instances.
Qed.

Ltac foo T₁ T₂ T₃ :=
  let tac := ltac:(clear T₁ T₂ T₃; apply _)
  in
  repeat match goal with
         | [ H : _; _; _ ⊢ _ :: _ / _ |- _] =>
           let Hτ := fresh "HKτ" in
           let Hε := fresh "HKε" in
           assert (Hτ := H); apply T₁ in Hτ; [| tac | tac];
           assert (Hε := H); apply T₂ in Hε; [| tac | tac]; clear H
         | [ H : _; _; _ ⊢ᵛ _ :: _ |- _] =>
           let Hτ := fresh "HKτ" in
           assert (Hτ := H); apply T₃ in Hτ; [| tac | tac]; clear H             
         end; try tac.

  
Fixpoint etypes_wf_typ {TV LV V : Set} {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV}
      {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
      {WFΓ : K[ Δ; LV ⊢ᴱ Γ]} {e τ ε}
      (HT : Δ; Θ; Γ ⊢ e :: τ / ε) :
  K[ Δ ; LV ⊢ τ ∷ k_type ]
with etypes_wf_eff {TV LV V : Set} {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV}
      {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
      {WFΓ : K[ Δ; LV ⊢ᴱ Γ]} {e τ ε}
      (HT : Δ; Θ; Γ ⊢ e :: τ / ε) :
  K[ Δ ; LV ⊢ ε ∷ k_effect ]
with vtypes_wf  {TV LV V : Set} {Δ : TV → kind} {Θ : LV → typ_ TV LV} {Γ : V → typ_ TV LV}
      {WFΘ : K[ Δ; LV ⊢ᵀ Θ]}
      {WFΓ : K[ Δ; LV ⊢ᴱ Γ]} {v τ}
      (HT : Δ; Θ; Γ ⊢ᵛ v :: τ) :
  K[ Δ ; LV ⊢ τ ∷ k_type ].
Proof.
  - destruct HT; simpl; foo etypes_wf_eff etypes_wf_typ vtypes_wf.
    + inversion_clear HKτ0; assumption.
    + inversion_clear HKτ; eauto with typeclass_instances.
    + inversion_clear HKτ; eauto with typeclass_instances.
    + apply sinst_wf in HI; [destruct HI as [HI₁ HI₂] |]; apply _.
    + apply (subtyp_wf _ HSTτ), _.
  - destruct HT; simpl; foo etypes_wf_eff etypes_wf_typ vtypes_wf.
    + inversion_clear HKτ0; assumption.
    + apply (subtyp_wf _ HSTε), _.
  - destruct HT; simpl; foo etypes_wf_eff etypes_wf_typ vtypes_wf.
Qed.
