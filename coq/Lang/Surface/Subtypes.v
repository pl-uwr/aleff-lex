Require Import Utf8.
Require Import Binding.Lib Binding.Env Binding.Product.
Require Import Types Kinding.
Require Import Surface.Syntax.

Reserved Notation " Δ ; Σ ⊢ σ₁ ≃ σ₂ " (at level 70, Σ, σ₁, σ₂ at level 40, no associativity).
Reserved Notation " Δ ; Σ ⊢ σ₁ ≲ σ₂ " (at level 70, Σ, σ₁, σ₂ at level 40, no associativity).

Local Open Scope typ_scope.

Inductive tequiv {TV LV : Set} (Δ : TV → kind) : typ_ TV LV → typ_ TV LV → Prop :=
| TE_Refl σ :
    Δ ; LV ⊢ σ ≃ σ
| TE_Symm σ₁ σ₂
          (HTE : Δ ; LV ⊢ σ₁ ≃ σ₂) :
    Δ ; LV ⊢ σ₂ ≃ σ₁
| TE_Trans σ₁ σ₂ σ₃
           (HTE₁ : Δ; LV ⊢ σ₁ ≃ σ₂)
           (HTE₂ : Δ; LV ⊢ σ₂ ≃ σ₃) :
    Δ; LV ⊢ σ₁ ≃ σ₃
| TE_Arrow σ₁ σ₂ τ₁ τ₂ ε₁ ε₂
           (HTEσ : Δ; LV ⊢ σ₁ ≃ σ₂)
           (HTEτ : Δ; LV ⊢ τ₁ ≃ τ₂)
           (HTEε : Δ; LV ⊢ ε₁ ≃ ε₂) :
    Δ; LV ⊢ σ₁ →[ε₁] τ₁ ≃ σ₂ →[ε₂] τ₂
| TE_ForallT κ (τ₁ τ₂ : typ (incV {| sfst := _ |}))
             (HTE : Δ ▹ κ; LV ⊢ τ₁ ≃ τ₂) :
    Δ; LV ⊢ ∀ᵗ κ, τ₁ ≃ ∀ᵗ κ, τ₂
| TE_ForallL σ₁ σ₂ τ₁ τ₂
             (HTEσ : Δ; LV ⊢ σ₁ ≃ σ₂)
             (HTEτ : Δ; inc LV ⊢ τ₁ ≃ τ₂) :
    Δ; LV ⊢ ∀ˡ σ₁, τ₁ ≃ ∀ˡ σ₂, τ₂
| TE_Cons ε₁ ε₂ ε₁' ε₂'
          (HTE₁ : Δ; LV ⊢ ε₁ ≃ ε₂)
          (HTE₂ : Δ; LV ⊢ ε₁' ≃ ε₂') :
    Δ; LV ⊢ ε₁ · ε₁' ≃ ε₂ · ε₂'
| TE_OpSig σ₁ σ₂ τ₁ τ₂
           (HTEσ : Δ; LV ⊢ σ₁ ≃ σ₂)
           (HTEτ : Δ; LV ⊢ τ₁ ≃ τ₂) :
    Δ; LV ⊢ σ₁ ⇒ᵈ τ₁ ≃ σ₂ ⇒ᵈ τ₂
| TE_AllTS κ (τ₁ τ₂ : typ (incV {| sfst := _ |}))
             (HTE : Δ ▹ κ; LV ⊢ τ₁ ≃ τ₂) :
    Δ; LV ⊢ ∀ˢ κ, τ₁ ≃ ∀ˢ κ, τ₂

| TE_Comm ε₁ ε₂ :
    Δ; LV ⊢ ε₁ · ε₂ ≃ ε₂ · ε₁
| TE_Assoc ε₁ ε₂ ε₃ :
           Δ; LV ⊢ ε₁ · ε₂ · ε₃ ≃ ε₁ · (ε₂ · ε₃)
| TE_IdL  ε (HK : K[ Δ ; LV ⊢ ε ∷ k_effect ]) :
    Δ; LV ⊢ ε ≃ ι · ε
| TE_IdR  ε (HK : K[ Δ ; LV ⊢ ε ∷ k_effect ]) :
    Δ; LV ⊢ ε ≃ ε · ι
| TE_Idem ε (HK : K[ Δ ; LV ⊢ ε ∷ k_effect ]) :
    Δ; LV ⊢ ε ≃ ε · ε

where " Δ ; Σ ⊢ σ₁ ≃ σ₂ " := (@tequiv _ Σ Δ σ₁ σ₂).

Inductive subtyp {TV LV : Set} (Δ : TV → kind) : typ_ TV LV → typ_ TV LV → Prop :=
| ST_TE τ₁ τ₂
        (HTE : Δ; LV ⊢ τ₁ ≃ τ₂) :
    Δ; LV ⊢ τ₁ ≲ τ₂
| ST_Arrow σ₁ σ₂ ε₁ ε₂ τ₁ τ₂
           (HSTσ : Δ; LV ⊢ σ₂ ≲ σ₁)
           (HSTε : Δ; LV ⊢ ε₁ ≲ ε₂)
           (HSTτ : Δ; LV ⊢ τ₁ ≲ τ₂) :
    Δ; LV ⊢ σ₁ →[ε₁] τ₁ ≲ σ₂ →[ε₂] τ₂
| ST_TAll κ (τ₁ τ₂ : typ (incV {| sfst := _ |}))
          (HST : Δ ▹ κ; LV ⊢ τ₁ ≲ τ₂) :
    Δ; LV ⊢ ∀ᵗ κ, τ₁ ≲ ∀ᵗ κ, τ₂
| ST_LAll σ₁ σ₂ τ₁ τ₂
          (HTE : Δ; LV ⊢ σ₁ ≃ σ₂)
          (HST : Δ; inc LV ⊢ τ₁ ≲ τ₂) :
    Δ; LV ⊢ ∀ˡ σ₁, τ₁ ≲ ∀ˡ σ₂, τ₂
| ST_Cons ε₁ ε₂ ε₁' ε₂'
          (HST₁ : Δ; LV ⊢ ε₁ ≲ ε₁')
          (HST₂ : Δ; LV ⊢ ε₂ ≲ ε₂') :
    Δ; LV ⊢ ε₁ · ε₂ ≲ ε₁' · ε₂'
| ST_Pure ε (HK : K[ Δ ; LV ⊢ ε ∷ k_effect ]) :
    Δ; LV ⊢ ι ≲ ε
where " Δ ; Σ ⊢ σ₁ ≲ σ₂ " := (@subtyp _ Σ Δ σ₁ σ₂).

Lemma kinding_unique {TV LV : Set} {Δ : TV → kind} {τ κ₁ κ₂}
      (HK₁ : K[Δ; LV ⊢ τ ∷ κ₁])
      (HK₂ : K[Δ; LV ⊢ τ ∷ κ₂]) :
  κ₁ = κ₂.
Proof.
  revert κ₂ HK₂; induction HK₁; intros; inversion_clear HK₂; reflexivity.
Qed.

Lemma tequiv_wf {TV LV : Set} {Δ : TV → kind} {τ₁ τ₂} κ
      (HEQ : Δ; LV ⊢ τ₁ ≃ τ₂) :
  K[Δ; LV ⊢ τ₁ ∷ κ] ↔ K[Δ; LV ⊢ τ₂ ∷ κ].
Proof.
  revert κ; induction HEQ; intros;
    try (split; intros ?HK;
         repeat match goal with
                  [ H : ∀ κ : kind, _ ↔ _ |- _] => edestruct H; clear H
                end;
         now eauto with typeclass_instances || 
         inversion_clear HK; now eauto with typeclass_instances); [| | |].
  - split; intros HK; inversion_clear HK as [| | | | | | | ? ? HK₁ HK₂ | |];
      [inversion_clear HK₁ | inversion_clear HK₂]; repeat constructor; assumption.
  - split; intros HK'; [| now inversion_clear HK'].
    assert (EQ := kinding_unique HK HK'); subst; apply _.
  - split; intros HK'; [| now inversion_clear HK'].
    assert (EQ := kinding_unique HK HK'); subst; apply _.
  - split; intros HK'; [| now inversion_clear HK'].
    assert (EQ := kinding_unique HK HK'); subst; apply _.
Qed.

Lemma subtyp_wf {TV LV : Set} {Δ : TV → kind} {τ₁ τ₂} κ
      (HST : Δ; LV ⊢ τ₁ ≲ τ₂) :
  K[Δ; LV ⊢ τ₁ ∷ κ] ↔ K[Δ; LV ⊢ τ₂ ∷ κ].
Proof.
  revert κ; induction HST; intros; [now apply tequiv_wf |..];
    try (split; let HK := fresh "HK" in intros HK;
                                        repeat match goal with
                                                 [ H : ∀ κ : kind, _ ↔ _ |- _] => edestruct H; clear H
                                               end;
                                        inversion_clear HK; now eauto with typeclass_instances); [|].
  - apply tequiv_wf with k_sig in HTE; specialize (IHHST k_type).
    split; intros HK; inversion_clear HK; (constructor; [apply HTE | apply IHHST]); assumption.
  - split; intros HK'; [inversion_clear HK'; assumption |].
    assert (EQ := kinding_unique HK HK'); subst; apply _.
Qed.
