Require Import Utf8.
Require Import Binding.Lib Binding.Product Binding.Auto.
Require Import Lang.Surface.Types.

Fixpoint tmap_id {A : Set × Set} (f : A [→] A) (τ : typ A) :
  arrow_eq f arrow_id → tmap f τ = τ.
Proof. auto_map_id. Qed.

Fixpoint tmap_map_comp {A B C : Set × Set}
         (f : B [→] C) (g : A [→] B) h (τ : typ A) :
  arrow_eq (arrow_comp f g) h → tmap f (tmap g τ) = tmap h τ.
Proof. auto_map_map_comp. Qed.

Instance FMap_tmap : FMap (@tmap).
Proof.
  split; [exact @tmap_id | exact @tmap_map_comp].
Qed.

Instance PreSubst_typ_sub : PreSubst typ_sub.
Proof.
  split; simpl; firstorder; congruence.
Qed.

Instance LAS_typV : LiftAShift arrow incV.
Proof.
  split; intros x; simpl; reflexivity.
Qed.

Instance LAS_typL : LiftAShift arrow incL.
Proof.
  split; intros x; simpl; reflexivity.
Qed.

Instance ASL_typV : ASLiftable arrow typ_sub incV.
Proof.
  split; intros; [destruct x as [| x]; [reflexivity |] | apply H]; simpl.
  change tmap with (fmap (A := incV B') (B := incV C)).
  rewrite fmap_liftA_shift_comm; f_equal; apply H.
Qed.
  
Instance ASL_typL : ASLiftable arrow typ_sub incL.
Proof.
  split; intros; [| destruct x as [| x]; simpl; f_equal; apply H]; simpl.
  change tmap with (fmap (A := incL B') (B := incL C)).
  rewrite fmap_liftA_shift_comm; f_equal; apply H.
Qed.

Fixpoint bind_map_typ (A B B' C : Set × Set) (f : B {t⇒} C) (g : A [→] B) (g' : B' [→] C) 
    (f' : A {t⇒} B') (t : typ A) :
    subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f')
    → bind f (fmap g t) = fmap g' (bind f' t).
Proof.
  auto_bind_map_comp; term_simpl; f_equal; auto; [| | |].
  - apply bind_map_typ, liftS_liftA_comp, Heq.
  - apply bind_map_typ, liftS_liftA_comp, Heq.
  - apply Heq.
  - apply bind_map_typ, liftS_liftA_comp, Heq.
Qed.

Instance BM_typ : BindMap (@tmap) (@tbind) := {| bind_map_comp := bind_map_typ |}.
  
Instance SL_type_incV : SLiftable arrow typ_sub incV.
Proof.
  split; intros.
  - split; [intros [| x]; [reflexivity |] | intros x; apply H]; simpl.
    rewrite (proj1 H x); reflexivity.
  - split; [intros [| x]; [reflexivity |] | intros x; apply H]; simpl.
    rewrite <- (proj1 H x); simpl; apply bind_map_comp.
    split; intros y; simpl; reflexivity.
  - split; [intros [| x]; simpl; f_equal | intros x]; apply EQf. 
Qed.

Instance SL_type_incL : SLiftable arrow typ_sub incL.
Proof.
  split; intros.
  - split; [intros x; simpl; now rewrite (proj1 H x) | intros [| x]; simpl; f_equal; apply H].
  - split; [intros x; simpl | intros [| x]; simpl; f_equal; apply H].
    rewrite <- (proj1 H x); simpl; apply bind_map_comp.
    split; intros y; simpl; reflexivity.
  - split; [intros x | intros [| x]]; simpl; f_equal; apply EQf.
Qed.
    
Instance LSS_type_incV : LiftSShift arrow typ_sub incV.
Proof.
  split; intros x; simpl; reflexivity.
Qed.

Instance LSS_type_incL : LiftSShift arrow typ_sub incL.
Proof.
  split; intros x; simpl; reflexivity.
Qed.

Fixpoint tbind_pure {A : Set × Set} (f : A {t⇒} A) τ :
  subst_eq f subst_pure → tbind f τ = τ.
Proof.
  auto_bind_pure; apply Heq.
Qed.

Fixpoint tbind_bind_comp {A B C : Set × Set}
         (f : B {t⇒} C) (g : A {t⇒} B) h (τ : typ A) :
  subst_eq (subst_comp f g) h → tbind f (tbind g τ) = tbind h τ.
Proof. auto_bind_bind_comp. Qed.

Instance Bind_typ : Bind (@tbind).
Proof.
  split; [exact @tbind_pure | exact @tbind_bind_comp].
Qed.

Instance Subst_typ_sub : Subst arrow typ_sub.
Proof.
  split; intros; split; intros x; try reflexivity.
  - simpl; apply map_id; reflexivity.
  - simpl; etransitivity; [| eapply bind_map_comp with (g0 := arrow_id)]; [rewrite map_id'; reflexivity |].
    split; intros y; simpl; reflexivity.
  - simpl; rewrite (proj1 EQg); apply map_equiv, EQf.
  - simpl; rewrite (proj2 EQg); apply EQf.
  - simpl; symmetry; eapply map_map_comp; split; intros y; simpl; reflexivity.
  - simpl; apply bind_pure; reflexivity.
  - simpl; symmetry; apply bind_bind_comp; reflexivity.
  - simpl. rewrite <- (bind_pure' (tsub_var g₁ x)), (proj1 EQg); apply bind_bind_comp.
    split; intros y; simpl; apply EQf.
  - simpl; rewrite (proj2 EQg); apply EQf.
Qed.

Instance SFM_typV : SubstFMap arrow typ_sub typ incV.
Proof.
  split; intros x; [destruct x as [| x] |]; simpl; reflexivity.
Qed.

Instance SS_typV : SubstShift arrow typ_sub typ incV.
Proof.
  split; intros x; simpl; reflexivity.
Qed.

Instance SB_typV : SubstBind arrow typ_sub typ incV.
Proof.
  split; [intros [| x] | intros x]; simpl; try reflexivity; [].
  symmetry; apply subst_shift_id.
Qed.
