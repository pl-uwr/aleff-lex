Require Import Utf8.
Require Import Binding.Lib Binding.Product Binding.Env.
Require Import Lang.Surface.Types.

Reserved Notation "'K[' Δ ';' LV '⊢₀' τ '∷' κ ]".

Inductive kinding {TV LV : Set} (Δ : TV → kind) : typ_ TV LV → kind → Prop :=
| K_Unit :
    K[ Δ ; LV ⊢₀ t_unit ∷ k_type ]
| K_Var : ∀ α,
    K[ Δ ; LV ⊢₀ t_var α ∷ Δ α ]
| K_Arrow : ∀ τ₁ ε τ₂,
    K[ Δ ; LV ⊢₀ τ₁ ∷ k_type   ] →
    K[ Δ ; LV ⊢₀ ε  ∷ k_effect ] →
    K[ Δ ; LV ⊢₀ τ₂ ∷ k_type   ] →
    K[ Δ ; LV ⊢₀ t_arrow τ₁ ε τ₂ ∷ k_type ]
| K_ForallT : ∀ κ τ,
    K[ Δ ▹ κ ; LV ⊢₀ τ ∷ k_type ] →
    K[ Δ ; LV ⊢₀ t_forallT κ (τ : typ (incV {| sfst := _ |})) ∷ k_type ]
| K_ForallL : ∀ σ τ,
    K[ Δ ; LV     ⊢₀ σ ∷ k_sig  ] →
    K[ Δ ; inc LV ⊢₀ τ ∷ k_type ] →
    K[ Δ ; LV ⊢₀ t_forallL σ τ ∷ k_type ]
| K_Label : ∀ l,
    K[ Δ ; LV ⊢₀ t_label l ∷ k_effect ]
| K_Pure  :
    K[ Δ ; LV ⊢₀ t_pure ∷ k_effect ]
| K_Cons  : ∀ ε₁ ε₂,
    K[ Δ ; LV ⊢₀ ε₁ ∷ k_effect ] →
    K[ Δ ; LV ⊢₀ ε₂ ∷ k_effect ] →
    K[ Δ ; LV ⊢₀ t_cons ε₁ ε₂ ∷ k_effect ]
| K_OpSig : ∀ τ₁ τ₂,
    K[ Δ ; LV ⊢₀ τ₁ ∷ k_type ] →
    K[ Δ ; LV ⊢₀ τ₂ ∷ k_type ] →
    K[ Δ ; LV ⊢₀ t_opsig τ₁ τ₂ ∷ k_sig ]
| K_AllTS : ∀ κ σ,
    K[ Δ ▹ κ ; LV ⊢₀ σ ∷ k_sig ] →
    K[ Δ ; LV ⊢₀ t_allTS κ (σ : typ (incV {| sfst := _ |})) ∷ k_sig ]
where "'K[' Δ ; LV ⊢₀ τ ∷ κ ]" := (@kinding _ LV Δ τ κ).

Class kinding_class {TV LV : Set} Δ (τ : typ_ TV LV) κ : Prop :=
  well_kinded : K[ Δ ; LV ⊢₀ τ ∷ κ ].

Notation "'K[' Δ ';' LV '⊢' τ '∷' κ ]" := (@kinding_class _ LV Δ τ κ) (Δ, LV, τ, κ at level 40).

Class kinding_env {TV LV V : Set} Δ (Γ : V → typ_ TV LV) : Prop :=
  well_kinded_env : ∀ x, K[ Δ ; LV ⊢ (Γ x) ∷ k_type ].

Class kinding_lenv {TV LV₁ LV₂ : Set} Δ (Θ : LV₁ → typ_ TV LV₂) : Prop :=
  well_kinded_lenv : ∀ a,  K[ Δ ; LV₂ ⊢ (Θ a) ∷ k_sig ].

Class wf_subst {TA TB LA LB : Set} (Δ₁ : TA → kind) (Δ₂ : TB → kind)
    (η : {| sfst := TA; ssnd := LA |} {t⇒} {| sfst := TB; ssnd := LB |}) : Prop :=
  well_kinded_subst : ∀ x, K[ Δ₂ ; LB ⊢ tsub_var η x ∷ Δ₁ x].

Notation "'K[' Δ ';' LV '⊢ᴱ' Γ ]" := (@kinding_env _ LV _ Δ Γ) (Δ, LV, Γ at level 40).
Notation "'K[' Δ ';' LV '⊢ᵀ' Θ ]" := (@kinding_lenv _ _ LV Δ Θ) (Δ, LV, Θ at level 40).

Instance kinding_unit {TV LV : Set} (Δ : TV → kind) :
  K[ Δ ; LV ⊢ t_unit ∷ k_type ] := K_Unit Δ.

Instance kinding_var {TV LV : Set} (Δ : TV → kind) α :
  K[ Δ ; LV ⊢ t_var α ∷ Δ α ] := K_Var Δ α.

Instance kinding_arrow {TV LV : Set} (Δ : TV → kind) τ₁ ε τ₂
    {WFτ₁ : K[ Δ ; LV ⊢ τ₁ ∷ k_type   ]}
    {WFε  : K[ Δ ; LV ⊢ ε  ∷ k_effect ]}
    {WFτ₂ : K[ Δ ; LV ⊢ τ₂ ∷ k_type   ]} :
  K[ Δ ; LV ⊢ t_arrow τ₁ ε τ₂ ∷ k_type ] := K_Arrow Δ _ _ _ WFτ₁ WFε WFτ₂.

Instance kinding_label {TV LV : Set} (Δ : TV → kind) l :
  K[ Δ ; LV ⊢ t_label l ∷ k_effect ] := K_Label Δ l.

Instance kinding_pure {TV LV : Set} (Δ : TV → kind) :
  K[ Δ ; LV ⊢ t_pure ∷ k_effect ] := K_Pure Δ.

Instance kinding_cons {TV LV : Set} (Δ : TV → kind) ε₁ ε₂
    {WFε₁ : K[ Δ ; LV ⊢ ε₁ ∷ k_effect ]}
    {WFε₂ : K[ Δ ; LV ⊢ ε₂ ∷ k_effect ]} :
  K[ Δ ; LV ⊢ t_cons ε₁ ε₂ ∷ k_effect ] := K_Cons Δ _ _ WFε₁ WFε₂.

Instance kinding_opsig {TV LV : Set} (Δ : TV → kind) τ₁ τ₂
    {WFτ₁ : K[ Δ ; LV ⊢ τ₁ ∷ k_type   ]}
    {WFτ₂ : K[ Δ ; LV ⊢ τ₂ ∷ k_type   ]} :
  K[ Δ ; LV ⊢ t_opsig τ₁ τ₂ ∷ k_sig ] := K_OpSig Δ _ _ WFτ₁ WFτ₂.

Instance kinding_tall {TV LV : Set} (Δ : TV → kind) κ (τ : typ (incV {| sfst := _ |}))
         {WFτ : K[ Δ ▹ κ ; LV ⊢ τ ∷ k_type ]} :
  K[ Δ ; LV ⊢ t_forallT κ τ ∷ k_type ] := K_ForallT _ _ _ WFτ.

Instance kinding_lall {TV LV : Set} (Δ : TV → kind) σ (τ : typ (incL {| sfst := _ |}))
         {WFσ : K[ Δ ; LV ⊢ σ ∷ k_sig ]}
         {WFτ : K[ Δ ; inc LV ⊢ τ ∷ k_type ]} :
  K[ Δ ; LV ⊢ t_forallL σ τ ∷ k_type ] := K_ForallL _ _ _ WFσ WFτ.

Instance kinding_tallS {TV LV : Set} (Δ : TV → kind) κ (τ : typ (incV {| sfst := _ |}))
         {WFτ : K[ Δ ▹ κ ; LV ⊢ τ ∷ k_sig ]} :
  K[ Δ ; LV ⊢ t_allTS κ τ ∷ k_sig ] := K_AllTS _ _ _ WFτ.

Require Import Binding.Product Binding.Set.

Lemma kinding_fmap_eq {TV₁ LV₁ TV₂ LV₂ : Set} (Δ₁ : TV₁ → kind) (Δ₂ : TV₂ → kind)
      (f : {| sfst := TV₁; ssnd := LV₁ |} [→] {| sfst := TV₂; ssnd := LV₂ |}) τ κ
      (EQΔ : Δ₁ ≃ Δ₂ ∘ arr₁ f)
      (HK : K[ Δ₁ ; LV₁ ⊢ τ ∷ κ]) :
  K[ Δ₂ ; LV₂ ⊢ fmap f τ ∷ κ].
Proof.
  revert TV₂ LV₂ Δ₂ f EQΔ; induction HK; intros; term_simpl; eauto with typeclass_instances; [].
  rewrite EQΔ; apply K_Var with (α0 := arr₁ f α).
Qed.

Instance kinding_fmap {TV₁ LV₁ TV₂ LV₂ : Set} (Δ : TV₂ → kind)
      (f : {| sfst := TV₁; ssnd := LV₁ |} [→] {| sfst := TV₂; ssnd := LV₂ |}) τ κ
      (HK : K[ Δ ∘ arr₁ f ; LV₁ ⊢ τ ∷ κ]) :
  K[ Δ ; LV₂ ⊢ fmap f τ ∷ κ].
Proof.
  now eauto using kinding_fmap_eq.
Qed.

Instance kinding_shiftL {TV LV : Set} (Δ : TV → kind) τ κ
    {WF : K[ Δ ; LV ⊢ τ ∷ κ]} :
  K[ Δ ; inc LV ⊢ shift τ ∷ κ ].
Proof.
  eapply kinding_fmap_eq, WF; solve_equiv.
Qed.

Instance kinding_shiftT {TV LV : Set} (Δ : TV → kind) τ κ κ'
         {WF : K[ Δ ; LV ⊢ τ ∷ κ]} :
  K[ Δ ▹ κ' ; LV ⊢ shift τ ∷ κ].
Proof.
  eapply kinding_fmap_eq, WF; solve_equiv.
Qed.

Instance kinds_subst {TV₁ TV₂ LV₁ LV₂ : Set} (Δ₁ : TV₁ → kind) (Δ₂ : TV₂ → kind)
      (η : {| sfst := TV₁; ssnd := LV₁ |} {t⇒} {| sfst := TV₂; ssnd := LV₂ |})
      (HKS : wf_subst Δ₁ Δ₂ η) α :
  K[ Δ₂ ; LV₂ ⊢ tsub_var η α ∷ Δ₁ α].
Proof.
  apply HKS.
Defined.

Instance wf_pure {TV LV : Set} {Δ : TV → kind} :
  wf_subst (LB := LV) Δ Δ subst_pure.
Proof.
  intros α; apply _.
Defined.

Instance wf_mk_subst {TV LV : Set} {Δ : TV → kind} κ τ
         {WF : K[ Δ ; LV ⊢ τ ∷ κ]} :
  wf_subst (Δ ▹ κ) Δ (mk_subst τ).
Proof.
  intros [| α]; simpl; apply _.
Qed.

Instance kinds_subst_extend {TV₁ TV₂ LV₁ LV₂ : Set} (Δ₁ : TV₁ → kind) (Δ₂ : TV₂ → kind)
      (η : {| sfst := TV₁; ssnd := LV₁ |} {t⇒} {| sfst := TV₂; ssnd := LV₂ |}) κ
      (HKS : wf_subst Δ₁ Δ₂ η) :
  wf_subst (Δ₁ ▹ κ) (Δ₂ ▹ κ) (liftS η).
Proof.
  intros [| x]; term_simpl; [| specialize (HKS x)]; apply _.
Defined.

Instance kinds_subst_inc {TV₁ TV₂ LV₁ LV₂ : Set} (Δ₁ : TV₁ → kind) (Δ₂ : TV₂ → kind)
      (η : {| sfst := TV₁; ssnd := LV₁ |} {t⇒} {| sfst := TV₂; ssnd := LV₂ |})
      (HKS : wf_subst Δ₁ Δ₂ η) :
  wf_subst Δ₁ Δ₂ (liftS η).
Proof.
  intros x; term_simpl; specialize (HKS x); apply _.
Defined.

Instance kinds_bind  {TV₁ TV₂ LV₁ LV₂ : Set} (Δ₁ : TV₁ → kind) (Δ₂ : TV₂ → kind)
      (η : {| sfst := TV₁; ssnd := LV₁ |} {t⇒} {| sfst := TV₂; ssnd := LV₂ |}) τ κ
      (HKS : wf_subst Δ₁ Δ₂ η)
      (HK : K[ Δ₁ ; LV₁ ⊢ τ ∷ κ]) :
  K[ Δ₂ ; LV₂ ⊢ bind η τ ∷ κ].
Proof.
  revert TV₂ LV₂ Δ₂ η HKS; induction HK; intros; term_simpl;
    now eauto using kinds_subst_extend, kinds_subst_inc with typeclass_instances.
Qed.

Instance kinds_substT {TV LV : Set} (Δ : TV → kind)
      σ τ κ₁ κ₂
      (HKσ : K[ Δ ; LV ⊢ σ ∷ κ₁])
      (HKτ : K[ Δ ▹ κ₁ ; LV ⊢ τ ∷ κ₂]) :
  K[ Δ ; LV ⊢ subst τ σ ∷ κ₂].
Proof.
  eapply kinds_bind, HKτ.
  intros [| x]; term_simpl; apply _.
Qed.

Instance kinds_substL {TV LV : Set} (Δ : TV → kind)
         τ κ (a : s_lbl _)
         (HK : K[ Δ ; inc LV ⊢ τ ∷ κ]) :
  K[ Δ ; LV ⊢ subst τ a ∷ κ].
Proof.
  eapply kinds_bind, HK.
  intros x; term_simpl; apply _.
Qed.

Instance kinds_env {TV LV V : Set} {Δ : TV → kind} (Γ : V → typ_ TV LV)
         (HK : K[ Δ; LV ⊢ᴱ Γ]) x :
  K[ Δ; LV ⊢ Γ x ∷ k_type].
Proof.
  apply HK.
Qed.

Instance kinds_extend {TV LV V : Set} (Δ : TV → kind) (Γ : V → typ_ TV LV) τ
         (HKτ : K[ Δ ; LV ⊢ τ ∷ k_type])
         (HKΓ : K[ Δ ; LV ⊢ᴱ Γ]) :
  K[ Δ ; LV ⊢ᴱ Γ ▹ τ].
Proof.
  intros [| x]; term_simpl; apply _.
Qed.

Instance kind_env_shiftV {TV LV V : Set}
  (Δ : TV → kind) (Γ : V → typ_ TV LV) κ
  (HK : K[ Δ; LV ⊢ᴱ Γ]) :
  K[ Δ ▹ κ; LV ⊢ᴱ shift Γ].
Proof.
  intros x; term_simpl; apply kinding_shiftT, _.
Qed.

Instance kind_env_shiftL {TV LV V : Set}
  (Δ : TV → kind) (Γ : V → typ_ TV LV)
  (HK : K[ Δ; LV ⊢ᴱ Γ]) :
  K[ Δ; inc LV ⊢ᴱ shift Γ].
Proof.
  intros x; term_simpl; apply kinding_shiftL, _.
Qed.

Instance kinds_lenv {TV LV₁ LV₂ : Set} {Δ : TV → kind} (Θ : LV₁ → typ_ TV LV₂)
         (HK : K[Δ ; LV₂ ⊢ᵀ Θ]) a :
  K[ Δ; LV₂ ⊢ Θ a ∷ k_sig].
Proof.
  apply HK.
Qed.

Instance kinds_lenv_ext {TV LV₁ LV₂ : Set} {Δ : TV → kind} (Θ : LV₁ → typ_ TV LV₂) σ
         (HKΘ : K[Δ; LV₂ ⊢ᵀ Θ])
         (HKσ : K[Δ; LV₂ ⊢ σ ∷ k_sig]) :
  K[Δ; LV₂ ⊢ᵀ Θ ▹ σ].
Proof.
  intros [| a]; simpl; apply _.
Qed.

Instance kinds_lenv_shiftV {TV LV₁ LV₂ : Set} {Δ : TV → kind} (Θ : LV₁ → typ_ TV LV₂) κ
         (HKΘ : K[Δ; LV₂ ⊢ᵀ Θ]) :
  K[Δ ▹ κ; LV₂ ⊢ᵀ shift Θ].
Proof.
  intros a; term_simpl; apply kinding_shiftT, _.
Qed.

Instance kinds_lenv_shiftL {TV LV₁ LV₂ : Set} {Δ : TV → kind} (Θ : LV₁ → typ_ TV LV₂)
         (HKΘ : K[Δ; LV₂ ⊢ᵀ Θ]) :
  K[Δ; inc LV₂ ⊢ᵀ shift Θ].
Proof.
  intros a; term_simpl; apply kinding_shiftL, _.
Qed.

Section Inversion.

  Lemma K_Var_inv {TV LV : Set} {Δ : TV → kind} {α : TV} {κ : kind}
        (WF : K[ Δ ; LV ⊢ t_var (α : s_var {| sfst := TV |}) ∷ κ ]) :
    Δ α = κ.
  Proof.
    inversion_clear WF; reflexivity.
  Defined.

  Lemma K_Arrow_inv1 {TV LV : Set} {Δ : TV → kind} {τ₁ ε τ₂ : typ_ TV LV}
        (WF : K[ Δ ; LV ⊢ t_arrow τ₁ ε τ₂ ∷ k_type ]) :
    K[ Δ ; LV ⊢ τ₁ ∷ k_type ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

  Lemma K_Arrow_inv2 {TV LV : Set} {Δ : TV → kind} {τ₁ ε τ₂ : typ_ TV LV}
        (WF : K[ Δ ; LV ⊢ t_arrow τ₁ ε τ₂ ∷ k_type ]) :
    K[ Δ ; LV ⊢ ε ∷ k_effect ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

  Lemma K_Arrow_inv3 {TV LV : Set} {Δ : TV → kind} {τ₁ ε τ₂ : typ_ TV LV}
        (WF : K[ Δ ; LV ⊢ t_arrow τ₁ ε τ₂ ∷ k_type ]) :
    K[ Δ ; LV ⊢ τ₂ ∷ k_type ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

  Lemma K_ForallT_inv {TV LV : Set} {Δ : TV → kind} {κ : kind} {τ : typ (incV _)}
        (WF : K[ Δ ; LV ⊢ t_forallT κ τ ∷ k_type ]) :
    K[ Δ ▹ κ ; LV ⊢ τ ∷ k_type ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

  Lemma K_ForallL_inv1 {TV LV : Set} {Δ : TV → kind} {σ : typ _} {τ : typ (incL _)}
        (WF : K[ Δ ; LV ⊢ t_forallL σ τ ∷ k_type ]) :
    K[ Δ ; LV ⊢ σ ∷ k_sig ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

  Lemma K_ForallL_inv2 {TV LV : Set} {Δ : TV → kind} {σ : typ _} {τ : typ (incL _)}
        (WF : K[ Δ ; LV ⊢ t_forallL σ τ ∷ k_type ]) :
    K[ Δ ; inc LV ⊢ τ ∷ k_type ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

  Lemma K_Cons_inv1 {TV LV : Set} {Δ : TV → kind} {ε₁ ε₂ : typ_ TV LV}
        (WF : K[ Δ ; LV ⊢ t_cons ε₁ ε₂ ∷ k_effect ]) :
    K[ Δ ; LV ⊢ ε₁ ∷ k_effect ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

  Lemma K_Cons_inv2 {TV LV : Set} {Δ : TV → kind} {ε₁ ε₂ : typ_ TV LV}
        (WF : K[ Δ ; LV ⊢ t_cons ε₁ ε₂ ∷ k_effect ]) :
    K[ Δ ; LV ⊢ ε₂ ∷ k_effect ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

  Lemma K_OpSig_inv1 {TV LV : Set} {Δ : TV → kind} {τ₁ τ₂ : typ_ TV LV}
        (WF : K[ Δ ; LV ⊢ t_opsig τ₁ τ₂ ∷ k_sig ]) :
    K[ Δ ; LV ⊢ τ₁ ∷ k_type ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

  Lemma K_OpSig_inv2 {TV LV : Set} {Δ : TV → kind} {τ₁ τ₂ : typ_ TV LV}
        (WF : K[ Δ ; LV ⊢ t_opsig τ₁ τ₂ ∷ k_sig ]) :
    K[ Δ ; LV ⊢ τ₂ ∷ k_type ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

  Lemma K_AllTS_inv {TV LV : Set} {Δ : TV → kind} {κ : kind} {τ : typ (incV _)}
        (WF : K[ Δ ; LV ⊢ t_allTS κ τ ∷ k_sig ]) :
    K[ Δ ▹ κ ; LV ⊢ τ ∷ k_sig ].
  Proof.
    inversion_clear WF; assumption.
  Defined.

End Inversion.

Ltac invert_kind_lem H N :=
  match type of H with
  | K[ _ ; _ ⊢ t_unit    ∷ ?κ ] =>
    let J := fresh in
    assert (J : κ = k_type) by (inversion H; reflexivity);
    first [subst κ | clear J]
  | K[ _ ; _ ⊢ t_label _ ∷ ?κ ] =>
    let J := fresh in
    assert (J : κ = k_effect) by (inversion H; reflexivity);
    first [subst κ | clear J]
  | K[ _ ; _ ⊢ t_pure ∷ ?κ ] =>
    let J := fresh in
    assert (J : κ = k_effect) by (inversion H; reflexivity);
    first [subst κ | clear J]
  | K[ _ ; _ ⊢ t_var _ ∷ ?κ ] =>
    let J := fresh N in assert (J := K_Var_inv H); try subst κ
  | K[ _ ; _ ⊢ t_arrow _ _ _ ∷ ?κ ] =>
    let J := fresh in
    assert (J : κ = k_type) by (inversion H; reflexivity);
    first [subst κ | clear J];
    let J1 := fresh N in let J2 := fresh N in let J3 := fresh N in
    assert (J1 := K_Arrow_inv1 H); assert (J2 := K_Arrow_inv2 H); assert (J3 := K_Arrow_inv3 H);
    invert_kind_lem J1 N; invert_kind_lem J2 N; invert_kind_lem J3 N
  | K[ _ ; _ ⊢ t_forallT _ _ ∷ ?κ ] =>
    let J := fresh in
    assert (J : κ = k_type) by (inversion H; reflexivity);
    first [subst κ | clear J];
    let J := fresh N in assert (J := K_ForallT_inv H); invert_kind_lem J N
  | K[ _ ; _ ⊢ t_forallL _ _ ∷ ?κ ] =>
    let J := fresh in
    assert (J : κ = k_type) by (inversion H; reflexivity);
    first [subst κ | clear J];
    let J1 := fresh N in let J2 := fresh N in
    assert (J1 := K_ForallL_inv1 H); assert (J2 := K_ForallL_inv2 H);
    invert_kind_lem J1 N; invert_kind_lem J2 N
  | K[ _ ; _ ⊢ t_cons  _ _ ∷ ?κ ] =>
    let J := fresh in
    assert (J : κ = k_effect) by (inversion H; reflexivity);
    first [subst κ | clear J];
    let J1 := fresh N in let J2 := fresh N in
    assert (J1 := K_Cons_inv1 H); assert (J2 := K_Cons_inv2 H);
    invert_kind_lem J1 N; invert_kind_lem J2 N
  | K[ _ ; _ ⊢ t_opsig _ _ ∷ ?κ ] =>
    let J := fresh in
    assert (J : κ = k_sig) by (inversion H; reflexivity);
    first [subst κ | clear J];
    let J1 := fresh N in let J2 := fresh N in
     assert (J1 := K_OpSig_inv1 H); assert (J2 := K_OpSig_inv2 H);
    invert_kind_lem J1 N; invert_kind_lem J2 N
  | K[ _ ; _ ⊢ t_allTS _ _ ∷ ?κ ] =>
    let J := fresh in
    assert (J : κ = k_sig) by (inversion H; reflexivity);
    first [subst κ | clear J];
    let J := fresh N in assert (J := K_AllTS_inv H); invert_kind_lem J N
  | K[ ?Δ ; ?LV ⊢ ?τ ∷ ?κ ] => idtac
  end.

Tactic Notation "invert_kind" hyp(H) := invert_kind_lem H H.
Tactic Notation "invert_kind" hyp(H) "as" ident(N) := invert_kind_lem H N.
