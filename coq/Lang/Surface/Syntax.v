Require Import Utf8.
Require Import Binding.Lib Binding.Set Binding.Product.

Definition evar (A : Set × Set) := sfst A.
Definition lvar (A : Set × Set) := ssnd A.

Inductive expr {A : Set × Set} : Set :=
| e_value  : value → expr
| e_let    : expr  → @expr (on₁ inc A) → expr
| e_app    : value → value → expr
| e_tapp   : value → expr
| e_lapp   : value → lvar A → expr
| e_do     : lvar A → value → expr
| e_handle : @expr (on₂ inc A) → handler → @expr (on₁ inc A) → expr
with value {A : Set × Set} : Set :=
| v_var  : evar A → value
| v_lam  : @expr (on₁ inc A) → value
| v_tlam : expr → value
| v_llam : @expr (on₂ inc A) → value
| v_unit : value
with handler {A : Set × Set} : Set :=
| h_do : @expr (on₁ inc (on₁ inc A)) → handler
.
Arguments expr A : clear implicits.
Arguments value A : clear implicits.
Arguments handler A : clear implicits.

Coercion e_value : value >-> expr.
Coercion v_var   : evar  >-> value.

Notation expr_      V LV := (expr      {| sfst := V; ssnd := LV |}).
Notation value_     V LV := (value     {| sfst := V; ssnd := LV |}).
Notation handler_   V LV := (handler   {| sfst := V; ssnd := LV |}).

Notation expr0      := (expr_      ∅ ∅).
Notation value0     := (value_     ∅ ∅).
Notation handler0   := (handler_   ∅ ∅).