Require Import Utf8.
Require Import Binding.Lib Binding.Set Binding.Product.

Inductive kind : Set := k_type | k_effect | k_sig.

Definition s_var (A : Set × Set) := sfst A.
Definition s_lbl (A : Set × Set) := ssnd A.

Notation incV := (on₁ inc).
Notation incL := (on₂ inc).

Inductive typ (A : Set × Set) : Set :=
| t_unit    : typ A
| t_var     : s_var A → typ A
| t_arrow   : typ A → typ A → typ A → typ A
| t_forallT : kind → typ (on₁ inc A) → typ A
| t_forallL : typ A → typ (on₂ inc A) → typ A
| t_label   : s_lbl A → typ A
| t_pure    : typ A
| t_cons    : typ A → typ A → typ A
| t_opsig   : typ A → typ A → typ A
| t_allTS   : kind → typ (on₁ inc A) → typ A
.

Arguments t_unit    {A}.
Arguments t_var     {A}.
Arguments t_arrow   {A}.
Arguments t_forallT {A}.
Arguments t_forallL {A}.
Arguments t_label   {A}.
Arguments t_pure    {A}.
Arguments t_cons    {A}.
Arguments t_opsig   {A}.
Arguments t_allTS   {A}.

Notation t_label_ l := (t_label (l : s_lbl {| sfst := _ |})).

Notation " τ₁ '→[' ε ']' τ₂ " := (t_arrow τ₁ ε τ₂) (at level 30, right associativity) : typ_scope.
Notation " '∀ᵗ' κ , τ " := (t_forallT κ τ) (at level 30, no associativity) : typ_scope.
Notation " '∀ˡ' σ , τ " := (t_forallL σ τ) (at level 30, no associativity) : typ_scope.
Notation " 'ι' " := t_pure : typ_scope.
Notation " ε₁ · ε₂ " := (t_cons ε₁ ε₂) (at level 20, left associativity) : typ_scope.
Notation " τ₁ '⇒ᵈ' τ₂ " := (t_opsig τ₁ τ₂) (at level 20, no associativity) : typ_scope.
Notation " '()ᵗ' "   := t_unit : typ_scope.
Notation " '∀ˢ' κ , σ " := (t_allTS κ σ) (at level 30, no associativity) : typ_scope.

Delimit Scope typ_scope with typ.

Coercion t_var   : s_var >-> typ.
Coercion t_label : s_lbl >-> typ.

Notation typ_ TV LV := (typ {| sfst := TV; ssnd := LV |}).

Fixpoint tmap {A B : Set × Set} (φ : A [→] B) (τ : typ A) : typ B :=
  match τ with
  | ()ᵗ        => ()ᵗ
  | t_var   α  => arr₁ φ α : s_var _
  | τ₁ →[ε] τ₂ => tmap φ τ₁ →[tmap φ ε] tmap φ τ₂
  | ∀ᵗ κ, τ    => ∀ᵗ κ, tmap (liftA φ) τ
  | ∀ˡ σ, τ    => ∀ˡ tmap φ σ, tmap (liftA φ) τ
  | t_label l  => arr₂ φ l : s_lbl _
  | ι          => ι
  | ε₁ · ε₂    => tmap φ ε₁ · tmap φ ε₂
  | τ₁ ⇒ᵈ τ₂   => tmap φ τ₁ ⇒ᵈ tmap φ τ₂
  | ∀ˢ κ, τ    => ∀ˢ κ, tmap (liftA φ) τ
  end%typ.

Instance IsMapCore_tmap : IsMapCore (@tmap).

Record typ_sub (A B : Set × Set) : Set :=
  { tsub_var : s_var A → typ B
  ; tsub_lbl : s_lbl A → s_lbl B
  }.

Arguments tsub_var {A} {B}.
Arguments tsub_lbl {A} {B}.

Notation "A '{t⇒}' B" := (typ_sub A B) (at level 99) : type_scope.

Instance PreSubstCore_typ_sub : PreSubstCore typ_sub :=
  {| subst_pure := λ A, {| tsub_var := t_var; tsub_lbl := λ a, a |}
  ;  subst_eq   := λ A B f g,
      (∀ x, tsub_var f x = tsub_var g x) ∧ (∀ x, tsub_lbl f x = tsub_lbl g x)
  |}.

Instance SLiftableCore_typ_sub_incV : SLiftableCore typ_sub incV :=
  { liftS := λ A B (f : A {t⇒} B),
      {| tsub_var := λ (x : s_var (incV A)),
           match x with
           | VZ   => t_var (VZ : s_var (incV B))
           | VS y => shift (tsub_var f y)
           end
      ;  tsub_lbl := tsub_lbl f
      |}
  }.

Instance SLiftableCore_typ_sub_incL : SLiftableCore typ_sub incL :=
  { liftS := λ A B (f : A {t⇒} B),
      {| tsub_var := λ (x : s_var (incL A)), shift (tsub_var f x)
      ;  tsub_lbl := λ (x : s_lbl (incL A)),
           match x with
           | VZ   => (VZ : s_lbl (incL B))
           | VS y => VS (tsub_lbl f y)
           end
      |}
  }.

Fixpoint tbind {A B : Set × Set} (φ : A {t⇒} B) (τ : typ A) : typ B :=
  match τ with
  | ()ᵗ        => ()ᵗ
  | t_var   α  => tsub_var φ α
  | τ₁ →[ε] τ₂ => tbind φ τ₁ →[tbind φ ε] tbind φ τ₂
  | ∀ᵗ κ, τ    => ∀ᵗ κ, tbind (liftS φ) τ
  | ∀ˡ σ, τ    => ∀ˡ tbind φ σ, tbind (liftS φ) τ
  | t_label l  => tsub_lbl φ l
  | ι          => ι
  | ε₁ · ε₂    => tbind φ ε₁ · tbind φ ε₂
  | τ₁ ⇒ᵈ τ₂   => tbind φ τ₁ ⇒ᵈ tbind φ τ₂
  | ∀ˢ κ, τ    => ∀ˢ κ, tbind (liftS φ) τ
  end%typ.

Instance IsBindCore_tbind : IsBindCore (@tbind).

Instance SubstCore_typ_sub : SubstCore prod_arr typ_sub :=
  {| arrow_subst_comp := λ A B C f g,
      {| tsub_var := λ x, tmap f (tsub_var g x)
      ;  tsub_lbl := λ x, arr₂ f (tsub_lbl g x)
      |}
  ;  subst_comp := λ A B C f g,
      {| tsub_var := λ x, tbind f (tsub_var g x)
      ;  tsub_lbl := λ x, tsub_lbl f (tsub_lbl g x)
      |}
  |}.

Instance SC_typ_incV : SubstitutableCore typ_sub typ incV :=
  λ A τ, {| tsub_var := λ x : s_var (incV A),
                              match x with
                              | VZ => τ
                              | VS x => t_var x
                              end;
            tsub_lbl := λ x, x |}.

Instance SC_lvar_incL : SubstitutableCore typ_sub s_lbl incL :=
  λ A a, {| tsub_var := λ x : s_var (incL A), t_var (x : s_var A);
            tsub_lbl := λ b, match b with
                              | VZ => a
                              | VS b => b
                              end |}.
