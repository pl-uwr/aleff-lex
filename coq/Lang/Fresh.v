Require Import Fresh.Syntax.
Require Import Fresh.Reduction.
Require Import Fresh.SyntaxMeta.
Require Import Fresh.SurfaceTrans.
Require Import Fresh.Fresh.

Module F  := Fresh.Syntax.
Module FR := Fresh.Reduction.
Module FF := Fresh.Fresh.
Module FM := Fresh.SyntaxMeta.
Module FT := Fresh.SurfaceTrans.

Export Notations.

Delimit Scope fsyn_scope with fsyn.

Coercion F.v_var   : F.evar  >-> F.value.
Coercion F.e_value : F.value >-> F.expr.
