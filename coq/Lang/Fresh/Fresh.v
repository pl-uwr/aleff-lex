Require Import Utf8.
Require Import Binding.Product.
Require Import Fresh.Syntax.

Class IsFreshCore (X : Type) (fr : ltag → X → Prop) : Prop.

Definition fresh {X : Type} {fr} {FR : IsFreshCore X fr} l e := fr l e.
Arguments fresh {_ _ _} _ _ /.

Fixpoint lfresh {A : Set} (l₀ : ltag) (l : label A) {struct l} : Prop :=
  match l with
  | l_var a  => True
  | l_tag l' => l₀ ≠ l'
  end.

Instance IsFreshCore_label {A : Set} : IsFreshCore (label A) (@lfresh A).

Fixpoint efresh {A : Set × Set} (l : ltag) (e : expr A) : Prop :=
  match e with
  | e_value v        => vfresh l v
  | e_let e₁ e₂      => efresh l e₁ ∧ efresh l e₂
  | e_app v₁ v₂      => vfresh l v₁ ∧ vfresh l v₂
  | e_tapp v         => vfresh l v
  | e_lapp v l'      => vfresh l v ∧ fresh l l'
  | e_do l' v        => lfresh l l' ∧ vfresh l v
  | e_handle     e h r => efresh l e ∧ hfresh l h ∧ efresh l r
  | e_rhandle l' e h r => l ≠ l' ∧ efresh l e ∧ hfresh l h ∧ efresh l r
  end
with vfresh {A : Set × Set} (l : ltag) (v : value A) : Prop :=
  match v with
  | v_var  _ => True
  | v_lam  e => efresh l e
  | v_tlam e => efresh l e
  | v_llam e => efresh l e
  | v_unit   => True
  end
with hfresh {A : Set × Set} (l : ltag) (h : handler A) : Prop :=
  match h with
  | h_do e => efresh l e
  end.

Instance IsFreshCore_expr  {A : Set × Set}   : IsFreshCore (expr A)  (@efresh A).
Instance IsFreshCore_value {A : Set × Set}   : IsFreshCore (value A) (@vfresh A).
Instance IsFreshCore_handler {A : Set × Set} : IsFreshCore (handler A) (@hfresh A).

Fixpoint xfresh {A : Set × Set} (l : ltag) (F : ectx A) : Prop :=
  match F with
  | x_hole            => True
  | x_let    F e      => xfresh l F ∧ efresh l e
  | x_handle l' F h r => l ≠ l' ∧ xfresh l F ∧ hfresh l h ∧ efresh l r
  end.

Instance IsFreshCore_ectx {A : Set × Set} : IsFreshCore (ectx A) (@xfresh A).

Definition sfresh {A B : Set × Set} (l : ltag) (f : A {⇒} B) : Prop :=
  (∀ x : evar A, fresh l (sub_var f x)) ∧
  (∀ a : lvar A, fresh l (sub_lbl f a)).

Instance IsFreshCore_sub {A B : Set × Set} : IsFreshCore (A {⇒} B) (@sfresh A B).

Require Import Binding.Lib Binding.Product Fresh.SyntaxMeta.

Lemma fresh_map_l {A B : Set} (l : ltag) (f : A [→] B) (l' : label A)
      (HFl : fresh l l') : fresh l (fmap f l').
Proof.
  destruct l'; simpl; [exact I | assumption].
Qed.
  
Fixpoint fresh_map_e {A B : Set × Set} (l : ltag) (f : A [→] B) (e : expr A)
         (HFe : fresh l e) {struct e} : fresh l (fmap f e)
with fresh_map_v {A B : Set × Set} (l : ltag) (f : A [→] B) (v : value A)
         (HFe : fresh l v) {struct v} : fresh l (fmap f v)
with fresh_map_h {A B : Set × Set} (l : ltag) (f : A [→] B) (h : handler A)
                 (HFe : fresh l h) {struct h} : fresh l (fmap f h).
Proof.
  - destruct e; simpl in *; (intuition eauto); [|]; now apply fresh_map_l.
  - destruct v; simpl in *; intuition eauto.
  - destruct h; apply fresh_map_e, HFe.
Qed.

Lemma fresh_liftSV {A B : Set × Set} (l : ltag) (f : A {⇒} B) (HF : fresh l f) :
  fresh l (liftS f : incV A {⇒} incV B).
Proof.
  split; [| apply HF].
  intros [| x]; term_simpl; [exact I | apply fresh_map_v, HF].
Qed.
  
Lemma fresh_liftSL {A B : Set × Set} (l : ltag) (f : A {⇒} B) (HF : fresh l f) :
  fresh l (liftS f : incL A {⇒} incL B).
Proof.
  destruct HF; split; [intros x; now apply fresh_map_v |].
  intros [| a]; term_simpl; [exact I | now apply fresh_map_l].
Qed.

Fixpoint fresh_bind_e {A B : Set × Set} (l : ltag) (f : A {⇒} B) (e : expr A)
         (HFf : fresh l f) (HFe : fresh l e) {struct e} : fresh l (bind f e)
with fresh_bind_v {A B : Set × Set} (l : ltag) (f : A {⇒} B) (v : value A)
         (HFf : fresh l f) (HFe : fresh l v) {struct v} : fresh l (bind f v)
with fresh_bind_h {A B : Set × Set} (l : ltag) (f : A {⇒} B) (h : handler A)
         (HFf : fresh l f) (HFe : fresh l h) {struct h} : fresh l (bind f h).
Proof.
  - destruct e; simpl in *; try (now intuition eauto); [| | | |].
    + destruct HFe; split; apply fresh_bind_e; try apply fresh_liftSV; assumption.
    + destruct HFe; split; [now auto |].
      destruct l0; simpl in *; [apply HFf | assumption].
    + destruct HFe; split; [| now auto].
      destruct l0; simpl in *; [apply HFf | assumption].
    + destruct HFe as [HE1 [HH HE2]]; split; [now (apply fresh_bind_e; [apply fresh_liftSL |]) |].
      split; [| apply fresh_bind_e; [apply fresh_liftSV |]]; now auto.
    + destruct HFe as [HL [HE1 [HH HE2]]]; split; [assumption |].
      split; [| split; [| apply fresh_bind_e; [apply fresh_liftSV |]]]; now auto.
  - destruct v; simpl in *; [apply HFf | | now auto | | exact I].
    + apply fresh_bind_e; [apply fresh_liftSV |]; now trivial.
    + apply fresh_bind_e; [apply fresh_liftSL |]; now trivial.
  - destruct h; apply fresh_bind_e; [apply fresh_liftSV, fresh_liftSV |]; now trivial.
Qed.

Require Import PeanoNat.
Import Nat.

Definition getFreshl {A : Set} (l : label A) :=
  match l with
  | l_var a => 0
  | l_tag l => S l
  end.

Fixpoint getFreshe {A : Set × Set} (e : expr A) : ltag :=
  match e with
  | e_value v   => getFreshv v
  | e_let e₁ e₂ => max (getFreshe e₁) (getFreshe e₂)
  | e_app v₁ v₂ => max (getFreshv v₁) (getFreshv v₂)
  | e_tapp v    => getFreshv v
  | e_lapp v l  => max (getFreshv v) (getFreshl l)
  | e_do l v    => max (getFreshl l) (getFreshv v)
  | e_handle e h r => max (getFreshe e) (max (getFreshh h) (getFreshe r))
  | e_rhandle l e h r => max (S l) (max (getFreshe e) (max (getFreshh h) (getFreshe r)))
  end
with getFreshv {A : Set × Set} (v : value A) : ltag :=
  match v with
  | v_var  x => 0
  | v_lam  e => getFreshe e
  | v_tlam e => getFreshe e
  | v_llam e => getFreshe e
  | v_unit   => 0
  end
with getFreshh {A : Set × Set} (h : handler A) : ltag :=
  match h with
    h_do e => getFreshe e
  end.

Fixpoint getFreshx {A : Set × Set} (E : ectx A) : ltag :=
  match E with
  | x_hole => 0
  | x_let E e => max (getFreshx E) (getFreshe e)
  | x_handle l E h r => max (S l) (max (getFreshx E) (max (getFreshh h) (getFreshe r)))
  end.

Definition lbfresh {X : Type} {fr} {FR : IsFreshCore X fr} e l :=
  ∀ l', l ≤ l' → fresh l' e.

Lemma lbfresh_l {A : Set} (l : label A) : lbfresh l (getFreshl l).
Proof.
  destruct l; unfold lbfresh; simpl in *; intros l' HL; [exact I |].
  intros HEq; subst l'; contradiction (nle_succ_diag_l l).
Qed.

Fixpoint lbfresh_e {A : Set × Set} (e : expr A)    : lbfresh e (getFreshe e)
with     lbfresh_v {A : Set × Set} (v : value A)   : lbfresh v (getFreshv v)
with     lbfresh_h {A : Set × Set} (h : handler A) : lbfresh h (getFreshh h).
Proof.
  - destruct e; unfold lbfresh in *; intros l' HL; simpl in *; [now auto | | | now auto | ..].
    + split; apply lbfresh_e; rewrite <- HL; [apply le_max_l | apply le_max_r].
    + split; apply lbfresh_v; rewrite <- HL; [apply le_max_l | apply le_max_r].
    + split; [apply lbfresh_v | apply lbfresh_l]; rewrite <- HL; [apply le_max_l | apply le_max_r].
    + split; [apply lbfresh_l | apply lbfresh_v]; rewrite <- HL; [apply le_max_l | apply le_max_r].
    + split; [apply lbfresh_e; rewrite <- HL; apply le_max_l | rewrite <- le_max_r in HL].
      split; [apply lbfresh_h | apply lbfresh_e]; rewrite <- HL; [apply le_max_l | apply le_max_r].
    + change (max (S l) (max (getFreshe e1) (max (getFreshh h) (getFreshe e2))) ≤ l') in HL.
      split; [intros EQ; subst l'; rewrite <- le_max_l in HL; contradiction (nle_succ_diag_l l) |].
      rewrite <- le_max_r in HL; split; [apply lbfresh_e; rewrite <- HL; apply le_max_l |].
      rewrite <- le_max_r in HL; split; [apply lbfresh_h | apply lbfresh_e]; rewrite <- HL; [apply le_max_l | apply le_max_r].
  - destruct v; unfold lbfresh in *; intros l' HL; simpl in *; now auto.
  - destruct h; unfold lbfresh; simpl; apply lbfresh_e.
Qed.

Lemma lbfresh_x {A : Set × Set} (E : ectx A) : lbfresh E (getFreshx E).
Proof.
  unfold lbfresh; induction E; simpl; intros l' HL; [exact I | |].
  - split; [apply IHE | apply lbfresh_e]; rewrite <- HL; [apply le_max_l | apply le_max_r].
  - change (max (S l) (max (getFreshx E) (max (getFreshh h) (getFreshe e))) ≤ l') in HL.
      split; [intros EQ; subst l'; rewrite <- le_max_l in HL; contradiction (nle_succ_diag_l l) |].
      rewrite <- le_max_r in HL; split; [apply IHE; rewrite <- HL; apply le_max_l |].
      rewrite <- le_max_r in HL; split; [apply lbfresh_h | apply lbfresh_e]; rewrite <- HL; [apply le_max_l | apply le_max_r].
Qed.    

Lemma lbfresh_fresh {X : Type} {fr} {FR : IsFreshCore X fr} e l :
  lbfresh e l → fresh l e.
Proof.
  intros HL; apply HL; reflexivity.
Qed.

Require Import RelationClasses Morphisms.

Instance Proper_lbfresh {X : Type} {fr} {FR : IsFreshCore X fr} e :
  Proper (Peano.le ==> Basics.impl) (lbfresh e).
Proof.
  intros l₁ l₂ Hle HF l HL; apply HF; etransitivity; eassumption.
Qed.

Fixpoint maximum (xs : list nat) : nat :=
  match xs with
  | nil => 0
  | cons x xs => max x (maximum xs)
  end.

Lemma maximum_lub (xs : list nat) x (HIn : List.In x xs) :
  x ≤ maximum xs.
Proof.
  induction xs; inversion_clear HIn; subst; simpl.
  - apply le_max_l.
  - etransitivity; [apply IHxs, H | apply le_max_r].
Qed.

Lemma gt_maximum_nin (xs : list nat) x (HGt : x > maximum xs) :
  ¬ List.In x xs.
Proof.
  unfold gt, Peano.lt in HGt; intros HIn; apply maximum_lub in HIn.
  contradiction (nle_succ_diag_l (maximum xs)); etransitivity; eassumption.
Qed.
