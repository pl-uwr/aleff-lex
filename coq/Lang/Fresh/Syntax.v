Require Import Utf8.
Require Import Binding.Lib Binding.Set Binding.Product.
Require List.

Definition ltag := nat.

Inductive label (A : Set) : Set :=
| l_var : A → label A
| l_tag : ltag → label A
.

Arguments l_var {A}.
Arguments l_tag {A}.

Instance SetPureCore_label : SetPureCore label := @l_var.
Notation incV := (on₁ inc).
Notation incL := (on₂ inc).
Definition evar (A : Set × Set) := sfst A.
Definition lvar (A : Set × Set) := ssnd A.

Inductive expr (A : Set × Set) : Set :=
| e_value   : value A → expr A
| e_let     : expr A → expr (incV A) → expr A
| e_app     : value A → value A → expr A
| e_tapp    : value A → expr A
| e_lapp    : value A → label (lvar A) → expr A
| e_do      : label (lvar A) → value A → expr A
| e_handle  : expr (incL A) → handler A → expr (incV A) → expr A
| e_rhandle : ltag → expr A → handler A → expr (incV A) → expr A
with value (A : Set × Set) : Set :=
| v_var   : evar A → value A
| v_lam   : expr (on₁ inc A) → value A
| v_tlam  : expr A → value A
| v_llam  : expr (incL A) → value A
| v_unit  : value A
with handler (A : Set × Set) : Set :=
| h_do : expr (incV (incV A)) → handler A
.

Arguments e_value   {A}.
Arguments e_let     {A}.
Arguments e_app     {A}.
Arguments e_tapp    {A}.
Arguments e_lapp    {A}.
Arguments e_do      {A}.
Arguments e_handle  {A}.
Arguments e_rhandle {A}.
Arguments v_var     {A}.
Arguments v_lam     {A}.
Arguments v_tlam    {A}.
Arguments v_llam    {A}.
Arguments v_unit    {A}.
Arguments h_do      {A}.


Inductive ectx (A : Set × Set) : Set :=
| x_hole   : ectx A
| x_let    : ectx A → expr (incV A) → ectx A
| x_handle : ltag → ectx A → handler A → expr (incV A) → ectx A
.

Arguments x_hole   {A}.
Arguments x_let    {A}.
Arguments x_handle {A}.

Fixpoint plug {A : Set × Set} (E : ectx A) (e : expr A) : expr A :=
  match E with
  | x_hole           => e
  | x_let    E e'    => plug E (e_let e e')
  | x_handle l E h r => plug E (e_rhandle l e h r)
  end.

Inductive rctx (A : Set × Set) : Set :=
| r_hole   : rctx A
| r_let    : rctx A → expr (incV A) → rctx A
| r_handle : ltag → rctx A → handler A → expr (incV A) → rctx A
.

Arguments r_hole   {A}.
Arguments r_let    {A}.
Arguments r_handle {A}.

Notation e_do_ l v := (e_do (l : label (ssnd {| ssnd := _ |})) v).
Notation v_var_ x := (v_var (x : sfst {| sfst := _ |})).

Notation expr_      V LV := (expr      {| sfst := V; ssnd := LV |}).
Notation value_     V LV := (value     {| sfst := V; ssnd := LV |}).
Notation handler_   V LV := (handler   {| sfst := V; ssnd := LV |}).
Notation ectx_ V LV := (ectx {| sfst := V; ssnd := LV |}).
Notation rctx_ V LV := (rctx {| sfst := V; ssnd := LV |}).

Notation expr0      := (expr_      ∅ ∅).
Notation value0     := (value_     ∅ ∅).
Notation handler0   := (handler_   ∅ ∅).
Notation ectx0 := (ectx_ ∅ ∅).
Notation rctx0 := (rctx_ ∅ ∅).

Module Notations.

  Coercion e_value : value >-> expr.

  Coercion v_var   : evar >-> value.

  Notation "'λᵉ' e" := (v_lam e)  (at level 30, no associativity) : fsyn_scope.
  Notation "'λᵗ' e" := (v_tlam e) (at level 30, no associativity) : fsyn_scope.
  Notation "'λˡ' e" := (v_llam e) (at level 30, no associativity) : fsyn_scope.
  Notation "'()ᵉ'"  := (v_unit) : fsyn_scope.
  Notation "e₁ >>= e₂ " := (e_let e₁ e₂) (at level 25, right associativity) : fsyn_scope.
  Notation "v₁ '@ᵉ' v₂" := (e_app v₁ v₂) (at level 24, no associativity) : fsyn_scope.
  Notation "v  '@ᵗ'"    := (e_tapp v) (at level 24, no associativity) : fsyn_scope.
  Notation "v  '@ˡ' l"  := (e_lapp v l) (at level 24, no associativity) : fsyn_scope.

  Delimit Scope fsyn_scope with fsyn.

End Notations.
Export Notations.

Fixpoint rplug {A : Set × Set} (E : rctx A) (e : expr A) : expr A :=
  match E with
  | r_hole           => e
  | r_let    E e'    => e_let (rplug E e) e'
  | r_handle l E h r => e_rhandle l (rplug E e) h r
  end.

Fixpoint rfree {A : Set × Set} (l : ltag) (E : rctx A) : Prop :=
  match E with
  | r_hole            => True
  | r_let    E _      => rfree l E
  | r_handle l' E h r => l ≠ l' ∧ rfree l E
  end.

Definition rfree_l {A : Set × Set} (σ : list ltag) (E : rctx A) : Prop :=
  ∀ l, List.In l σ → rfree l E.

(* ========================================================================= *)
(* Renaming *)

Fixpoint lmap {A B : Set} (f : A [→] B) (l : label A) : label B :=
  match l with
  | l_var a => l_var (f a)
  | l_tag l => l_tag l
  end.

Instance IsMapCore_lmap : IsMapCore (@lmap).

Fixpoint emap {A B : Set × Set} (φ : A [→] B) (e : expr A) : expr B :=
  match e with
  | e_value v       => e_value (vmap φ v)
  | e_let e₁ e₂     => e_let (emap φ e₁) (emap (liftA φ) e₂)
  | e_app v₁ v₂     => e_app (vmap φ v₁) (vmap φ v₂)
  | e_tapp v        => e_tapp (vmap φ v)
  | e_lapp v l      => e_lapp (vmap φ v) (lmap {| apply_arr := arr₂ φ |} l)
  | e_do l v        => e_do (lmap {| apply_arr := arr₂ φ |} l) (vmap φ v)
  | e_handle e h r  => e_handle (emap (liftA φ) e) (hmap φ h) (emap (liftA φ) r)
  | e_rhandle l e h r => e_rhandle l (emap φ e) (hmap φ h) (emap (liftA φ) r)
  end
with vmap {A B : Set × Set} (φ : A [→] B) (v : value A) : value B :=
  match v with
  | v_var  x => v_var  (arr₁ φ x)
  | v_lam  e => v_lam  (emap (liftA φ) e)
  | v_tlam e => v_tlam (emap φ e)
  | v_llam e => v_llam (emap (liftA φ) e)
  | v_unit   => v_unit
  end
with hmap {A B : Set × Set} (φ : A [→] B) (h : handler A) : handler B :=
  match h with
  | h_do e => h_do (emap (liftA (liftA φ)) e)
  end.

Instance IsMapCore_emap  : IsMapCore (@emap).
Instance IsMapCore_vmap  : IsMapCore (@vmap).
Instance IsMapCore_hmap  : IsMapCore (@hmap).

Fixpoint rmap {A B : Set × Set} (φ : A [→] B) (E : rctx A) : rctx B :=
  match E with
  | r_hole           => r_hole
  | r_let E  e       => r_let (rmap φ E) (fmap (liftA φ) e)
  | r_handle l E h r => r_handle l (rmap φ E) (fmap φ h) (fmap (liftA φ) r)
  end.

Instance IsMapCore_rmap : IsMapCore (@rmap).

(* ========================================================================= *)
(* Substitution *)

Fixpoint lbind {A B : Set} (f : A [⇒] B) (l : label A) : label B :=
  match l with
  | l_var a => f a
  | l_tag l => l_tag l
  end.

Instance IsBindCore_lbind : IsBindCore (@lbind).

Record sub (A B : Set × Set) : Set :=
  { sub_var : evar A → value B
  ; sub_lbl : lvar A → label (lvar B)
  }.

Arguments sub_var {A} {B}.
Arguments sub_lbl {A} {B}.

Notation "A '{⇒}' B" := (sub A B) (at level 99) : type_scope.

Instance PreSubstCore_sub : PreSubstCore sub :=
  {| subst_pure := λ A,
       {| sub_var := λ x, v_var x
       ;  sub_lbl := λ a, l_var a
       |}
  ;  subst_eq := λ A B s₁ s₂,
      (∀ x, sub_var s₁ x = sub_var s₂ x) ∧
      (∀ a, sub_lbl s₁ a = sub_lbl s₂ a)
  |}.

Instance SLiftableCore_sub_incV : SLiftableCore sub incV :=
  { liftS := λ A B (f : A {⇒} B),
      {| sub_var := λ (x : evar (incV A)),
           match x with
           | VZ   => v_var (VZ : evar (incV B))
           | VS y => shift (sub_var f y)
           end
      ;  sub_lbl := sub_lbl f
      |}
  }.

Instance SLiftableCore_sub_incL : SLiftableCore sub incL :=
  { liftS := λ A B (f : A {⇒} B),
      {| sub_var := λ (x : evar (incL A)), shift (sub_var f x)
      ;  sub_lbl := λ (a : lvar (incL A)),
           match a with
           | VZ   => l_var VZ
           | VS b => shift (sub_lbl f b)
           end
      |}
  }.

Fixpoint ebind {A B : Set × Set} (φ : A {⇒} B) (e : expr A) : expr B :=
  match e with
  | e_value v       => e_value (vbind φ v)
  | e_let e₁ e₂     => e_let (ebind φ e₁) (ebind (liftS φ) e₂)
  | e_app v₁ v₂     => e_app (vbind φ v₁) (vbind φ v₂)
  | e_tapp v        => e_tapp (vbind φ v)
  | e_lapp v l      =>
      e_lapp (vbind φ v) (lbind {| apply_sub := sub_lbl φ |} l)
  | e_do l v        => e_do (lbind {| apply_sub := sub_lbl φ |} l) (vbind φ v)
  | e_handle e h r  => e_handle (ebind (liftS φ) e) (hbind φ h) (ebind (liftS φ) r)
  | e_rhandle l e h r => e_rhandle l (ebind φ e) (hbind φ h) (ebind (liftS φ) r)
  end
with vbind {A B : Set × Set} (φ : A {⇒} B) (v : value A) : value B :=
  match v with
  | v_var  x => sub_var φ x
  | v_lam  e => v_lam  (ebind (liftS φ) e)
  | v_tlam e => v_tlam (ebind φ e)
  | v_llam e => v_llam (ebind (liftS φ) e)
  | v_unit   => v_unit
  end
with hbind {A B : Set × Set} (φ : A {⇒} B) (h : handler A) : handler B :=
  match h with
  | h_do e => h_do (ebind (liftS (liftS φ)) e)
  end.

Instance IsBindCore_ebind  : IsBindCore (@ebind).
Instance IsBindCore_vbind  : IsBindCore (@vbind).
Instance IsBindCore_hbind  : IsBindCore (@hbind).

Fixpoint rbind {A B : Set × Set} (φ : A {⇒} B) (E : rctx A) : rctx B :=
  match E with
  | r_hole    => r_hole
  | r_let E e => r_let (rbind φ E) (bind (liftS φ) e)
  | r_handle l E h r => r_handle l (rbind φ E) (bind φ h) (bind (liftS φ) r)
  end.

Instance IsBindCore_rbind : IsBindCore (@rbind).

Instance SubstCore_sub : SubstCore prod_arr sub :=
  {| arrow_subst_comp := λ (A B C : Set × Set) (f : B [→] C) (s : A {⇒} B),
       {| sub_var := λ x, vmap f (sub_var s x)
       ;  sub_lbl := λ a, fmap {| apply_arr := arr₂ f |} (sub_lbl s a)
       |}
  ;  subst_comp := λ (A B C : Set × Set) (s₁ : B {⇒} C) (s₂ : A {⇒} B),
       {| sub_var := λ x, bind s₁ (sub_var s₂ x)
       ;  sub_lbl := λ a, bind {| apply_sub := sub_lbl s₁ |} (sub_lbl s₂ a)
       |}
  |}.

Instance SubstitutableCore_value_incV : SubstitutableCore sub value incV :=
  λ A v,
  {| sub_var := λ x : evar (incV A),
      match x with
      | VZ   => v
      | VS y => v_var y
      end
  ;  sub_lbl := λ l, l_var l
  |}.

Instance SubstitutableCore_label_incL :
    SubstitutableCore sub (λ A, label (lvar A)) incL :=
  λ A l,
  {| sub_var := λ x : evar (incL A), v_var (x : evar A)
  ;  sub_lbl := λ a,
      match a with
      | VZ   => l
      | VS l => l_var l
      end
  |}.
