Require Import Utf8.
Require Import Binding.Lib Binding.Product.
Require Import Fresh.Syntax Fresh.Fresh.

Inductive op_clause e : handler0 → Prop :=
| opcl_do : op_clause e (h_do e)
.

Inductive hmatch : ltag → rctx0 → expr_ (inc (inc ∅)) ∅ → Prop :=
| match_h l h r R e
          (HF : rfree l R)
          (HO : op_clause e h) :
    hmatch l (r_handle l R h r) e.

Inductive contr : expr0 → expr0 → Prop :=
| contr_let   : ∀ (v : value0) (e : expr (on₁ inc _)),
    contr (e_let v e) (subst e v)
| contr_beta  : ∀ (e : expr (on₁ inc _)) (v : value0),
    contr (e_app (v_lam e) v) (subst e v)
| contr_tbeta : ∀ e,
    contr (e_tapp (v_tlam e)) e
| contr_lbeta : ∀ (e : expr (on₂ inc _)) l,
    contr (e_lapp (v_llam e) l) (subst e l)
| contr_do    : ∀ l E (v : value0) he,
    hmatch l E he →
    contr (rplug E (e_do (l_tag l) v))
          (subst (subst he
                        (shift (v_lam (rplug (shift E) (VZ : evar (incV _))))))
                 v)
| contr_rhandle : ∀ (l : ltag) (v : value0) (h : handler0) r,
    contr (e_rhandle l v h r) (subst r v)
.

Inductive hcontr : expr0 → expr0 → Prop :=
| contr_handle : ∀ (l : ltag) (e : expr (on₂ inc _)) (h : handler0) r,
    fresh l e →
    fresh l h →
    fresh l r →
    hcontr (e_handle e h r)
           (e_rhandle l (subst e (l_tag l)) h r)
.

Inductive red : expr0 → expr0 → Prop :=
| red_contr : ∀ F e e',
    contr e e' →
    red (plug F e) (plug F e')
| red_hcontr : ∀ F e e',
    hcontr e e' →
    (∀ l, fresh l e → ¬fresh l e' → fresh l F) →
    red (plug F e) (plug F e')
.

Require Import Relation_Operators.

(** The reflexive and transitive closure of the reduction relation. *)

Notation red_rtc := (@clos_refl_trans_1n _ red).

Require Import PeanoNat.
Import Nat.

Lemma fresh_red (E : ectx0) e (h : handler0) r l :
  exists l', l ≤ l' ∧ red (plug E (e_handle e h r)) (plug E (e_rhandle l' (subst e (l_tag l')) h r)).
Proof.
  remember (max l (max (getFreshe (e_handle e h r)) (getFreshx E))) as l'; exists l'.
  split; [subst; apply le_max_l |].
  apply red_hcontr; [apply contr_handle; apply lbfresh_fresh; subst; simpl |].
  - rewrite <- le_max_r, <- !le_max_l; apply lbfresh_e.
  - rewrite <- le_max_r, <- le_max_l, <- le_max_r, <- le_max_l; apply lbfresh_h.
  - rewrite <- le_max_r, <- le_max_l, <- !le_max_r; apply lbfresh_e.
  - intros ll HL HNL; destruct (Peano_dec.eq_nat_dec ll l'); [subst ll |].
    + apply lbfresh_fresh; subst; rewrite <- !le_max_r; apply lbfresh_x.
    + contradiction HNL; simpl; simpl in HL; split; [assumption |].
      destruct HL as [He Hhr]; split; [| assumption].
      apply fresh_bind_e, He; split; simpl; [intros [] | intros [| []]; assumption].
Qed.

