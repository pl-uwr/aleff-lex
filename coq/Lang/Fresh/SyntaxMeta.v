Require Import Utf8.
Require Import Binding.Lib Binding.Set Binding.Product Binding.Auto.
Require Import Fresh.Syntax.

Lemma emap_rplug {A B : Set × Set} (f : A [→] B) (E : rctx A) (e : expr A) :
  emap f (rplug E e) = rplug (rmap f E) (emap f e).
Proof.
induction E; simpl; try rewrite IHE; reflexivity.
Qed.

Lemma ebind_rplug {A B : Set × Set} (f : A {⇒} B) (E : rctx A) (e : expr A) :
  ebind f (rplug E e) = rplug (rbind f E) (ebind f e).
Proof.
induction E; simpl; try rewrite IHE; reflexivity.
Qed.

Hint Rewrite -> @emap_rplug  : term_simpl_raw.
Hint Rewrite -> @ebind_rplug : term_simpl_raw.

Instance SetPure_label : SetPure label.
Proof. split; reflexivity. Qed.

(* ========================================================================= *)
(* Map instances *)

Fixpoint lmap_id {A : Set} (f : A [→] A) (l : label A) :
  arrow_eq f arrow_id → lmap f l = l.
Proof. auto_map_id. Qed.

Fixpoint lmap_map_comp {A B C : Set}
         (f : B [→] C) (g : A [→] B) h (l : label A) :
  arrow_eq (arrow_comp f g) h → lmap f (lmap g l) = lmap h l.
Proof. auto_map_map_comp. Qed.

Instance FMap_lmap : FMap (@lmap).
Proof.
split; [exact @lmap_id | exact @lmap_map_comp].
Qed.

Fixpoint emap_id {A : Set × Set} (f : A [→] A) (e : expr A) :
  arrow_eq f arrow_id → emap f e = e
with vmap_id {A : Set × Set} (f : A [→] A) (v : value A) :
  arrow_eq f arrow_id → vmap f v = v
with hmap_id {A : Set × Set} (f : A [→] A) (h : handler A) :
  arrow_eq f arrow_id → hmap f h = h.
Proof.
+ auto_map_id.
+ auto_map_id.
+ auto_map_id.
Qed.

Fixpoint emap_map_comp {A B C : Set × Set}
         (f : B [→] C) (g : A [→] B) h (e : expr A) :
  arrow_eq (arrow_comp f g) h → emap f (emap g e) = emap h e
with vmap_map_comp {A B C : Set × Set}
         (f : B [→] C) (g : A [→] B) h (v : value A) :
  arrow_eq (arrow_comp f g) h → vmap f (vmap g v) = vmap h v
with hmap_map_comp {A B C : Set × Set}
         (f : B [→] C) (g : A [→] B) h (hh : handler A) :
  arrow_eq (arrow_comp f g) h → hmap f (hmap g hh) = hmap h hh.
Proof.
+ auto_map_map_comp.
+ auto_map_map_comp.
+ auto_map_map_comp.
Qed.

Instance FMap_emap : FMap (@emap).
Proof.
split; [exact @emap_id | exact @emap_map_comp].
Qed.

Instance FMap_vmap : FMap (@vmap).
Proof.
split; [exact @vmap_id | exact @vmap_map_comp].
Qed.

Instance FMap_hmap : FMap (@hmap).
Proof.
split; [exact @hmap_id | exact @hmap_map_comp].
Qed.

Fixpoint rmap_id {A : Set × Set} (f : A [→] A) (E : rctx A) :
  arrow_eq f arrow_id → rmap f E = E.
Proof. auto_map_id. Qed.

Fixpoint rmap_map_comp {A B C : Set × Set}
         (f : B [→] C) (g : A [→] B) h (E : rctx A) :
  arrow_eq (arrow_comp f g) h → rmap f (rmap g E) = rmap h E.
Proof. auto_map_map_comp. Qed.

Instance FMap_rmap : FMap (@rmap).
Proof.
split; [exact @rmap_id | exact @rmap_map_comp].
Qed.

(* ========================================================================= *)
(* LiftA - LiftS *)

Instance ASLiftable_incV : ASLiftable arrow sub (on₁ inc).
Proof.
intros A B B' C f g g' f' Heq; split.
+ intros [ | x ]; term_simpl; [ reflexivity | ].
  destruct Heq as [ Heq _ ]; term_simpl in Heq; rewrite Heq.
  unfold shift; rewrite map_map_comp'; apply map_map_comp.
  split; reflexivity.
+ intro a; apply Heq.
Qed.

Instance ASLiftable_incL : ASLiftable arrow sub (on₂ inc).
Proof.
intros A B B' C f g g' f' Heq; split.
+ intro x; term_simpl.
  destruct Heq as [ Heq _ ]; term_simpl in Heq; rewrite Heq.
  reflexivity.
+ intros [ | a ]; term_simpl; [ reflexivity | ].
  destruct Heq as [ _ Heq ]; term_simpl in Heq; rewrite Heq.
  unfold shift; rewrite map_map_comp'; apply map_map_comp.
  split; reflexivity.
Qed.

Instance LiftSShift_sub_incV : LiftSShift arrow sub (on₁ inc).
Proof.
split; intro; term_simpl.
+ reflexivity.
+ symmetry; apply map_id.
  intro; reflexivity.
Qed.

Instance LiftSShift_sub_incL : LiftSShift arrow sub (on₂ inc).
Proof.
split; intro; term_simpl.
+ reflexivity.
+ apply map_equiv; intro; reflexivity.
Qed.

(* ========================================================================= *)
(* Bind-Map instances *)

Fixpoint lbind_map_comp {A B B' C : Set}
         (f : B [⇒] C) (g : A [→] B) (g' : B' [→] C) (f' : A [⇒] B')
         (l : label A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  lbind f (lmap g l) = lmap g' (lbind f' l).
Proof.
+ auto_bind_map_comp.
Qed.

Instance BindMap_lmap_lbind : BindMap (@lmap) (@lbind).
Proof. split; exact @lbind_map_comp. Qed.

Fixpoint ebind_map_comp {A B B' C : Set × Set}
         (f : B {⇒} C) (g : A [→] B) (g' : B' [→] C) (f' : A {⇒} B')
         (e : expr A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  ebind f (emap g e) = emap g' (ebind f' e)
with vbind_map_comp {A B B' C : Set × Set}
         (f : B {⇒} C) (g : A [→] B) (g' : B' [→] C) (f' : A {⇒} B')
         (v : value A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  vbind f (vmap g v) = vmap g' (vbind f' v)
with hbind_map_comp {A B B' C : Set × Set}
         (f : B {⇒} C) (g : A [→] B) (g' : B' [→] C) (f' : A {⇒} B')
         (h : handler A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  hbind f (hmap g h) = hmap g' (hbind f' h).
Proof.
+ auto_bind_map_comp.
  - intro; apply Heq.
  - intro; apply Heq.
+ auto_bind_map_comp.
+ auto_bind_map_comp.
Qed.

Instance BindMap_emap_ebind : BindMap (@emap) (@ebind).
Proof. split; exact @ebind_map_comp. Qed.

Instance BindMap_vmap_vbind : BindMap (@vmap) (@vbind).
Proof. split; exact @vbind_map_comp. Qed.

Instance BindMap_hmap_hbind : BindMap (@hmap) (@hbind).
Proof. split; exact @hbind_map_comp. Qed.

Fixpoint rbind_map_comp {A B B' C : Set × Set}
         (f : B {⇒} C) (g : A [→] B) (g' : B' [→] C) (f' : A {⇒} B')
         (E : rctx A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  rbind f (rmap g E) = rmap g' (rbind f' E).
Proof. auto_bind_map_comp. Qed.

Instance BindMap_rmap_rbind : BindMap (@rmap) (@rbind).
Proof. split; exact @rbind_map_comp. Qed.

(* ========================================================================= *)
(* LiftS instances *)

Instance SLiftable_incV : SLiftable arrow sub (on₁ inc).
Proof.
split.
+ intros A f Heq; split.
  - intros [ | x ]; simpl; [ reflexivity | ].
    destruct Heq as [ Heq _ ]; rewrite Heq; reflexivity.
  - intro a; simpl; apply Heq.
+ intros A B C f g h Heq; split.
  - intros [ | x ]; simpl; [ reflexivity | ].
    rewrite bind_liftS_shift_comm.
    destruct Heq as [ Heq _ ]; simpl in Heq; rewrite Heq; reflexivity.
  - intros a; simpl.
    destruct Heq as [ _ Heq ]; simpl in Heq; rewrite Heq; reflexivity.
+ intros A B f₁ f₂ Heq; split.
  - intros [ | x ]; simpl; [ reflexivity | ].
    destruct Heq as [ Heq _ ]; simpl in Heq; rewrite Heq; reflexivity.
  - intro a; simpl; apply Heq.
Qed.

Instance SLiftable_incL : SLiftable arrow sub (on₂ inc).
Proof.
split.
+ intros A f Heq; split.
  - intro x; simpl.
    destruct Heq as [ Heq _ ]; rewrite Heq; reflexivity.
  - intros [ | a ]; simpl; [ reflexivity | ].
    destruct Heq as [ _ Heq ]; rewrite Heq; reflexivity.
+ intros A B C f g h Heq; split.
  - intros x; simpl.
    rewrite bind_liftS_shift_comm.
    destruct Heq as [ Heq _ ]; simpl in Heq. rewrite Heq; reflexivity.
  - intros [ | a ]; simpl; [ reflexivity | ].
    destruct Heq as [ _ Heq ]; simpl in Heq; rewrite <- Heq.
    apply bind_map_comp; reflexivity.
+ intros A B f₁ f₂ Heq; split.
  - intro x; simpl.
    destruct Heq as [ Heq _ ]; simpl in Heq; rewrite Heq; reflexivity.
  - intros [ | a ]; simpl; [ reflexivity | ].
    destruct Heq as [ _ Heq ]; simpl in Heq; rewrite Heq; reflexivity.
Qed.

(* ========================================================================= *)
(* Bind instances *)

Fixpoint lbind_pure {A : Set} (f : A [⇒] A) (l : label A) :
  subst_eq f subst_pure → lbind f l = l.
Proof. auto_bind_pure. Qed.

Fixpoint lbind_bind_comp {A B C : Set}
         (f : B [⇒] C) (g : A [⇒] B) h (l : label A) :
  subst_eq (subst_comp f g) h → lbind f (lbind g l) = lbind h l.
Proof. auto_bind_bind_comp. Qed.

Instance Bind_lbind : Bind (@lbind).
Proof.
split.
+ exact @lbind_pure.
+ exact @lbind_bind_comp.
Qed.

Fixpoint ebind_pure {A : Set × Set} (f : A {⇒} A) (e : expr A) :
  subst_eq f subst_pure → ebind f e = e
with vbind_pure {A : Set × Set} (f : A {⇒} A) (v : value A) :
  subst_eq f subst_pure → vbind f v = v
with hbind_pure {A : Set × Set} (f : A {⇒} A) (h : handler A) :
  subst_eq f subst_pure → hbind f h = h.
Proof.
+ auto_bind_pure.
  - intro; apply Heq.
  - intro; apply Heq.
+ auto_bind_pure.
+ auto_bind_pure.
Qed.

Fixpoint ebind_bind_comp {A B C : Set × Set}
         (f : B {⇒} C) (g : A {⇒} B) h (e : expr A) :
  subst_eq (subst_comp f g) h → ebind f (ebind g e) = ebind h e
with vbind_bind_comp {A B C : Set × Set}
         (f : B {⇒} C) (g : A {⇒} B) h (v : value A) :
  subst_eq (subst_comp f g) h → vbind f (vbind g v) = vbind h v
with hbind_bind_comp {A B C : Set × Set}
         (f : B {⇒} C) (g : A {⇒} B) h (hh : handler A) :
  subst_eq (subst_comp f g) h → hbind f (hbind g hh) = hbind h hh.
Proof.
+ auto_bind_bind_comp.
  - intro; apply Heq.
  - intro; apply Heq.
+ auto_bind_bind_comp.
+ auto_bind_bind_comp.
Qed.

Instance Bind_ebind : Bind (@ebind).
Proof.
split.
+ exact @ebind_pure.
+ exact @ebind_bind_comp.
Qed.

Instance Bind_vbind : Bind (@vbind).
Proof.
split.
+ exact @vbind_pure.
+ exact @vbind_bind_comp.
Qed.

Instance Bind_hbind : Bind (@hbind).
Proof.
split.
+ exact @hbind_pure.
+ exact @hbind_bind_comp.
Qed.

Fixpoint rbind_pure {A : Set × Set} (f : A {⇒} A) (E : rctx A) :
  subst_eq f subst_pure → rbind f E = E.
Proof. auto_bind_pure. Qed.

Fixpoint rbind_bind_comp {A B C : Set × Set}
         (f : B {⇒} C) (g : A {⇒} B) h (E : rctx A) :
  subst_eq (subst_comp f g) h → rbind f (rbind g E) = rbind h E.
Proof. auto_bind_bind_comp. Qed.

Instance Bind_rbind : Bind (@rbind).
Proof.
split.
+ exact @rbind_pure.
+ exact @rbind_bind_comp.
Qed.

(* ========================================================================= *)
(* Subst instances *)

Instance PreSubst_sub : PreSubst sub.
Proof.
split.
+ intros; split; reflexivity.
+ intros A B f g Heq; split; symmetry; apply Heq.
+ intros A B f g h Heq₁ Heq₂; split; etransitivity; try apply Heq₁; apply Heq₂.
Qed.

Instance Subst_arrow_sub : Subst arrow sub.
Proof.
split.
+ intros; split; intros; simpl; apply map_id; simpl; firstorder.
+ intros; split; simpl.
  - intro x; erewrite <- (vbind_map_comp _ arrow_id).
    * rewrite vmap_id; [ | reflexivity ]; reflexivity.
    * split; reflexivity.
  - intro a; erewrite <- (lbind_map_comp _ arrow_id).
    * rewrite lmap_id; [ | reflexivity ]; reflexivity.
    * split; reflexivity.
+ intros; split.
  - intro x; simpl.
    destruct EQg as [ EQg _ ]; rewrite EQg.
    apply map_equiv; assumption.
  - intro a; simpl.
    destruct EQg as [ _ EQg ]; rewrite EQg.
    apply map_equiv; apply EQf.
+ intros; split; simpl.
  - intro x; symmetry; apply map_map_comp.
    split; reflexivity.
  - intro x; symmetry; apply map_map_comp.
    reflexivity.
+ split; simpl.
  - intro x; apply bind_pure.
    split; reflexivity.
  - intro a; apply bind_pure.
    intro; reflexivity.
+ split; simpl; reflexivity.
+ split; simpl.
  - intro x; symmetry; apply bind_bind_comp.
    split; reflexivity.
  - intro x; symmetry; apply bind_bind_comp.
    split; reflexivity.
+ intros A B C f₁ f₂ g₁ g₂ Heq₁ Heq₂.
  split.
  - intro x; simpl.
    destruct Heq₂ as [ Heq₂ _ ]; rewrite Heq₂.
    rewrite <- (bind_pure' (bind f₁ (sub_var g₂ x))); apply bind_bind_comp.
    split; intro.
    * rewrite <- (proj1 Heq₁); apply bind_pure'.
    * rewrite <- (proj2 Heq₁); simpl.
      apply bind_pure; intro; reflexivity.
  - intro a; simpl.
    destruct Heq₂ as [ _ Heq₂ ]; rewrite Heq₂.
    apply bind_equiv; intro; apply Heq₁.
Qed.

Instance Subst_shift_incV : SubstShift arrow sub value (on₁ inc).
Proof. split; reflexivity. Qed.

Instance Subst_shift_incL : SubstShift arrow sub _ (on₂ inc).
Proof.
  split; intros; term_simpl; reflexivity.
Qed.

Instance SB_val_incV : SubstBind arrow sub value incV.
Proof.
  intros A B f v; split; [intros [| x]; term_simpl; reflexivity |].
  intros a; term_simpl; symmetry; apply bind_pure; intros b; reflexivity.
Qed.
