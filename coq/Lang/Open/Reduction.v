Require Import Utf8.
Require Import Binding.Lib Binding.Product Binding.Set.
Require Import Open.Syntax Open.SyntaxMeta.


Inductive op_clause {A} e : handler_ ∅ A  → Prop :=
| opcl_do : op_clause e (h_do e)
.

Inductive hmatch {A B} : rctx A B → expr_ (inc (inc ∅)) B → lvar A → Prop :=
| match_h h r R e
          (HO : op_clause e h) :
    hmatch (r_handle R h r) e (fmap (mapOf R) (lclosed VZ)).

Fixpoint openBy {A B} (R : rctx A B) : A [⇒] B :=
  match R with
  | r_hole => subst_pure
  | r_let R _ => openBy R
  | r_handle R _ _ => subst_comp (mk_subst lopen) (openBy R)
  end.

Inductive contr {A} : expr_ ∅ A → expr_ ∅ A → Prop :=
| contr_let  (v : value_ ∅ A) (e : expr (incV _)) :
    contr (e_let v e) (subst e v)
| contr_beta (e : expr (incV _)) (v : value_ ∅ A) :
    contr (e_app (v_lam e) v) (subst e v)
| contr_tbeta e :
    contr (e_tapp (v_tlam e)) e
| contr_lbeta (e : expr (incL _)) l :
    contr (e_lapp (v_llam e) l) (subst e l)
| contr_ret h r (v : value _) :
    contr (e_handle v h r) (subst r (subst v lopen))
| contr_do B (R : rctx B A) e (l : lvar (lv ⟨∅, B⟩)) (v : value_ ∅ B)
          (HM : hmatch R e l) :
    contr (rplug R (e_do l v))
          (subst (subst e (shift (v_lam (rplug R (VZ : ev (incV ⟨∅, B⟩)) : expr (incV ⟨∅, A⟩)))))
                 (bind (injLSub (openBy R)) v)).

Inductive red : expr0 → expr0 → Prop :=
| red_contr A (E : ectx A) e e'
            (HC : contr e e') :
    red (xplug E e) (xplug E e').

Require Import Relation_Operators.

(** The reflexive and transitive closure of the reduction relation. *)

Notation red_rtc := (@clos_refl_trans_1n _ red).

Lemma liftBy_openBy_comp {A B C} (ρ : A [⇒] C) (R : rctx B A) :
  subst_eq (subst_comp ρ (openBy R)) (subst_comp (openBy (liftByR R ρ)) (liftByρ R ρ)).
Proof.
  revert C ρ; induction R; intros; intros x; term_simpl.
  - rewrite bind_pure; reflexivity.
  - apply IHR.
  - specialize (IHR _ (liftS ρ)); simpl in IHR; rewrite IHR; term_simpl.
    unfold subst; rewrite bind_bind_comp'; apply bind_equiv; reflexivity.
Qed.

Lemma contr_bind {A B} (ρ : A [⇒] B) (e₁ e₂ : expr_ ∅ A) (HR : contr e₁ e₂) :
  contr (bind (injLSub ρ) e₁) (bind (injLSub ρ) e₂).
Proof.
  inversion_clear HR; term_simpl; [constructor .. |].
  rewrite liftBy_plug; term_simpl.
  match goal with |- contr _ ?ea =>
                  evar (eb : expr_ ∅ B); cutrewrite (ea = eb); revert eb; simpl; [eapply contr_do |]
  end.
  - inversion_clear HM; simpl; erewrite bind_map_comp; [| symmetry; apply liftBy_mapOf_comm].
    constructor; inversion_clear HO; constructor.
  - term_simpl; f_equal; [f_equal; f_equal |].
    + erewrite bind_map_comp with (g' := liftA mk_shift) (f' := liftS (injLSub ρ));
        [| split; [intros [| []] | intros x]; term_simpl; [| symmetry; apply map_id]; reflexivity].
      f_equal; transitivity (bind (injLSub ρ) (rplug R (v_var (VZ : ev ⟨inc ∅, B0⟩)))).
      * apply bind_equiv; split; [intros [| []] | intros x]; reflexivity.
      * rewrite liftBy_plug; term_simpl; reflexivity.
    + now rewrite !bind_bind_comp', !comp_injL, liftBy_openBy_comp.
Qed.
