Require Import Utf8.
Require Import Binding.Lib Binding.Set Binding.Product.
Require Import List.

Notation incV := (on₁ inc).
Notation incL := (on₂ inc).

Inductive lvar {A : Set} : Set :=
| lclosed : A     → lvar
| lopen   : lvar.
Arguments lvar A : clear implicits.

Instance SPC_lvar : SetPureCore lvar := @lclosed.

Definition ev (A : Set × Set) := sfst A.
Definition lv (A : Set × Set) := ssnd A.
(*Notation ev A := (sfst A).
Notation lv A := (ssnd A).*)

Inductive expr {A : Set × Set} : Set :=
| e_value  : value → expr
| e_let    : expr  → @expr (incV A) → expr
| e_app    : value → value → expr
| e_tapp   : value → expr
| e_lapp   : value → lvar (lv A) → expr
| e_do     : lvar (lv A) → value → expr
| e_handle : @expr (incL A) → handler → @expr (incV A) → expr
with value {A : Set × Set} : Set :=
| v_var  : ev A → value
| v_lam  : @expr (incV A) → value
| v_tlam : expr → value
| v_llam : @expr (incL A) → value
| v_unit : value
with handler {A : Set × Set} : Set :=
| h_do : @expr (incV (incV A)) → handler
.
Arguments expr A : clear implicits.
Arguments value A : clear implicits.
Arguments handler A : clear implicits.

Notation expr_      V LV := (expr      {| sfst := V; ssnd := LV |}).
Notation value_     V LV := (value     {| sfst := V; ssnd := LV |}).
Notation handler_   V LV := (handler   {| sfst := V; ssnd := LV |}).

Notation expr0      := (expr_      ∅ ∅).
Notation value0     := (value_     ∅ ∅).
Notation handler0   := (handler_   ∅ ∅).

Inductive ectx : Set → Type :=
| x_hole   : ectx ∅
| x_let    A (E : ectx A) (e : expr_ (inc ∅) A) : ectx A
| x_handle A (E : ectx A) (h : handler_ ∅ A) (r : expr_ (inc ∅) A) : ectx (inc A).
Arguments x_let {A} E e.
Arguments x_handle {A} E h r.

Inductive rctx {A : Set} : Set → Type :=
| r_hole   : rctx A
| r_let    B (R : rctx B) (e : expr_ (inc ∅) B) : rctx B
| r_handle B (R : rctx (inc B)) (h : handler_ ∅ B) (r : expr_ (inc ∅) B) : rctx B.
Arguments rctx A B : clear implicits.
Arguments r_let {A B} R e.
Arguments r_handle {A B} R h r.

Module Notations.

  Coercion e_value : value >-> expr.
  Coercion v_var   : ev    >-> value.

  Notation "'λᵉ' e" := (v_lam e)  (at level 30, no associativity) : osyn_scope.
  Notation "'λᵗ' e" := (v_tlam e) (at level 30, no associativity) : osyn_scope.
  Notation "'λˡ' e" := (v_llam e) (at level 30, no associativity) : osyn_scope.
  Notation "'()ᵉ'"  := (v_unit) : osyn_scope.
  Notation "e₁ >>= e₂ " := (e_let e₁ e₂) (at level 25, right associativity) : osyn_scope.
  Notation "v₁ '@ᵉ' v₂" := (e_app v₁ v₂) (at level 24, no associativity) : osyn_scope.
  Notation "v  '@ᵗ'"    := (e_tapp v) (at level 24, no associativity) : osyn_scope.
  Notation "v  '@ˡ' l"  := (e_lapp v l) (at level 24, no associativity) : osyn_scope.

  Delimit Scope osyn_scope with osyn.

End Notations.
Export Notations.

Fixpoint lmap {A B : Set} (f : A [→] B) (l : lvar A) : lvar B :=
  match l with
  | lclosed a => lclosed (f a)
  | lopen     => lopen
  end.

Instance IsMapCore_lmap : IsMapCore (@lmap).

Fixpoint emap {A B : Set × Set} (φ : A [→] B) (e : expr A) : expr B :=
  match e with
  | e_value v       => e_value (vmap φ v)
  | e_let e₁ e₂     => e_let (emap φ e₁) (emap (liftA φ) e₂)
  | e_app v₁ v₂     => e_app (vmap φ v₁) (vmap φ v₂)
  | e_tapp v        => e_tapp (vmap φ v)
  | e_lapp v l      => e_lapp (vmap φ v) (lmap (arr₂ φ) l)
  | e_do l v        => e_do (lmap (arr₂ φ) l) (vmap φ v)
  | e_handle e h r  => e_handle (emap (liftA φ) e) (hmap φ h) (emap (liftA φ) r)
  end
with vmap {A B : Set × Set} (φ : A [→] B) (v : value A) : value B :=
  match v with
  | v_var  x => v_var  (arr₁ φ x)
  | v_lam  e => v_lam  (emap (liftA φ) e)
  | v_tlam e => v_tlam (emap φ e)
  | v_llam e => v_llam (emap (liftA φ) e)
  | v_unit   => v_unit
  end
with hmap {A B : Set × Set} (φ : A [→] B) (h : handler A) : handler B :=
  match h with
  | h_do e => h_do (emap (liftA (liftA φ)) e)
  end.

Instance IsMapCore_emap  : IsMapCore (@emap).
Instance IsMapCore_vmap  : IsMapCore (@vmap).
Instance IsMapCore_hmap  : IsMapCore (@hmap).

(* ========================================================================= *)
(* Substitution *)

Fixpoint lbind {A B : Set} (f : A [⇒] B) (l : lvar A) : lvar B :=
  match l with
  | lclosed a => f a
  | lopen     => lopen
  end.

Instance IsBindCore_lbind : IsBindCore (@lbind).

Record sub (A B : Set × Set) : Set :=
  { sub_var : ev A → value B
  ; sub_lbl : lv A [⇒] lv B
  }.

Arguments sub_var {A} {B}.
Arguments sub_lbl {A} {B}.

Notation "A '{⇒}' B" := (sub A B) (at level 99) : type_scope.

Instance SLiftableCore_sub_incV : SLiftableCore sub incV :=
  { liftS := λ A B (f : A {⇒} B),
      {| sub_var := λ (x : ev (incV A)),
           match x with
           | VZ   => v_var (VZ : ev (incV B))
           | VS y => shift (sub_var f y)
           end
      ;  sub_lbl := sub_lbl f
      |}
  }.

Instance SLiftableCore_sub_incL : SLiftableCore sub incL :=
  { liftS := λ A B (f : A {⇒} B),
      {| sub_var := λ (x : ev (incL A)), shift (sub_var f x)
      ;  sub_lbl := liftS (sub_lbl f)
      |}
  }.

Fixpoint ebind {A B : Set × Set} (φ : A {⇒} B) (e : expr A) : expr B :=
  match e with
  | e_value v       => e_value (vbind φ v)
  | e_let e₁ e₂     => e_let (ebind φ e₁) (ebind (liftS φ) e₂)
  | e_app v₁ v₂     => e_app (vbind φ v₁) (vbind φ v₂)
  | e_tapp v        => e_tapp (vbind φ v)
  | e_lapp v l      =>
      e_lapp (vbind φ v) (lbind (sub_lbl φ) l)
  | e_do l v        => e_do (lbind (sub_lbl φ) l) (vbind φ v)
  | e_handle e h r  => e_handle (ebind (liftS φ) e) (hbind φ h) (ebind (liftS φ) r)
  end
with vbind {A B : Set × Set} (φ : A {⇒} B) (v : value A) : value B :=
  match v with
  | v_var  x => sub_var φ x
  | v_lam  e => v_lam  (ebind (liftS φ) e)
  | v_tlam e => v_tlam (ebind φ e)
  | v_llam e => v_llam (ebind (liftS φ) e)
  | v_unit   => v_unit
  end
with hbind {A B : Set × Set} (φ : A {⇒} B) (h : handler A) : handler B :=
  match h with
  | h_do e => h_do (ebind (liftS (liftS φ)) e)
  end.

Instance IsBindCore_ebind  : IsBindCore (@ebind).
Instance IsBindCore_vbind  : IsBindCore (@vbind).
Instance IsBindCore_hbind  : IsBindCore (@hbind).

Instance SubstitutableCore_value_incV : SubstitutableCore sub value incV :=
  λ A v,
  {| sub_var := λ x : ev (incV A),
      match x with
      | VZ   => v
      | VS y => v_var y
      end
  ;  sub_lbl := subst_pure
  |}.

Instance SubstitutableCore_label_incL :
    SubstitutableCore sub (λ A, lvar (lv A)) incL :=
  λ A l,
  {| sub_var := λ x : ev (incL A), v_var (x : ev A)
  ;  sub_lbl := mk_subst (A := lv A) l
  |}.

Definition embed (A : Set) : Arr ∅ A :=
  {| apply_arr := λ x : ∅, match x return A with end |}.

Definition embedV {A B : Set} : ⟨∅, B⟩ [→] ⟨A, B⟩ :=
  {| arr₁ := embed A  : Arr (sfst ⟨∅, B⟩) (sfst ⟨A, B⟩);
     arr₂ := arrow_id : Arr (ssnd ⟨∅, B⟩) (ssnd ⟨A, B⟩) |}.

Fixpoint xplug {A : Set} (E : ectx A) : expr_ ∅ A → expr0 :=
  match E in (ectx B) return (expr_ ∅ B → expr0) with
  | x_hole         => λ e, e
  | x_let    E e'  => λ e, xplug E (e >>= e')%osyn
  | x_handle E h r => λ e, xplug E (e_handle (e : expr (incL ⟨ ∅, _ ⟩)) h r)
  end.

Fixpoint rplug {V A B : Set} (R : rctx A B) : expr_ V A → expr_ V B :=
  match R in (rctx _ C) return (expr_ V A → expr_ V C) with
  | r_hole         => λ e, e
  | r_let R e'     => λ e, (rplug R e >>= fmap (liftA embedV) e')%osyn
  | r_handle R h r => λ e, e_handle (rplug R e : expr (incL ⟨_, _⟩))
                                    (fmap embedV h) (fmap (liftA embedV) r)
  end.

Instance PreSubstCore_sub : PreSubstCore sub :=
  {| subst_pure := λ A,
       {| sub_var := λ x, v_var x
       ;  sub_lbl := subst_pure
       |}
  ;  subst_eq := λ A B s₁ s₂,
      (∀ x, sub_var s₁ x = sub_var s₂ x) ∧
      (∀ a, sub_lbl s₁ a = sub_lbl s₂ a)
  |}.

Instance SubstCore_sub : SubstCore prod_arr sub :=
  {| arrow_subst_comp := λ (A B C : Set × Set) (f : B [→] C) (s : A {⇒} B),
       {| sub_var := λ x, vmap f (sub_var s x)
       ;  sub_lbl := arrow_subst_comp (arr₂ f) (sub_lbl s)
       |}
  ;  subst_comp := λ (A B C : Set × Set) (s₁ : B {⇒} C) (s₂ : A {⇒} B),
       {| sub_var := λ x, bind s₁ (sub_var s₂ x)
       ;  sub_lbl := subst_comp (sub_lbl s₁) (sub_lbl s₂)
       |}
  |}.

Definition injLSub {V A B} (ρ : A [⇒] B) : ⟨V, A⟩ {⇒} ⟨V, B⟩ :=
  {| sub_var := λ (x : ev ⟨V, A⟩), x : ev ⟨V, B⟩;
     sub_lbl := ρ
  |}.


Fixpoint mapOf {A B : Set} (R : rctx B A) : A [→] B :=
  match R with
  | r_hole => arrow_id
  | r_let R _ => mapOf R
  | r_handle R _ _ => arrow_comp (mapOf R) mk_shift
  end.

Fixpoint liftByT {A B : Set} (C : Set) (R : rctx B A) : Set :=
  match R with
  | r_hole => C
  | r_let R _ => liftByT C R
  | r_handle R _ _ => liftByT (inc C) R
  end.

Fixpoint liftByR {A B C : Set} (R : rctx B A) (ρ : A [⇒] C) : rctx (liftByT C R) C :=
  match R return ∀ C, (_ [⇒] C) → rctx (liftByT C R) C with
  | r_hole => λ C ρ, r_hole
  | r_let R e => λ C ρ, r_let (liftByR R ρ) (bind (injLSub ρ) e)
  | r_handle R h r => λ C ρ, r_handle (liftByR R (liftS ρ)) (bind (injLSub ρ) h) (bind (injLSub ρ) r)
  end C ρ.

Fixpoint liftByρ {A B C : Set} (R : rctx B A) (ρ : A [⇒] C) : B [⇒] liftByT C R :=
  match R return ∀ C, _ [⇒] C → B [⇒] liftByT C R with
  | r_hole => λ C ρ, ρ
  | r_let R _ => λ C ρ, liftByρ R ρ
  | r_handle R _ _ => λ C ρ, liftByρ R (liftS ρ)
  end C ρ.

Fixpoint liftBy {A B C : Set} (R : rctx B A) (ρ : A [⇒] C) : sigT (fun D => rctx D C * (B [⇒] D))%type :=
    match R with
  | r_hole => λ ρ, existT _ C (r_hole, ρ)
  | r_let R e => λ ρ, match liftBy R ρ with
                      | existT _ D (R', ρ') => existT _ D (r_let R' (bind (injLSub ρ) e), ρ')
                      end
  | r_handle R h r => λ ρ, match liftBy R (liftS ρ) with
                           | existT _ D (R', ρ') =>
                             existT _ D (r_handle R' (bind (injLSub ρ) h) (bind (injLSub ρ) r), ρ')
                           end
    end ρ.
