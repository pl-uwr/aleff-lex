Require Import Utf8.
Require Import Binding.Lib Binding.Set Binding.Product Binding.Auto.
Require Import Open.Syntax.

Instance SetPure_label : SetPure lvar.
Proof. split; reflexivity. Qed.

(* ========================================================================= *)
(* Map instances *)

Fixpoint lmap_id {A : Set} (f : A [→] A) (l : lvar A) :
  arrow_eq f arrow_id → lmap f l = l.
Proof. auto_map_id. Qed.

Fixpoint lmap_map_comp {A B C : Set}
         (f : B [→] C) (g : A [→] B) h (l : lvar A) :
  arrow_eq (arrow_comp f g) h → lmap f (lmap g l) = lmap h l.
Proof. auto_map_map_comp. Qed.

Instance FMap_lmap : FMap (@lmap).
Proof.
split; [exact @lmap_id | exact @lmap_map_comp].
Qed.

Fixpoint emap_id {A : Set × Set} (f : A [→] A) (e : expr A) :
  arrow_eq f arrow_id → emap f e = e
with vmap_id {A : Set × Set} (f : A [→] A) (v : value A) :
  arrow_eq f arrow_id → vmap f v = v
with hmap_id {A : Set × Set} (f : A [→] A) (h : handler A) :
  arrow_eq f arrow_id → hmap f h = h.
Proof.
+ auto_map_id.
+ auto_map_id.
+ auto_map_id.
Qed.

Fixpoint emap_map_comp {A B C : Set × Set}
         (f : B [→] C) (g : A [→] B) h (e : expr A) :
  arrow_eq (arrow_comp f g) h → emap f (emap g e) = emap h e
with vmap_map_comp {A B C : Set × Set}
         (f : B [→] C) (g : A [→] B) h (v : value A) :
  arrow_eq (arrow_comp f g) h → vmap f (vmap g v) = vmap h v
with hmap_map_comp {A B C : Set × Set}
         (f : B [→] C) (g : A [→] B) h (hh : handler A) :
  arrow_eq (arrow_comp f g) h → hmap f (hmap g hh) = hmap h hh.
Proof.
+ auto_map_map_comp.
+ auto_map_map_comp.
+ auto_map_map_comp.
Qed.

Instance FMap_emap : FMap (@emap).
Proof.
split; [exact @emap_id | exact @emap_map_comp].
Qed.

Instance FMap_vmap : FMap (@vmap).
Proof.
split; [exact @vmap_id | exact @vmap_map_comp].
Qed.

Instance FMap_hmap : FMap (@hmap).
Proof.
split; [exact @hmap_id | exact @hmap_map_comp].
Qed.

(* ========================================================================= *)
(* LiftA - LiftS *)

Instance ASLiftable_incV : ASLiftable arrow sub (on₁ inc).
Proof.
intros A B B' C f g g' f' Heq; split.
+ intros [ | x ]; term_simpl; [ reflexivity | ].
  destruct Heq as [ Heq _ ]; term_simpl in Heq; rewrite Heq.
  unfold shift; rewrite map_map_comp'; apply map_map_comp.
  split; reflexivity.
+ intro a; apply Heq.
Qed.

Instance ASLiftable_incL : ASLiftable arrow sub (on₂ inc).
Proof.
intros A B B' C f g g' f' Heq; split.
+ intro x; term_simpl.
  destruct Heq as [ Heq _ ]; term_simpl in Heq; rewrite Heq.
  reflexivity.
+ intros [ | a ]; term_simpl; [ reflexivity | ].
  destruct Heq as [ _ Heq ]; term_simpl in Heq; rewrite Heq.
  unfold shift; rewrite map_map_comp'; apply map_map_comp.
  split; reflexivity.
Qed.

Instance LiftSShift_sub_incV : LiftSShift arrow sub (on₁ inc).
Proof.
split; intro; term_simpl.
+ reflexivity.
+ symmetry; apply map_id.
  intro; reflexivity.
Qed.

Instance LiftSShift_sub_incL : LiftSShift arrow sub (on₂ inc).
Proof.
split; intro; term_simpl.
+ reflexivity.
+ apply map_equiv; intro; reflexivity.
Qed.

(* ========================================================================= *)
(* Bind-Map instances *)

Fixpoint lbind_map_comp {A B B' C : Set}
         (f : B [⇒] C) (g : A [→] B) (g' : B' [→] C) (f' : A [⇒] B')
         (l : lvar A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  lbind f (lmap g l) = lmap g' (lbind f' l).
Proof.
+ auto_bind_map_comp.
Qed.

Instance BindMap_lmap_lbind : BindMap (@lmap) (@lbind).
Proof. split; exact @lbind_map_comp. Qed.

Fixpoint ebind_map_comp {A B B' C : Set × Set}
         (f : B {⇒} C) (g : A [→] B) (g' : B' [→] C) (f' : A {⇒} B')
         (e : expr A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  ebind f (emap g e) = emap g' (ebind f' e)
with vbind_map_comp {A B B' C : Set × Set}
         (f : B {⇒} C) (g : A [→] B) (g' : B' [→] C) (f' : A {⇒} B')
         (v : value A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  vbind f (vmap g v) = vmap g' (vbind f' v)
with hbind_map_comp {A B B' C : Set × Set}
         (f : B {⇒} C) (g : A [→] B) (g' : B' [→] C) (f' : A {⇒} B')
         (h : handler A) :
  subst_eq (subst_comp f (of_arrow g)) (arrow_subst_comp g' f') →
  hbind f (hmap g h) = hmap g' (hbind f' h).
Proof.
+ auto_bind_map_comp.
  - intro; apply Heq.
  - intro; apply Heq.
+ auto_bind_map_comp.
+ auto_bind_map_comp.
Qed.

Instance BindMap_emap_ebind : BindMap (@emap) (@ebind).
Proof. split; exact @ebind_map_comp. Qed.

Instance BindMap_vmap_vbind : BindMap (@vmap) (@vbind).
Proof. split; exact @vbind_map_comp. Qed.

Instance BindMap_hmap_hbind : BindMap (@hmap) (@hbind).
Proof. split; exact @hbind_map_comp. Qed.

(* ========================================================================= *)
(* LiftS instances *)

Instance SLiftable_incV : SLiftable arrow sub (on₁ inc).
Proof.
split.
+ intros A f Heq; split.
  - intros [ | x ]; simpl; [ reflexivity | ].
    destruct Heq as [ Heq _ ]; rewrite Heq; reflexivity.
  - intro a; simpl; apply Heq.
+ intros A B C f g h Heq; split.
  - intros [ | x ]; simpl; [ reflexivity | ].
    rewrite bind_liftS_shift_comm.
    destruct Heq as [ Heq _ ]; simpl in Heq; rewrite Heq; reflexivity.
  - intros a; simpl.
    destruct Heq as [ _ Heq ]; simpl in Heq; rewrite Heq; reflexivity.
+ intros A B f₁ f₂ Heq; split.
  - intros [ | x ]; simpl; [ reflexivity | ].
    destruct Heq as [ Heq _ ]; simpl in Heq; rewrite Heq; reflexivity.
  - intro a; simpl; apply Heq.
Qed.

Instance SLiftable_incL : SLiftable arrow sub (on₂ inc).
Proof.
split.
+ intros A f Heq; split.
  - intro x; simpl.
    destruct Heq as [ Heq _ ]; rewrite Heq; reflexivity.
  - intros [ | a ]; simpl; [ reflexivity | ].
    destruct Heq as [ _ Heq ]; rewrite Heq; reflexivity.
+ intros A B C f g h Heq; split.
  - intros x; simpl.
    rewrite bind_liftS_shift_comm.
    destruct Heq as [ Heq _ ]; simpl in Heq. rewrite Heq; reflexivity.
  - intros [ | a ]; simpl; [ reflexivity | ].
    destruct Heq as [ _ Heq ]; simpl in Heq; rewrite <- Heq.
    apply bind_map_comp; reflexivity.
+ intros A B f₁ f₂ Heq; split.
  - intro x; simpl.
    destruct Heq as [ Heq _ ]; simpl in Heq; rewrite Heq; reflexivity.
  - intros [ | a ]; simpl; [ reflexivity | ].
    destruct Heq as [ _ Heq ]; simpl in Heq; rewrite Heq; reflexivity.
Qed.

(* ========================================================================= *)
(* Bind instances *)

Fixpoint lbind_pure {A : Set} (f : A [⇒] A) (l : lvar A) :
  subst_eq f subst_pure → lbind f l = l.
Proof. auto_bind_pure. Qed.

Fixpoint lbind_bind_comp {A B C : Set}
         (f : B [⇒] C) (g : A [⇒] B) h (l : lvar A) :
  subst_eq (subst_comp f g) h → lbind f (lbind g l) = lbind h l.
Proof. auto_bind_bind_comp. Qed.

Instance Bind_lbind : Bind (@lbind).
Proof.
split.
+ exact @lbind_pure.
+ exact @lbind_bind_comp.
Qed.

Fixpoint ebind_pure {A : Set × Set} (f : A {⇒} A) (e : expr A) :
  subst_eq f subst_pure → ebind f e = e
with vbind_pure {A : Set × Set} (f : A {⇒} A) (v : value A) :
  subst_eq f subst_pure → vbind f v = v
with hbind_pure {A : Set × Set} (f : A {⇒} A) (h : handler A) :
  subst_eq f subst_pure → hbind f h = h.
Proof.
+ auto_bind_pure.
  - intro; apply Heq.
  - intro; apply Heq.
+ auto_bind_pure.
+ auto_bind_pure.
Qed.

Fixpoint ebind_bind_comp {A B C : Set × Set}
         (f : B {⇒} C) (g : A {⇒} B) h (e : expr A) :
  subst_eq (subst_comp f g) h → ebind f (ebind g e) = ebind h e
with vbind_bind_comp {A B C : Set × Set}
         (f : B {⇒} C) (g : A {⇒} B) h (v : value A) :
  subst_eq (subst_comp f g) h → vbind f (vbind g v) = vbind h v
with hbind_bind_comp {A B C : Set × Set}
         (f : B {⇒} C) (g : A {⇒} B) h (hh : handler A) :
  subst_eq (subst_comp f g) h → hbind f (hbind g hh) = hbind h hh.
Proof.
+ auto_bind_bind_comp.
  - intro; apply Heq.
  - intro; apply Heq.
+ auto_bind_bind_comp.
+ auto_bind_bind_comp.
Qed.

Instance Bind_ebind : Bind (@ebind).
Proof.
split.
+ exact @ebind_pure.
+ exact @ebind_bind_comp.
Qed.

Instance Bind_vbind : Bind (@vbind).
Proof.
split.
+ exact @vbind_pure.
+ exact @vbind_bind_comp.
Qed.

Instance Bind_hbind : Bind (@hbind).
Proof.
split.
+ exact @hbind_pure.
+ exact @hbind_bind_comp.
Qed.

(* ========================================================================= *)
(* Subst instances *)

Instance PreSubst_sub : PreSubst sub.
Proof.
split.
+ intros; split; reflexivity.
+ intros A B f g Heq; split; symmetry; apply Heq.
+ intros A B f g h Heq₁ Heq₂; split; etransitivity; try apply Heq₁; apply Heq₂.
Qed.

Instance Subst_arrow_sub : Subst arrow sub.
Proof.
split.
+ intros; split; intros; simpl; apply map_id; simpl; firstorder.
+ intros; split; simpl.
  - intro x; erewrite <- (vbind_map_comp _ arrow_id).
    * rewrite vmap_id; [ | reflexivity ]; reflexivity.
    * split; reflexivity.
  - intro a; erewrite <- (lbind_map_comp _ arrow_id).
    * rewrite lmap_id; [ | reflexivity ]; reflexivity.
    * split; reflexivity.
+ intros; split.
  - intro x; simpl.
    destruct EQg as [ EQg _ ]; rewrite EQg.
    apply map_equiv; assumption.
  - intro a; simpl.
    destruct EQg as [ _ EQg ]; rewrite EQg.
    apply map_equiv; apply EQf.
+ intros; split; simpl.
  - intro x; symmetry; apply map_map_comp.
    split; reflexivity.
  - intro x; symmetry; apply map_map_comp.
    reflexivity.
+ split; simpl.
  - intro x; apply bind_pure.
    split; reflexivity.
  - intro a; apply bind_pure.
    intro; reflexivity.
+ split; simpl; reflexivity.
+ split; simpl.
  - intro x; symmetry; apply bind_bind_comp.
    split; reflexivity.
  - intro x; symmetry; apply bind_bind_comp.
    split; reflexivity.
+ intros A B C f₁ f₂ g₁ g₂ Heq₁ Heq₂.
  split.
  - intro x; simpl.
    destruct Heq₂ as [ Heq₂ _ ]; rewrite Heq₂.
    rewrite <- (bind_pure' (bind f₁ (sub_var g₂ x))); apply bind_bind_comp.
    split; intro.
    * rewrite <- (proj1 Heq₁); apply bind_pure'.
    * rewrite <- (proj2 Heq₁); simpl.
      apply bind_pure; intro; reflexivity.
  - intro a; simpl.
    destruct Heq₂ as [ _ Heq₂ ]; rewrite Heq₂.
    apply bind_equiv; intro; apply Heq₁.
Qed.

Instance Subst_shift_incV : SubstShift arrow sub value (on₁ inc).
Proof. split; reflexivity. Qed.

Instance Subst_shift_incL : SubstShift arrow sub _ (on₂ inc).
Proof.
  split; intros; term_simpl; reflexivity.
Qed.

Instance SB_val_incV : SubstBind arrow sub value incV.
Proof.
  intros A B f v; split; [intros [| x]; term_simpl; reflexivity |].
  intros a; term_simpl; symmetry; apply bind_pure; intros b; reflexivity.
Qed.

Instance BC_lab_sub : IsBindCore (λ A B (ρ : A {⇒} B) (l : lvar (lv A)), lbind (sub_lbl ρ) l).

Instance SB_lab_incL : SubstBind arrow sub (λ A, lvar (lv A)) incL.
Proof.
  intros A B [ρv ρl] l; split; [intros x; term_simpl; reflexivity |].
  intros [| a]; term_simpl; [reflexivity |].
  change (fmap {| apply_arr := VS |}) with (shift (A := lv B)).
  now rewrite subst_shift_id.
Qed.

Lemma liftBy_mapOf_comm {A B C : Set} (R : rctx B A) (ρ : A [⇒] C) :
  subst_eq (arrow_subst_comp (mapOf (liftByR R ρ)) ρ)
           (subst_comp (liftByρ R ρ) (of_arrow (mapOf R))).
Proof.
  revert C ρ; induction R; intros; intros a; term_simpl.
  - apply map_id; intros x; reflexivity.
  - apply IHR.
  - etransitivity; [| eapply IHR]; simpl.
    symmetry; apply map_map_comp; intros x; reflexivity.
Qed.

Lemma liftBy_plug {A B C V : Set} (R : rctx B A) (ρ : A [⇒] C) (e : expr_ V B) :
  bind (injLSub ρ) (rplug R e) = rplug (liftByR R ρ) (bind (injLSub (liftByρ R ρ)) e).
Proof.
  revert C ρ; induction R; intros; term_simpl.
  - reflexivity.
  - f_equal; [apply IHR | apply bind_map_comp].
    split; [intros [ | []] | intros]; term_simpl; [reflexivity |].
    symmetry; apply map_id; intros b; reflexivity.
  - f_equal; [apply IHR | apply bind_map_comp; split; intros; term_simpl ..].
    + destruct x.
    + symmetry; apply map_id; intros b; reflexivity.
    + destruct x as [| []]; simpl; reflexivity.
    + symmetry; apply map_id; intros b; reflexivity.
Qed.

Fixpoint liftByT_eq {A B C D} (ρ : A [⇒] D) (R : rctx B A) : liftByT C (liftByR R ρ) = liftByT C R.
Proof.
  destruct R; simpl; [reflexivity | apply liftByT_eq ..].
Defined.

Lemma liftByR_eq {A B C} (ρ₁ ρ₂ : A [⇒] C) (EQ : subst_eq ρ₁ ρ₂) (R : rctx B A) :
  liftByR R ρ₁ = liftByR R ρ₂.
Proof.
  revert C ρ₁ ρ₂ EQ; induction R; intros; simpl; [reflexivity | ..].
  - f_equal; [| apply bind_equiv]; now auto.
  - f_equal; [apply IHR; now rewrite EQ | now apply bind_equiv ..].
Qed.

Lemma liftByRcomp_plug {A B C D} (R : rctx B A) (ρ₁ : A [⇒] C) (ρ₂ : C [⇒] D) e :
  rplug (liftByR (liftByR R ρ₁) ρ₂) e =
  rplug (liftByR R (subst_comp ρ₂ ρ₁)) (eq_rect _ (λ X, expr_ ∅ X) e _ (liftByT_eq ρ₁ R)).
Proof.
  revert C D ρ₁ ρ₂ e; induction R; intros; term_simpl.
  - reflexivity.
  - f_equal; [apply IHR | f_equal; apply bind_bind_comp; reflexivity].
  - f_equal; [| f_equal; apply bind_bind_comp; reflexivity ..].
    rewrite IHR; f_equal; apply liftByR_eq, liftS_comp; reflexivity.
Qed.

Require Import Morphisms.
Instance Proper_injL A B X : Proper (subst_eq ==> subst_eq) (@injLSub X A B).
Proof.
  intros x y EQ; split; [reflexivity | apply EQ].
Qed.

Lemma comp_injL {A B C} X (ρ₁ : A [⇒] B) (ρ₂ : B [⇒] C) :
  subst_eq (subst_comp (injLSub (V := X) ρ₂) (injLSub ρ₁)) (injLSub (subst_comp ρ₂ ρ₁)).
Proof.
  split; intros x; reflexivity.
Qed.

Lemma liftByRcomp_inj_plug {A B C D} (R : rctx B A) (ρ₁ : A [⇒] C) (ρ₂ : C [⇒] D) (e : expr_ ∅ D) :
  rplug (liftByR (liftByR R ρ₁) ρ₂) (bind (injLSub (of_arrow (mapOf (liftByR (liftByR R ρ₁) ρ₂)))) e) =
  rplug (liftByR R (subst_comp ρ₂ ρ₁)) (bind (injLSub (of_arrow (mapOf (liftByR R (subst_comp ρ₂ ρ₁))))) e).
Proof.
  revert C D ρ₁ ρ₂ e; induction R; intros; term_simpl.
  - reflexivity.
  - f_equal; [apply IHR | f_equal; apply bind_bind_comp, comp_injL].
  - f_equal; [| f_equal; apply bind_bind_comp, comp_injL ..].
    erewrite <- bind_bind_comp with (g := (of_arrow mk_shift));
      [etransitivity; [apply IHR |] | split; reflexivity].
    f_equal; [apply liftByR_eq, liftS_comp; reflexivity | apply bind_bind_comp].
    split; intros x; term_simpl; [reflexivity | erewrite liftByR_eq; [reflexivity |]].
    intros [| y]; term_simpl; [reflexivity |].
    apply bind_map_comp; intros z; reflexivity.
Qed.
