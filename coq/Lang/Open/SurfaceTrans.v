Require Import Utf8.
Require Import Binding.Product.
Require Surface.Syntax.
Require Import Open.Syntax.

Fixpoint tr_expr {A : Set × Set} (e : Surface.Syntax.expr A) : expr A :=
  match e with
  | Surface.Syntax.e_value  v     => tr_value v
  | Surface.Syntax.e_let e₁ e₂    => e_let (tr_expr e₁) (tr_expr e₂)
  | Surface.Syntax.e_app v₁ v₂    => e_app (tr_value v₁) (tr_value v₂)
  | Surface.Syntax.e_tapp v       => e_tapp (tr_value v)
  | Surface.Syntax.e_lapp v l     => e_lapp (tr_value v) (lclosed l)
  | Surface.Syntax.e_do   l v     => e_do (lclosed l) (tr_value v)
  | Surface.Syntax.e_handle e h r =>
    e_handle (tr_expr e) (tr_handler h) (tr_expr r)
  end
with tr_value {A : Set × Set} (v : Surface.Syntax.value A) : value A :=
  match v with
  | Surface.Syntax.v_var  x => v_var x
  | Surface.Syntax.v_lam  e => v_lam (tr_expr e)
  | Surface.Syntax.v_tlam e => v_tlam (tr_expr e)
  | Surface.Syntax.v_llam e => v_llam (tr_expr e)
  | Surface.Syntax.v_unit   => v_unit
  end
with tr_handler {A : Set × Set} (h : Surface.Syntax.handler A) : handler A :=
  match h with
  | Surface.Syntax.h_do e => h_do (tr_expr e)
  end.