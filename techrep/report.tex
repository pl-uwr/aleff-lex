%% -*- fill-column: 90; -*-
\documentclass[a4paper, 10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{gentium}
\usepackage[a4paper, top=1in, bottom=1.25in, left=0.75in, right=0.75in]{geometry}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{mathpartir}
\usepackage{stmaryrd}
\usepackage{microtype}
\usepackage{bm}
\usepackage{stackrel}
\usepackage{pbox}

\usepackage{hyperref}

\newtheorem{lem}{Lemma}
\newtheorem{thm}{Theorem}

\input{../macros.tex}

%\newcommand{\header}[2]{\paragraph{#1}\hspace{\fill}\fbox{\pbox[t]{0.7\textwidth}{#2}}}

\title{Lexically Scoped Effects: Technical Report}
\author{}

\begin{document}
\maketitle

\section{The Surface Language}

\begin{align*}
  \tvars \ni \alpha,     & \ldots \tag{type variables}     \\
  \ivars \ni a, b,       & \ldots \tag{instance variables} \\
  \vars  \ni f, k, x, y, & \ldots \tag{variables}          \\[2em]
  \kinds \ni \kappa              & \cceq \ktyp \mid \keff \mid \ksch \tag{kinds}         \\
  \tlikes \ni \sigma, \tau, \eps & \cceq \alpha \mid \tunit \mid \tarrow{\tau}{\eps}{\tau} \mid
                                   \tall{\alpha}{\kappa}{\tau} \mid \tilam{a}{\sigma}{\tau}
                                   \mid \eemp \mid \ecomp{\eps}{\eps} \mid a \mid
                                   \sch{\tau}{\tau} \tag{types, effects, schemes}        \\
  \vals \ni u, v                 & \cceq x \mid \elam{x}{e} \mid \eilam{a}{e} \mid
                                   \etlam{\alpha}{e} \mid \eunit \tag{values}            \\
  \exps \ni e                    & \cceq v \mid \elet{x}{e}{e} \mid \eapp{v}{v} \mid
                                   \eiapp{v}{a} \mid \etapp{v} \mid
                                   \edo{a}{v} \mid \ehandle{a}{e}{h}{r}\tag{expressions} \\
  h                              & \cceq \hdo{x}{k}{e}                                   \\
  r                              & \cceq \hreturn{x}{e}
\end{align*}

\paragraph{Binding structure}

The language sports a reasonably standard binding structure for term and type variables:
universal quantifier binds type variables within its body in types, and big lambda does
likewise for terms; the small lambda and the $\kwlet$ construct bind the term variable
within the body and the second expression respectively; all binding is lexical. Binding
for instance variables is also lexical, and is introduced by three constructs. The
instance quantifier/binder unsurprisingly binds the name in its third argument (either
type or expression, respectively); the handler binds the name in its second argument
only. This is the only non-standard occurrence, as the name is thus bound within the
\emph{evaluation} context, and thus reductions will need to occur under binders. Thus, we
implicitly alpha-rename bound instances in expressions to ensure all names in evaluation
contexts are distinct (much like the usual convention for type and term contexts) and
employ a standard capture-avoiding substitution for substituting effect names.

\subsection{Well-Formedness}

\header{Well-formedness of types}{$\wftDX{\tau}{\kappa}$}

\begin{mathpar}
  \inferrule{\alpha \cc \kappa \in \Delta}{\wftDX{\alpha}{\kappa}}
  \and
  \inferrule{ }{\wftDX{\tunit}{\ktyp}}
  \and
  \inferrule{\wftDX{\tau_1}{\ktyp}\\\wftDX{\eps}{\keff}\\\wftDX{\tau_2}{\ktyp}}{%
    \wftDX{\tarrow{\tau_1}{\eps}{\tau_2}}{\ktyp}}
  \and
  \inferrule{\wft{\Delta, \alpha \cc \kappa}{X}{\tau}{\ktyp}}{%
    \wftDX{\tall{\alpha}{\kappa}{\tau}}{\ktyp}}
  \and
  \inferrule{\wftDX{\sigma}{\ksch}\\\wft{\Delta}{X \uplus \setof{a}}{\tau}{\ktyp}}{%
    \wftDX{\tilam{a}{\sigma}{\tau}}{\ktyp}}
  \and
  \inferrule{ }{\wftDX{\eemp}{\keff}}
  \and
  \inferrule{\wftDX{\eps_1}{\keff}\\\wftDX{\eps_2}{\keff}}{%
    \wftDX{\ecomp{\eps_1}{\eps_2}}{\keff}}
  \and
  \inferrule*{a \in X}{\wftDX{a}{\keff}}
  \and
  \inferrule*{\wftDX{\tau_1}{\ktyp}\\\wftDX{\tau_2}{\ktyp}}{%
    \wftDX{\sch{\tau_1}{\tau_2}}{\ksch}}
  \and
  \inferrule*[right=*]{\wft{\Delta, \alpha \cc \kappa}{X}{\tau}{\ksch}}{%
    \wftDX{\tall{\alpha}{\kappa}{\tau}}{\ksch}}
\end{mathpar}

\pagebreak
\subsection{Type-and-Effect System}

\header{Type equivalence}{$\tequiv{\Delta}{\Theta}{\tau}{\tau}$}

We define type equivalence as a congruence closure for all constructions on types, effects
and schemes over an idempotent, commutative monoid structure over the effects, and work
implicitly up to this notion.

\header{Subtyping}{$\subtDT{\tau}{\tau}$}

Like type equivalence, subtyping is inhereted from the commutative, idempotent monoid
structure of the effects, for which it forms the natural ordering. The only distinctions
are the standard variance flip on the left of the arrow type, and the fact that we do not
consider (nontrivial) subtyping for schemes.

\header{Signature instantiation}{$\effinstDr{\sigma}$}

Effect instantiation is non-trivial only in the variant of the calculus that allows for
polymorphic effect signatures, but allows for a uniform and judgmental way of expressing
the two cases. As with the well-formedneses, a star marks the rule that is added when
allowing for polymorphic effect signatures.

\begin{mathpar}
  \inferrule*{ }{\effinstDr{\sch{\tau_1}{\tau_2}}}
  \and
  \inferrule*[right=*]{\wftDT{\tau}{\kappa}\\\effinstDr{\sigma\subst{\alpha}{\tau}}}{%
    \effinstDr{\tall{\alpha}{\kappa}{\sigma}}}
\end{mathpar}

\header{Typing relation}{$\vtypeDTG{v}{\tau}$\\$\typeDTG{e}{\tau}{\eps}$\\$\htypeDTG{h}{\sigma}{\tau}{\eps}$}

\begin{mathpar}
  \inferrule{x \col \tau \in \Gamma}{\vtypeDTG{x}{\tau}}
  \and
  \inferrule{ }{\vtypeDTG{\eunit}{\tunit}}
  \and
  \inferrule{\wftDT{\tau_1}{\ktyp}\\\typeDT{\Gamma, x \col \tau_1}{e}{\tau_2}{\eps}}{%
    \vtypeDTG{\elam{x}{e}}{\tarrow{\tau_1}{\eps}{\tau_2}}}
  \and
  \inferrule{\type{\Delta, \alpha \cc \kappa}{\Theta}{\Gamma}{e}{\tau}{\eemp}}{%
    \vtypeDTG{\etlam{\alpha}{e}}{\tall{\alpha}{\kappa}{\tau}}}
  \and
  \inferrule{\wftDT{\sigma}{\ksch}\\\type{\Delta}{\Theta, a \col \sigma}{\Gamma}{e}{\tau}{\eemp}}{%
    \vtypeDTG{\eilam{a}{e}}{\tilam{a}{\sigma}{\tau}}}
  \\\\\\
  \inferrule{\vtypeDTG{v}{\tau}}{\typeDTG{v}{\tau}{\eemp}}
  \and
  \inferrule{\typeDTG{e_1}{\tau_1}{\eps}\\\typeDT{\Gamma, x\col\tau_1}{e_2}{\tau_2}{\eps}}{%
    \typeDTG{\elet{x}{e_1}{e_2}}{\tau_2}{\eps}}
  \and
  \inferrule{\vtypeDTG{u}{\tarrow{\tau_1}{\eps}{\tau_2}}\\\vtypeDTG{v}{\tau_1}}{%
    \typeDTG{\eapp{u}{v}}{\tau_2}{\eps}}
  \and
  \inferrule{\vtypeDTG{v}{\tall{\alpha}{\kappa}{\tau}}\\\wftDT{\tau'}{\kappa}}{%
    \typeDTG{\etapp{v}{\tau'}}{\tau}{\eemp}}
  \and
  \inferrule{\vtypeDTG{v}{\tilam{a}{\sigma}{\tau}}\\b \col \sigma \in \Theta}{%
    \typeDTG{\eiapp{v}{b}}{\tau\subst{a}{b}}{\eemp}}
  \and
  \inferrule{\vtypeDTG{v}{\tau_1}\\ \effinstDTR{\Theta(a)}}{%
    \typeDTG{\edo{a}{v}}{\tau_2}{a}}
  \and
  \inferrule{
    \wftDT{\sigma}{\ksch}\\\wftDT{\tau'}{\ktyp}\\
    \type{\Delta}{\Theta, a \col \sigma}{\Gamma}{e}{\tau'}{\ecomp{a}{\eps}}\\
    \htypeDTG{h}{\sigma}{\tau}{\eps}\\
    \typeDT{\Gamma, x \col \tau'}{e_r}{\tau}{\eps}}{%
    \typeDTG{\ehandle{a}{e}{h}{\hreturn{x}{e_r}}}{\tau}{\eps}}
  \and
  \inferrule{\typeDTG{e}{\tau}{\eps}\\\subtDT{\tau}{\tau'}\\\subtDT{\eps}{\eps'}}{%
    \typeDTG{e}{\tau'}{\eps'}}
  \\\\\\
  \inferrule*{\typeDT{\Gamma, x \col \tau_1, r \col \tarrow{\tau_2}{\eps}{\tau}}{e}{\tau}{\eps}}{%
    \htypeDTG{\hdo{x}{r}{e}}{\sch{\tau_1}{\tau_2}}{\tau}{\eps}}
  \and
  \inferrule*[right=*]{\htype{\Delta,\alpha\cc\kappa}{\Theta}{\Gamma}{h}{\sigma}{\tau}{\eps}}{%
    \htypeDTG{h}{\tall{\alpha}{\kappa}{\sigma}}{\tau}{\eps}}
\end{mathpar}

\section{Open Semantics}

This is the operational semantics where we treat handlers as binders and reduce
accordingly, applying capture-avoiding substitution as needed. Note that this is somewhat
nontrivial, since we reduce under binders, and the binding occurrences can be captured as
a resumption. Note also that bound variables might escape their scope and becomp
\emph{globally free} as a result of reduction of the handler (both in case of return and
do reductions). The evaluation contexts and resumptions are given as follows:
\begin{align*}
  \econts \ni E & \cceq \hole \mid E[\elet{x}{\hole}{e}] \mid E[\ehandle{a}{\hole}{h}{r}] \\
  \rconts \ni R & \cceq \hole \mid \elet{x}{R}{e} \mid \ehandle{a}{R}{h}{r}
\end{align*}

\subsection{Operational Semantics}

\begin{mathpar}
  \inferrule{ }{\contr{\eapp{(\elam{x}{e})}{v}}{e\subst{x}{v}}}
  \and
  \inferrule{ }{\contr{\etapp{(\etlam{\alpha}{e})}}{e}}
  \and
  \inferrule{ }{\contr{\eiapp{(\eilam{a}{e})}{b}}{e\subst{a}{b}}}
  \and
  \inferrule{ }{\contr{\ehandle{a}{v}{h}{\hreturn{x}{e}}}{e\subst{x}{v}}}
  \and
  \inferrule{R = \ehandle{a}{R'}{x\col\tau_1, y \col \tau_2 \kwds e}{r}}{%
      \contr{\plug{R}{\edo{a}{v}}}{e\subst{x}{v}\subst{y}{\elam{z}{\plug{R}{z}}}}}
  \and
  \inferrule{ }{\contr{\elet{x}{v}{e}}{e\subst{x}{v}}}
  \\
  \inferrule{\contr{e_1}{e_2}}{\reds{\plug{E}{e_1}}{\plug{E}{e_2}}}
\end{mathpar}


\section{Instance Semantics}

In the alternative operational semantics, we treat handlers as binders, but reduce them
whenever encountered by replacing the instance variable with a freshly allocated instance,
which is no longer binding. This means that, in contrast to the open semantics, we never
reduce under binders, but the runtime language becomes more complex. More precisely, we
get:
\begin{align*}
  \insts  \ni l & \ldots \tag{instances} \\
  \exps   \ni e & \cceq \ldots \mid \ehandling{l}{e}{h}{r} \mid \eiapp{v}{l} \mid \edo{l}{v}\\
  \econts \ni E & \cceq \hole \mid E[\elet{x}{\hole}{e}] \mid E[\ehandling{l}{\hole}{h}{r}] \\
  \rconts \ni R & \cceq \hole \mid \elet{x}{R}{e} \mid \ehandling{l}{R}{h}{r}
\end{align*}

\subsection{Operational Semantics}

\begin{mathpar}
  \inferrule{ }{\contr{\eapp{(\elam{x}{e})}{v}}{e\subst{x}{v}}}
  \and
  \inferrule{ }{\contr{\etapp{(\etlam{\alpha}{e})}}{e}}
  \and
  \inferrule{ }{\contr{\eiapp{(\eilam{a}{e})}{l}}{e\subst{a}{l}}}
  \and
  \inferrule{ }{\contr{\ehandling{l}{v}{h}{\hreturn{x}{e}}}{e\subst{x}{v}}}
  \and
  \inferrule{R = \ehandling{l}{R'}{\hdo{x}{k}{e}}{r} \\ \free(l, R')}{%
    \contr{\plug{R}{\edo{l}{v}}}{e\subst{x}{v}\subst{k}{\elam{z}{\plug{R}{z}}}}}
  \and
  \inferrule{ }{\contr{\elet{x}{v}{e}}{e\subst{x}{v}}}
  \\
  \inferrule{\contr{e_1}{e_2}}{\reds{\plug{E}{e_1}}{\plug{E}{e_2}}}
  \and
  \inferrule{\fresh(l)}{\reds{\plug{E}{\ehandle{a}{e}{h}{r}}}{\plug{E}{\ehandling{l}{e\subst{a}{l}}{h}{r}}}}
\end{mathpar}

\pagebreak

\section{Logical Relations}

\subsection{Instance semantics with polymorphism in signatures}

\header{Interpretation of kinds}{$\kden{\kappa} \col \cofe$, $\den{\Delta} \col \cofe^{\dom(\Delta)}$}

\begin{align*}
  \semcomps & \eqdef \upred(\exps^2)\\
  \kden{\ktyp} \eqdef \semtypes & \eqdef \upred(\vals^2)\\
  \kden{\keff} \eqdef \semeffs  & \eqdef \setcomp{R \in \upred(\exps^2 \times \Pfin(\insts)^2 \times \semcomps)}{\text{$R$ has finite instance support}}\\
  \kden{\ksch} \eqdef \semsigs  & \eqdef \upred(\vals^2 \times \semtypes)\\
  \seminsts & \eqdef \semsigs \times \insts^2\\[2ex]
  \den{\Delta}(\alpha) & \eqdef \kden{\Delta(\alpha)}
\end{align*}

\header{Interpretation of (general) types}{$\tden{\wftDX{\tau}{\kappa}} \col \den\Delta \to \seminsts^X \to \kden\kappa$}

\begin{align*}
  \tden{\alpha}^\dheta & \eqdef \delta(\alpha)\\[2ex]
  \tden{\tunit}^\dheta & \eqdef \setof{(\eunit, \eunit)}\\
  \tden{\tarrow{\tau_1}{\eps}{\tau_2}}^\dheta & \eqdef \setcomp{(v_1, v_2)}{\forall (u_1, u_2) \in \tden{\tau_1}^\dheta.~(\eapp{v_1}{u_1}, \eapp{v_2}{u_2}) \in \eden{\tau_2}{\eps}^\dheta}\\
  \tden{\tall{\alpha}{\kappa}{\tau}}^\dheta & \eqdef \setcomp{(v_1, v_2)}{\forall \mu \in \kden\kappa.~(\etapp{v_1}, \etapp{v_2}) \in \eden{\tau}{\eemp}^{\delta[\alpha\mapsto\mu], \vartheta}}\\
  \tden{\tilam{a}{\sigma}{\tau}}^\dheta & \eqdef \setcomp{(v_1, v_2)}{\forall l_1, l_2, I.~I = (\tden{\sigma}^\dheta, l_1, l_2) \impl (\eiapp{v_1}{l_1}, \eiapp{v_2}{l_2}) \in \eden{\tau}{\eemp}^{\delta, \vartheta[a\mapsto I]}}\\[2ex]
  \tden{\eemp}^\dheta & \eqdef \emptyset\\
  \tden{\ecomp{\eps_1}{\eps_2}}^\dheta & \eqdef \tden{\eps_1}^\dheta \cup \tden{\eps_2}^\dheta\\
  \tden{a}^\dheta      & \eqdef \setcomp{(\edo{l_1}{v_1}, \edo{l_2}{v_2}, \setof{l_1}, \setof{l_2}, \mu)}{\exists \nu.~\vartheta(a) = (\nu, l_1, l_2) \land (v_1, v_2, \mu) \in \nu}\\[2ex]
  \tden{\sch{\tau_1}{\tau_2}}^\dheta & \eqdef \setcomp{(v_1, v_2, \tden{\tau_2}^\dheta)}{(v_1, v_2) \in \tden{\tau_1}^\dheta}\\
  \tden{\tall{\alpha}{\kappa}{\sigma}}^\dheta & \eqdef \setcomp{(v_1, v_2, \mu)}{\exists \nu\in\kden{\kappa}.~(v_1, v_2, \mu) \in \tden{\sigma}^{\delta[\alpha\mapsto\nu], \vartheta}}
\end{align*}

\header{Interpretation of instance and term contexts}{$\den{\Delta \vdash \Theta} \col \den\Delta \to \upred(\seminsts^{\dom(\Theta)} \times (\insts^{\dom(\Theta)})^2)$\\$\den{\wfvcDX{\Gamma}} \col \den\Delta \to \seminsts^X \to \upred((\semtypes^{\dom(\Gamma)})^2)$}

% \begin{align*}
%   \den{\cdot}^\delta  & \eqdef \setof{(\cdot, \cdot, \cdot)}\\
%   \den{\Theta, a \col \sigma}^\delta & \eqdef \setcomp{(\vartheta[a\mapsto I], \eta_1[a \mapsto l_1], \eta_2[a \mapsto l_2])}{(\vartheta, \eta_1, \eta_2) \in \den{\Theta}^\delta \wedge I = (\den{\sigma}^\dheta, l_1, l_2)}
% \end{align*}


\begin{align*}
  \den{\Theta}^\delta & \eqdef \setcomp{(\vartheta, \eta_1, \eta_2)}{\forall a\in\dom(\Theta).~\vartheta(a) = (\den{\Theta(a)}^\dheta, \eta_1(a), \eta_2(a))}\\
  \den{\Gamma}^\dheta & \eqdef \setcomp{(\gamma_1, \gamma_2)}{\forall x\in\dom(\Gamma).~(\gamma_1(x), \gamma_2(x))\in\den{\Gamma(x)}^\dheta}
\end{align*}

\header{Observation}{$\obs \col \semcomps$}

\[ \obs \eqdef \setcomp{(e_1, e_2)}{\left(e_1 = \eunit \land \redsclo{e_2}{\eunit}{*}\right) \lor \left(\exists e_1', e_2'.~\reds{e_1}{e_1'} \land \redsclo{e_2}{e_2'}{*} \land (e_1', e_2')\in\later\obs\right)} \]

\pagebreak
\header{Closures}{$\eden{\tau}{\eps}$, $\xden{\tau}{\eps}$, $\csden{\tau}{\eps}$}

\begin{align*}
  \eden{\tau}{\eps}^\dheta & \eqdef \setcomp{(e_1, e_2)}{\forall (E_1, E_2) \in \xden{\tau}{\eps}^\dheta.~(\plug{E_1}{e_1}, \plug{E_2}{e_2})\in\obs}\\
  \xden{\tau}{\eps}^\dheta & \eqdef \setcomp{(E_1, E_2)}{
                      \begin{aligned}
                        &\forall (v_1, v_2) \in \den{\tau}^\dheta.~(\plug{E_1}{v_1}, \plug{E_2}{v_2})\in\obs~\land\\
                        &\forall (e_1, e_2) \in \csden{\tau}{\eps}^\dheta.~(\plug{E_1}{e_1}, \plug{E_2}{e_2})\in\obs
                      \end{aligned}}\\
  \csden{\tau}{\eps}^\dheta & \eqdef \setcomp{(\plug{R_1}{e_1}, \plug{R_2}{e_2})}{%
                              \begin{aligned}
                                \exists L_1,&\,L_2, \mu.~(e_1, e_2, L_1, L_2, \mu) \in \den{\eps}^\dheta
                                \land (\forall l \in L_i.~\free(l, R_i))_{i\in\setof{1,2}} \land~\\
                                & \forall (e_1', e_2') \in \mu.~(\plug{R_1}{e_1'}, \plug{R_2}{e_2'})\in\later\eden{\tau}{\eps}^\dheta
                              \end{aligned}}\\
  \hden{\sigma}{\tau}{\eps}^\dheta & \eqdef \setcomp{(\hdo{x}{k}{e_1}, \hdo{x}{k}{e_2})}{%
                                     \begin{aligned}
                                       \forall u_1,\,&u_2, v_1, v_2, \mu.~(u_1, u_2, \mu) \in \den{\sigma}^\dheta \impl\\
                                       & \left(\forall (w_1, w_2) \in \mu.~(\eapp{v_1}{w_1}, \eapp{v_2}{w_2}) \in \eden{\tau}{\eps}^\dheta\right) \impl\\
                                       & (e_1\subst{x}{u_1}\subst{y}{v_1}, e_2\subst{x}{u_2}\subst{y}{v_2}) \in \eden{\tau}{\eps}^\dheta
                                     \end{aligned}}
\end{align*}

\header{Logical approximation}{$\relvDTG{v_1}{v_2}{\tau}$\\$\releDTG{e_1}{e_2}{\tau}{\eps}$\\$\relhDTG{h_1}{h_2}{\sigma}{\tau}{\eps}$}

\begin{align*}
  \relvDTG{v_1}{v_2}{\tau} &\iff \forall \delta\in\den{\Delta}, (\vartheta, \eta_1, \eta_2) \in \den{\Theta}^\delta, (\gamma_1, \gamma_2)\in\den{\Gamma}^\dheta.\\
  &\qquad\qquad(\eta_1(\gamma_1(v_1)), \eta_2(\gamma_2(v_2))) \in \den{\tau}^\dheta\\
  \releDTG{e_1}{e_2}{\tau}{\eps}&\iff \forall \delta\in\den{\Delta}, (\vartheta, \eta_1, \eta_2) \in \den{\Theta}^\delta, (\gamma_1, \gamma_2)\in\den{\Gamma}^\dheta.\\
  &\qquad\qquad(\eta_1(\gamma_1(e_1)), \eta_2(\gamma_2(e_2))) \in \eden{\tau}{\eps}^\dheta\\
  \relhDTG{h_1}{h_2}{\sigma}{\tau}{\eps} & \iff \forall \delta\in\den{\Delta}, (\vartheta, \eta_1, \eta_2) \in \den{\Theta}^\delta, (\gamma_1, \gamma_2)\in\den{\Gamma}^\dheta.\\
  &\qquad\qquad(\eta_1(\gamma_1(h_1)), \eta_2(\gamma_2(h_2))) \in \hden{\sigma}{\tau}{\eps}^\dheta
\end{align*}

\subsection{Connecting the Two Semantics}

\paragraph{Worlds, world transitions and world-indexed predicates}

\begin{align*}
  \world^X & \eqdef \Sigma_{Y} X \parto Y\\
  \rho_1 \sqsubseteq^\rho \rho_2 & \iff \rho \circ \rho_1 = \rho_2\\
  \rho_1 \sqsubseteq \rho_2 & \iff \exists\rho.~ \rho_1 \sqsubseteq^\rho \rho_2
\end{align*}
\begin{align*}
  - \downarrow - & \col \upred(F(Y)) \to (X \parto Y) \to \upred(F(X)) \quad\text{where $F$ is functorial}\\
  P \downarrow \rho & \eqdef \setcomp{e \col F(X)}{F(\rho)(e) \in P}\\
  \wipred^X(F) & \eqdef \setcomp{R \in \Pi_{\rho \in \world^X}\upred(F(\cod(\rho)))}{\forall \rho_1 \sqsubseteq^\rho \rho_2.~R(\rho_1) \subseteq R(\rho_2) \downarrow \rho}\\
  - \Uparrow - & \col \wipred^X(F) \to (X \parto Y) \to \wipred^Y(F)\\
  (\mu \Uparrow \rho)(\rho')& \eqdef \mu (\rho' \circ \rho)
\end{align*}

\pagebreak
\header{Interpretation of kinds}{$\kden{\kappa}^X$, $\den{\Delta}^X$}

\begin{align*}
  \semcomps^X & \eqdef \wipred^X(Y \mapsto \exps \times \exps_Y)\\
  \kden{\ktyp}^X \eqdef \semtypes^X & \eqdef \wipred^X(Y \mapsto \vals \times \vals_Y)\\
  \kden{\keff}^X \eqdef \semeffs^X & \eqdef \setcomp{R \in \wipred^X(Y \mapsto \exps \times \exps_Y \times \Pfin(\insts) \times \semcomps^Y)}{\text{$R$ has finite instance support}}\\
  \kden{\ksch}^X \eqdef \semsigs^X & \eqdef \wipred^X(Y \mapsto \vals \times \vals_Y \times \semtypes^Y)\\
  \seminsts^X & \eqdef \semsigs^X \times \insts\\
  \den{\Delta}^X(\alpha) &\eqdef \kden{\Delta(\alpha)}^X
\end{align*}

\header{Interpretation of types}{$\tden{\wftDX{\tau}{\kappa}} \col \Pi_{(W, \rho) \in \world^X} \den{\Delta}^W \to (X \to \seminsts^W) \to \kden{\kappa}^W(\id)$}

\begin{align*}
  \tden{\alpha}_\rho^\dheta & \eqdef \delta(\alpha)(\id)\\[2ex]
  \tden{\tunit}_\rho^\dheta & \eqdef \setof{(\eunit, \eunit)}\\
  \tden{\tarrow{\tau_1}{\eps}{\tau_2}}_\rho^\dheta & \eqdef \setcomp{(v_1, v_2)}{\forall \rho'.~\forall (u_1, u_2) \in \tden{\tau_1}_{\rho'\circ\rho}^{\dhetalift{\rho'}}.~(\eapp{v_1}{u_1}, \eapp{\rho'(v_2)}{u_2}) \in \eden{\tau_2}{\eps}_{\rho'\circ\rho}^{\dhetalift{\rho'}}}\\
  \tden{\tall{\alpha}{\kappa}{\tau}}_{\rho}^\dheta & \eqdef \setcomp{(v_1, v_2)}{\forall \rho'.~\forall \mu \in \kden{\kappa}^{\cod(\rho')}.~(\etapp{v_1}, \etapp{\rho'(v_2)}) \in \eden{\tau}{\eemp}_{\rho'\circ\rho}^{\delta\Uparrow\rho'[\alpha\mapsto\mu], \vartheta\Uparrow\rho'}}\\
  \tden{\tilam{a}{\sigma}{\tau}}_\rho^\dheta & \eqdef \setcomp{(v_1, v_2)}{%
                                              \begin{aligned}
                                                \forall W,\,&\rho' : \cod(\rho) \parto W.~\forall l, w \col W^\bot, I.~I = (\rho'\mapsto\tden{\sigma}_{\rho'\circ\rho}^{\dhetalift{\rho'}}, l) \impl\\
                                                &(\eiapp{v_1}{l}, \eiapp{\rho'(v_2)}{a\subst{a}{w}}) \in \eden{\tau}{\eemp}_{(\rho'\circ\rho)[a\mapsto w]}^{\delta\Uparrow\rho', \vartheta[a\mapsto I]\Uparrow\rho'}
                                              \end{aligned}}\\[2ex]
  \tden{\eemp}_\rho^\dheta & \eqdef \emptyset\\
  \tden{\ecomp{\eps_1}{\eps_2}}_\rho^\dheta & \eqdef \tden{\eps_1}_\rho^\dheta \cup \tden{\eps_2}_\rho^\dheta\\
  \tden{a}_\rho^\dheta      & \eqdef \setcomp{(\edo{l}{v_1}, \edo{\rho(a)}{v_2}, \setof{l}, \mu)}{\exists \nu.~\vartheta(a) = (\nu, l) \land (v_1, v_2, \mu) \in \nu(\id)}\\[2ex]
  \tden{\sch{\tau_1}{\tau_2}}_\rho^\dheta & \eqdef \setcomp{(v_1, v_2, \rho' \mapsto \tden{\tau_2}_{\rho'\circ\rho}^{\dhetalift{\rho'}})}{(V_1, v_2) \in \tden{\tau_1}_\rho^\dheta}
\end{align*}

\header{Interpretation of instance and term contexts}{$\den{\Delta \vdash \Theta}_\rho \col \den{\Delta}^{\cod(\rho)} \to \upred((\seminsts^{\cod(\rho)})^{\dom(\Theta)} \times \insts^{\dom(\Theta)})$\\$\den{\wfvcDX{\Gamma}}_\rho \col \den{\Delta}^{\cod(\rho)} \to (\seminsts^{\cod(\rho)})^X \to \upred((\semtypes^{\dom(\Gamma)})^2)$}

\begin{align*}
  \den{\Theta}_\rho^\delta & \eqdef \setcomp{(\vartheta, \eta)}{\forall a\in\dom(\Theta).~\vartheta(a) = (\den{\Theta(a)}_\rho^\dheta, \eta(a))}\\
  \den{\Gamma}_\rho^\dheta & \eqdef \setcomp{(\gamma_1, \gamma_2)}{\forall x\in\dom(\Gamma).~(\gamma_1(x), \gamma_2(x))\in\den{\Gamma(x)}_\rho^\dheta}
\end{align*}

\header{Observation}{$\obs \col \upred(\exps\times\exps_\emptyset)$}

\[ \obs \eqdef \setcomp{(e_1, e_2)}{\left(e_1 = \eunit \land \redsclo{e_2}{\eunit}{*}\right) \lor \left(\exists e_1', e_2'.~\reds{e_1}{e_1'} \land \redsclo{e_2}{e_2'}{*} \land (e_2, e_2')\in\later\obs\right)} \]

\pagebreak
\header{Closures}{$\eden{\tau}{\eps}$, $\xden{\tau}{\eps}$, $\csden{\tau}{\eps}$}

\begin{align*}
  \semarr(\mu, \nu) & \eqdef \setcomp{(v_1, v_2)}{\forall\rho.~\forall(u_1, u_2)\in\mu(\rho).~(\eapp{v_1}{u_1}, \eapp{\rho(v_2)}{u_2}) \in \nu(\rho)}\\
  \eden{\tau}{\eps}_\rho^\dheta & \eqdef \setcomp{(e_1, e_2)}{\forall\rho'.~\forall (E_1, E_2) \in \xden{\tau}{\eps}_{\rho'\circ\rho}^{\dhetalift{\rho'}}.~(\plug{E_1}{e_1}, \plug{E_2}{e_2})\in\obs}\\
  \xden{\tau}{\eps}_\rho^\dheta & \eqdef \setcomp{(E_1, E_2)}{
                      \begin{aligned}
                        &\forall (v_1, v_2) \in \den{\tau}_\rho^\dheta.~(\plug{E_1}{v_1}, \plug{E_2}{v_2})\in\obs~\land\\
                        &\forall (e_1, e_2) \in \csden{\tau}{\eps}_\rho^\dheta.~(\plug{E_1}{e_1}, \plug{E_2}{e_2})\in\obs
                      \end{aligned}}\\
  \csden{\tau}{\eps}_\rho^\dheta & \eqdef \setcomp{(\plug{R_1}{e_1}, \plug{R_2}{e_2})}{%
                              \begin{aligned}
                                \exists \mu.~&(e_1, e_2, L, \mu) \in \den{\eps}_{\rho_R\circ\rho}^{\dhetalift{\rho_R}}
                                \land (\forall l \in L.~\free(l, R_1)) \land~\\
                                & \forall \rho'.~\forall (e_1', e_2') \in \mu(\rho_R').~(\plug{R_1}{e_1'}, \plug{\rho'(R_2)}{e_2'})\in\later\eden{\tau}{\eps}_{\rho'\circ\rho}^{\dhetalift{\rho'}}
                              \end{aligned}}\\
  &\text{where $\rho_R$ is the inclusion induced by $R_2$ and $\rho'_R$ is the lifting of $\rho'$ by this inclusion}\\
  \hden{\sigma}{\tau}{\eps}_\rho^\dheta & \eqdef \setcomp{(\hdo{x}{k}{e_1}, \hdo{x}{k}{e_2})}{%
                                     \begin{aligned}
                                       \forall \rho',\,&\rho^\uparrow, \rho^\downarrow.~\rho^\downarrow\circ\rho^\uparrow = \id \impl
                                       \forall (u_1, u_2, \mu) \in \den{\sigma}_{\rho^\uparrow\circ\rho'\circ\rho}^{\dhetalift{\rho^\uparrow\circ\rho'}}.~\\
                                       & \exists \mu'.~\mu = \mu' \Uparrow \rho^\uparrow \land\forall (v_1, v_2) \in \semarr(\mu', \rho''\mapsto\eden{\tau}{\eps}_{\rho''\circ\rho'\circ\rho}^{\dhetalift{\rho''\circ\rho'}}).~\\
                                       & \qquad(e_1\subst{x}{u_1}\subst{y}{v_1}, \rho'(e_2)\subst{x}{\rho^\downarrow(u_2)}\subst{y}{v_2}) \in \eden{\tau}{\eps}_{\rho'\circ\rho}^{\dhetalift{\rho'}}
                                     \end{aligned}}
\end{align*}

\header{Logical approximation}{$\relvDTG{v_1}{v_2}{\tau}$\\$\releDTG{e_1}{e_2}{\tau}{\eps}$\\$\relhDTG{h_1}{h_2}{\sigma}{\tau}{\eps}$}

\begin{align*}
  \relvDTG{v_1}{v_2}{\tau} &\iff \forall \rho.~\forall \delta\in\den{\Delta}^{\cod(\rho)}, (\vartheta, \eta) \in \den{\Theta}_\rho^\delta, (\gamma_1, \gamma_2)\in\den{\Gamma}_\rho^\dheta.\\
  &\qquad\qquad(\eta(\gamma_1(v_1)), \rho(\gamma_2(v_2))) \in \den{\tau}_\rho^\dheta\\
  \releDTG{e_1}{e_2}{\tau}{\eps}&\iff \forall \rho.~\forall \delta\in\den{\Delta}^{\cod(\rho)}, (\vartheta, \eta) \in \den{\Theta}_\rho^\delta, (\gamma_1, \gamma_2)\in\den{\Gamma}_\rho^\dheta.\\
  &\qquad\qquad(\eta(\gamma_1(e_1)), \rho(\gamma_2(e_2))) \in \eden{\tau}{\eps}_\rho^\dheta\\
  \relhDTG{h_1}{h_2}{\sigma}{\tau}{\eps} & \iff \forall \rho.~\forall \delta\in\den{\Delta}^{\cod(\rho)}, (\vartheta, \eta) \in \den{\Theta}_\rho^\delta, (\gamma_1, \gamma_2)\in\den{\Gamma}_\rho^\dheta.\\
  &\qquad\qquad(\eta(\gamma(h_1)), \rho(\gamma_2(h_2))) \in \hden{\sigma}{\tau}{\eps}_\rho^\dheta
\end{align*}

\end{document}
